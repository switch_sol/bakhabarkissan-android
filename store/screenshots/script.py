from os import listdir
from subprocess import call

for filename in listdir("."):
    if filename.endswith("png"):
        i = filename
        o = i + ".jpg"
        cmd = "convert {} -strip -interlace Plane -resize 1024x1024 -background white -flatten -alpha off -quality 85% {}".format(i, o)
        call(cmd.split(" "))
