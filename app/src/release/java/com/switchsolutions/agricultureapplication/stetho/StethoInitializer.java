package com.switchsolutions.agricultureapplication.stetho;

import android.content.Context;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public final class StethoInitializer {

    private StethoInitializer() {
    }

    public static void initialize(Context context) {
        // Nothing.
    }

    public static Interceptor createInterceptor() {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return null;
            }
        };
    }
}
