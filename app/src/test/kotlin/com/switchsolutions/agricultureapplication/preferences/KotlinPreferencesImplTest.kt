package com.switchsolutions.agricultureapplication.preferences

import android.preference.PreferenceManager
import com.google.common.truth.Truth.assertThat
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.utils.Coordinates
import com.switchsolutions.agricultureapplication.utils.Languages
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import rx.observers.TestSubscriber

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class KotlinPreferencesImplTest {

    private var analytics: AnalyticsClient? = null
    private var preferences: Preferences? = null

    @Before
    fun setUp() {
        analytics = Mockito.mock(AnalyticsClient::class.java)
        preferences = KotlinPreferencesImpl(PreferenceManager.getDefaultSharedPreferences(RuntimeEnvironment.application), analytics!!)
    }

    @Test
    fun shouldReturnDefaultLanguageUrdu() {
        assertThat(preferences!!.getLanguage()).isEqualTo(Languages.UR)
    }

    @Test
    fun shouldGetSetLanguage() {
        preferences!!.setLanguage(Languages.EN)
        assertThat(preferences!!.getLanguage()).isEqualTo(Languages.EN)
        verify(analytics!!).sendLanguageSelectedEvent(Languages.EN)

        preferences!!.setLanguage(Languages.UR)
        assertThat(preferences!!.getLanguage()).isEqualTo(Languages.UR)
        verify(analytics!!).sendLanguageSelectedEvent(Languages.UR)
    }

    @Test
    fun shouldObserveLanguageChanges() {
        val subscriber = TestSubscriber<String>()

        preferences!!.getLanguageObservable().subscribe(subscriber)
        preferences!!.setLanguage(Languages.EN)
        subscriber.unsubscribe()

        assertThat(subscriber.onErrorEvents).isEmpty()
        assertThat(subscriber.onNextEvents).hasSize(1)
        assertThat(subscriber.onNextEvents[0]).isEqualTo(Languages.EN)
    }

    @Test
    fun shouldGetSetFirstRun() {
        preferences!!.setIsFirstRun(false)
        assertThat(preferences!!.isFirstRun()).isFalse()
        preferences!!.setIsFirstRun(true)
        assertThat(preferences!!.isFirstRun()).isTrue()
    }

    @Test
    fun shouldGetNullRequestToken() {
        assertThat(preferences!!.getRequestToken()).isNull()
    }

    @Test
    fun shouldGetSetRequestToken() {
        preferences!!.setRequestToken("test_token")
        assertThat(preferences!!.getRequestToken()).isEqualTo("test_token")
    }

    @Test
    fun shouldGetNullRefreshToken() {
        assertThat(preferences!!.getRequestToken()).isNull()
    }

    @Test
    fun shouldGetSetRefreshToken() {
        preferences!!.setRefreshToken("test_token")
        assertThat(preferences!!.getRefreshToken()).isEqualTo("test_token")
    }

    @Test
    fun shouldGetNullLastLocation() {
        assertThat(preferences!!.getLastLocation()).isNull()
    }

    @Test
    fun shouldSetGetLastLocation() {
        preferences!!.setLastLocation(Coordinates(2.0, 2.0, 100))

        val res = preferences!!.getLastLocation()
        assertThat(res).isNotNull()
        assertThat(res!!.lat).isWithin(0.0001).of(res.lat)
        assertThat(res!!.lon).isWithin(0.0001).of(res.lon)
        assertThat(res!!.datetime).isEqualTo(100)
    }

    @Test
    fun shouldObserveLocationChanges() {
        val subscriber = TestSubscriber<Coordinates>()

        preferences!!.getLastLocationObservable().subscribe(subscriber)
        preferences!!.setLastLocation(Coordinates(2.0, 2.0, 100))

        subscriber.unsubscribe()

        assertThat(subscriber.onErrorEvents).isEmpty()
        assertThat(subscriber.onNextEvents).hasSize(1)

        val res = subscriber.onNextEvents[0]
        assertThat(res.lat).isWithin(0.0001).of(res.lat)
        assertThat(res.lon).isWithin(0.0001).of(res.lon)
        assertThat(res.datetime).isEqualTo(100)
    }

    @Test
    fun shouldGetDefaultFalsePreferGrid() {
        assertThat(preferences!!.getMainViewPreferGrid()).isFalse()
    }

    @Test
    fun shouldSetPreferGrid() {
        preferences!!.setMainViewPreferGrid(true)
        assertThat(preferences!!.getMainViewPreferGrid()).isTrue()

        preferences!!.setMainViewPreferGrid(false)
        assertThat(preferences!!.getMainViewPreferGrid()).isFalse()
    }

    @Test
    fun shouldObserveGridPreference() {
        val subscriber = TestSubscriber<Boolean>()

        preferences!!.getMainViewPreferGridObservable().subscribe(subscriber)
        preferences!!.setMainViewPreferGrid(true)
        preferences!!.setMainViewPreferGrid(false)

        subscriber.unsubscribe()

        assertThat(subscriber.onErrorEvents).isEmpty()
        assertThat(subscriber.onNextEvents).hasSize(2)
        assertThat(subscriber.onNextEvents[0]).isTrue()
        assertThat(subscriber.onNextEvents[1]).isFalse()
    }

}