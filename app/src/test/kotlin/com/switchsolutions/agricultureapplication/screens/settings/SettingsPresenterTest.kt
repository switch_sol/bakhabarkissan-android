package com.switchsolutions.agricultureapplication.screens.settings

import android.os.Bundle
import com.switchsolutions.agricultureapplication.ApplicationComponent
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.preferences.Preferences
import com.switchsolutions.agricultureapplication.utils.Languages
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import rx.subjects.PublishSubject

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class SettingsPresenterTest {

    private var analytics: AnalyticsClient? = null
    private var preferences: Preferences? = null
    private var component: ApplicationComponent? = null
    private var view: SettingsContract.View? = null

    @Before
    fun setUp() {
        analytics = mock(AnalyticsClient::class.java)

        preferences = mock(Preferences::class.java)

        component = mock(ApplicationComponent::class.java)
        `when`(component!!.analyticsClient()).thenReturn(analytics)
        `when`(component!!.preferences()).thenReturn(preferences)

        view = mock(SettingsContract.View::class.java)
    }

    @Test
    fun shouldOnCreate() {
        val publisher = PublishSubject.create<String>()
        `when`(preferences!!.getLanguageObservable()).thenReturn(publisher)

        val presenter = SettingsPresenter(view!!, component!!)
        presenter.onCreate(mock(Bundle::class.java))

        publisher.onNext(Languages.UR)

        verify(analytics!!).sendScreenEvent(SettingsPresenter.SCREEN_NAME)
        verify(view!!).setLanguageSummary(R.string.urdu)
    }

    @Test
    fun shouldOnResume() {
        val publisher = PublishSubject.create<String>()
        `when`(preferences!!.getLanguageObservable()).thenReturn(publisher)

        val presenter = SettingsPresenter(view!!, component!!)
        presenter.onCreate(mock(Bundle::class.java))
        presenter.onResume()

        verify(view!!).setLanguageSummary(R.string.urdu)
    }

    @Test
    fun shouldOnDestroy() {
        val publisher = PublishSubject.create<String>()
        `when`(preferences!!.getLanguageObservable()).thenReturn(publisher)

        val presenter = SettingsPresenter(view!!, component!!)
        presenter.onCreate(mock(Bundle::class.java))
        presenter.onResume()
        presenter.onDestroy()

        publisher.onNext(Languages.UR)

        verify(view!!, times(1)).setLanguageSummary(R.string.urdu)
    }

    @Test
    fun shouldShowImageCreditsScreen() {
        val publisher = PublishSubject.create<String>()
        `when`(preferences!!.getLanguageObservable()).thenReturn(publisher)

        val presenter = SettingsPresenter(view!!, component!!)
        presenter.onCreate(mock(Bundle::class.java))
        presenter.onResume()
        presenter.onImageCreditsClicked()

        verify(view!!).showImageCreditsScreen()
    }

    @Test
    fun shouldChangeLanguageSelection() {
        val publisher = PublishSubject.create<String>()
        `when`(preferences!!.getLanguageObservable()).thenReturn(publisher)

        val presenter = SettingsPresenter(view!!, component!!)
        presenter.onCreate(mock(Bundle::class.java))
        presenter.onResume()

        publisher.onNext(Languages.UR)
        publisher.onNext(Languages.EN)

        verify(view!!, times(2)).setLanguageSummary(R.string.urdu)
        verify(view!!, times(1)).setLanguageSummary(R.string.english)
    }

}