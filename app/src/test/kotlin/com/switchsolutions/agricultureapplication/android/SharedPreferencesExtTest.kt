package com.switchsolutions.agricultureapplication.android

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import rx.observers.TestSubscriber

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class SharedPreferencesExtTest {

    private var preferences: SharedPreferences? = null

    @Before
    fun setUp() {
        preferences = PreferenceManager.getDefaultSharedPreferences(RuntimeEnvironment.application)
    }

    @Test
    fun shouldObserveChanges() {
        val subscriber = TestSubscriber<String>()

        preferences!!.getObservable().subscribe(subscriber)
        preferences!!.edit().putInt("test_key_1", 1).commit()
        preferences!!.edit().putInt("test_key_2", 2).commit()

        subscriber.unsubscribe()

        assertThat(subscriber.onErrorEvents).isEmpty()
        assertThat(subscriber.onNextEvents).hasSize(2)
        assertThat(subscriber.onNextEvents[0]).isEqualTo("test_key_1")
        assertThat(subscriber.onNextEvents[1]).isEqualTo("test_key_2")
    }

}