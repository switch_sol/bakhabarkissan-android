package com.switchsolutions.agricultureapplication.stetho;

import android.content.Context;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

public final class StethoInitializer {

    private StethoInitializer() {
    }

    public static void initialize(Context context) {
        try {
            Stetho.initializeWithDefaults(context);
        } catch (Exception ignored) {

        }
    }

    public static Interceptor createInterceptor() {
        try {
            return new StethoInterceptor();
        } catch (Exception ignored) {
            return new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    return chain.proceed(chain.request());
                }
            };
        }
    }
}
