package org.jsoup.nodes

import com.switchsolutions.agricultureapplication.html.HTMLElement
import java.util.*

fun Document.toCustomElements(): List<HTMLElement> {
    val elements = ArrayList<HTMLElement>()

    var position = 0
    for (element in body().children()) {
        val htmlElement = HTMLElement.from(element, position)
        if (htmlElement != null) {
            elements.add(htmlElement)
            position++
        }
    }

    return elements
}