package com.switchsolutions.agricultureapplication.screens.farming_technique_page

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.Toolbar
import com.switchsolutions.agricultureapplication.BaseApplication
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.html.*
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorFragment
import com.switchsolutions.agricultureapplication.screens.document_outline.DocumentOutlineFragment
import com.switchsolutions.agricultureapplication.widgets.BaseActivity
import java.util.*

class FarmingTechniquePageActivity : BaseActivity(), FarmingTechniquePageContract.View {

    companion object {
        private const val KEY_FARMING_TECHNIQUE = "farming_technique"

        fun createIntent(context: Context, farmingTechnique: FarmingTechnique): Intent {
            val intent = Intent(context, FarmingTechniquePageActivity::class.java)
            intent.putExtra(KEY_FARMING_TECHNIQUE, farmingTechnique)
            return intent
        }
    }

    private var presenter: FarmingTechniquePageContract.Presenter? = null

    private var viewDrawer: DrawerLayout? = null
    private var viewToolbar: Toolbar? = null
    private var viewSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var viewContent: HTMLRecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farming_technique_page)

        viewDrawer = findViewById(R.id.container_drawer) as DrawerLayout

        viewToolbar = findViewById(R.id.toolbar) as Toolbar
        viewToolbar?.apply {
            inflateMenu(R.menu.menu_farming_technique_page)

            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_outline -> presenter?.onOutlineButtonClicked()
                }
                false
            }

            setNavigationOnClickListener {
                presenter?.onNavigationButtonClicked()
            }
        }

        viewSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout) as SwipeRefreshLayout

        viewContent = findViewById(R.id.content_view) as HTMLRecyclerView
        viewContent?.setOnElementClickListener(object : HTMLRecyclerView.OnElementClickListener {
            override fun onElementClicked(e: HTMLHeader?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }

            override fun onElementClicked(e: HTMLParagraph?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }

            override fun onElementClicked(e: HTMLImage?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }

            override fun onElementClicked(e: HTMLTable?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }

            override fun onElementClicked(e: HTMLUnorderedList?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }

            override fun onElementClicked(e: HTMLOrderedList?) {
                e?.let { presenter?.onHTMLElementClicked(it) }
            }
        })

        val farmingTechnique = intent.getParcelableExtra<FarmingTechnique>(KEY_FARMING_TECHNIQUE)
        presenter = FarmingTechniquePagePresenter(this, farmingTechnique, BaseApplication.getComponent(this))
        presenter?.onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        presenter?.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter?.onDestroy()
    }

    override fun navigateUp() {
        finish()
    }

    override fun setToolbarTitle(title: String) {
        viewToolbar?.title = title
    }

    override fun setContent(content: List<HTMLElement>) {
        viewContent?.setContent(content)
    }

    override fun setContentItemInFocus(position: Int) {
        viewContent?.setFocusedPosition(position)
    }

    override fun clearContentItemInFocus() {
        viewContent?.clearFocusedPosition()
    }

    override fun setOutline(headers: ArrayList<HTMLHeader>) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container_outline, DocumentOutlineFragment.newInstance(headers))
                .commit()
    }

    override fun showOutline() {
        viewDrawer?.openDrawer(GravityCompat.END)
    }

    override fun hideOutline() {
        viewDrawer?.closeDrawer(GravityCompat.END)
    }

    override fun setNarrator(elements: ArrayList<HTMLElement>) {
        val lp = viewSwipeRefreshLayout?.layoutParams as CoordinatorLayout.LayoutParams
        lp.bottomMargin = resources.getDimensionPixelSize(R.dimen.narrator_container_height)
        viewSwipeRefreshLayout?.layoutParams = lp

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container_narration, DocumentNarratorFragment.newInstance(elements))
                .commit()
    }

    override fun scrollToContentPosition(position: Int) {
        viewContent?.smoothScrollToPosition(position)
    }

    override fun setProgressBarEnabled(enabled: Boolean) {
        viewSwipeRefreshLayout?.isEnabled = enabled
        viewSwipeRefreshLayout?.isRefreshing = enabled
    }

    override fun showError(stringRes: Int) {
        viewContent?.let {
            Snackbar.make(it, stringRes, Snackbar.LENGTH_LONG).show()
        }
    }
}