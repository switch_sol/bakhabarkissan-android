package com.switchsolutions.agricultureapplication.screens.farming_technique_page

import android.os.Bundle
import com.switchsolutions.agricultureapplication.ApplicationComponent
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.api.farming_techniques.FarmingTechniquesClient
import com.switchsolutions.agricultureapplication.html.*
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorEvents
import com.switchsolutions.agricultureapplication.screens.document_outline.DocumentOutlineEvents
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.*

internal class FarmingTechniquePagePresenter(view: FarmingTechniquePageContract.View, ft: FarmingTechnique, component: ApplicationComponent) : FarmingTechniquePageContract.Presenter {

    companion object {
        private const val SCREEN_NAME = "Farming Technique Page"
    }

    private val view: FarmingTechniquePageContract.View
    private val farmingTechnique: FarmingTechnique

    private val farmingTechniquesClient: FarmingTechniquesClient
    private val eventBus: EventBus
    private val analyticsClient: AnalyticsClient

    private var contentSubscription: Subscription? = null
    private var outlineSubscription: Subscription? = null
    private var narratorSubscription: Subscription? = null

    init {
        this.view = view
        this.farmingTechnique = ft

        this.farmingTechniquesClient = component.farmingTechniquesClient()
        this.eventBus = component.eventBus()
        this.analyticsClient = component.analyticsClient()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        view.setToolbarTitle(farmingTechnique.name)

        requestContent()

        eventBus.register(this)

        analyticsClient.sendScreenEvent(SCREEN_NAME)
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onDestroy() {
        eventBus.unregister(this)

        contentSubscription?.apply { if (!isUnsubscribed) unsubscribe() }
        outlineSubscription?.apply { if (!isUnsubscribed) unsubscribe() }
        narratorSubscription?.apply { if (!isUnsubscribed) unsubscribe() }
    }

    override fun onNavigationButtonClicked() {
        view.navigateUp()
    }

    override fun onOutlineButtonClicked(): Boolean {
        view.showOutline()
        return true
    }

    override fun onHTMLElementClicked(e: HTMLHeader) {
        if (e.audioPath != null) {
            eventBus.post(DocumentNarratorEvents.PrepareTrack(e))
        }
    }

    override fun onHTMLElementClicked(e: HTMLParagraph) {
        sendPrepareTrackEvent(e)
    }

    override fun onHTMLElementClicked(e: HTMLImage) {
//        sendPrepareTrackEvent(e)
    }

    override fun onHTMLElementClicked(e: HTMLTable) {
        sendPrepareTrackEvent(e)
    }

    override fun onHTMLElementClicked(e: HTMLOrderedList) {
        sendPrepareTrackEvent(e)
    }

    override fun onHTMLElementClicked(e: HTMLUnorderedList) {
        sendPrepareTrackEvent(e)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnTrackPrepared) {
        view.setContentItemInFocus(event.element.index)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnTrackStarted) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnTrackPaused) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnTrackStopped) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnTrackReset) {
        view.clearContentItemInFocus()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentNarratorEvents.OnError) {
        view.showError(event.stringRes)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: DocumentOutlineEvents.OnHeaderSelected) {
        view.scrollToContentPosition(event.header.index)
        view.hideOutline()
    }

    internal fun requestContent() {
        if (contentSubscription != null && !contentSubscription!!.isUnsubscribed) return

        view.setProgressBarEnabled(true)
        contentSubscription = farmingTechniquesClient
                .getTechniquePage(farmingTechnique.id)
                .map { ArrayList(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<ArrayList<HTMLElement>>() {
                    override fun onCompleted() {
                        Timber.d("onCompleted")
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e, "onError")

                        handleRequestContentError(e)
                    }

                    override fun onNext(response: ArrayList<HTMLElement>) {
                        Timber.d("onNext: %s", response.toString())

                        handleRequestContentResponse(response)
                    }
                })
    }

    internal fun handleRequestContentError(e: Throwable) {
        when (e) {
            is HttpException            -> handleRequestContentError(e)
            is SocketTimeoutException   -> handleRequestContentError(e)
            is ConnectException         -> handleRequestContentError(e)
            else                        -> {
                Timber.e(e, "Unable to get Farming Technique Page.")
                view.showError(R.string.an_error_occurred)
            }
        }

        view.setProgressBarEnabled(false)
    }

    internal fun handleRequestContentError(e: HttpException) {
        view.showError(R.string.an_error_occurred);
    }

    internal fun handleRequestContentError(e: SocketTimeoutException) {
        view.showError(R.string.unable_to_contact_server)
    }

    internal fun handleRequestContentError(e: ConnectException) {
        view.showError(R.string.please_check_your_internet_connection)
    }

    internal fun handleRequestContentResponse(response: ArrayList<HTMLElement>) {
        view.setContent(response)
        view.setProgressBarEnabled(false)

        prepareOutline(response)
        prepareNarrator(response)
    }

    internal fun prepareOutline(elements: ArrayList<HTMLElement>) {
        if (outlineSubscription != null && !outlineSubscription!!.isUnsubscribed) return

        outlineSubscription = Observable
                .from(elements)
                .filter { it is HTMLHeader }
                .cast(HTMLHeader::class.java)
                .toList()
                .map { ArrayList(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<ArrayList<HTMLHeader>>() {
                    override fun onCompleted() {
                        Timber.d("onCompleted")
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e, "onError")

                        handlePrepareOutlineError(e)
                    }

                    override fun onNext(response: ArrayList<HTMLHeader>) {
                        Timber.d("onNext: %s", response)

                        handlePrepareOutlineResponse(response)
                    }
                })
    }

    internal fun handlePrepareOutlineError(e: Throwable) {
        Timber.e(e, "Unable to Prepare Outline.")
    }

    internal fun handlePrepareOutlineResponse(elements: ArrayList<HTMLHeader>) {
        view.setOutline(elements)
    }

    internal fun prepareNarrator(elements: ArrayList<HTMLElement>) {
        if (narratorSubscription != null && !narratorSubscription!!.isUnsubscribed) return

        narratorSubscription = Observable
                .from(elements)
                .filter { it.audioPath != null }
                .toList()
                .map { ArrayList(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<ArrayList<HTMLElement>>() {
                    override fun onCompleted() {
                        Timber.d("onCompleted")
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e, "onError")

                        handlePrepareNarratorError(e)
                    }

                    override fun onNext(t: ArrayList<HTMLElement>) {
                        Timber.d("onNext: %s", t)

                        handlePrepareNarratorResponse(t)
                    }
                })
    }

    internal fun handlePrepareNarratorError(e: Throwable) {
        // TODO
    }

    internal fun handlePrepareNarratorResponse(elements: ArrayList<HTMLElement>) {
        if (elements.size > 0) {
            view.setNarrator(elements)
        }
    }

    internal fun sendPrepareTrackEvent(e: HTMLElement) {
        if (e.audioPath != null) {
            eventBus.post(DocumentNarratorEvents.PrepareTrack(e))
        }
    }
}