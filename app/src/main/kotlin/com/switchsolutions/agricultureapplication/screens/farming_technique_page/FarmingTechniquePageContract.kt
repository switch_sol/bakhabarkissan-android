package com.switchsolutions.agricultureapplication.screens.farming_technique_page

import android.os.Bundle
import android.support.annotation.StringRes
import com.switchsolutions.agricultureapplication.html.*
import java.util.*

internal interface FarmingTechniquePageContract {

    interface View {

        fun navigateUp()

        fun setToolbarTitle(title: String)

        fun setContent(content: List<HTMLElement>)

        fun setContentItemInFocus(position: Int)

        fun clearContentItemInFocus()

        fun setOutline(headers: ArrayList<HTMLHeader>)

        fun showOutline()

        fun hideOutline()

        fun setNarrator(elements: ArrayList<HTMLElement>)

        fun scrollToContentPosition(position: Int)

        fun setProgressBarEnabled(enabled: Boolean)

        fun showError(@StringRes stringRes: Int)
    }

    interface Presenter {

        fun onCreate(savedInstanceState: Bundle?)

        fun onSaveInstanceState(outState: Bundle)

        fun onDestroy()

        fun onNavigationButtonClicked()

        fun onOutlineButtonClicked(): Boolean

        fun onHTMLElementClicked(e: HTMLHeader)

        fun onHTMLElementClicked(e: HTMLParagraph)

        fun onHTMLElementClicked(e: HTMLImage)

        fun onHTMLElementClicked(e: HTMLTable)

        fun onHTMLElementClicked(e: HTMLOrderedList)

        fun onHTMLElementClicked(e: HTMLUnorderedList)
    }

}