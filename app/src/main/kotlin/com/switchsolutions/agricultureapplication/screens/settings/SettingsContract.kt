package com.switchsolutions.agricultureapplication.screens.settings

import android.os.Bundle
import android.support.annotation.StringRes

internal interface SettingsContract {

    interface View {

        fun setLanguageSummary(@StringRes stringRes: Int)

        fun restartApplication()

        fun showImageCreditsScreen()
    }

    interface Presenter {

        fun onCreate(onSaveInstanceState: Bundle?)

        fun onResume()

        fun onSaveInstanceState(outState: Bundle)

        fun onDestroy()

        fun onImageCreditsClicked()
    }

}