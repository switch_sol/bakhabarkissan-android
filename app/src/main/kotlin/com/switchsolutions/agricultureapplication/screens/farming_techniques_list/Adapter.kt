package com.switchsolutions.agricultureapplication.screens.farming_techniques_list

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.switchsolutions.agricultureapplication.R

internal class Adapter(private val presenter: FarmingTechniquesListContract.Presenter) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.viewholder_farming_technique_list_item, parent, false)
        val holder = ViewHolder(view)

        view.setOnClickListener { presenter.onListItemClicked(holder.adapterPosition) }

        return holder
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        presenter.onBindViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return presenter.getItemCount()
    }
}