package com.switchsolutions.agricultureapplication.screens.settings

import android.os.Bundle
import android.preference.ListPreference
import android.preference.Preference
import android.preference.PreferenceFragment
import com.jakewharton.processphoenix.ProcessPhoenix
import com.switchsolutions.agricultureapplication.BaseApplication
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.preferences.Preferences
import com.switchsolutions.agricultureapplication.screens.image_sources_list.ImageSourcesListActivity

class SettingsFragment : PreferenceFragment(), SettingsContract.View {

    companion object {
        fun newInstance(): SettingsFragment {
            val bundle = Bundle()
            val fragment = SettingsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private var presenter: SettingsContract.Presenter? = null

    private var preferenceList: ListPreference? = null
    private var preferenceImageCredits: Preference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.settings)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        preferenceList = preferenceManager.findPreference(Preferences.KEY_LANGUAGE) as ListPreference

        preferenceImageCredits = preferenceManager.findPreference("image_sources")
        preferenceImageCredits?.setOnPreferenceClickListener {
            presenter?.onImageCreditsClicked()
            true
        }

        presenter = SettingsPresenter(this, BaseApplication.getComponent(activity))
        presenter?.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()

        presenter?.onResume()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        presenter?.onSaveInstanceState(outState)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        presenter?.onDestroy()
    }

    override fun setLanguageSummary(stringRes: Int) {
        preferenceList?.summary = activity.getString(stringRes)
    }

    override fun restartApplication() {
        ProcessPhoenix.triggerRebirth(activity)
    }

    override fun showImageCreditsScreen() {
        startActivity(ImageSourcesListActivity.createIntent(activity))
    }
}