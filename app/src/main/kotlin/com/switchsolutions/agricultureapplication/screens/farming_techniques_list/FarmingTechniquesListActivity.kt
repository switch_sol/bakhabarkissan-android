package com.switchsolutions.agricultureapplication.screens.farming_techniques_list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import com.switchsolutions.agricultureapplication.BaseApplication
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment
import com.switchsolutions.agricultureapplication.screens.farming_technique_page.FarmingTechniquePageActivity
import com.switchsolutions.agricultureapplication.widgets.BaseActivity

class FarmingTechniquesListActivity : BaseActivity(), FarmingTechniquesListContract.View {

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, FarmingTechniquesListActivity::class.java)
        }
    }

    private var presenter: FarmingTechniquesListContract.Presenter? = null

    private var adapter: Adapter? = null

    private var viewToolbar: Toolbar? = null
    private var viewList: RecyclerView? = null
    private var viewSwipeRefreshLayout: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farming_techniques_list)

        viewToolbar = findViewById(R.id.toolbar) as Toolbar
        viewToolbar?.setNavigationOnClickListener {
            presenter?.onNavigationButtonClicked()
        }

        viewList = findViewById(R.id.list_view) as RecyclerView
        viewList?.layoutManager = LinearLayoutManager(this)

        viewSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout) as SwipeRefreshLayout

        presenter = FarmingTechniquesListPresenter(this, BaseApplication.getComponent(this))
        presenter?.onCreate(savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        presenter?.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter?.onDestroy()
    }

    override fun navigateUp() {
        finish()
    }

    override fun showLoginScreen() {
        val intent = AccountLoginActivity.createIntent(this)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

        finish()
    }

    override fun showFarmingTechniqueScreen(farmingTechnique: FarmingTechnique) {
        val intent = FarmingTechniquePageActivity.createIntent(this, farmingTechnique)
        startActivity(intent)
    }

    override fun showBanner() {
        val params = viewSwipeRefreshLayout?.layoutParams as CoordinatorLayout.LayoutParams
        params.bottomMargin = resources.getDimensionPixelSize(R.dimen.bottom_banner_container_height)
        viewSwipeRefreshLayout?.layoutParams = params

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit()
    }

    override fun setAdapter() {
        presenter?.let {
            adapter = Adapter(it)
            viewList?.adapter = adapter
        }
    }

    override fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun notifyItemRangeInserted(positionStart: Int, additionCount: Int) {
        adapter?.notifyItemRangeInserted(positionStart, additionCount)
    }

    override fun setProgressBarEnabled(enabled: Boolean) {
        viewSwipeRefreshLayout?.apply {
            isEnabled = enabled
            isRefreshing = enabled
        }
    }

    override fun showError(stringRes: Int) {
        viewList?.let {
            Snackbar.make(it, stringRes, Snackbar.LENGTH_LONG).show()
        }
    }
}