package com.switchsolutions.agricultureapplication.screens.settings

import android.os.Bundle
import com.switchsolutions.agricultureapplication.ApplicationComponent
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.preferences.Preferences
import com.switchsolutions.agricultureapplication.utils.Languages
import com.switchsolutions.agricultureapplication.utils.LocaleUtils
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

internal class SettingsPresenter(view: SettingsContract.View, component: ApplicationComponent) : SettingsContract.Presenter {

    companion object {
        val SCREEN_NAME = "Settings"
    }

    private val view: SettingsContract.View
    private val preferences: Preferences
    private val analytics: AnalyticsClient

    private var subscription: Subscription? = null

    init {
        this.view = view
        this.preferences = component.preferences()
        this.analytics = component.analyticsClient()
    }

    override fun onCreate(onSaveInstanceState: Bundle?) {
        analytics.sendScreenEvent(SCREEN_NAME)

        subscription = preferences.getLanguageObservable().subscribe({
            setLanguageSummary(it)
            LocaleUtils.setLocale(it)

            restart()
        })
    }

    override fun onResume() {
        setLanguageSummary(preferences.getLanguage())
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onDestroy() {
        subscription?.let { if (!it.isUnsubscribed) { it.unsubscribe() } }
    }

    override fun onImageCreditsClicked() {
        view.showImageCreditsScreen()
    }

    internal fun setLanguageSummary(language: String?) {
        when (language ?: Languages.UR) {
            Languages.EN -> view.setLanguageSummary(R.string.english)
            Languages.UR -> view.setLanguageSummary(R.string.urdu)
        }
    }

    internal fun restart() {
        Observable.timer(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe { view.restartApplication() }
    }
}