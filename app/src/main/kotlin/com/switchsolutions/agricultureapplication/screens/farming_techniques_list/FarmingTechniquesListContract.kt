package com.switchsolutions.agricultureapplication.screens.farming_techniques_list

import android.os.Bundle
import android.support.annotation.StringRes
import com.switchsolutions.agricultureapplication.models.FarmingTechnique

internal interface FarmingTechniquesListContract {

    interface View {

        fun navigateUp()

        fun showLoginScreen()

        fun showFarmingTechniqueScreen(farmingTechnique: FarmingTechnique)

        fun showBanner()

        fun setAdapter()

        fun notifyDataSetChanged()

        fun notifyItemRangeInserted(positionStart: Int, additionCount: Int)

        fun setProgressBarEnabled(enabled: Boolean)

        fun showError(@StringRes stringRes: Int)
    }

    interface Presenter {

        fun onCreate(savedInstanceState: Bundle?)

        fun onSaveInstanceState(outState: Bundle)

        fun onDestroy()

        fun onNavigationButtonClicked()

        fun getItemCount(): Int

        fun onBindViewHolder(viewHolder: ViewHolder, position: Int)

        fun onListItemClicked(position: Int)
    }

}