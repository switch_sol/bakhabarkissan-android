package com.switchsolutions.agricultureapplication.screens.farming_techniques_list

import android.os.Bundle
import com.switchsolutions.agricultureapplication.ApplicationComponent
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.api.farming_techniques.FarmingTechniquesClient
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import retrofit2.adapter.rxjava.HttpException
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import timber.log.Timber
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.util.*

internal class FarmingTechniquesListPresenter(view: FarmingTechniquesListContract.View, component: ApplicationComponent) : FarmingTechniquesListContract.Presenter {

    companion object {
        private const val PAGINATION_LIMIT = 10
        private const val SCREEN_NAME = "Farming Techniques List"
    }

    private val view: FarmingTechniquesListContract.View
    private val farmingTechniquesClient: FarmingTechniquesClient
    private val analyticsClient: AnalyticsClient
    private val remoteConfigClient: RemoteConfigClient

    private val farmingTechniqueList: MutableList<FarmingTechnique> = ArrayList()

    private var subscription: Subscription? = null
    private var shouldRequestNext = true

    init {
        this.view = view
        this.farmingTechniquesClient = component.farmingTechniquesClient()
        this.analyticsClient = component.analyticsClient()
        this.remoteConfigClient = component.remoteConfigClient()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        view.setAdapter()

        requestContent()

        analyticsClient.sendScreenEvent(SCREEN_NAME)

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {

    }

    override fun onDestroy() {
        subscription?.apply { if (!isUnsubscribed) unsubscribe() }
    }

    override fun onNavigationButtonClicked() {
        view.navigateUp()
    }

    override fun getItemCount(): Int {
        return farmingTechniqueList.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        farmingTechniqueList[position].let {
            viewHolder.setTitle(it.name)
            viewHolder.setImage(it.imagePath)
        }

        if (shouldRequestNext && farmingTechniqueList.size-1 == position) {
            requestContent()
        }
    }

    override fun onListItemClicked(position: Int) {
        view.showFarmingTechniqueScreen(farmingTechniqueList[position])
    }

    internal fun requestContent() {
        if (subscription != null && !subscription!!.isUnsubscribed) return

        subscription = farmingTechniquesClient
                .getTechniques(PAGINATION_LIMIT, getItemCount())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<List<FarmingTechnique>>() {
                    override fun onCompleted() {
                        Timber.d("onCompleted")
                    }

                    override fun onError(e: Throwable) {
                        Timber.d(e, "onError")

                        handleError(e)
                    }

                    override fun onNext(response: List<FarmingTechnique>) {
                        Timber.d("onNext: %s", response)

                        handleResponse(response)
                    }
                })
    }

    internal fun handleError(e: Throwable) {
        when (e) {
            is HttpException            -> handleError(e)
            is SocketTimeoutException   -> handleError(e)
            is ConnectException         -> handleError(e)
            else                        -> {
                Timber.e(e, "Unable to get Farming Techniques List")
                view.showError(R.string.an_error_occurred)
            }
        }

        view.setProgressBarEnabled(false)
    }

    internal fun handleError(e: HttpException) {
        when (e.code()) {
            403     -> view.showLoginScreen()
            else    -> {
                view.showError(R.string.an_error_occurred)
            }
        }
    }

    internal fun handleError(e: SocketTimeoutException) {
        view.showError(R.string.unable_to_contact_server)
    }

    internal fun handleError(e: ConnectException) {
        view.showError(R.string.please_check_your_internet_connection)
    }

    internal fun handleResponse(response: List<FarmingTechnique>) {
        shouldRequestNext = response.size == PAGINATION_LIMIT

        if (farmingTechniqueList.isEmpty()) {
            farmingTechniqueList.addAll(response)
            view.notifyDataSetChanged()
        } else {
            farmingTechniqueList.addAll(response)
            view.notifyItemRangeInserted(farmingTechniqueList.size-response.size, response.size)
        }

        view.setProgressBarEnabled(false)
    }
}