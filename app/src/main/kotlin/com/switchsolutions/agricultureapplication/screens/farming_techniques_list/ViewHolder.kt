package com.switchsolutions.agricultureapplication.screens.farming_techniques_list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.switchsolutions.agricultureapplication.BuildConfig
import com.switchsolutions.agricultureapplication.R
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils

internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val viewImage: ImageView
    private val viewTitle: TextView

    init {
        this.viewImage = itemView.findViewById(R.id.farming_technique_image) as ImageView
        this.viewTitle = itemView.findViewById(R.id.farming_technique_title) as TextView
    }

    internal fun setImage(path: String) {
        Glide.with(itemView.context)
                .load(RemoteFileUrlUtils.toImageUrl(path))
                .thumbnail(Glide.with(itemView.context)
                        .load(RemoteFileUrlUtils.toThumbUrl(path))
                        .diskCacheStrategy(if (BuildConfig.DEBUG) DiskCacheStrategy.NONE else DiskCacheStrategy.ALL)
                        .centerCrop())
                .diskCacheStrategy(if (BuildConfig.DEBUG) DiskCacheStrategy.NONE else DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage)
    }

    internal fun setTitle(title: String) {
        viewTitle.text = title
    }

}