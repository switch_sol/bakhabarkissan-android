package com.switchsolutions.agricultureapplication.android

import android.content.SharedPreferences
import rx.Observable
import rx.subscriptions.Subscriptions

fun SharedPreferences.getObservable(): Observable<String> {
    return Observable
            .defer({ Observable.create<String> {
                val onPreferenceChangeListener = SharedPreferences.OnSharedPreferenceChangeListener { sp, key ->
                    run {
                        if (!it.isUnsubscribed) {
                            it.onNext(key)
                        }
                    }
                }

                registerOnSharedPreferenceChangeListener(onPreferenceChangeListener)

                it.add(Subscriptions.create {
                    unregisterOnSharedPreferenceChangeListener(onPreferenceChangeListener)
                })
            } })
}