package com.switchsolutions.agricultureapplication.api.farming_techniques

import com.switchsolutions.agricultureapplication.html.HTMLElement
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import rx.Observable

interface FarmingTechniquesClient {

    fun getTechniques(limit: Int, offset: Int, lang: String? = null): Observable<List<FarmingTechnique>>

    fun getTechniquesCached(limit: Int, offset: Int, lang: String? = null): Observable<List<FarmingTechnique>>

    fun getTechniquesFresh(limit: Int, offset: Int, lang: String? = null): Observable<List<FarmingTechnique>>

    fun getTechniquePage(id: Long, lang: String? = null): Observable<List<HTMLElement>>

    fun getTechniquePageCached(id: Long, lang: String): Observable<List<HTMLElement>>

    fun getTechniquePageFresh(id: Long, lang: String): Observable<List<HTMLElement>>
}