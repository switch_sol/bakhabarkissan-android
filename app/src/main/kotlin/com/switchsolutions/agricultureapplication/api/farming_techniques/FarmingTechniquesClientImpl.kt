package com.switchsolutions.agricultureapplication.api.farming_techniques

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer
import com.switchsolutions.agricultureapplication.html.HTMLElement
import com.switchsolutions.agricultureapplication.models.FarmingTechnique
import com.switchsolutions.agricultureapplication.network.NetworkUtility
import com.switchsolutions.agricultureapplication.preferences.Preferences
import com.switchsolutions.agricultureapplication.storage.LruCache
import com.switchsolutions.agricultureapplication.storage.Storage
import com.switchsolutions.agricultureapplication.utils.Languages
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.toCustomElements
import retrofit2.adapter.rxjava.HttpException
import rx.Observable
import java.io.IOException
import java.util.*
import javax.inject.Inject

internal class FarmingTechniquesClientImpl
@Inject
constructor(private val api: Endpoints.API,
            private val content: Endpoints.Content,
            private val preferences: Preferences,
            private val accounts: AccountsClient,
            private val network: NetworkUtility,
            storage: Storage) : FarmingTechniquesClient {

    companion object {
        private const val CACHE_SIZE: Long = 1024 * 1024
        private const val CACHE_DIR = "cache_farming_techniques"

        private fun cacheKeyTechniquesList(lang: String, limit: Int, offset: Int): String = "${lang}_techniques_list_${limit}_${offset}"
        private fun cacheKeyTechniquePage(lang: String, id: Long): String = "${lang}_technique_page_${id}"
    }

    private val techniquesReader: ObjectReader
    private val techniquesWriter: ObjectWriter

    private val cache: LruCache?

    init {
        val om = ObjectMapper()
        techniquesReader = om.readerFor(Responses.FarmingTechniquesList::class.java)
        techniquesWriter = om.writerFor(Responses.FarmingTechniquesList::class.java)

        var cache: LruCache?
        try {
            cache = storage.getLruCache(CACHE_DIR, CACHE_SIZE)
        } catch (e: IOException) {
            cache = null
        }
        this.cache = cache
    }

    override fun getTechniques(limit: Int, offset: Int, lang: String?): Observable<List<FarmingTechnique>> {
        if (network.isInternetConnected) {
            return getTechniquesFresh(limit, offset, lang ?: preferences.getLanguage())
        } else {
            return getTechniquesCached(limit, offset, lang ?: preferences.getLanguage())
                    .concatWith(getTechniquesCached(limit, offset, lang ?: preferences.getLanguage()))
                    .first()
        }
    }

    override fun getTechniquesCached(limit: Int, offset: Int, lang: String?): Observable<List<FarmingTechnique>> {
        if (cache == null) return Observable.empty()

        return Observable
                .fromCallable {
                    val response: Responses.FarmingTechniquesList
                    try {
                        response = cache.readJSON(cacheKeyTechniquesList(lang ?: preferences.getLanguage(), limit, offset), techniquesReader)
                    } catch (e: IOException) {
                        throw RuntimeException(e)
                    }
                    ArrayList(response.result)
                }
                .onErrorResumeNext(Observable.empty<ArrayList<FarmingTechnique>>())
                .map { it }
    }

    override fun getTechniquesFresh(limit: Int, offset: Int, lang: String?): Observable<List<FarmingTechnique>> {
        return api
                .getTechniques(preferences.getLanguage(), limit, offset)
                .compose(TokensRefreshTransformer<Responses.FarmingTechniquesList>(accounts))
                .doOnNext { response ->
                    cache?.let {
                        try {
                            cache.writeJSON(cacheKeyTechniquesList(lang ?: preferences.getLanguage(), limit, offset), techniquesWriter, response)
                        } catch (e: IOException) {

                        }
                    }
                }
                .map { ArrayList(it.result) }
    }

    override fun getTechniquePage(id: Long, lang: String?): Observable<List<HTMLElement>> {
        var newLang: String = lang ?: preferences.getLanguage()
        when (newLang) {
            Languages.SD -> newLang = Languages.UR
            Languages.PS -> newLang = Languages.UR
        }

        if (network.isInternetConnected) {
            return getTechniquePageFresh(id, newLang)
        } else {
            return getTechniquePageCached(id, newLang)
                    .concatWith(getTechniquePageFresh(id, newLang))
                    .first()
        }
    }

    override fun getTechniquePageCached(id: Long, lang: String): Observable<List<HTMLElement>> {
        if (cache == null) return Observable.empty()

        return Observable
                .fromCallable {
                    val response: String
                    try {
                        response = cache.readString(cacheKeyTechniquePage(lang, id))
                    } catch (e: IOException) {
                        throw RuntimeException(e)
                    }
                    Jsoup.parse(response, "")
                }
                .map(Document::toCustomElements)
    }

    override fun getTechniquePageFresh(id: Long, lang: String): Observable<List<HTMLElement>> {
        return content
                .getTechniquePage(lang, id)
                .onErrorResumeNext {
                    if (it is HttpException) {
                        if (it.code() == 404) {
                            if (lang == Languages.PS || lang == Languages.SD) {
                                content.getTechniquePage(Languages.UR, id)
                            }
                        }
                    }
                    Observable.error(it)
                }
                .map {
                    try {
                        it.string()
                    } catch (e: IOException) {
                        throw RuntimeException(e)
                    } finally {
                        it.close()
                    }
                }
                .doOnNext {
                    val key = cacheKeyTechniquePage(lang, id)
                    try {
                        cache?.writeString(key, it)
                    } catch (e: IOException) {

                    }
                }
                .map { Jsoup.parse(it, "") }
                .map { it.toCustomElements() }
    }
}