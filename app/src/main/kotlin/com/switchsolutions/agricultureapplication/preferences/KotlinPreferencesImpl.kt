package com.switchsolutions.agricultureapplication.preferences

import android.content.SharedPreferences
import com.switchsolutions.agricultureapplication.android.getObservable
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient
import com.switchsolutions.agricultureapplication.utils.Coordinates
import com.switchsolutions.agricultureapplication.utils.Languages
import rx.Observable
import javax.inject.Inject

internal class KotlinPreferencesImpl @Inject internal constructor(sharedPreferences: SharedPreferences, analytics: AnalyticsClient): Preferences {

    private val sp: SharedPreferences
    private val analytics: AnalyticsClient

    init {
        this.sp = sharedPreferences
        this.analytics = analytics
    }

    override fun getLanguage(): String {
        return sp.getString(Preferences.KEY_LANGUAGE, Languages.UR)
    }

    override fun getLanguageNormalized(): String {
        when (getLanguage()) {
            Languages.EN -> return Languages.EN
        }
        return Languages.UR
    }

    override fun setLanguage(lang: String) {
        sp.edit().putString(Preferences.KEY_LANGUAGE, lang).commit()

        analytics.sendLanguageSelectedEvent(lang)
    }

    override fun getLanguageObservable(): Observable<String> {
        return sp.getObservable()
                .filter { it == Preferences.KEY_LANGUAGE }
                .map { getLanguage() }
    }

    override fun getLanguagePicked(): Boolean {
        return sp.getBoolean(Preferences.KEY_LANGUAGE_PICKED, false)
    }

    override fun setLanguagePicked(picked: Boolean) {
        sp.edit().putBoolean(Preferences.KEY_LANGUAGE_PICKED, picked).commit()
    }

    override fun getLanguagePickedObservable(): Observable<Boolean> {
        return sp.getObservable()
                .filter { it == Preferences.KEY_LANGUAGE_PICKED }
                .map { getLanguagePicked() }
    }

    override fun isFirstRun(): Boolean {
        return sp.getBoolean(Preferences.KEY_FIRST_RUN, true)
    }

    override fun setIsFirstRun(isFirstRun: Boolean) {
        sp.edit().putBoolean(Preferences.KEY_FIRST_RUN, isFirstRun).commit()
    }

    override fun getRequestToken(): String? {
        return sp.getString(Preferences.KEY_REQUEST_TOKEN, null)
    }

    override fun setRequestToken(token: String) {
        sp.edit().putString(Preferences.KEY_REQUEST_TOKEN, token).apply()
    }

    override fun getRefreshToken(): String? {
        return sp.getString(Preferences.KEY_REFRESH_TOKEN, null)
    }

    override fun setRefreshToken(token: String) {
        sp.edit().putString(Preferences.KEY_REFRESH_TOKEN, token).apply()
    }

    override fun getLastLocation(): Coordinates? {
        return sp.getString(Preferences.KEY_LAST_LOCATION, null)?.let {
            val split = it.split("|")
            return Coordinates(split[0].toDouble(), split[1].toDouble(), split[2].toLong())
        }
    }

    override fun setLastLocation(coordinates: Coordinates) {
        sp.edit().putString(Preferences.KEY_LAST_LOCATION, "${coordinates.lat}|${coordinates.lon}|${coordinates.datetime}").apply()
    }

    override fun getLastLocationObservable(): Observable<Coordinates> {
        return sp.getObservable()
                .filter { it == Preferences.KEY_LAST_LOCATION }
                .map { getLastLocation() }
    }

    override fun getMainViewPreferGrid(): Boolean {
        return sp.getBoolean(Preferences.KEY_MAIN_VIEW_PREFER_GRID, false)
    }

    override fun setMainViewPreferGrid(preferGrid: Boolean) {
        sp.edit().putBoolean(Preferences.KEY_MAIN_VIEW_PREFER_GRID, preferGrid).apply()
    }

    override fun getMainViewPreferGridObservable(): Observable<Boolean> {
        return sp.getObservable()
                .filter { it == Preferences.KEY_MAIN_VIEW_PREFER_GRID }
                .map { getMainViewPreferGrid() }
    }
}