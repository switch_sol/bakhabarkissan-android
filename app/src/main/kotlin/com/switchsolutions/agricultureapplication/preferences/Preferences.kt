package com.switchsolutions.agricultureapplication.preferences

import com.switchsolutions.agricultureapplication.utils.Coordinates
import rx.Observable

interface Preferences {

    companion object {
        val KEY_LANGUAGE = "language"
        val KEY_LANGUAGE_PICKED = "language_picked"
        val KEY_REQUEST_TOKEN = "request_token"
        val KEY_REFRESH_TOKEN = "refresh_token"
        val KEY_FIRST_RUN = "first_run"
        val KEY_LAST_LOCATION = "last_location"
        val KEY_MAIN_VIEW_PREFER_GRID = "main_view_prefer_grid"
    }

    fun getLanguage(): String

    fun getLanguageNormalized(): String

    fun setLanguage(lang: String)

    fun getLanguageObservable(): Observable<String>

    fun getLanguagePicked(): Boolean

    fun setLanguagePicked(picked: Boolean)

    fun getLanguagePickedObservable(): Observable<Boolean>

    fun isFirstRun(): Boolean

    fun setIsFirstRun(isFirstRun: Boolean)

    fun getRequestToken(): String?

    fun setRequestToken(token: String)

    fun getRefreshToken(): String?

    fun setRefreshToken(token: String)

    fun getLastLocation(): Coordinates?

    fun setLastLocation(coordinates: Coordinates)

    fun getLastLocationObservable(): Observable<Coordinates>

    fun getMainViewPreferGrid(): Boolean

    fun setMainViewPreferGrid(preferGrid: Boolean)

    fun getMainViewPreferGridObservable(): Observable<Boolean>
}