package com.switchsolutions.agricultureapplication.farmtohome.cart;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v8.renderscript.Double2;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.MyCartResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.TokenBodyRequest;

import java.text.DecimalFormat;
import java.util.List;
import java.util.StringTokenizer;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Danish on 3/30/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {


    class ViewHolder extends RecyclerView.ViewHolder {
        String prodname;

        public TextView tvproductName, tvquantity, tvprice, tvnumberquantity, tvnumberquantity_gram;
        public Button btnup, btndown;
        public Button btnup_gram, btndown_gram;
        @Nullable
        private CompoundButton.OnCheckedChangeListener onCheckedChangeListener = null;
        private ImageView cartItemImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvproductName = (TextView) itemView.findViewById(R.id.cartItemProductName);
            tvquantity = (TextView) itemView.findViewById(R.id.cartItemquantity);
            tvprice = (TextView) itemView.findViewById(R.id.cartItemprice);
            tvnumberquantity = (TextView) itemView.findViewById(R.id.Product_Counter);
            tvnumberquantity_gram = (TextView) itemView.findViewById(R.id.Product_Counter_gram);
            btnup = (Button) itemView.findViewById(R.id.btnup);
            btndown = (Button) itemView.findViewById(R.id.btndown);
            btnup_gram = (Button) itemView.findViewById(R.id.btnup_gram);
            btndown_gram = (Button) itemView.findViewById(R.id.btndown_gram);
            cartItemImage = (ImageView) itemView.findViewById(R.id.cartItemImage);
        }

        void setTvproductName(String name) {
            this.tvproductName.setText(name);
        }

        void setTvnumberquantity(String number) {
            this.tvnumberquantity.setText(number);
        }
    }

    ViewHolder vv;
    private int counter = 0;
    private String number;
    Context context;
    float order_total_price;
    ViewHolder viewHolder;
    List<MyCartResponse> myCartResponseList;
    BaseApplication baseApplication;
    CartFragment cartFragment;

    DecimalFormat format = new DecimalFormat("0.#");

    public CartAdapter(@NonNull Context context, float order_total_price, List<MyCartResponse> myCartResponseList, CartFragment cartFragment) {

        this.context = context;
        this.myCartResponseList = myCartResponseList;
        baseApplication = (BaseApplication) context.getApplicationContext();
        this.cartFragment = cartFragment;
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_list_cart, parent, false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        Glide.with(context).load(myCartResponseList.get(position).getPrdImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.cartItemImage);

        holder.tvprice.setText("Rs. " + Integer.valueOf(myCartResponseList.get(position).getPrice()) * Float.valueOf(myCartResponseList.get(position).getQuantity()));
        holder.tvproductName.setText("" + myCartResponseList.get(position).getEnName());

        StringTokenizer tokens = new StringTokenizer(myCartResponseList.get(position).getQuantity().trim(), ".");

        double quantity = Double.valueOf(myCartResponseList.get(position).getQuantity());
        String[] quantity_total = myCartResponseList.get(position).getQuantity().split(".");

        holder.tvquantity.setText("" + myCartResponseList.get(position).getQuantity() + " " + myCartResponseList.get(position).getEnUnit());
        String sec = "";

        String first = tokens.nextToken();

        if (tokens.hasMoreTokens())
            sec = tokens.nextToken();
        else
            sec = "0";

        holder.tvnumberquantity.setText("" + first);
        holder.tvnumberquantity_gram.setText("" + sec);

/*

        if (quantity != Math.round(quantity) && myCartResponseList.get(position).getEnUnit().equalsIgnoreCase("Kg") && quantity < 1) {
            holder.tvquantity.setText("" + format.format(quantity * 1000) + " " + "Gram");
            holder.tvnumberquantity.setText("0");
            holder.tvnumberquantity_gram.setText("" + format.format(quantity * 1000));
        } else {
            holder.tvquantity.setText("" + myCartResponseList.get(position).getQuantity() + " " + myCartResponseList.get(position).getEnUnit());
            holder.tvnumberquantity.setText("" + myCartResponseList.get(position).getQuantity() + "  ");
        }
*/

        holder.btndown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove_item_from_cart(holder.getAdapterPosition());
            }
        });

        holder.btnup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_item(holder.getAdapterPosition());
            }
        });
        holder.btndown_gram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove_item_from_cart_gram(holder.getAdapterPosition());
            }
        });

        holder.btnup_gram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_item_gram(holder.getAdapterPosition());
            }
        });
    }

    private void add_item_gram(int position) {

        if (position != -1) {
            if (Double.valueOf(myCartResponseList.get(position).getQuantity()) < 10)
                addItemtoCartGram(position, 1);
            else {
                Toast.makeText(context, "Item quantity should be less than 10", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Cannot update cart", Toast.LENGTH_SHORT).show();
        }
    }

    private void remove_item_from_cart_gram(int position) {
        if (position != -1)
            addItemtoCartGram(position, 0);
    }


    private void add_item(int position) {

        if (position != -1) {
            if (Double.valueOf(myCartResponseList.get(position).getQuantity()) < 10)
                addItemtoCart(position, 1);
            else {
                Toast.makeText(context, "Item quantity should be less than 10", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Cannot update cart", Toast.LENGTH_SHORT).show();
        }
    }

    private void remove_item_from_cart(int position) {
        if (position != -1)
            addItemtoCart(position, 0);

    }


    private void addItemtoCart(int pos, int action) {


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        if (action == 0)
            addToCartRequest.setAction("remove");
        else if (action == 1)
            addToCartRequest.setAction("add");

        addToCartRequest.setProductId(myCartResponseList.get(pos).getmPrdId());

        addToCartRequest.setQuantity("" + 1);


        addToCartRequest.setToken(Constants.token);
        baseApplication.showProgresDialogue("updating your cart", context);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Toast.makeText(context, "cart update error", Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Toast.makeText(context, "" + jsonObject, Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        baseApplication.hideProgressDialogue();
                        notifyDataSetChanged();
                        getMyCart();
                    }
                });

    }

    private void addItemtoCartGram(int pos, int action) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        if (action == 0)
            addToCartRequest.setAction("remove");
        else if (action == 1)
            addToCartRequest.setAction("add");

        addToCartRequest.setProductId(myCartResponseList.get(pos).getmPrdId());

        addToCartRequest.setQuantity("" + 0.1);

        addToCartRequest.setToken(Constants.token);
        baseApplication.showProgresDialogue("updating your cart", context);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Toast.makeText(context, "cart update error", Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Toast.makeText(context, "" + jsonObject, Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        baseApplication.hideProgressDialogue();
                        notifyDataSetChanged();
                        getMyCart();
                    }
                });

    }

    private void getMyCart() {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        TokenBodyRequest tokenBodyRequest = new TokenBodyRequest();
        tokenBodyRequest.setmToken(Constants.token);
        Observable<List<MyCartResponse>> observable = iEndPoints.getMyCartApi(tokenBodyRequest);
        // ApplicationClass.showProgresDialogue("getting cart items", context);

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<MyCartResponse>>() {
                    @Override
                    public void onCompleted() {
                        // ApplicationClass.hideProgressDialogue();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "mycart on error");
                        myCartResponseList.clear();
                        notifyDataSetChanged();
                        show_cart_items(myCartResponseList);
                        //ApplicationClass.hideProgressDialogue();
                    }

                    @Override
                    public void onNext(@NonNull List<MyCartResponse> myCartResponseList) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "mycart onnext");
                        show_cart_items(myCartResponseList);
                    }
                });
    }

    private void show_cart_items(@NonNull List<MyCartResponse> myCartResponseList) {

        this.myCartResponseList = myCartResponseList;
        notifyDataSetChanged();
        order_total_price = 0;

        for (int i = 0; i < myCartResponseList.size(); i++) {
            order_total_price = order_total_price + Integer.valueOf(myCartResponseList.get(i).getPrice()) * Float.valueOf(myCartResponseList.get(i).getQuantity());
        }
        cartFragment.set_order_total_price(order_total_price);
//        tvquantitynumber.setText("" + order_total_price);
    }

    @Override
    public int getItemCount() {
        return myCartResponseList.size();
    }


}