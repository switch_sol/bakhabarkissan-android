package com.switchsolutions.agricultureapplication.farmtohome.main;

/**
 * Created by Muhammad Hassan on 4/26/2017.
 */

public interface IChangeCurrentFragment {

    void newFragmentPosition(int position);
}
