
package com.switchsolutions.agricultureapplication.farmtohome.signup;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SignUpApi {

    @SerializedName("customer")
    private Customer mCustomer;

    public Customer getCustomer() {
        return mCustomer;
    }

    public void setCustomer(Customer customer) {
        mCustomer = customer;
    }

}
