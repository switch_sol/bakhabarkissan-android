package com.switchsolutions.agricultureapplication.farmtohome.cart;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonPrimitive;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.login.LoginFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.MyCartResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.PlaceOrder;
import com.switchsolutions.agricultureapplication.farmtohome.model.TokenBodyRequest;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Muhammad Hassan on 4/4/2017.
 */

public class CartFragment extends Fragment implements View.OnClickListener {

    private RecyclerView viewList;
    Button btnProceedToNext;
    Context context;
    @NonNull
    ArrayList<String> productitems = new ArrayList<>();
    TextView ordertotal, totalamount;
    public CartAdapter adapter;
    RecyclerView.ViewHolder vh;
    Button btnDelete, btnup, btndown;
    TextView tvquantitynumber;
    String number;
    float order_total_price = 0;
    boolean isVisible;
    boolean isCreated;
    BaseApplication baseApplication;
    LoginResponse loginResponse;

    EditText etAdsress;

    //Overriden method onCreateView
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.tab2);
        isCreated = true;
    }

    public void set_order_total_price(float price) {

        if (price == 0)
            tvquantitynumber.setText("Rs. " + price);

        else if (price < 1000)
            price = price + 100;

        tvquantitynumber.setText("Rs. " + price);

        order_total_price = price;

        /* if (add) {
            tvquantitynumber.setText("" + price);
            order_total_price = order_total_price + price;
            Log.d(Constants.FARM_TO_HOME_LOGS, "order total price" + order_total_price);
        } else {
            tvquantitynumber.setText("" + price);
            order_total_price = order_total_price - price;
            Log.d(Constants.FARM_TO_HOME_LOGS, "order total price" + order_total_price);
        }*/
    }

    private void dataset_changed_notify() {

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // called here
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible)
            isVisible = visible;
        if (visible && isCreated) {
            // showProgresDialogue("getting cart data...");
            getMyCart();
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // boolean isResume = isResumed();

        if (isVisible && isCreated && !Constants.isFromCart) {
            //showProgresDialogue("getting cart data...");
            getMyCart();
        } else if (isVisible && isCreated && Constants.isFromCart) {

            //add_billing_info();
        }
    }

    @NonNull
    public static CartFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("", page);
        CartFragment fragment = new CartFragment();
        fragment.setArguments(args);
        return fragment;
    }


    public ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        viewList = (RecyclerView) view.findViewById(R.id.RV);
        viewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        tvquantitynumber = (TextView) view.findViewById(R.id.AmountTotal);
        btnProceedToNext = (Button) view.findViewById(R.id.btnProceedToNext);
        btnProceedToNext.setOnClickListener(this);

        return view;
    }


    public void showProgresDialogue(String mess) {

        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);

            if (mProgressDialog != null && !mProgressDialog.isShowing()) {
                mProgressDialog.setMessage("" + mess);
                mProgressDialog.show();
            }
        } else {
            mProgressDialog.setMessage("" + mess);
            mProgressDialog.show();
        }
    }


    public void hideProgressDialogue() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }


    private void getMyCart() {

        baseApplication = (BaseApplication) getActivity().getApplication();

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        TokenBodyRequest tokenBodyRequest = new TokenBodyRequest();
        tokenBodyRequest.setmToken(Constants.token);
        Observable<List<MyCartResponse>> observable = iEndPoints.getMyCartApi(tokenBodyRequest);
        baseApplication.showProgresDialogue("getting cart items", getActivity());

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<MyCartResponse>>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "mycart on error");

                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onNext(@NonNull List<MyCartResponse> myCartResponseList) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "mycart onnext");
                        show_cart_items(myCartResponseList);
                    }
                });
    }

    private void show_cart_items(@NonNull List<MyCartResponse> myCartResponseList) {

        adapter = new CartAdapter(getActivity(), order_total_price, myCartResponseList, this);
        viewList.setAdapter(adapter);
        order_total_price = 0;

        for (int i = 0; i < myCartResponseList.size(); i++) {
            order_total_price = order_total_price + Float.valueOf(myCartResponseList.get(i).getPrice()) * Float.valueOf(myCartResponseList.get(i).getQuantity());
        }

        if (order_total_price == 0)
            tvquantitynumber.setText("Rs. " + order_total_price);

        else if (order_total_price < 1000)
            order_total_price = order_total_price + 100;

        tvquantitynumber.setText("Rs." + order_total_price);

        if (Constants.isFromCart && myCartResponseList != null)
            if (myCartResponseList.size() != 0)
                showChangeAddressDialog();
        // place_order();
    }

    @Override
    public void onClick(View v) {
        String isLoggedIn = Sharedprefrencesutil.getSomeStringValue(getActivity(), "isLoggedIn");
        if (order_total_price != 0) {
            if (isLoggedIn != null && isLoggedIn != "" && isLoggedIn != "null") {
                if (isLoggedIn.equalsIgnoreCase("true")) {
                    loginResponse = Sharedprefrencesutil.getLoginData(getActivity(), "loginData");
                    if (loginResponse != null) {
                        showChangeAddressDialog();
                        // place_order();
                    } else {
                        loginIntent();
                    }
                }
            } else {
                loginIntent();
            }
        } else {
            Snackbar.make(btnProceedToNext, "No item in cart", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void showChangeAddressDialog() {

        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_change_address);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        loginResponse = Sharedprefrencesutil.getLoginData(getActivity(), "loginData");
        //  dialog.setTitle("Title...");
        etAdsress = (EditText) dialog.findViewById(R.id.etAdsress);
        if (loginResponse != null) {
            etAdsress.setText(loginResponse.getAddress() + " " + loginResponse.getCity());
            Constants.msisdn = loginResponse.getMobile();
        }


        TextView txtProductQuantity = (TextView) dialog.findViewById(R.id.txtProductQuantity);

        Button btnAddressChange = (Button) dialog.findViewById(R.id.btnAddressChange);
        // if button is clicked, close the custom dialog
        btnAddressChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etAdsress.setText("");
                etAdsress.setEnabled(true);
            }
        });

        Button btnCancelAddressChange = (Button) dialog.findViewById(R.id.btnCancelAddressChange);
        // if button is clicked, close the custom dialog
        btnCancelAddressChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etAdsress.getText().length() > 12) {
                    newAddress = etAdsress.getText().toString();
                    dialog.dismiss();

                    Intent intent = new Intent(getActivity(), CartOrderSummary.class);
                    //String totalprice = String.valueOf(order_total_price);
                    intent.putExtra("totalprice", "" + order_total_price);
                    intent.putExtra("shippingAddress", newAddress);
                    getActivity().startActivity(intent);

                } else {
                    etAdsress.setError("Enter complete address");
                }
            }
        });
        dialog.show();
    }

    private String newAddress = "";

    private void place_order() {

        if (order_total_price != 0) {
            Constants.isFromCart = false;
            baseApplication = (BaseApplication) getActivity().getApplication();

            baseApplication.showProgresDialogue("placing order", getActivity());

            IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
            PlaceOrder placeOrder = new PlaceOrder();

            placeOrder.setPaymentMethod("Not Specified");
            placeOrder.setShippingAddress("" + newAddress);
            placeOrder.setShippingPhone("" + loginResponse.getMobile());

            Constants.msisdn = loginResponse.getMobile();
            placeOrder.setmToken("" + Constants.token);
            Observable<JsonPrimitive> observable = iEndPoints.placeOrderApi(placeOrder);
            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<JsonPrimitive>() {
                        @Override
                        public void onCompleted() {
                            baseApplication.hideProgressDialogue();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            e.printStackTrace();
                            baseApplication.hideProgressDialogue();
                            Log.d(Constants.FARM_TO_HOME_LOGS, "error place order");
                        }

                        @Override
                        public void onNext(JsonPrimitive jsonObject) {
                            Log.d(Constants.FARM_TO_HOME_LOGS, "onnext place order");
                            Snackbar.make(btnProceedToNext, "" + jsonObject, Snackbar.LENGTH_SHORT).show();
                            List<MyCartResponse> myCartResponses = new ArrayList<MyCartResponse>();
                            show_cart_items(myCartResponses);
                        }
                    });
        } else {
            Snackbar.make(btnProceedToNext, "No item in cart", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void loginIntent() {
        Toast.makeText(getActivity(), "please loggin before proceeding to checkout", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), LoginFarmActivity.class);
        intent.putExtra("isFromCart", "true");
        startActivity(intent);
    }
}
