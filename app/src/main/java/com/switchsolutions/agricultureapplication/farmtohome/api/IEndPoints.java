package com.switchsolutions.agricultureapplication.farmtohome.api;

import android.support.annotation.NonNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.Categories;
import com.switchsolutions.agricultureapplication.farmtohome.model.DealsResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.FeedbackResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.ForgotPasswordRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.MyCartResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.OrderHistory;
import com.switchsolutions.agricultureapplication.farmtohome.model.OrderHistoryRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.PlaceOrder;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductDetailRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductDetailResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductsList;
import com.switchsolutions.agricultureapplication.farmtohome.model.RateProductRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.SignUpResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.SubmitFeedback;
import com.switchsolutions.agricultureapplication.farmtohome.model.TokenBodyRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.UpdateRequestApi;
import com.switchsolutions.agricultureapplication.farmtohome.model.UpdateResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.UserProfileUpdate;
import com.switchsolutions.agricultureapplication.farmtohome.model.VerifyPinRequest;
import com.switchsolutions.agricultureapplication.farmtohome.signup.SignUpApi;
import com.switchsolutions.agricultureapplication.farmtohome.signup.VerifyApiPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Muhammad Hassan on 4/26/2017.
 */

public interface IEndPoints {


    // get token
    @NonNull
    @GET("api/token.php")
    @Headers("Content-Type: application/json")
    Observable<String> getToken();

    // submit feedback
    @NonNull
    @POST("api/feedback.php")
    @Headers("Content-Type: application/json")
    Observable<FeedbackResponse> submitFeedback(@Body SubmitFeedback submitFeedback);


    @NonNull
    @POST("/api/userprofile_update.php")
    @Headers("Content-Type: application/json")
    rx.Observable<UpdateResponse> profileUpdate(@Body UpdateRequestApi updateRequestApi);


    // get categories
    @NonNull
    @GET("/api/deal.php")
    @Headers("Content-Type: application/json")
    Observable<List<DealsResponse>> getDeals();


    // get categories
    @NonNull
    @GET("api/categories.php")
    @Headers("Content-Type: application/json")
    Observable<List<Categories>> getCategories();


    //get products
    @NonNull
    @GET("api/products.php")
    Observable<List<ProductsList>> getAllProducts();

    //pending
    // get product details
    @NonNull
    @POST("api/product_detail.php")
    @Headers("Content-Type: application/json")
    Observable<List<ProductDetailResponse>> getProductdDetail(@Body ProductDetailRequest productDetailRequest);

    // order history api
    @NonNull
    @POST("api/order_history.php")
    @Headers("Content-Type: application/json")
    Observable<List<OrderHistory>> getOrderHistory(@Body OrderHistoryRequest orderHistoryRequest);

    //place order
    @NonNull
    @POST("api/place_order.php")
    @Headers("Content-Type: application/json")
    Observable<JsonPrimitive> placeOrderApi(@Body PlaceOrder placeOrder);

    // add product to cart
    @NonNull
    @POST("api/add_to_cart.php")
    @Headers("Content-Type: application/json")
    Observable<String> addToCartApi(@Body AddToCartRequest addToCartRequest);

    //  my cart api
    @NonNull
    @POST("api/mycart.php")
    @Headers("Content-Type: application/json")
    Observable<List<MyCartResponse>> getMyCartApi(@Body TokenBodyRequest request);  // Sign Up Model bcoz


    // signup
    @NonNull
    @POST("api/signup.php")
    @Headers("Content-Type: application/json")
    Observable<SignUpResponse> signUpApi(@Body SignUpApi signUpApi);

    //login
    // api/login.php
    @NonNull
    @POST("api/login.php")
    @Headers("Content-Type: application/json")
    Call<String> loginApi(@Body LoginRequest loginRequest);


    //forgot password
    @NonNull
    @POST("api/forgotpass.php")
    @Headers("Content-Type: application/json")
    Observable<String> forgotPasswordApi(@Body ForgotPasswordRequest forgotPasswordRequest);

    //rating api
    @NonNull
    @POST("api/rating.php")
    @Headers("Content-Type: application/json")
    Observable<String> rateProductApi(@Body RateProductRequest rateProductRequest);

    // user profile update
    @NonNull
    @POST("api/userprofile_update.php")
    @Headers("Content-Type: application/json")
    Observable<String> userProfileUpdateApi(@Body UserProfileUpdate userProfileUpdate);


    @POST("api/verifyaccount.php")
    @Headers("Content-Type: application/json")
    Observable<VerifyApiPojo> getVeryPin(@Body VerifyPinRequest verifypojo);

    //For resending the  pin code

    @POST("api/signup.php")
    @Headers("Content-Type: application/json")
    Observable<JsonObject> getResendVerify(@Body SignUpApi signUpApi);

}
