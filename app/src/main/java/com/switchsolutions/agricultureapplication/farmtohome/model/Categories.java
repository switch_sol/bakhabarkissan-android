package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Muhammad Hassan on 5/15/2017.
 */

public class Categories {

    @SerializedName("cat_id")
    private int mCatId;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("en_name")
    private String mEnName;
    @SerializedName("ur_name")
    private String mUrName;

    @SerializedName("products")
    private String mProducts;

    public int getCatId() {
        return mCatId;
    }

    public void setCatId(int catId) {
        mCatId = catId;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEnName() {
        return mEnName;
    }

    public void setEnName(String enName) {
        mEnName = enName;
    }

    public String getUrName() {
        return mUrName;
    }

    public void setUrName(String urName) {
        mUrName = urName;
    }

    public String getmProducts() {
        return mProducts;
    }

    public void setmProducts(String mProducts) {
        this.mProducts = mProducts;
    }
}
