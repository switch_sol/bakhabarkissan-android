package com.switchsolutions.agricultureapplication.farmtohome.main;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.switchsolutions.agricultureapplication.farmtohome.cart.CartFragment;
import com.switchsolutions.agricultureapplication.farmtohome.categories.CategoriesFragment;
import com.switchsolutions.agricultureapplication.farmtohome.deals.DealsFragment;
import com.switchsolutions.agricultureapplication.farmtohome.order_history.OrderHistoryFragment;
import com.switchsolutions.agricultureapplication.farmtohome.products.chicken.ChickenFragment;
import com.switchsolutions.agricultureapplication.farmtohome.products.fruits.FruitsFragment;
import com.switchsolutions.agricultureapplication.farmtohome.products.vegetables.VegetablesFragment;


/**
 * Created by Muhammad Hassan on 4/5/2017.
 */

public class FragmentPagerAdapter extends FragmentStatePagerAdapter {

    final int PAGE_COUNT = 6;

    @NonNull
    private String tabTitles[] = new String[]{"All Categories", "All deals", "My Cart", "History", "Fruits", "Vegetable"};
    private final FragmentManager mFragmentManager;
    public Fragment mFragmentAtPos0;
    private Context context;
    IChangeCurrentFragment iChangeCurrentFragment;

    public FragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
        mFragmentManager = fm;
        this.iChangeCurrentFragment = iChangeCurrentFragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            mFragmentAtPos0 = CategoriesFragment.newInstance(position + 1);
            return mFragmentAtPos0;
        } else if (position == 1) {
            mFragmentAtPos0 = DealsFragment.newInstance(position + 1);
            return mFragmentAtPos0;
        } else if (position == 2) {
            mFragmentAtPos0 = CartFragment.newInstance(position + 1);
            return mFragmentAtPos0;
        } else if (position == 3) {
            mFragmentAtPos0 = OrderHistoryFragment.newInstance(position + 1);
            return mFragmentAtPos0;
        } else if (position == 4) {
            mFragmentAtPos0 = FruitsFragment.newInstance(position);
            return mFragmentAtPos0;
        } else if (position == 5) {
            mFragmentAtPos0 = VegetablesFragment.newInstance(position);
            return mFragmentAtPos0;
        } else if (position == 6) {
            mFragmentAtPos0 = ChickenFragment.newInstance(position);
            return mFragmentAtPos0;
        }
        // mFragmentAtPos0 = ProductDetailFragment.newInstance(0);
        return mFragmentAtPos0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}