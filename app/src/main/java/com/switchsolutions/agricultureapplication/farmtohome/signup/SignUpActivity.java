package com.switchsolutions.agricultureapplication.farmtohome.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.SignUpResponse;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;
import com.switchsolutions.agricultureapplication.farmtohome.util.TextDrawable;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SignUpActivity extends AppCompatActivity {

    private EditText name, lastName, email, mobile_number, address, city, password, street, conf_password;

    Button signup_button, cross_button;
    BaseApplication baseApplication;

    boolean isFromCart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        baseApplication = (BaseApplication) getApplication();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Sign up");
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null)
            if (bundle.containsKey("isFromCart")) {
                String isFromca = bundle.getString("isFromCart");
                if (isFromca != "" && isFromca != null && isFromca != "null") {
                    if (isFromca.equalsIgnoreCase("true"))
                        isFromCart = true;
                    else
                        isFromCart = false;
                }
            }


        init_ui();
    }

    private void init_ui() {
        name = (EditText) findViewById(R.id.name);

        email = (EditText) findViewById(R.id.email);
        mobile_number = (EditText) findViewById(R.id.phone_number);
        String code = "+92";
        // mobile_number.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(code), null, null, null);
        // mobile_number.setCompoundDrawablePadding(code.length() * 13);
        city = (EditText) findViewById(R.id.city_name);
        password = (EditText) findViewById(R.id.password);
        conf_password = (EditText) findViewById(R.id.conf_password);
        street = (EditText) findViewById(R.id.street);
        name.requestFocus();

        String firebaseUserEmail = "";
        String firebaseUserName = "";

        //close button
        cross_button = (Button) findViewById(R.id.close_btn);
        cross_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (Constants.userEmail != null)
            firebaseUserEmail = Constants.userEmail;

        email.setText(firebaseUserEmail);
        if (Constants.userName != null)
            firebaseUserName = Constants.userName;

        name.setText(firebaseUserName);


        signup_button = (Button) findViewById(R.id.signUpDefault_button);
        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name1 = name.getText().toString();
                String email1 = email.getText().toString();
                String phonnum = mobile_number.getText().toString();
                String city1 = city.getText().toString();
                String passw = password.getText().toString();
                String conf_passw = conf_password.getText().toString();
                String street1 = street.getText().toString().trim();
                List<String> street_list = new ArrayList<String>();

                //String addresss = address.getText().toString().trim();

                street_list.add(street1);
                if (name1.isEmpty() && email1.isEmpty() && phonnum.isEmpty() && city1.isEmpty() && passw.isEmpty() && street1.isEmpty()) {
                    Toast.makeText(SignUpActivity.this, "Please Enter ALL Fields correctly", Toast.LENGTH_LONG).show();
                } else if (!name1.matches("^[a-zA-Z ]+$")) {
                    name.setError("User Name Must Contain Only Alphabets");
                } else if (email1.equals("") || !email1.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
                )) {
                    email.setError("Enter A Valid Email Address");
                } else if (phonnum.length() != 11 || phonnum.charAt(0) != '0' || phonnum.charAt(1) != '3') {
                    mobile_number.setError("Please enter valid number");
                    mobile_number.requestFocus();
                } else if (phonnum.charAt(2) != '4' && phonnum.charAt(2) != '3' && phonnum.charAt(2) != '1' && phonnum.charAt(2) != '2' && phonnum.charAt(2) != '0') {
                    mobile_number.setError("Please enter valid number");
                    mobile_number.requestFocus();
                } else if (street1.matches("^[0-9]+$")) {
                    street.setError("Enter A Valid Address e.g G9");
                } else if (!city1.matches("^[a-zA-Z ]+$")) {
                    city.setError("City Name Must Contain Alphabets");
                } else if (isValidPassword(passw) == false) {
                    password.setError("password must contain minimum 6 characters");
                    password.requestFocus();
                } else if (!passw.equals(conf_passw)) {
                    password.setError("Pasword doesn't match");

                } else {
                    signupProcess(name1, street1, email1, phonnum, city1, passw);
                }

            }
        });
    }


    public static boolean isValidPassword(final String password) {

        if (password != null) {
            if (password.length() < 6) {
                return false;
            } else {
                return true;
            }
        } else
            return false;

    }


    public void signupProcess(String name1, String adresss, String email1, final String phonnum, String city1, String passw) {
        SignUpApi signUpApi = new SignUpApi();
        Customer customer = new Customer();
        customer.setEmail(email1);
        customer.setName(name1);
        customer.setMobile(phonnum);
        customer.setPassword(passw);

        customer.setAddress(adresss);
        customer.setCity(city1);
        customer.setCompany("swith_solutions");
        signUpApi.setCustomer(customer);

        IEndPoints iCreateCart = ApiClient.getClient().create(IEndPoints.class);
        Observable<SignUpResponse> jsonObjectCall = iCreateCart.signUpApi(signUpApi).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        baseApplication.showProgresDialogue("Registration in progress", SignUpActivity.this);
        jsonObjectCall.subscribe(new Subscriber<SignUpResponse>() {
            @Override
            public void onCompleted() {
                baseApplication.hideProgressDialogue();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                e.printStackTrace();
                Intent intent = new Intent(SignUpActivity.this, VerifyPin.class);
                intent.putExtra("mobile", phonnum);
                intent.putExtra("mobile", mobile_number.getText().toString());
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("email", email.getText().toString());
                intent.putExtra("city", city.getText().toString());
                intent.putExtra("password", password.getText().toString());
                intent.putExtra("street", street.getText().toString().trim());
                //   startActivity(intent);
                Log.d(Constants.FARM_TO_HOME_LOGS, "signup error");
                Toast.makeText(baseApplication, "An error has occured", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(@NonNull SignUpResponse s) {
                Log.d(Constants.FARM_TO_HOME_LOGS, "signup onnext");

                Toast.makeText(SignUpActivity.this, "" + s.getMsg(), Toast.LENGTH_SHORT).show();

                if (s.getStatus() == 0) {
                    if (!isFromCart) {
                        Intent intent = new Intent(SignUpActivity.this, HomeFarmActivity.class);
                        intent.putExtra("mobile", phonnum);
                        intent.putExtra("mobile", mobile_number.getText().toString());
                        intent.putExtra("name", name.getText().toString());
                        intent.putExtra("email", email.getText().toString());
                        intent.putExtra("city", city.getText().toString());
                        intent.putExtra("password", password.getText().toString());
                        intent.putExtra("street", street.getText().toString().trim());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Constants.userName = name.getText().toString();
                        Constants.userEmail = email.getText().toString();
                        Sharedprefrencesutil.setSomeStringValue(SignUpActivity.this, "isLoggedIn", "true");
                        LoginResponse loginResponse = new LoginResponse();
                        loginResponse.setMobile(mobile_number.getText().toString());
                        loginResponse.setEmail(email.getText().toString());
                        loginResponse.setAddress(street.getText().toString().trim() + " ");
                        loginResponse.setCity(city.getText().toString());
                        loginResponse.setName(name.getText().toString());

                        Sharedprefrencesutil.setLoginData(SignUpActivity.this, loginResponse, "loginData");
                        startActivity(intent);
//                  finish();
                    } else {
                        Intent intent = new Intent(SignUpActivity.this, MainFarmActivity.class);
                        intent.putExtra("mobile", phonnum);
                        intent.putExtra("isFromCart", true);
                        intent.putExtra("mobile", mobile_number.getText().toString());
                        intent.putExtra("name", name.getText().toString());
                        intent.putExtra("email", email.getText().toString());
                        intent.putExtra("city", city.getText().toString());
                        intent.putExtra("password", password.getText().toString());
                        intent.putExtra("street", street.getText().toString().trim());
                        intent.putExtra("street", street.getText().toString().trim());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        Constants.userName = name.getText().toString();
                        Constants.userEmail = email.getText().toString();
                        Sharedprefrencesutil.setSomeStringValue(SignUpActivity.this, "isLoggedIn", "true");
                        LoginResponse loginResponse = new LoginResponse();
                        loginResponse.setMobile(mobile_number.getText().toString());
                        loginResponse.setEmail(email.getText().toString());
                        loginResponse.setAddress(street.getText().toString().trim() + " " + city.getText().toString());
                        loginResponse.setName(name.getText().toString());

                        Sharedprefrencesutil.setLoginData(SignUpActivity.this, loginResponse, "loginData");
                        startActivity(intent);
//
                    }
                } else {
                    Intent intent = new Intent(SignUpActivity.this, MainFarmActivity.class);
                    intent.putExtra("mobile", phonnum);
                    intent.putExtra("isFromCart", true);
                    intent.putExtra("mobile", mobile_number.getText().toString());
                    intent.putExtra("name", name.getText().toString());
                    intent.putExtra("email", email.getText().toString());
                    intent.putExtra("city", city.getText().toString());
                    intent.putExtra("password", password.getText().toString());
                    intent.putExtra("street", street.getText().toString().trim());
                    intent.putExtra("street", street.getText().toString().trim());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    Constants.userName = name.getText().toString();
                    Constants.userEmail = email.getText().toString();
                    Sharedprefrencesutil.setSomeStringValue(SignUpActivity.this, "isLoggedIn", "true");
                    LoginResponse loginResponse = new LoginResponse();
                    loginResponse.setMobile(mobile_number.getText().toString());
                    loginResponse.setEmail(email.getText().toString());
                    loginResponse.setAddress(street.getText().toString().trim() + " " + city.getText().toString());
                    loginResponse.setName(name.getText().toString());

                    Sharedprefrencesutil.setLoginData(SignUpActivity.this, loginResponse, "loginData");
                    startActivity(intent);
//
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
