package com.switchsolutions.agricultureapplication.farmtohome.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.product_detail.ProductDetailActivity;
import com.switchsolutions.agricultureapplication.farmtohome.products.fruits.FruitsList;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class ItemAllFruits extends RecyclerView.Adapter<ItemAllFruits.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    Context mContext;
    String productCount = "";
    float countProduct;

    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_to_cart:
                return true;
            default:
        }
        return false;
    }

    private void addItemtoCart(int pos, String productCount) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAction("add");
        addToCartRequest.setProductId(fruitsList.get(pos).getPrdId());
        addToCartRequest.setQuantity(productCount);
        addToCartRequest.setToken(Constants.token);
        baseApplication.showProgresDialogue("adding item in cart", mContext);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        Toast.makeText(mContext, "" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_item_list_more;
        public TextView txt_item_list_product_name;
        public TextView txt_item_list_price;
        public ImageView img_item_list_product;
        public RelativeLayout rltv_item_list_all_product;


        public ViewHolder(@NonNull View v) {
            super(v);
            img_item_list_more = (ImageView) v.findViewById(R.id.img_item_list_more);
            txt_item_list_product_name = (TextView) v.findViewById(R.id.txt_item_list_product_name);
            txt_item_list_price = (TextView) v.findViewById(R.id.txt_item_list_price);
            img_item_list_product = (ImageView) v.findViewById(R.id.img_item_list_product);
            rltv_item_list_all_product = (RelativeLayout) v.findViewById(R.id.rltv_item_list_all_product);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    List<FruitsList> fruitsList;
    BaseApplication baseApplication;

    public ItemAllFruits(Context context, List<FruitsList> fruitsList) {
        mContext = context;
        this.fruitsList = fruitsList;

        baseApplication = (BaseApplication) mContext.getApplicationContext();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ItemAllFruits.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                       int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_all_products, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        holder.img_item_list_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopupMenu(holder.rltv_item_list_all_product, holder.getAdapterPosition());
            }
        });

        holder.rltv_item_list_all_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(holder.rltv_item_list_all_product, holder.getAdapterPosition());
            }
        });

        holder.txt_item_list_product_name.setText(fruitsList.get(position).getEnName());
        holder.txt_item_list_price.setText("Rs. " + fruitsList.get(position).getPrice() + "/" + fruitsList.get(position).getmEngUnit());
        Glide.with(mContext).load(fruitsList.get(position).getPrdImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_item_list_product);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return fruitsList.size();
    }


    private void showPopupMenu(@NonNull View view, final int popUpPosition) {

        final PopupWindow popupWindow = new PopupWindow(mContext); // inflet your layout or diynamic add view
        View view1 = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view1 = inflater.inflate(R.layout.popup_layout, null);
        TextView item = (TextView) view1.findViewById(R.id.button1);


        TextView buttonLivePic = (TextView) view1.findViewById(R.id.button4);
        buttonLivePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ProductDetailActivity.class).putExtra("prdId", fruitsList.get(popUpPosition).getPrdId()));

            }
        });


        popupWindow.setFocusable(true);

        view1.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int width = view1.getMeasuredWidth();
        int height = view1.getMeasuredHeight();
        popupWindow.setWidth(width);
        popupWindow.setHeight(height);

        item.setGravity(Gravity.CENTER);
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        popupWindow.setContentView(view1);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            int pxValue = pxToDp(85);
            popupWindow.showAsDropDown(view, view.getWidth() - popupWindow.getWidth(), -pxValue);
        } else {
            popupWindow.showAsDropDown(view, view.getWidth() - popupWindow.getWidth(), 0);
        }

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null)
                    popupWindow.dismiss();
                show_product_count_dialog(popUpPosition);

            }
        });
    }

    private void show_product_count_dialog(final int popUpPosition) {

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_product_count);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //  dialog.setTitle("Title...");
        final EditText editTextProductCount = (EditText) dialog.findViewById(R.id.etProductCount);

        TextView txtProductQuantity = (TextView) dialog.findViewById(R.id.txtProductQuantity);

        SpannableString str = new SpannableString(fruitsList.get(popUpPosition).getmEngUnit());
        str.setSpan(new StyleSpan(Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtProductQuantity.setText("Enter Quantity in " + str);

        Button dialogButton = (Button) dialog.findViewById(R.id.btnSubmitProductCount);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextProductCount.getText().length() > 0) {
                    productCount = editTextProductCount.getText().toString();
                    countProduct = Float.valueOf(productCount);
                    if (countProduct == 0 || countProduct > 10) {
                        Toast.makeText(mContext, "Enter Valid Quantity. Between 1 to 10", Toast.LENGTH_SHORT).show();
                    } else {
                        addItemtoCart(popUpPosition, productCount);
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
