package com.switchsolutions.agricultureapplication.farmtohome.product_detail;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.switchsolutions.agricultureapplication.R;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arshad on 4/21/2017.
 */

public class ScreenSliderAdapterTop extends PagerAdapter {
    Context context;
    ArrayList imagesArray;
    LayoutInflater inflater;

    List<String> prdLiveImagesUrl;

    ScreenSliderAdapterTop(Context context,
                           List<String> prdLiveImagesUrl) {
        this.context = context;
        this.prdLiveImagesUrl = prdLiveImagesUrl;
        inflater = LayoutInflater.from(context);

    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.screen_slide_images, container, false);
        ImageView image = (ImageView) view.findViewById(R.id.custom_image_for_slide);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);

        Glide.with(context).load(prdLiveImagesUrl.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(image);

        container.addView(view, 0);
        return view;
    }

    @Override
    public int getCount() {
        return prdLiveImagesUrl.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, Object object) {
        return view.equals(object);
    }
}
