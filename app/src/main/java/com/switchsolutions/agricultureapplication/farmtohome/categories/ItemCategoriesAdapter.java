package com.switchsolutions.agricultureapplication.farmtohome.categories;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.model.Categories;
import com.switchsolutions.agricultureapplication.farmtohome.product_detail.ProductDetailActivity;

import java.util.List;

/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class ItemCategoriesAdapter extends RecyclerView.Adapter<ItemCategoriesAdapter.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    Context mContext;

    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                return true;
            default:
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_item_list_more;
        public TextView txt_item_list_product_name;
        public TextView txt_product_count_cat;
        public ImageView img_product_cat;


        public ViewHolder(@NonNull View v) {
            super(v);
            img_item_list_more = (ImageView) v.findViewById(R.id.img_item_list_more);
            img_product_cat = (ImageView) v.findViewById(R.id.img_product_cat);
            txt_item_list_product_name = (TextView) v.findViewById(R.id.txt_item_list_product_name);
            txt_product_count_cat = (TextView) v.findViewById(R.id.txt_product_count_cat);
        }
    }

    List<Categories> categoriesList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemCategoriesAdapter(Context context, List<Categories> categoriesList) {
        mContext = context;
        this.categoriesList = categoriesList;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ItemCategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                               int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_categories, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        holder.txt_item_list_product_name.setText(categoriesList.get(position).getEnName());
        //holder.txt_product_count_cat.setText(categoriesList.get(position).get);

        if (categoriesList.get(position).getCatId() == 1) {
            holder.img_product_cat.setImageResource(R.drawable.cat_vegetables);
        } else if (categoriesList.get(position).getCatId() == 2) {
            holder.img_product_cat.setImageResource(R.drawable.cat_fruits);
        } else if (categoriesList.get(position).getCatId() == 3) {
            holder.itemView.setVisibility(View.GONE);
           // holder.img_product_cat.setImageResource(R.drawable.cat_meat);
        }

        holder.txt_product_count_cat.setText(categoriesList.get(position).getmProducts() + " " + "products");


        holder.img_item_list_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailActivity.class);
                mContext.startActivity(intent);
                //showPopupMenu(holder.img_item_list_more);
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return categoriesList.size();
    }


    private void showPopupMenu(@NonNull View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_list_item, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }
}
