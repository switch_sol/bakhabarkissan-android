package com.switchsolutions.agricultureapplication.farmtohome.products.vegetables;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;


/**
 * Created by Muhammad Hassan on 4/5/2017.
 */

public class VegetablesFragment extends Fragment {

    RecyclerView mRecyclerView;
    GridLayoutManager mLayoutManager;
    ItemVegetableAdapter itemProduucts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    public static VegetablesFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("", page);
        VegetablesFragment fragment = new VegetablesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_fruits, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(getActivity(), 2);
        if (Constants.vegetablesList != null) {
            itemProduucts = new ItemVegetableAdapter(getActivity(), Constants.vegetablesList);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(itemProduucts);
        }
        return view;
    }
}
