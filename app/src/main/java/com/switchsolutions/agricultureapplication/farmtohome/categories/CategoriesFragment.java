package com.switchsolutions.agricultureapplication.farmtohome.categories;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.main.IChangeCurrentFragment;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.Categories;
import com.switchsolutions.agricultureapplication.farmtohome.util.RecyclerItemClickListener;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Muhammad Hassan on 4/5/2017.
 */

public class CategoriesFragment extends Fragment {

   // private int mPage;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    IChangeCurrentFragment iChangeCurrentFragment;

    boolean isVisible;
    boolean isCreated;


    @NonNull
    public static CategoriesFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("ARG_PAGE", page);
        CategoriesFragment fragment = new CategoriesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // mPage = getArguments().getInt(ARG_PAGE);
        isCreated = true;
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

        if (visible)
            isVisible = visible;
        if (visible && isCreated) {
            makeRequestForCategoreis();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       // boolean isResume = isResumed();
        if (isVisible && isCreated) {
            makeRequestForCategoreis();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_category, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        MainFarmActivity.observer.update(null, position);
                    }
                })
        );

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void makeRequestForCategoreis() {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        Observable<List<Categories>> observableCategories = iEndPoints.getCategories();
        observableCategories.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<Categories>>() {
                    @Override
                    public void onCompleted() {
                    }
                    @Override
                    public void onError(Throwable e) {
                    }
                    @Override
                    public void onNext(List<Categories> categories) {
                        ItemCategoriesAdapter itemCategoriesAdapter = new ItemCategoriesAdapter(getActivity(), categories);
                        mRecyclerView.setAdapter(itemCategoriesAdapter);
                    }
                });
    }


    public ProgressDialog mProgressDialog;

    public void showProgresDialogue(String mess) {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.setMessage("" + mess);
            mProgressDialog.show();
        }
    }

    public void hideProgressDialogue() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

}
