
package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class MyCartResponse {

    @SerializedName("en_name")
    private String mEnName;
    @SerializedName("en_unit")
    private String mEnUnit;

    @SerializedName("prd_image")
    private String mPrdImage;

    public String getmPrdId() {
        return mPrdId;
    }

    public void setmPrdId(String mPrdId) {
        this.mPrdId = mPrdId;
    }

    @SerializedName("prd_id")
    private String mPrdId;


    @SerializedName("price")
    private String mPrice;
    @SerializedName("quantity")
    private String mQuantity;

    public String getEnName() {
        return mEnName;
    }

    public void setEnName(String enName) {
        mEnName = enName;
    }

    public String getEnUnit() {
        return mEnUnit;
    }

    public void setEnUnit(String enUnit) {
        mEnUnit = enUnit;
    }

    public String getPrdImage() {
        return mPrdImage;
    }

    public void setPrdImage(String prdImage) {
        mPrdImage = prdImage;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

}
