package com.switchsolutions.agricultureapplication.farmtohome.home;

/**
 * Created by Muhammad Hassan on 4/4/2017.
 */

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.adapter.ItemAllChickens;
import com.switchsolutions.agricultureapplication.farmtohome.adapter.ItemAllFruits;
import com.switchsolutions.agricultureapplication.farmtohome.adapter.ItemAllMeat;
import com.switchsolutions.agricultureapplication.farmtohome.adapter.ItemAllVegetables;
import com.switchsolutions.agricultureapplication.farmtohome.interface_callbacks.IOnItemClickHome;
import com.switchsolutions.agricultureapplication.farmtohome.products.fruits.FruitsList;
import com.switchsolutions.agricultureapplication.farmtohome.products.meat.MeatList;
import com.switchsolutions.agricultureapplication.farmtohome.products.vegetables.VegetablesList;

import java.util.List;

/**
 * Created by Arshad on 4/3/2017.
 */
public class CommonRecycleAdapter extends RecyclerView.Adapter<CommonRecycleAdapter.ViewHolder> {

    private List<String> itemsData;
    Context context;
    int cat_count;
    IOnItemClickHome iOnItemClickHome;
    List<FruitsList> fruitsList;
    List<VegetablesList> vegetablesList;
    List<MeatList> meatList;

    public CommonRecycleAdapter(int cat_count, Context mContext, IOnItemClickHome iOnItemClickHomeR
            , List<FruitsList> fruitsList, List<VegetablesList> vegetablesList, List<MeatList> meatList
    ) {
        this.context = mContext;
        this.cat_count = cat_count;
        this.iOnItemClickHome = iOnItemClickHomeR;
        this.fruitsList = fruitsList;
        this.vegetablesList = vegetablesList;
        this.meatList = meatList;
    }

    @NonNull
    @Override
    public CommonRecycleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_common_items, null);
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {
//        viewHolder.text.setText(itemsData.get(position));

        if (position == 0 && fruitsList != null) {
            viewHolder.txt_prd_cat_label.setText("Fruits");
            ItemAllFruits itemProduucts = new ItemAllFruits(context, fruitsList);
            viewHolder.recyclerViewProducts.setAdapter(itemProduucts);

        } else if (position == 1 && vegetablesList != null) {
            viewHolder.txt_prd_cat_label.setText("Vegetable");
            ItemAllVegetables itemProduucts = new ItemAllVegetables(context, vegetablesList);
            viewHolder.recyclerViewProducts.setAdapter(itemProduucts);
        } else if (position == 2) {
            //viewHolder.txt_prd_cat_label.setText("Meat");
            // ItemAllMeat itemProduucts = new ItemAllMeat(context, meatList);
            // viewHolder.recyclerViewProducts.setAdapter(itemProduucts);
        } else if (position == 3) {
            //viewHolder.txt_prd_cat_label.setText("Chicken");
            //ItemAllChickens itemProduucts = new ItemAllChickens(context);
            //viewHolder.recyclerViewProducts.setAdapter(itemProduucts);

        }

        viewHolder.txt_prd_cat_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnItemClickHome.onItemClick(viewHolder.getAdapterPosition());
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // @Bind(R.id.tx)
        public RecyclerView recyclerViewProducts;
        public LinearLayoutManager linearLayoutManager;
        public TextView txt_prd_cat_label;

        public ViewHolder(@NonNull View itemLayoutView) {
            super(itemLayoutView);
            recyclerViewProducts = (RecyclerView) itemLayoutView.findViewById(R.id.rv_all_products);
            txt_prd_cat_label = (TextView) itemLayoutView.findViewById(R.id.txt_prd_cat_label);
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            recyclerViewProducts.setLayoutManager(linearLayoutManager);

            //text = (TextView) itemLayoutView.findViewById(R.id.txt_recycler_item);
            //ButterKnife.bind(this, itemLayoutView);
        }
    }

    @Override
    public int getItemCount() {
        return cat_count;
    }
}