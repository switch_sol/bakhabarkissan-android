package com.switchsolutions.agricultureapplication.farmtohome.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.product_detail.ProductDetailActivity;
import com.switchsolutions.agricultureapplication.farmtohome.profile.ProfileActivity;
import com.switchsolutions.agricultureapplication.farmtohome.service_areas.ServiceAreasActivity;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


public class MainFarmActivity extends AppCompatActivity implements IChangeCurrentFragment {

    Toolbar myToolbar;
    TabLayout tabLayout;

    @NonNull
    private int[] imageResId = {
            R.drawable.categories,
            R.drawable.my_cart,
            R.drawable.history, R.drawable.fruite, R.drawable.vegetables, R.drawable.meat
    };

    DrawerLayout drawerLayout;
    ViewPager viewPager;

    public ProgressDialog mProgressDialog;
    @Nullable
    public static Observer observer;
    boolean isFromCart = false;
    MaterialSearchView searchView;

    String prdId = "";
    // MaterialSearchView searchView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_main);
        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_menu_white);
        getSupportActionBar().setTitle("Farm to Home");

        myToolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));

        Bundle extras = getIntent().getExtras();
        int page_num = 0;

        if (extras != null && extras.containsKey("page_num")) {
            page_num = extras.getInt("page_num", 0);
        }
        if (extras != null && extras.containsKey("isFromCart")) {
            isFromCart = true;
            page_num = 1;
        }

        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        Snackbar.make(myToolbar, "Only in Islamabad/Rawalpindi", Snackbar.LENGTH_LONG).setAction("Locations", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainFarmActivity.this, ServiceAreasActivity.class);
                startActivity(intent);
            }
        }).show();

        final NavigationView mNavigationView;
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setItemIconTintList(null);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        View headerView = mNavigationView.inflateHeaderView(R.layout.nav_header);
        TextView textViewName = (TextView) headerView.findViewById(R.id.name);
        textViewName.setText("" + Constants.userName);

        TextView textViewEail = (TextView) headerView.findViewById(R.id.email);
        textViewEail.setText("" + Constants.userEmail);

        mNavigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        mNavigationView.setCheckedItem(menuItem.getItemId());
                        //  menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.nav_home: {
                                finish();
                                break;
                            }
                            case R.id.bkk_id2: {
                                Intent intent = MainScreenActivity.createIntent(MainFarmActivity.this);
                                startActivity(intent);
                                break;
                            }

                            case R.id.nav_deals: {
                                change_current_page(1);
                                // Toast.makeText(MainActivity.this, "Not Implemented Yet..", Toast.LENGTH_SHORT).show();
                                break;
                            }
                            case R.id.nav_veggies: {
                                change_current_page(5);
                                break;
                            }
                            case R.id.nav_fruits: {
                                change_current_page(4);
                                break;
                            }
                            /*case R.id.nav_meat_chicken: {
                                change_current_page(5);
                                change_current_page(5);
                                break;
                            }
                            */
                            case R.id.my_orders: {
                                change_current_page(3);
                                break;
                            }

                            case R.id.logout: {
                                LoginResponse loginResponse = new LoginResponse();
                                Sharedprefrencesutil.setLoginData(MainFarmActivity.this, loginResponse, "loginData");
                                Sharedprefrencesutil.setSomeStringValue(MainFarmActivity.this, "isLoggedIn", "");
                                Constants.userEmail = "";
                                Constants.msisdn = "";
                                Constants.userName = "";

                                Toast.makeText(MainFarmActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();

                                Intent intent = MainScreenActivity.createIntent(MainFarmActivity.this);
                                startActivity(intent);


                                break;
                            }

                            case R.id.profile: {
                                startActivity(new Intent(MainFarmActivity.this, ProfileActivity.class));
                                // Toast.makeText(MainActivity.this, "Not Implemented Yet..", Toast.LENGTH_SHORT).show();
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                        return true;
                    }
                });


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        observer = new Observer() {
            @Override
            public void update(Observable o, @NonNull Object arg) {
                int position = Integer.valueOf(arg.toString());
                if (position == 0)
                    viewPager.setCurrentItem(4);
                else if (position == 1)
                    viewPager.setCurrentItem(3);
                else if (position == 2)
                    viewPager.setCurrentItem(5);
                else if (position == 3)
                    viewPager.setCurrentItem(5);
            }
        };

        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager(), MainFarmActivity.this));
        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(page_num);


        setupimageResId();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        observer = null;
    }

    public void showProgresDialogue(String mess) {

        mProgressDialog = new ProgressDialog(MainFarmActivity.this);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);

        if (mProgressDialog != null && !mProgressDialog.isShowing()) {
            mProgressDialog.setMessage("" + mess);
            mProgressDialog.show();
        }
    }

    public void hideProgressDialogue() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }


    private void change_current_page(int pageNum) {
        viewPager.setCurrentItem(pageNum);
    }

    @Override
    public void onBackPressed() {

        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }


    private void setupimageResId() {

        tabLayout.getTabAt(0).setIcon(imageResId[0]);
        tabLayout.getTabAt(1).setIcon(imageResId[0]);
        tabLayout.getTabAt(2).setIcon(imageResId[1]);
        tabLayout.getTabAt(3).setIcon(imageResId[2]);
        tabLayout.getTabAt(4).setIcon(imageResId[3]);
        tabLayout.getTabAt(5).setIcon(imageResId[4]);
        // tabLayout.getTabAt(5).setIcon(imageResId[5]);

        LinearLayout ll = (LinearLayout) tabLayout.getChildAt(0);
        for (int i = 0; i < ll.getChildCount(); i++) {
            LinearLayout tabView = (LinearLayout) ll.getChildAt(i);
            for (int j = 0; j < tabView.getChildCount(); j++) {
                if (tabView.getChildAt(j) instanceof TextView) {
                    TextView textView = (TextView) tabView.getChildAt(j);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) textView.getLayoutParams();
                    layoutParams.topMargin = 2;
                    textView.setLayoutParams(layoutParams);
                } else if (tabView.getChildAt(j) instanceof ImageView) {
                    ImageView imageView = (ImageView) tabView.getChildAt(j);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
                    layoutParams.topMargin = 5;
                    layoutParams.width = 60;
                    layoutParams.height = 60;
                    imageView.setLayoutParams(layoutParams);
                }
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_farmtohome, menu);
        MenuItem searchItem; // = menu.findItem(R.id.action_search);
        searchItem = menu.findItem(R.id.action_search);

        searchView.setMenuItem(searchItem);


        searchView.setSubmitOnClick(true);


        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic


                for (int i = 0; i < Constants.meatList.size(); i++) {

                    if (Constants.meatList.get(i).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.meatList.get(i).getPrdId();
                    }
                }
                for (int k = 0; k < Constants.fruitsList.size(); k++) {

                    if (Constants.fruitsList.get(k).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.fruitsList.get(k).getPrdId();
                    }
                }
                for (int j = 0; j < Constants.vegetablesList.size(); j++) {
                    if (Constants.vegetablesList.get(j).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.vegetablesList.get(j).getPrdId();
                    }
                }
                if (prdId != "")
                    startActivity(new Intent(MainFarmActivity.this, ProductDetailActivity.class).putExtra("prdId", prdId));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                add_search_suggestions();
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });

        searchView.setCursorDrawable(R.drawable.custom_cursor);
        //add_search_suggestions();
        // Configure the search info and add any event listeners...
        MenuItem menuItemCart = menu.findItem(R.id.action_favorite);
        if (BaseApplication.cart_count > 0)
            menuItemCart.setIcon(ContextCompat.getDrawable(MainFarmActivity.this, R.drawable.basket_hover));
        menuItemCart.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (viewPager != null) {
                    viewPager.setCurrentItem(1);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_search:
                add_search_suggestions();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void newFragmentPosition(int position) {

    }

    private void add_search_suggestions() {
        List<String> suggestionsList = new ArrayList<>();
        for (int i = 0; i < Constants.fruitsList.size(); i++) {
            suggestionsList.add(Constants.fruitsList.get(i).getEnName());
        }

        for (int i = 0; i < Constants.vegetablesList.size(); i++) {
            suggestionsList.add(Constants.vegetablesList.get(i).getEnName());
        }

        for (int i = 0; i < Constants.meatList.size(); i++) {
            suggestionsList.add(Constants.meatList.get(i).getEnName());
        }

        String[] sugg = suggestionsList.toArray(new String[0]);
        searchView.setSuggestions(sugg);

    }
}
