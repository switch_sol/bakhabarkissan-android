package com.switchsolutions.agricultureapplication.farmtohome.product_detail;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductDetailRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductDetailResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.RateProductRequest;
import com.switchsolutions.agricultureapplication.farmtohome.product_detail.reviews.ReviewsAdapter;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class ProductDetailActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    AdapterSecondDetail itemProduucts;
    Toolbar myToolbar;
    public ViewPager mPager;
    public CircleIndicator indicator;
    public ViewPager mPagerTop;
    public CircleIndicator indicatorTop;
    public RelativeLayout rltv_item_prd_detail_addcart;
    public Button btn_item_prd_detail_addcart;
    int prdId;
    List<String> prdLiveImagesUrl;
    int ratingTotal;
    String productName = "";
    BaseApplication baseApplication;
    List<ProductDetailResponse> jsonObjectProduct;
    boolean hasRated = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_second);
        baseApplication = (BaseApplication) getApplication();

        initUiVariables();
        hasRated = false;

        prdLiveImagesUrl = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("prdId")) {
                prdId = Integer.valueOf(bundle.getString("prdId", ""));
            }
        }

        getProductDetails(prdId);

    }

    private void initUiVariables() {

        myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        myToolbar.setTitle("Fruits Store");
        myToolbar.setTitleTextColor(Color.WHITE);

        mPager = (ViewPager) findViewById(R.id.viewPager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        rltv_item_prd_detail_addcart = (RelativeLayout) findViewById(R.id.rltv_item_prd_detail_addcart);
        btn_item_prd_detail_addcart = (Button) findViewById(R.id.btn_item_prd_detail_addcart);

        btn_item_prd_detail_addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ////Toast.makeText(ProductDetailActivity.this, "click", ////Toast.LENGTH_SHORT).show();
                show_product_count_dialog(1);
            }
        });

        mPager.setAdapter(new ScreenSlidePagerAdapter(getSupportFragmentManager()));
        indicator.setViewPager(mPager);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_product_reviews);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
        // mRecyclerView.setNestedScrollingEnabled(true);


    }

    String productCount = "";
    float countProduct = 0;

    private void show_product_count_dialog(final int popUpPosition) {

        final Dialog dialog = new Dialog(ProductDetailActivity.this);
        dialog.setContentView(R.layout.dialog_product_count);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //  dialog.setTitle("Title...");
        final EditText editTextProductCount = (EditText) dialog.findViewById(R.id.etProductCount);

        TextView txtProductQuantity = (TextView) dialog.findViewById(R.id.txtProductQuantity);
        txtProductQuantity.setText("Enter Quantity ");

        Button dialogButton = (Button) dialog.findViewById(R.id.btnSubmitProductCount);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextProductCount.getText().length() > 0) {
                    productCount = editTextProductCount.getText().toString();
                    countProduct = Float.valueOf(productCount);
                    if (countProduct == 0 || countProduct > 10) {
                        Toast.makeText(ProductDetailActivity.this, "Enter Valid Quantity. Between 1 to 10", Toast.LENGTH_SHORT).show();
                    } else {
                        addItemtoCart(prdId, 1, productCount);
                    }
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void addItemtoCart(int prdId, int action, String productCount) {


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        if (action == 0)
            addToCartRequest.setAction("remove");
        else if (action == 1)
            addToCartRequest.setAction("add");

        addToCartRequest.setProductId(String.valueOf(prdId));
        addToCartRequest.setQuantity(productCount);
        addToCartRequest.setToken(Constants.token);
        if (baseApplication != null)
            baseApplication.showProgresDialogue("updating your cart", ProductDetailActivity.this);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        ////Toast.makeText(ProductDetailActivity.this, "cart update error", ////Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        ////Toast.makeText(ProductDetailActivity.this, "" + jsonObject, ////Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        baseApplication.hideProgressDialogue();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                break;
            }
            default: {
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void getProductDetails(final int product_id) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);

        if (baseApplication != null)
            baseApplication.showProgresDialogue("getting product details", ProductDetailActivity.this);

        ProductDetailRequest productDetailRequest = new ProductDetailRequest();
        productDetailRequest.setProductId(Long.valueOf(product_id));
        Observable<List<ProductDetailResponse>> observable = iEndPoints.getProductdDetail(productDetailRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<ProductDetailResponse>>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        ////Toast.makeText(ProductDetailActivity.this, "error fetching products", ////Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "product details onerror");
                    }

                    @Override
                    public void onNext(@Nullable List<ProductDetailResponse> jsonObject) {
                        ////Toast.makeText(ProductDetailActivity.this, "" + jsonObject, ////Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "product details onnext");
                        baseApplication.hideProgressDialogue();
                        if (jsonObject != null)
                            if (jsonObject.size() > 0) {
                                productName = jsonObject.get(0).getEnName();
                                myToolbar.setTitle("" + productName);
                                for (int i = 0; i < jsonObject.size(); i++) {
                                    if (i == 0)
                                        prdLiveImagesUrl.add(jsonObject.get(i).getImagePath());
                                    else if (prdLiveImagesUrl.get(prdLiveImagesUrl.size() - 1).equalsIgnoreCase(jsonObject.get(i).getImagePath())) {
                                    } else {
                                        prdLiveImagesUrl.add(jsonObject.get(i).getImagePath());
                                    }
                                }
                                mRecyclerView.setAdapter(new ReviewsAdapter(ProductDetailActivity.this, jsonObject));
                                jsonObjectProduct = jsonObject;
                                setupTopviewPager();
                            }
                        // process_product_response(jsonObject);
                    }
                });
    }

    private void process_product_response(@Nullable List<ProductDetailResponse> detailResponseList) {
        if (detailResponseList != null) {
            itemProduucts = new AdapterSecondDetail(this, this.getSupportFragmentManager(), 10);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(itemProduucts);
        }
    }

    private void setupTopviewPager() {

        mPagerTop = (ViewPager) findViewById(R.id.viewPagerTop);
        mPagerTop.setAdapter(new ScreenSliderAdapterTop(ProductDetailActivity.this, prdLiveImagesUrl));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicatorTop);
        indicator.setViewPager(mPagerTop);
    }

    public void ratingSelected(int rating) {
        ////Toast.makeText(ProductDetailActivity.this, "rate product" + rating, ////Toast.LENGTH_SHORT).show();
        ratingTotal = rating;
        mPager.setCurrentItem(1);
    }

    public void reviewAdded(String review) {
        ////Toast.makeText(ProductDetailActivity.this, "review product" + review, ////Toast.LENGTH_SHORT).show();
        rate_review_product_api(prdId, review, ratingTotal);
    }


    private void rate_review_product_api(int prdId, String review, int rating) {

        if (Constants.msisdn == null || Constants.msisdn == "") {
            Toast.makeText(ProductDetailActivity.this, "You are not logged in", Toast.LENGTH_SHORT).show();
        } else {
            if (hasRated == false) {
                RateProductRequest rateProductRequest = new RateProductRequest();
                rateProductRequest.setProductId((long) prdId);
                rateProductRequest.setReview(review);
                rateProductRequest.setStars((long) rating);
                rateProductRequest.setUsername(Constants.msisdn);

                baseApplication.showProgresDialogue("please wait", ProductDetailActivity.this);
                IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
                Observable<String> observable = iEndPoints.rateProductApi(rateProductRequest);
                observable.observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.newThread())
                        .subscribe(new Subscriber<String>() {
                            @Override
                            public void onCompleted() {
                                baseApplication.hideProgressDialogue();
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                e.printStackTrace();
                                hasRated = true;
                                baseApplication.hideProgressDialogue();
                                Toast.makeText(ProductDetailActivity.this, "Thankyou for feedback", Toast.LENGTH_SHORT).show();
                                Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                            }

                            @Override
                            public void onNext(String jsonObject) {
                                baseApplication.hideProgressDialogue();
                                hasRated = true;
                                Toast.makeText(ProductDetailActivity.this, "" + jsonObject, Toast.LENGTH_SHORT).show();
                                ////Toast.makeText(ProductDetailActivity.this, "" + jsonObject, ////Toast.LENGTH_SHORT).show();
                            }
                        });
            } else {
                //Toast.makeText(ProductDetailActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        public ScreenSlidePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                RatingSlider ratingSlider = new RatingSlider();
                Bundle bundle = new Bundle();
                bundle.putInt("prdId", prdId);
                ratingSlider.setArguments(bundle);
                return ratingSlider;
            } else {
                ReviewsSlider reviewsSlider = new ReviewsSlider();
                Bundle bundle = new Bundle();
                bundle.putInt("prdId", prdId);
                reviewsSlider.setArguments(bundle);
                return reviewsSlider;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
