package com.switchsolutions.agricultureapplication.farmtohome.farmconstants;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


import com.switchsolutions.agricultureapplication.farmtohome.products.fruits.FruitsList;
import com.switchsolutions.agricultureapplication.farmtohome.products.meat.MeatList;
import com.switchsolutions.agricultureapplication.farmtohome.products.vegetables.VegetablesList;

import java.util.List;

/**
 * Created by Muhammad Hassan on 4/24/2017.
 */

public class Constants {

    @Nullable
    public static String cart_id = null;
    @NonNull
    public static final String FARM_TO_HOME_LOGS = "farm_to_home_log";
    @Nullable
    public static String token;
    public static String msisdn;
    @NonNull
    public static String img_base_url = "http://peyzaar.com/pub/media/catalog/product/cache/small_image/240x300/beff4985b56e3afdbeabfc89641a4582";

    public static boolean isFromCart = false;
    public static String userName = "";
    public static String userEmail = "";

    public static List<FruitsList> fruitsList;
    public static List<VegetablesList> vegetablesList;
    public static List<MeatList> meatList;


    public static String getMonth(int month) {

        String monthStr = "";
        switch (month) {
            case 1: {
                monthStr = "Jan";
                break;
            }
            case 2: {
                monthStr = "Feb";
                break;
            }
            case 3: {
                monthStr = "Mar";
                break;
            }
            case 4: {
                monthStr = "April";
                break;
            }
            case 5: {
                monthStr = "May";
                break;
            }
            case 6: {
                monthStr = "June";
                break;
            }
            case 7: {
                monthStr = "July";
                break;
            }
            case 8: {
                monthStr = "Aug";
                break;
            }
            case 9: {
                monthStr = "Sep";
                break;
            }
            case 10: {
                monthStr = "Oct";
                break;
            }
            case 11: {
                monthStr = "Nov";
                break;
            }
            case 12: {
                monthStr = "Dec";
                break;
            }
        }
        return monthStr;
    }
}
