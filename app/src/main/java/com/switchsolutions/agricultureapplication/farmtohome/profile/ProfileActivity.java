package com.switchsolutions.agricultureapplication.farmtohome.profile;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.login.LoginFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.UpdateRequestApi;
import com.switchsolutions.agricultureapplication.farmtohome.model.UpdateResponse;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfileActivity extends AppCompatActivity {

    EditText et_name, et_email, et_number, et_city, et_company, et_address, et_password, et_conf_password;
    ImageView edit_button, save_button;
    LoginResponse response;
    String isLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Profile");


        et_name = (EditText) findViewById(R.id.et_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_number = (EditText) findViewById(R.id.et_number);
        et_city = (EditText) findViewById(R.id.et_city);
        et_company = (EditText) findViewById(R.id.et_company);
        et_address = (EditText) findViewById(R.id.et_address);
        et_password = (EditText) findViewById(R.id.et_password);
        et_conf_password = (EditText) findViewById(R.id.et_confirm_password);

        save_button = (ImageView) findViewById(R.id.save_button);
        edit_button = (ImageView) findViewById(R.id.edit_button);

        isLoggedIn = Sharedprefrencesutil.getSomeStringValue(ProfileActivity.this, "isLoggedIn");

        if (isLoggedIn != null && isLoggedIn != "" && isLoggedIn != "null") {
            if (isLoggedIn.equalsIgnoreCase("true")) {
                response = Sharedprefrencesutil.getLoginData(this, "loginData");
                if (response != null) {
                    String email = response.getEmail();
                    String add = response.getAddress();
                    String cit = response.getCity();
                    String mail = response.getEmail();
                    String mobile = response.getMobile();
                    String nam = response.getName();
                    String comp = response.getCompany();


                    et_name.setText(nam);
                    et_city.setText(cit);
                    et_email.setText(mail);
                    et_number.setText(mobile);
                    et_address.setText(add);
                    et_company.setText(comp);
                    et_password.setText(response.getPassword());
                }
            } else {

                Snackbar.make(et_address, "Not LoggedIn", BaseTransientBottomBar.LENGTH_INDEFINITE).setAction("Login Now", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ProfileActivity.this, LoginFarmActivity.class);
                        startActivity(intent);
                    }
                }).show();

            }
        } else {
            Snackbar.make(et_address, "Not LoggedIn", BaseTransientBottomBar.LENGTH_INDEFINITE).setAction("Login Now", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProfileActivity.this, LoginFarmActivity.class);
                    startActivity(intent);
                }
            }).show();
        }


        Disable();
        edit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Enable();
                save_button.setVisibility(View.VISIBLE);

            }
        });
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name1 = et_name.getText().toString();
                String email1 = et_email.getText().toString();
                String phonnum = et_number.getText().toString();
                String city1 = et_city.getText().toString();
                String passw = et_password.getText().toString();
                String conf_passw = et_conf_password.getText().toString();
                String addresss = et_address.getText().toString().trim();
                String company = et_company.getText().toString();
                if (name1.isEmpty() || email1.isEmpty() || phonnum.isEmpty() || city1.isEmpty() || passw.isEmpty() || addresss.isEmpty() || company.isEmpty()) {
                    Toast.makeText(ProfileActivity.this, "Please enter all values", Toast.LENGTH_LONG).show();
                    Toast.makeText(ProfileActivity.this, "", Toast.LENGTH_SHORT).show();
                } else if (!passw.equals(conf_passw)) {
                    Toast.makeText(ProfileActivity.this, "Password does not match. Please enter correct password", Toast.LENGTH_SHORT).show();
                } else if (email1.equals("") || !email1.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
                    Toast.makeText(getApplicationContext(), "Enter valid email ", Toast.LENGTH_SHORT).show();
                } else {
                    signupProcess(name1, addresss, email1, phonnum, city1, passw, company);
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isLoggedIn == "" || isLoggedIn == "null" || isLoggedIn == "false" || response == null) {
            Snackbar.make(et_address, "Not LoggedIn", BaseTransientBottomBar.LENGTH_INDEFINITE).setAction("Login Now", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProfileActivity.this, LoginFarmActivity.class);
                    startActivity(intent);
                }
            }).show();
        }
    }

    private void Disable() {
        et_name.setEnabled(false);
        et_name.getBackground().clearColorFilter();
        et_number.setEnabled(false);
        et_number.getBackground().clearColorFilter();
        et_email.setEnabled(false);
        et_email.getBackground().clearColorFilter();
        et_password.setEnabled(false);
        et_password.getBackground().clearColorFilter();
        et_city.setEnabled(false);
        et_city.getBackground().clearColorFilter();
        et_company.setEnabled(false);
        et_company.getBackground().clearColorFilter();
        et_address.setEnabled(false);
        et_address.getBackground().clearColorFilter();
        et_conf_password.setEnabled(false);
        et_conf_password.getBackground().clearColorFilter();
        et_conf_password.setVisibility(View.INVISIBLE);
        save_button.setVisibility(View.INVISIBLE);

    }

    private void Enable() {
        et_name.setEnabled(true);
        et_name.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        //et_number.setEnabled(true);
        // et_number.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_email.setEnabled(true);
        et_email.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_password.setEnabled(true);
        et_password.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_conf_password.setEnabled(true);
        et_conf_password.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_conf_password.setVisibility(View.VISIBLE);
        et_city.setEnabled(true);
        et_city.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_company.setEnabled(true);
        et_company.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        et_address.setEnabled(true);
        et_address.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
        save_button.setVisibility(View.VISIBLE);


    }

    public void signupProcess(String name1, final String adresss, final String email1, final String phonnum, final String city1, final String passw, String company1) {
        UpdateRequestApi updateRequestApi = new UpdateRequestApi();

       /* String baseUrl = "http://farm2home.peyzaar.com";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();*/

        updateRequestApi.setName(name1);
        updateRequestApi.setMobile(phonnum);
        updateRequestApi.setCity(city1);
        updateRequestApi.setCompany(company1);
        updateRequestApi.setEmail(email1);
        updateRequestApi.setPassword(passw);
        updateRequestApi.setAddress(adresss);


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);

        Observable<UpdateResponse> jsonObjectCall = iEndPoints.profileUpdate(updateRequestApi).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());
        jsonObjectCall.subscribe(new Subscriber<UpdateResponse>() {
            @Override
            public void onCompleted() {
                String s = "jkljaljg";
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(@NonNull UpdateResponse jsonObject) {
                long status = jsonObject.getStatus();
                if (status == 0) {
                    Toast.makeText(ProfileActivity.this, "Your information has been updated", Toast.LENGTH_SHORT).show();

                    response.setAddress(adresss);
                    response.setMobile(phonnum);
                    response.setCity(city1);
                    response.setCompany("switch solutions");
                    response.setPassword(passw);
                    response.setEmail(email1);
                    Sharedprefrencesutil.setLoginData(ProfileActivity.this, response, "loginData");

                } else {
                    Toast.makeText(ProfileActivity.this, "Information not updated", Toast.LENGTH_SHORT).show();
                }
                // finish();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
