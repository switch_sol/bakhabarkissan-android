package com.switchsolutions.agricultureapplication.farmtohome.order_history;

import android.os.Bundle;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.OrderHistory;
import com.switchsolutions.agricultureapplication.farmtohome.model.OrderHistoryRequest;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;


import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Muhammad Hassan on 4/5/2017.
 */

public class OrderHistoryFragment extends Fragment {

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    ItemOrderHistoryAdapter itemProduucts;
    boolean isVisible;
    boolean isCreated;
    BaseApplication baseApplication;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isCreated = true;
        if (isVisible && isCreated) {
            getOrdersHistory();
        }
    }

    @NonNull
    public static OrderHistoryFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt("", page);
        OrderHistoryFragment fragment = new OrderHistoryFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // called here
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible)
            isVisible = visible;
        if (visible && isCreated) {
            getOrdersHistory();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        //  getOrdersHistory();

        init_long_press();
        return view;
    }

    private void init_long_press() {
    }

    private void getOrdersHistory() {

        baseApplication = (BaseApplication) getActivity().getApplication();

        String isLogin = Sharedprefrencesutil.getSomeStringValue(getActivity(), "isLoggedIn");
        String mobile = "";
        if (isLogin == null || isLogin == "" || isLogin == "null") {

        } else {
            LoginResponse loginResponse = Sharedprefrencesutil.getLoginData(getActivity(), "loginData");
            if (loginResponse != null) {
                mobile = loginResponse.getMobile();
                Constants.msisdn = mobile;
            }
        }

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        OrderHistoryRequest orderHistoryRequest = new OrderHistoryRequest();
        orderHistoryRequest.setMobile("" + mobile);

        baseApplication.showProgresDialogue("getting order history", getActivity());

        Observable<List<OrderHistory>> observable = iEndPoints.getOrderHistory(orderHistoryRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<List<OrderHistory>>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "order history error");
                    }

                    @Override
                    public void onNext(List<OrderHistory> orderHistories) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "order history onnext");
                        itemProduucts = new ItemOrderHistoryAdapter(getActivity(), orderHistories);
                        mRecyclerView.setAdapter(itemProduucts);
                    }
                });
    }
}
