package com.switchsolutions.agricultureapplication.farmtohome.factory;

/**
 * Created by Muhammad Hassan on 4/25/2017.
 */

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

public class SynchronousCallAdapterFactory extends CallAdapter.Factory {
    @NonNull
    public static CallAdapter.Factory create() {
        return new SynchronousCallAdapterFactory();
    }

    @Nullable
    @Override
    public CallAdapter<Object> get(@NonNull final Type returnType, Annotation[] annotations, Retrofit retrofit) {
        // if returnType is retrofit2.Call, do nothing
        if (returnType.getClass().getName().contains("retrofit2.Call")) {
            return null;
        }

        return new CallAdapter<Object>() {
            @NonNull
            @Override
            public Type responseType() {
                return returnType;
            }

            @Override
            public <R> Object adapt(@NonNull Call<R> call) {
                try {
                    return call.execute().body();
                } catch (IOException e) {
                    throw new RuntimeException(); // do something better
                }
            }
        };
    }
}