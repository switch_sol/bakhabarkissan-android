package com.switchsolutions.agricultureapplication.farmtohome.signup;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.VerifyPinRequest;
import com.switchsolutions.agricultureapplication.farmtohome.util.ReceiveSms;
import com.switchsolutions.agricultureapplication.farmtohome.util.SmsListener;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Arshad on 5/23/2017.
 */

public class VerifyPin extends AppCompatActivity {

    EditText et1, et2, et3, et4;
    Button btnVerify;
    TextView tvResendCode;
    BroadcastReceiver smsReceiver;
    static String messag2;
    String userMobile = "";
    char[] strings;
    String pin = "";
    BaseApplication baseApplication;
    String userName = "";
    String userEmail = "";
    String userCity = "";
    String userStreet = "";
    String userPassword = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activityverifypin);
        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
        btnVerify = (Button) findViewById(R.id.btnVerify);
        tvResendCode = (TextView) findViewById(R.id.tv_resendcode);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("mobile")) {
                userMobile = bundle.getString("mobile", "");
                userName = bundle.getString("name", "");
                userEmail = bundle.getString("email", "");
                userCity = bundle.getString("city", "");
                userPassword = bundle.getString("password", "");
                userStreet = bundle.getString("street", "");
            }
        }


        baseApplication = (BaseApplication) getApplication();

        //registerSMSReceiver();
        ReceiveSms.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.d("Text", messageText);
                if (messageText != "" && messageText != null && messageText != "null")
                    if (messageText.length() == 4) {
                        strings = messageText.toCharArray();
                        char ch1 = strings[0];
                        char ch2 = strings[1];
                        char ch3 = strings[2];
                        char ch4 = strings[3];

                        et1.setText("" + ch1);
                        et2.setText("" + ch2);
                        et3.setText("" + ch3);
                        et4.setText("" + ch4);
                        pin = "" + ch1 + "" + ch2 + "" + ch3 + "" + ch4;
                        pinVerifyApiCall(userMobile, pin);
                    }
            }
        });


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et1.getText().toString() != "" && et2.getText().toString() != "" && et3.getText().toString() != "" && et4.getText().toString() != "") {
                    String etNumber1 = et1.getText().toString();
                    String etNumber2 = et2.getText().toString();
                    String etNumber3 = et3.getText().toString();
                    String etNumber4 = et4.getText().toString();
                    String textPin_code = etNumber1 + "" + etNumber2 + "" + etNumber3 + "" + etNumber4;
                    pinVerifyApiCall(userMobile, textPin_code);
                } else {
                    Toast.makeText(VerifyPin.this, "Enter All Fields Correctly", Toast.LENGTH_LONG).show();
                }
            }
        });

        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignUpActivity signUpActivity = new SignUpActivity();
                Log.d(Constants.FARM_TO_HOME_LOGS, "username" + userName);
                signupProcess(userName, userMobile, userStreet, userCity, userEmail, userPassword);

            }
        });

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (et1.getText().toString().length() == 0) {
                    et2.requestFocus();

                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }


    private void pinVerifyApiCall(final String mobile, String code) {

        VerifyPinRequest verifyPinRequest = new VerifyPinRequest();
        verifyPinRequest.setMobile(mobile);
        verifyPinRequest.setCode(code);
        baseApplication.showProgresDialogue("Verifying Pin", VerifyPin.this);

        IEndPoints iCreateCart = ApiClient.getClient().create(IEndPoints.class);
        Observable<VerifyApiPojo> jsonObjectCall = iCreateCart.getVeryPin(verifyPinRequest).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        jsonObjectCall.subscribe(new Subscriber<VerifyApiPojo>()

        {
            @Override
            public void onCompleted() {
                baseApplication.hideProgressDialogue();
            }

            @Override
            public void onError(Throwable e) {
//                e.printStackTrace();
//                Snackbar.make(, "Registration failed", Snackbar.LENGTH_SHORT).show();
                baseApplication.hideProgressDialogue();
                Log.d(Constants.FARM_TO_HOME_LOGS, "signup error");

            }

            @Override
            public void onNext(VerifyApiPojo s) {
                Log.d(Constants.FARM_TO_HOME_LOGS, "signup onnext");
                if (s != null) {
                    if (s.getStatus().intValue() == 1) {
                        Toast.makeText(VerifyPin.this, "Invalid pin code", Toast.LENGTH_SHORT).show();
                    } else if (s.getStatus().intValue() == 0) {
                        Toast.makeText(VerifyPin.this, "Registration Completed", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(VerifyPin.this, HomeFarmActivity.class);
                        intent.putExtra("mobile", mobile);
                        intent.putExtra("password", userPassword);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        Constants.userName = userName;
                        Constants.userEmail = userEmail;
                        finish();
                    }
                }
            }
        });
    }

    public void signupProcess(String name1, String phonnum, String street, String city1, String email1, String passw) {
        SignUpApi signUpApi = new SignUpApi();
        Customer customer = new Customer();
        customer.setEmail(email1);
        customer.setName(name1);
        customer.setMobile(phonnum);
        customer.setPassword(passw);
        customer.setAddress(street);
        customer.setCity(city1);
        customer.setCompany("swith_solutions");
        signUpApi.setCustomer(customer);

        IEndPoints iCreateCart = ApiClient.getClient().create(IEndPoints.class);
        Observable<JsonObject> jsonObjectCall = iCreateCart.getResendVerify(signUpApi).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());

        baseApplication.showProgresDialogue("Resending Pin Code", VerifyPin.this);

        jsonObjectCall.subscribe(new Subscriber<JsonObject>() {
            @Override
            public void onCompleted() {
                baseApplication.hideProgressDialogue();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                // Snackbar.make(city, "Registration failed", Snackbar.LENGTH_SHORT).show();

                Log.d(Constants.FARM_TO_HOME_LOGS, "signup onerror");
                baseApplication.hideProgressDialogue();
                Toast.makeText(VerifyPin.this, "Resend pin failedError", Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onNext(JsonObject s) {
                Log.d(Constants.FARM_TO_HOME_LOGS, "signup onnext" + s);
                baseApplication.hideProgressDialogue();

                Toast.makeText(VerifyPin.this, " Code Resent", Toast.LENGTH_SHORT).show();
                //finish();
            }
        });
    }
}
