package com.switchsolutions.agricultureapplication.farmtohome.home;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.switchsolutions.agricultureapplication.R;

import java.util.ArrayList;

/**
 * Created by Arshad on 4/21/2017.
 */

public class ScreenSliderAdapter extends PagerAdapter {
    Context context;
    ArrayList imagesArray;
    LayoutInflater inflater;


    ScreenSliderAdapter(Context context, ArrayList<Integer> imagesArray) {
        this.context = context;
        this.imagesArray = imagesArray;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.screen_slide_homeimages, container, false);
        ImageView image = (ImageView) view.findViewById(R.id.custom_image_for_slide);

        if (position == 0)
            image.setImageResource(R.drawable.banner);
        else if (position == 1)
            image.setImageResource(R.drawable.banner2);
        else if (position == 2)
            image.setImageResource(R.drawable.banner3);

        container.addView(view, 0);
        return view;
    }

    @Override
    public int getCount() {
        return imagesArray.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, Object object) {
        return view.equals(object);
    }
}
