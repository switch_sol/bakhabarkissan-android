package com.switchsolutions.agricultureapplication.farmtohome.api;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Muhammad Hassan on 4/24/2017.
 */

public class ApiClient {


    @NonNull
    public static final String mBaseUrl = "http://farmtohome.bakhaberkissan.com/";
    //    public static String mBaseUrl = "http://farm2home.peyzaar.com/";
//    public static String mBaseUrl = "http://peyzaar.com/index.php/rest/V1/";
    @Nullable
    private static Retrofit retrofit = null;
    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    @Nullable
    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(mBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
