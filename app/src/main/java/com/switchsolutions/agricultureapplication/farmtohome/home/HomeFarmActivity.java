/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.switchsolutions.agricultureapplication.farmtohome.home;

import android.content.Intent;
import android.os.Bundle;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.interface_callbacks.IOnItemClickHome;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductsList;
import com.switchsolutions.agricultureapplication.farmtohome.product_detail.ProductDetailActivity;
import com.switchsolutions.agricultureapplication.farmtohome.products.fruits.FruitsList;
import com.switchsolutions.agricultureapplication.farmtohome.products.meat.MeatList;
import com.switchsolutions.agricultureapplication.farmtohome.products.vegetables.VegetablesList;
import com.switchsolutions.agricultureapplication.farmtohome.profile.ProfileActivity;
import com.switchsolutions.agricultureapplication.farmtohome.service_areas.ServiceAreasActivity;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;


import java.util.ArrayList;
import java.util.List;


import me.relex.circleindicator.CircleIndicator;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * TODO
 */
public class HomeFarmActivity extends AppCompatActivity implements IOnItemClickHome {
    private DrawerLayout mDrawerLayout;

    private ViewPager mPager;
    @NonNull
    private int[] images = {R.drawable.cheese_1, R.drawable.cheese_1, R.drawable.cheese_1};
    @NonNull
    private ArrayList<Integer> ImagesArray = new ArrayList<>();
    TextView textViewName;
    TextView textViewEail;
    @NonNull
    String token = "";
    @Nullable
    List<String> productsName = null;
    @Nullable
    List<String> productsPrice = null;
    RecyclerView rv;
    IOnItemClickHome iOnItemClickHome;
    private SimpleCursorAdapter myAdapter;
    @Nullable
    MaterialSearchView searchView = null;
    MenuItem searchItem;
    String prdId = "";
    BaseApplication baseApplication;

    @Override
    public void onBackPressed() {

        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private View getParentLayoutRootView() {

        return HomeFarmActivity.this.getWindow().getDecorView().getRootView();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (baseApplication == null)
            baseApplication = (BaseApplication) getApplication();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Farm to Home");
        iOnItemClickHome = this;

        for (int i = 0; i < images.length; i++)
            ImagesArray.add(images[i]);

        mPager = (ViewPager) findViewById(R.id.viewPager);
        mPager.setAdapter(new ScreenSliderAdapter(HomeFarmActivity.this, ImagesArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        rv = (RecyclerView) findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HomeFarmActivity.this);
        rv.setLayoutManager(linearLayoutManager);
        rv.setNestedScrollingEnabled(false);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout != null)
            setup_tablayout(tabLayout);

        productsName = new ArrayList<>();
        productsPrice = new ArrayList<>();

        Snackbar.make(getParentLayoutRootView(), "Only in Islamabad/Rawalpindi", Snackbar.LENGTH_LONG).setAction("Locations", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeFarmActivity.this, ServiceAreasActivity.class);
                startActivity(intent);
            }
        }).show();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("mobile") && bundle.containsKey("password")) {

                String isLoggedIn = Sharedprefrencesutil.getSomeStringValue(this, "isLoggedIn");
                if (isLoggedIn != "" && isLoggedIn != "null" && isLoggedIn != "false") {
                    LoginResponse loginResponse = Sharedprefrencesutil.getLoginData(this, "loginData");
                    if (loginResponse != null) {
                        Constants.msisdn = loginResponse.getMobile();
                        Constants.userName = loginResponse.getName();
                        Constants.userEmail = loginResponse.getEmail();
                    }
                }
            }
        }
        String token = Sharedprefrencesutil.getSomeStringValue(HomeFarmActivity.this, "token");
        if (token == null || token == "" || token == "null")
            getToken();
        else {
            Constants.token = token;
            getProductsApi();
        }
    }

    private void getToken() {
        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        Observable<String> observable = iEndPoints.getToken();
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        getProductsApi();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "token error");
                        Sharedprefrencesutil.setSomeStringValue(HomeFarmActivity.this, "token", "");
                    }

                    @Override
                    public void onNext(String s) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "token onnext");
                        Constants.token = s;
                        Sharedprefrencesutil.setSomeStringValue(HomeFarmActivity.this, "token", s);
                    }
                });
    }

    public void Login_process(final String mobile, String password) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setMobile(mobile);
        loginRequest.setPassword(password);

/*
        Observable<JsonArray> loginResponseObservable
                = iEndPoints.loginApi(loginRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());


        loginResponseObservable.subscribe(new Subscriber<JsonArray>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                Log.d(Constants.FARM_TO_HOME_LOGS, "login error");
            }

            @Override
            public void onNext(JsonArray loginResponse) {

                Log.d(Constants.FARM_TO_HOME_LOGS, "login onnext");
                JsonElement jsonElement = loginResponse.get(0);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                LoginResponse loginResponse1 = new Gson().fromJson(jsonObject, LoginResponse.class);
                Sharedprefrencesutil.setLoginData(HomeFarmActivity.this, loginResponse1, "loginData");
                Sharedprefrencesutil.setSomeStringValue(HomeFarmActivity.this, "isLoggedIn", "true");
                Constants.msisdn = mobile;
                Constants.userName = loginResponse1.getName();
                Constants.userEmail = loginResponse1.getEmail();
                textViewEail.setText("" + Constants.userEmail);
                textViewName.setText("" + Constants.userName);
            }
        });*/
    }


    private void getProductsApi() {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        Observable<List<ProductsList>> productsListObservable = iEndPoints.getAllProducts();
        baseApplication.showProgresDialogue("getting products", HomeFarmActivity.this);

        //(ApplicationClass)getApplication().showProgresDialogue("getting products", HomeActivity.this);

        productsListObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ProductsList>>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "error get product api");

                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onNext(@Nullable List<ProductsList> productsLists) {
                        try {
                            if (productsLists != null) {
                                int cat_count = 0;

                                List<FruitsList> fruitsList = new ArrayList<FruitsList>();
                                List<VegetablesList> vegetablesList = new ArrayList<VegetablesList>();
                                List<MeatList> meatList = new ArrayList<MeatList>();
                                for (int i = 0; i < productsLists.size(); i++) {
                                    if (productsLists.get(i).getCatId().equalsIgnoreCase("1")) {
                                        VegetablesList vegetablesList1 = new VegetablesList();
                                        vegetablesList1.setCatId(productsLists.get(i).getCatId());
                                        vegetablesList1.setDiscount(productsLists.get(i).getDiscount());
                                        vegetablesList1.setEnDescription(productsLists.get(i).getEnDescription());
                                        vegetablesList1.setEnName(productsLists.get(i).getEnName());
                                        vegetablesList1.setGrade(productsLists.get(i).getGrade());
                                        vegetablesList1.setInStock(productsLists.get(i).getInStock());
                                        vegetablesList1.setPrdId(productsLists.get(i).getPrdId());
                                        vegetablesList1.setPrdImage(productsLists.get(i).getPrdImage());
                                        vegetablesList1.setPrice(productsLists.get(i).getPrice());
                                        vegetablesList1.setDiscount(productsLists.get(i).getDiscount());
                                        vegetablesList1.setQuantity(productsLists.get(i).getQuantity());
                                        vegetablesList1.setSku(productsLists.get(i).getSku());
                                        vegetablesList1.setUnitId(productsLists.get(i).getUnitId());
                                        vegetablesList1.setStatus(productsLists.get(i).getStatus());
                                        vegetablesList1.setUrName(productsLists.get(i).getUrName());
                                        vegetablesList1.setUrDescription(productsLists.get(i).getUrDescription());
                                        vegetablesList1.setmEngUnit(productsLists.get(i).getmEngUnit());


                                        vegetablesList.add(vegetablesList1);
                                    } else if (productsLists.get(i).getCatId().equalsIgnoreCase("2")) {
                                        FruitsList fruitsList1 = new FruitsList();
                                        fruitsList1.setCatId(productsLists.get(i).getCatId());
                                        fruitsList1.setDiscount(productsLists.get(i).getDiscount());
                                        fruitsList1.setEnDescription(productsLists.get(i).getEnDescription());
                                        fruitsList1.setEnName(productsLists.get(i).getEnName());
                                        fruitsList1.setGrade(productsLists.get(i).getGrade());
                                        fruitsList1.setInStock(productsLists.get(i).getInStock());
                                        fruitsList1.setPrdId(productsLists.get(i).getPrdId());
                                        fruitsList1.setPrdImage(productsLists.get(i).getPrdImage());
                                        fruitsList1.setPrice(productsLists.get(i).getPrice());
                                        fruitsList1.setDiscount(productsLists.get(i).getDiscount());
                                        fruitsList1.setQuantity(productsLists.get(i).getQuantity());
                                        fruitsList1.setSku(productsLists.get(i).getSku());
                                        fruitsList1.setUnitId(productsLists.get(i).getUnitId());
                                        fruitsList1.setStatus(productsLists.get(i).getStatus());
                                        fruitsList1.setUrName(productsLists.get(i).getUrName());
                                        fruitsList1.setUrDescription(productsLists.get(i).getUrDescription());
                                        fruitsList1.setmEngUnit(productsLists.get(i).getmEngUnit());

                                        fruitsList.add(fruitsList1);

                                    } else if (productsLists.get(i).getCatId().equalsIgnoreCase("3")) {

                                        MeatList meatList1 = new MeatList();
                                        meatList1.setCatId(productsLists.get(i).getCatId());
                                        meatList1.setDiscount(productsLists.get(i).getDiscount());
                                        meatList1.setEnDescription(productsLists.get(i).getEnDescription());
                                        meatList1.setEnName(productsLists.get(i).getEnName());
                                        meatList1.setGrade(productsLists.get(i).getGrade());
                                        meatList1.setInStock(productsLists.get(i).getInStock());
                                        meatList1.setPrdId(productsLists.get(i).getPrdId());
                                        meatList1.setPrdImage(productsLists.get(i).getPrdImage());
                                        meatList1.setPrice(productsLists.get(i).getPrice());
                                        meatList1.setDiscount(productsLists.get(i).getDiscount());
                                        meatList1.setQuantity(productsLists.get(i).getQuantity());
                                        meatList1.setSku(productsLists.get(i).getSku());
                                        meatList1.setUnitId(productsLists.get(i).getUnitId());
                                        meatList1.setStatus(productsLists.get(i).getStatus());
                                        meatList1.setUrName(productsLists.get(i).getUrName());
                                        meatList1.setUrDescription(productsLists.get(i).getUrDescription());
                                        meatList1.setmEngUnit(productsLists.get(i).getmEngUnit());

                                        meatList.add(meatList1);
                                    }
                                }
                                if (fruitsList.size() > 0 && vegetablesList.size() > 0 && meatList.size() > 0)
                                    cat_count = 3;
                                else if (fruitsList.size() > 0 && vegetablesList.size() > 0 && meatList.size() == 0)
                                    cat_count = 2;
                                else if (fruitsList.size() > 0 && meatList.size() > 0)
                                    cat_count = 1;

                                Constants.fruitsList = fruitsList;
                                Constants.vegetablesList = vegetablesList;
                                Constants.meatList = meatList;

                                add_search_suggestions();

                                CommonRecycleAdapter commonRecycleAdapter = new CommonRecycleAdapter(cat_count, HomeFarmActivity.this, iOnItemClickHome, fruitsList, vegetablesList, meatList);
                                rv.setAdapter(commonRecycleAdapter);

                                String isLogin = Sharedprefrencesutil.getSomeStringValue(HomeFarmActivity.this, "isLoggedIn");
                                String mobile = "";
                                if (isLogin == null || isLogin == "" || isLogin == "null") {

                                } else {
                                    LoginResponse loginResponse = Sharedprefrencesutil.getLoginData(HomeFarmActivity.this, "loginData");
                                    if (loginResponse != null)
                                        mobile = loginResponse.getMobile();
                                    textViewName.setText("" + loginResponse.getName());
                                    textViewEail.setText("" + loginResponse.getEmail());
                                    Constants.userEmail = loginResponse.getEmail();
                                    Constants.userName = loginResponse.getName();

                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                if (mDrawerLayout != null) {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                    break;
                }
                return true;

            }
            case R.id.action_favorite: {
                startActivity(new Intent(HomeFarmActivity.this, ProfileActivity.class));
                break;
            }
            default: {
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void setup_tablayout(@NonNull TabLayout tabLayout) {

        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(HomeFarmActivity.this, R.color.colorTrans));
        tabLayout.addTab(tabLayout.newTab().setText("All Categories").setIcon(R.drawable.categories).setTag("category"));
        tabLayout.addTab(tabLayout.newTab().setText("All Deals").setIcon(R.drawable.categories).setTag("deals"));
        tabLayout.addTab(tabLayout.newTab().setText("My Cart").setIcon(R.drawable.my_cart).setTag("cart"));
        tabLayout.addTab(tabLayout.newTab().setText("History").setIcon(R.drawable.history).setTag("history"));
        tabLayout.addTab(tabLayout.newTab().setText("Fruits").setIcon(R.drawable.fruite).setTag("fruit"));
        tabLayout.addTab(tabLayout.newTab().setText("Vegetable").setIcon(R.drawable.vegetables).setTag("vegetable"));
        // tabLayout.addTab(tabLayout.newTab().setText("Meat").setIcon(R.drawable.meat).setTag("meat"));

        LinearLayout ll = (LinearLayout) tabLayout.getChildAt(0);
        for (int i = 0; i < ll.getChildCount(); i++) {
            LinearLayout tabView = (LinearLayout) ll.getChildAt(i);
            for (int j = 0; j < tabView.getChildCount(); j++) {
                if (tabView.getChildAt(j) instanceof TextView) {
                    TextView textView = (TextView) tabView.getChildAt(j);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) textView.getLayoutParams();
                    layoutParams.topMargin = 2;
                    textView.setLayoutParams(layoutParams);
                } else if (tabView.getChildAt(j) instanceof ImageView) {
                    ImageView imageView = (ImageView) tabView.getChildAt(j);
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageView.getLayoutParams();
                    layoutParams.topMargin = 5;
                    layoutParams.width = 60;
                    layoutParams.height = 60;
                    imageView.setLayoutParams(layoutParams);
                }
            }
        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(@NonNull TabLayout.Tab tab) {
                if (tab.getTag().equals("category")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 0));
                } else if (tab.getTag().equals("deals")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 1));
                } else if (tab.getTag().equals("cart")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 2));
                } else if (tab.getTag().equals("history")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 3));
                } else if (tab.getTag().equals("fruit")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 4));
                } else if (tab.getTag().equals("vegetable")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
                } else if (tab.getTag().equals("meat")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 6));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(@NonNull TabLayout.Tab tab) {
                if (tab.getTag().equals("category")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 0));
                } else if (tab.getTag().equals("cart")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 1));
                } else if (tab.getTag().equals("history")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 2));
                } else if (tab.getTag().equals("fruit")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 3));
                } else if (tab.getTag().equals("vegetable")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 4));
                } else if (tab.getTag().equals("meat")) {
                    startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {

        getMenuInflater().inflate(R.menu.menu_home_farmtohome, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setMenuItem(searchItem);

        searchView.setSubmitOnClick(true);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic

                for (int i = 0; i < Constants.meatList.size(); i++) {

                    if (Constants.meatList.get(i).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.meatList.get(i).getPrdId();
                    }
                }
                for (int k = 0; k < Constants.fruitsList.size(); k++) {

                    if (Constants.fruitsList.get(k).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.fruitsList.get(k).getPrdId();
                    }
                }
                for (int j = 0; j < Constants.vegetablesList.size(); j++) {
                    if (Constants.vegetablesList.get(j).getEnName().equalsIgnoreCase(query)) {
                        prdId = Constants.vegetablesList.get(j).getPrdId();
                    }
                }
                if (prdId != "")
                    startActivity(new Intent(HomeFarmActivity.this, ProductDetailActivity.class).putExtra("prdId", prdId));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
                //Do some magic
            }

            @Override
            public void onSearchViewClosed() {
                //Do some magic
            }
        });
        searchView.setCursorDrawable(R.drawable.custom_cursor);
        return true;
    }

    private void add_search_suggestions() {
        List<String> suggestionsList = new ArrayList<>();
        for (int i = 0; i < Constants.fruitsList.size(); i++) {
            suggestionsList.add(Constants.fruitsList.get(i).getEnName());
        }

        for (int i = 0; i < Constants.vegetablesList.size(); i++) {
            suggestionsList.add(Constants.vegetablesList.get(i).getEnName());
        }

        for (int i = 0; i < Constants.meatList.size(); i++) {
            suggestionsList.add(Constants.meatList.get(i).getEnName());
        }


        String[] sugg = suggestionsList.toArray(new String[0]);
        searchView.setSuggestions(sugg);

    }


    private void setupDrawerContent(@NonNull final NavigationView navigationView) {
        navigationView.setItemIconTintList(null);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header);
        textViewName = (TextView) headerView.findViewById(R.id.name);
        textViewName.setText("" + Constants.userName);

        textViewEail = (TextView) headerView.findViewById(R.id.email);
        textViewEail.setText("" + Constants.userEmail);


        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        navigationView.setCheckedItem(menuItem.getItemId());
                        //menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        switch (menuItem.getItemId()) {
                            case R.id.nav_home: {
                                // startActivity(new Intent(HomeActivity.this, CartFragment.class));
                                break;
                            }
                            case R.id.bkk_id2: {
                                finish();
                                // startActivity(new Intent(HomeActivity.this, CartFragment.class));
                                break;
                            }
                            case R.id.nav_deals: {
                                startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 1));
                                break;
                            }
                            case R.id.nav_veggies: {
                                startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
                                break;
                            }
                            case R.id.nav_fruits: {
                                startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 4));
                                break;
                            }
                            case R.id.logout: {
                                LoginResponse loginResponse = new LoginResponse();
                                Sharedprefrencesutil.setLoginData(HomeFarmActivity.this, loginResponse, "loginData");
                                Sharedprefrencesutil.setSomeStringValue(HomeFarmActivity.this, "isLoggedIn", "");
                                Constants.userEmail = "";
                                Constants.msisdn = "";
                                Constants.userName = "";

                                Toast.makeText(HomeFarmActivity.this, "Logged out successfully", Toast.LENGTH_SHORT).show();
                                finish();

                                break;
                            }

                            /*case R.id.nav_meat_chicken: {
                                startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
                                break;
                            }*/
                            case R.id.my_orders: {
                                startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 3));
                                break;
                            }


                            case R.id.profile: {
                                startActivity(new Intent(HomeFarmActivity.this, ProfileActivity.class));
                                break;
                            }

                            default: {

                                break;
                            }
                        }
                        return true;
                    }
                });
    }

    @Override
    public void onItemClick(int position) {

        if (position == 0) {
            startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 3));
        } else if (position == 1) {
            startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 4));
        } else if (position == 2) {
            startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
        } else if (position == 3) {
            startActivity(new Intent(HomeFarmActivity.this, MainFarmActivity.class).putExtra("page_num", 5));
        }
    }

    @Override
    public void onItemClick(String itemName) {

    }
}
