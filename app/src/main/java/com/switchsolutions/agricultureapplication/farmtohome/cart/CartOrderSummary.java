package com.switchsolutions.agricultureapplication.farmtohome.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonPrimitive;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.MyCartResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.PlaceOrder;
import com.switchsolutions.agricultureapplication.farmtohome.service_areas.ServiceAreasActivity;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Muhammad Hassan on 6/16/2017.
 */

public class CartOrderSummary extends AppCompatActivity {
    BaseApplication baseApplication;
    String order_total_price = "";
    LoginResponse loginResponse;
    private TextView delivery_address, total_price;
    private Button btn_confirm_order;
    String shippingaddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        baseApplication = (BaseApplication) this.getApplication();
        delivery_address = (TextView) findViewById(R.id.txtDeliveryAddress);
        total_price = (TextView) findViewById(R.id.txtTotalPrice);
        delivery_address = (TextView) findViewById(R.id.txtDeliveryAddress);
        btn_confirm_order = (Button) findViewById(R.id.btnConfirmOrder);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            order_total_price = bundle.getString("totalprice");
            shippingaddress = bundle.getString("shippingAddress");
            total_price.setText("Rs. " + order_total_price);
            delivery_address.setText(shippingaddress);
        }

        Snackbar.make(delivery_address, "Only in Islamabad/Rawalpindi", Snackbar.LENGTH_LONG).setAction("Locations", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CartOrderSummary.this, ServiceAreasActivity.class);
                startActivity(intent);
            }
        }).show();

        btn_confirm_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeOrder();
//                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        order_total_price = "";
        finish();

    }

    private void placeOrder() {

        // baseApplication = (BaseApplication) getActivity().getApplication();

        if (order_total_price != "0") {
            Constants.isFromCart = false;

            baseApplication = (BaseApplication) getApplication();
            baseApplication.showProgresDialogue("placing order", CartOrderSummary.this);
            IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);

            PlaceOrder placeOrder = new PlaceOrder();
            placeOrder.setPaymentMethod("Not Specified");
            placeOrder.setShippingAddress("" + shippingaddress);
            placeOrder.setShippingPhone("" + Constants.msisdn);

            placeOrder.setmToken("" + Constants.token);
            Observable<JsonPrimitive> observable = iEndPoints.placeOrderApi(placeOrder);
            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<JsonPrimitive>() {
                        @Override
                        public void onCompleted() {

                            baseApplication.hideProgressDialogue();
                            Toast.makeText(baseApplication, "Order placed successfully", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(getApplicationContext(), HomeFarmActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            e.printStackTrace();
                            baseApplication.hideProgressDialogue();
                            Log.d(Constants.FARM_TO_HOME_LOGS, "error place order");
                            Toast.makeText(CartOrderSummary.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onNext(JsonPrimitive jsonObject) {
                            Log.d(Constants.FARM_TO_HOME_LOGS, "onnext place order");
                            Toast.makeText(CartOrderSummary.this, "" + jsonObject, Toast.LENGTH_SHORT).show();

                            List<MyCartResponse> myCartResponses = new ArrayList<MyCartResponse>();
                            //show_cart_items(myCartResponses);
                        }
                    });
        } else {
//            Snackbar.make(btnProceedToNext, "No item in cart", Snackbar.LENGTH_SHORT).show();
        }
    }
}

