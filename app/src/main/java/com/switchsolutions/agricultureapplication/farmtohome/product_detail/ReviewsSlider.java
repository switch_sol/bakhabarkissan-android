package com.switchsolutions.agricultureapplication.farmtohome.product_detail;

import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;


/**
 * Created by Muhammad Hassan on 5/18/2017.
 */

public class ReviewsSlider extends Fragment {

    TextView txt_submit_review;
    int prdId;
    EditText et_submit_review;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.reviews_slide_screen, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("prdId"))
                prdId = bundle.getInt("prdId", 0);
        }

        txt_submit_review = (TextView) rootView.findViewById(R.id.txt_submit_review);
        et_submit_review = (EditText) rootView.findViewById(R.id.et_submit_review);

        txt_submit_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_submit_review.getText() != null)
                    ((ProductDetailActivity) getActivity()).reviewAdded(et_submit_review.getText().toString());
                else
                    ((ProductDetailActivity) getActivity()).reviewAdded("");
            }
        });
        return rootView;
    }
}