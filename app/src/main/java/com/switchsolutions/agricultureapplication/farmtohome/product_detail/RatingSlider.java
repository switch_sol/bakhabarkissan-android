package com.switchsolutions.agricultureapplication.farmtohome.product_detail;

import android.os.Bundle;


import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;


/**
 * Created by Muhammad Hassan on 5/18/2017.
 */

public class RatingSlider extends Fragment {

    int prdId;
    TextView txt_submit_rating;
    RatingBar ratingBar;
    int ratingTotal;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.rating_slide_screen, container, false);
        txt_submit_rating = (TextView) rootView.findViewById(R.id.txt_submit_rating);
        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingbar_prd_rate);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                ratingTotal = (int) rating;
            }
        });

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("prdId"))
                prdId = bundle.getInt("prdId", 0);
        }

        txt_submit_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ProductDetailActivity) getActivity()).ratingSelected(ratingTotal);
            }
        });

        return rootView;
    }
}