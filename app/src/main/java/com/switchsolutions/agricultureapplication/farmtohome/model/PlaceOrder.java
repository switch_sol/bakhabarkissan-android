package com.switchsolutions.agricultureapplication.farmtohome.model;

/**
 * Created by Muhammad Hassan on 5/15/2017.
 */

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PlaceOrder {
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("shipping_address")
    private String mShippingAddress;

    @SerializedName("shipping_phone")
    private String mShippingPhone;


    @SerializedName("token")
    private String mToken;

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }


    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getShippingAddress() {
        return mShippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        mShippingAddress = shippingAddress;
    }

    public String getShippingPhone() {
        return mShippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        mShippingPhone = shippingPhone;
    }
}
