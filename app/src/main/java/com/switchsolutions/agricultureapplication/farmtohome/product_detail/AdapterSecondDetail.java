package com.switchsolutions.agricultureapplication.farmtohome.product_detail;

import android.content.Context;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;

import me.relex.circleindicator.CircleIndicator;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class AdapterSecondDetail extends RecyclerView.Adapter {

    Context mContext;
    BaseApplication baseApplication;

    public class ViewHolderImage extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ViewHolderImage(@NonNull View v) {
            super(v);
        }
    }

    public class ViewHolderText extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ViewPager mPager;
        public CircleIndicator indicator;
        public RelativeLayout rltv_item_prd_detail_addcart;
        public Button btn_item_prd_detail_addcart;

        public ViewHolderText(@NonNull View v) {
            super(v);

            mPager = (ViewPager) v.findViewById(R.id.viewPager);
            indicator = (CircleIndicator) v.findViewById(R.id.indicator);
            rltv_item_prd_detail_addcart = (RelativeLayout) v.findViewById(R.id.rltv_item_prd_detail_addcart);
            btn_item_prd_detail_addcart = (Button) v.findViewById(R.id.btn_item_prd_detail_addcart);
            btn_item_prd_detail_addcart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addItemtoCart(prdId, 1);
                }
            });

        }
    }

    public class ViewHolderReview extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public RecyclerView recyclerView;
        LinearLayoutManager linearLayoutManager;


        public ViewHolderReview(@NonNull View v) {
            super(v);
            recyclerView = (RecyclerView) v.findViewById(R.id.rv_product_reviews);
            linearLayoutManager = new LinearLayoutManager(mContext);
            recyclerView.setLayoutManager(linearLayoutManager);
        }
    }

    FragmentManager fragmentManager;

    // Provide a suitable constructor (depends on the kind of dataset)
    int prdId;

    public AdapterSecondDetail(Context context, FragmentManager fragmentManager, int prdId) {
        mContext = context;
        this.fragmentManager = fragmentManager;
        this.prdId = prdId;
        baseApplication = (BaseApplication) mContext.getApplicationContext();

    }

    // Create new views (invoked by the layout manager)
    @Nullable
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                      int viewType) {
        // create a new view
        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_product_img_detail, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolderImage vh = new ViewHolderImage(v);
            return vh;
        } else if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_product_txt_detail, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolderText vh = new ViewHolderText(v);
            return vh;
        } else if (viewType == 2) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_all_reviews_list, parent, false);
            // set the view's size, margins, paddings and layout parameters
            ViewHolderReview vh = new ViewHolderReview(v);
            return vh;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (position == 3) {
//            ((ViewHolderReview) holder).txt_all_reviews.setVisibility(View.VISIBLE);
        }
        if (position == 1) {
            ((ViewHolderText) holder).mPager.setAdapter(new ScreenSlidePagerAdapter(fragmentManager));
            ((ViewHolderText) holder).indicator.setViewPager(((ViewHolderText) holder).mPager);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else if (position == 1) {
            return 1;
        } else
            return 2;
    }

    private void addItemtoCart(int prdId, int action) {


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        if (action == 0)
            addToCartRequest.setAction("remove");
        else if (action == 1)
            addToCartRequest.setAction("add");

        addToCartRequest.setProductId(String.valueOf(prdId));
        addToCartRequest.setQuantity(""+1);
        addToCartRequest.setToken(Constants.token);
        baseApplication.showProgresDialogue("updating your cart", mContext);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Toast.makeText(mContext, "cart update error", Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Toast.makeText(mContext, "" + jsonObject, Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        baseApplication.hideProgressDialogue();
                    }
                });
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return 4;
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                RatingSlider ratingSlider = new RatingSlider();
                Bundle bundle = new Bundle();
                bundle.putInt("prdId", prdId);
                ratingSlider.setArguments(bundle);
                return ratingSlider;
            } else {
                ReviewsSlider reviewsSlider = new ReviewsSlider();
                Bundle bundle = new Bundle();
                bundle.putInt("prdId", prdId);
                reviewsSlider.setArguments(bundle);
                return reviewsSlider;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
