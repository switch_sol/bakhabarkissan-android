package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by Muhammad Hassan on 5/15/2017.
 */
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ForgotPasswordRequest {

    @SerializedName("mobile")
    private String mMobile;

    public String getmMobile() {
        return mMobile;
    }

    public void setmMobile(String mobile) {
        mMobile = mobile;
    }

}
