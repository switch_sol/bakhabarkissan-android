package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Muhammad Hassan on 5/15/2017.
 */


public class OrderHistory {

    @SerializedName("cat_id")
    private String mCatId;

    public String getmRating() {
        return mRating;
    }

    public void setmRating(String mRating) {
        this.mRating = mRating;
    }

    @SerializedName("rating")
    private String mRating;

    public String getmQuantOrdered() {
        return mQuantOrdered;
    }

    public void setmQuantOrdered(String mQuantOrdered) {
        this.mQuantOrdered = mQuantOrdered;
    }

    @SerializedName("quant")
    private String mQuantOrdered;


    @SerializedName("date_time")
    private String mDateTime;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("en_description")
    private String mEnDescription;
    @SerializedName("en_name")
    private String mEnName;
    @SerializedName("en_unit")
    private String mEnUnit;
    @SerializedName("grade")
    private String mGrade;
    @SerializedName("in_stock")
    private String mInStock;
    @SerializedName("op_id")
    private String mOpId;
    @SerializedName("order_id")
    private String mOrderId;
    @SerializedName("payment_method")
    private String mPaymentMethod;
    @SerializedName("prd_id")
    private String mPrdId;
    @SerializedName("prd_image")
    private String mPrdImage;
    @SerializedName("prduct_id")
    private String mPrductId;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("shipping_address")
    private String mShippingAddress;
    @SerializedName("shipping_phone")
    private String mShippingPhone;
    @SerializedName("sku")
    private String mSku;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("token")
    private String mToken;
    @SerializedName("total_cost")
    private String mTotalCost;
    @SerializedName("total_discount")
    private String mTotalDiscount;
    @SerializedName("unit_id")
    private String mUnitId;
    @SerializedName("ur_description")
    private String mUrDescription;
    @SerializedName("ur_name")
    private String mUrName;
    @SerializedName("ur_unit")
    private String mUrUnit;

    public String getCatId() {
        return mCatId;
    }

    public void setCatId(String catId) {
        mCatId = catId;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        mDateTime = dateTime;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getEnDescription() {
        return mEnDescription;
    }

    public void setEnDescription(String enDescription) {
        mEnDescription = enDescription;
    }

    public String getEnName() {
        return mEnName;
    }

    public void setEnName(String enName) {
        mEnName = enName;
    }

    public String getEnUnit() {
        return mEnUnit;
    }

    public void setEnUnit(String enUnit) {
        mEnUnit = enUnit;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public String getInStock() {
        return mInStock;
    }

    public void setInStock(String inStock) {
        mInStock = inStock;
    }

    public String getOpId() {
        return mOpId;
    }

    public void setOpId(String opId) {
        mOpId = opId;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        mOrderId = orderId;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getPrdId() {
        return mPrdId;
    }

    public void setPrdId(String prdId) {
        mPrdId = prdId;
    }

    public String getPrdImage() {
        return mPrdImage;
    }

    public void setPrdImage(String prdImage) {
        mPrdImage = prdImage;
    }

    public String getPrductId() {
        return mPrductId;
    }

    public void setPrductId(String prductId) {
        mPrductId = prductId;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getShippingAddress() {
        return mShippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        mShippingAddress = shippingAddress;
    }

    public String getShippingPhone() {
        return mShippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        mShippingPhone = shippingPhone;
    }

    public String getSku() {
        return mSku;
    }

    public void setSku(String sku) {
        mSku = sku;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getTotalCost() {
        return mTotalCost;
    }

    public void setTotalCost(String totalCost) {
        mTotalCost = totalCost;
    }

    public String getTotalDiscount() {
        return mTotalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        mTotalDiscount = totalDiscount;
    }

    public String getUnitId() {
        return mUnitId;
    }

    public void setUnitId(String unitId) {
        mUnitId = unitId;
    }

    public String getUrDescription() {
        return mUrDescription;
    }

    public void setUrDescription(String urDescription) {
        mUrDescription = urDescription;
    }

    public String getUrName() {
        return mUrName;
    }

    public void setUrName(String urName) {
        mUrName = urName;
    }

    public String getUrUnit() {
        return mUrUnit;
    }

    public void setUrUnit(String urUnit) {
        mUrUnit = urUnit;
    }
}
