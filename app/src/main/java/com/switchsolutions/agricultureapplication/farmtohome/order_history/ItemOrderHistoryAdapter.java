package com.switchsolutions.agricultureapplication.farmtohome.order_history;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.FeedbackResponse;
import com.switchsolutions.agricultureapplication.farmtohome.model.OrderHistory;
import com.switchsolutions.agricultureapplication.farmtohome.model.SubmitFeedback;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class ItemOrderHistoryAdapter extends RecyclerView.Adapter<ItemOrderHistoryAdapter.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    Context mContext;
    @NonNull
    private Paint p = new Paint();
    Bitmap icon;
    BaseApplication baseApplication;


    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
                return true;
            default:
        }
        return false;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView card_view;
        public TextView txt_order_date_time;
        public TextView txt_order_item_name;
        public TextView txt_order_item_weight;
        public TextView txt_order_item_price;
        public TextView txt_submit_feedback;
        public ImageView img_order_item;
        public RatingBar ratingBar;
        public Button btn_order_item_reorder;

        public ViewHolder(@NonNull View v) {
            super(v);
            card_view = (CardView) v.findViewById(R.id.card_view);
            txt_order_date_time = (TextView) v.findViewById(R.id.txt_order_date_time);
            txt_order_item_name = (TextView) v.findViewById(R.id.txt_order_item_name);
            txt_order_item_weight = (TextView) v.findViewById(R.id.txt_order_item_weight);
            txt_order_item_price = (TextView) v.findViewById(R.id.txt_order_item_price);
            txt_submit_feedback = (TextView) v.findViewById(R.id.txt_submit_feedback);
            img_order_item = (ImageView) v.findViewById(R.id.img_order_item);
            ratingBar = (RatingBar) v.findViewById(R.id.rating_item_order_history);
            btn_order_item_reorder = (Button) v.findViewById(R.id.btn_order_item_reorder);
        }
    }

    List<OrderHistory> orderHistories;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemOrderHistoryAdapter(Context context, List<OrderHistory> orderHistories) {
        mContext = context;
        this.orderHistories = orderHistories;
        baseApplication = (BaseApplication) mContext.getApplicationContext();


    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ItemOrderHistoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                 int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_orders_history, parent, false);
        // set the view's size, margins, paddings and layout parameters
        final ViewHolder vh = new ViewHolder(v);
        vh.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                int newPosition = vh.getAdapterPosition();
                //  notifyItemRemoved(newPosition);
                return false;
            }
        });

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        if (position == 0) {
            holder.card_view.setVisibility(View.VISIBLE);
        } else if (orderHistories.get(position).getDateTime().equalsIgnoreCase(orderHistories.get(position - 1).getDateTime())) {
            holder.card_view.setVisibility(View.GONE);
        } else
            holder.card_view.setVisibility(View.VISIBLE);

        holder.txt_order_item_name.setText(orderHistories.get(position).getEnName());
        holder.txt_order_item_price.setText("Rs. " + orderHistories.get(position).getTotalCost());
        holder.txt_order_item_weight.setText(orderHistories.get(position).getmQuantOrdered() + " " + orderHistories.get(position).getEnUnit());
        String dtStart = orderHistories.get(position).getDateTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            Date date = format.parse(dtStart);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH) + 1;
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int minute = cal.get(Calendar.MINUTE);

            holder.txt_order_date_time.setText(day + " " + Constants.getMonth(month) + " " + year + " " + hour + ":" + minute);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (orderHistories.get(position).getmRating() != "" && orderHistories.get(position).getmRating() != null)
            holder.ratingBar.setRating(Float.valueOf(orderHistories.get(position).getmRating()));
        else
            holder.ratingBar.setVisibility(View.GONE);
        //holder.ratingBar.setRating(4);

        holder.btn_order_item_reorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addItemtoCart(holder.getAdapterPosition());
            }
        });

        holder.txt_submit_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_feedback_dialogue(position);
            }
        });
        Glide.with(mContext).load(orderHistories.get(position).getPrdImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.img_order_item);
    }

    private void show_feedback_dialogue(final int pos) {

        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_order_feedback);
        dialog.setCancelable(true);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //  dialog.setTitle("Title...");
        final EditText editTextProductCount = (EditText) dialog.findViewById(R.id.etProductCount);
        TextView txtProductQuantity = (TextView) dialog.findViewById(R.id.txtProductQuantity);
        txtProductQuantity.setText("Feedback ");

        Button dialogButton = (Button) dialog.findViewById(R.id.btnSubmitProductCount);
        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (editTextProductCount.getText().length() > 4) {
                    submitFeedback(orderHistories.get(pos).getOrderId(), editTextProductCount.getText().toString());
                }
            }
        });
        dialog.show();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return orderHistories.size();
    }


    private void showPopupMenu(@NonNull View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_list_item, popup.getMenu());
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }

    private void addItemtoCart(int pos) {
        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAction("add");
        addToCartRequest.setProductId(orderHistories.get(pos).getPrdId());
        addToCartRequest.setQuantity("" + orderHistories.get(pos).getmQuantOrdered());
        addToCartRequest.setToken(Constants.token);
        baseApplication.showProgresDialogue("adding product in cart", mContext);
        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Toast.makeText(mContext, "cart update error", Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Toast.makeText(mContext, "" + jsonObject, Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                        baseApplication.hideProgressDialogue();
                    }
                });
    }

    private void submitFeedback(String orderId, String message) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        baseApplication.showProgresDialogue("submitting feedback", mContext);

        SubmitFeedback submitFeedback = new SubmitFeedback();
        submitFeedback.setMobile(Constants.msisdn);
        submitFeedback.setOrderId(orderId);
        submitFeedback.setMessage(message);

        Observable<FeedbackResponse> observable = iEndPoints.submitFeedback(submitFeedback);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<FeedbackResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        baseApplication.hideProgressDialogue();
                    }
                    @Override
                    public void onNext(FeedbackResponse feedbackResponse) {
                        baseApplication.hideProgressDialogue();
                        if (feedbackResponse != null)
                            Toast.makeText(mContext, "" + feedbackResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
