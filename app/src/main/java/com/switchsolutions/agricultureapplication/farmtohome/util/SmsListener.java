package com.switchsolutions.agricultureapplication.farmtohome.util;

/**
 * Created by Danish on 5/23/2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}
