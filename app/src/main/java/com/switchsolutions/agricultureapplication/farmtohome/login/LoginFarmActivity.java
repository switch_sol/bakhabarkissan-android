package com.switchsolutions.agricultureapplication.farmtohome.login;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.model.ForgotPasswordRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;
import com.switchsolutions.agricultureapplication.farmtohome.profile.ProfileActivity;
import com.switchsolutions.agricultureapplication.farmtohome.signup.SignUpActivity;
import com.switchsolutions.agricultureapplication.farmtohome.util.Sharedprefrencesutil;
import com.switchsolutions.agricultureapplication.farmtohome.util.TextDrawable;


import java.io.IOException;
import java.util.logging.Logger;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginFarmActivity extends AppCompatActivity implements View.OnClickListener {

    String token;
    EditText email_et, password_et;
    Button btn_Signin, btn_SignUp;
    private String tokenResponse1;
    LoginButton loginButton;
    CallbackManager callbackManager;
    Button fb;
    boolean isFromCart = false;
    TextView tv_forgetPassword;
    String phone = "";
    public String name;
    public String email;
    BaseApplication baseApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        Intent intent = getIntent();
        Constants.userName = "";
        Constants.userEmail = "";

        if (baseApplication == null)
            baseApplication = (BaseApplication) getApplication();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setTitleTextColor(ContextCompat.getColor(this, android.R.color.white));

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Login");

        /*
        try {           PackageInfo info = getPackageManager().getPackageInfo(
                "com.support.android.designlibdemo",
                PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }       } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

        callbackManager = CallbackManager.Factory.create();


        fb = (Button) findViewById(R.id.fb);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        Bundle bundle = intent.getExtras();
        if (bundle != null)
            if (bundle.containsKey("isFromCart")) {
                String isFromca = bundle.getString("isFromCart");
                if (isFromca != "" && isFromca != null && isFromca != "null") {
                    if (isFromca.equalsIgnoreCase("true"))
                        isFromCart = true;
                    else
                        isFromCart = false;
                }
            }
        email_et = (EditText) findViewById(R.id.id_userName);

        String code = "+92";

        //email_et.setCompoundDrawablesWithIntrinsicBounds(new TextDrawable(code), null, null, null);
        //email_et.setCompoundDrawablePadding(code.length() * 13);

        password_et = (EditText) findViewById(R.id.id_password);
        btn_Signin = (Button) findViewById(R.id.btn_signIn);
        btn_SignUp = (Button) findViewById(R.id.btn_SignUp);
        tv_forgetPassword = (TextView) findViewById(R.id.tv_forgetPassword);
        tv_forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (email_et.getText() != null) {
                    phone = email_et.getText().toString();
                    if (phone.isEmpty() || phone.length() != 11) {
                        email_et.setError("Please enter mobile number first");
                        email_et.requestFocus();
                    } else {
                        makeRequestForgotPassword(phone);
                    }
                } else
                    Snackbar.make(email_et, "Phone number is empty", Snackbar.LENGTH_SHORT).show();
            }
        });


        btn_Signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phonnum = email_et.getText().toString();
                String password = password_et.getText().toString();
                if (phonnum.equals("") && password.equals("")) {
                    Toast.makeText(LoginFarmActivity.this, "Enter Values First", Toast.LENGTH_LONG).show();
                } else if (phonnum.length() != 11 || phonnum.charAt(0) != '0' || phonnum.charAt(1) != '3') {
                    email_et.setError("Please enter valid number");
                    email_et.requestFocus();
                } else if (phonnum.charAt(2) != '4' && phonnum.charAt(2) != '3' && phonnum.charAt(2) != '2' && phonnum.charAt(2) != '0') {
                    email_et.setError("Please enter valid number");
                    email_et.requestFocus();
                } else if (password == null || password.length() < 6) {
                    password_et.setError("password must contain minimum 6 characters");
                    password_et.requestFocus();
                } else {
                    Login_process(phonnum, password);
                }
            }
        });

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginFarmActivity.this, SignUpActivity.class);
                intent.putExtra("isFromCart", "true");
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    private void makeRequestForgotPassword(String phone) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest();
        forgotPasswordRequest.setmMobile(phone);
        Observable<String> observableForgotPassword = iEndPoints.forgotPasswordApi(forgotPasswordRequest);
        baseApplication.showProgresDialogue("sending new password", LoginFarmActivity.this);
        observableForgotPassword.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(Throwable e) {

                        baseApplication.hideProgressDialogue();

                    }

                    @Override
                    public void onNext(String s) {
                        Snackbar.make(email_et, "Password Sent", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    public void Login_process(final String mobile, String password) {

        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.setMobile(mobile);
        loginRequest.setPassword(password);

        baseApplication.showProgresDialogue("Loggin in", LoginFarmActivity.this);

        /*Observable<JsonArray> loginResponseObservable
                = iEndPoints.loginApi(loginRequest)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());*/

        Call<String> loginResponse = iEndPoints.loginApi(loginRequest);

        loginResponse.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });

/*
        loginResponseObservable.subscribe(new Subscriber<JsonArray>() {
            @Override
            public void onCompleted() {
                baseApplication.hideProgressDialogue();
            }

            @Override
            public void onError( Throwable e) {
                if (e instanceof HttpException) {
                    ResponseBody body = ((HttpException) e).response().errorBody();
                    Log.d(Constants.FARM_TO_HOME_LOGS, "login error body:" + body);
                }

                e.printStackTrace();
                Log.d(Constants.FARM_TO_HOME_LOGS, "login error");
                baseApplication.hideProgressDialogue();
                Toast.makeText(LoginFarmActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNext(JsonArray loginResponse) {

                Log.d(Constants.FARM_TO_HOME_LOGS, "login onnext");
                JsonElement jsonElement = loginResponse.get(0);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                LoginResponse loginResponse1 = new Gson().fromJson(jsonObject, LoginResponse.class);
                Sharedprefrencesutil.setLoginData(LoginFarmActivity.this, loginResponse1, "loginData");
                Sharedprefrencesutil.setSomeStringValue(LoginFarmActivity.this, "isLoggedIn", "true");
                Constants.msisdn = mobile;
                Constants.userEmail = loginResponse1.getEmail();
                Constants.userName = loginResponse1.getName();
                Intent intent = new Intent(LoginFarmActivity.this, MainFarmActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (isFromCart)
                    intent.putExtra("isFromCart", "true");
                startActivity(intent);
                finish();

            }
        });*/

    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);

    }

    @Override
    public void onClick(View v) {
        if (v == fb) {
            loginButton.performClick();
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: {
                finish();
                return true;
            }
            default: {
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
