
package com.switchsolutions.agricultureapplication.farmtohome.signup;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class VerifyApiPojo {

    @SerializedName("msg")
    private String mMsg;
    @SerializedName("Status")
    private Long mStatus;

    public String getMsg() {
        return mMsg;
    }

    public void setMsg(String msg) {
        mMsg = msg;
    }

    public Long getStatus() {
        return mStatus;
    }

    public void setStatus(Long Status) {
        mStatus = Status;
    }

}
