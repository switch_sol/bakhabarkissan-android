package com.switchsolutions.agricultureapplication.farmtohome.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class ItemAllProducts extends RecyclerView.Adapter<ItemAllProducts.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    Context mContext;

    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add_to_cart:


                return true;
            default:
        }
        return false;
    }

    private void addItemtoCart(String skuId, int pos) {


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAction("add");
        addToCartRequest.setProductId(String.valueOf(0));
        addToCartRequest.setQuantity(""+1);
        addToCartRequest.setToken(Constants.token);
        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        e.printStackTrace();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror products");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext products");
                    }
                });
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_item_list_more;
        public TextView txt_item_list_product_name;
        public TextView txt_item_list_price;
        public ImageView img_item_list_product;
        public RelativeLayout rltv_item_list_all_product;


        public ViewHolder(@NonNull View v) {
            super(v);
            img_item_list_more = (ImageView) v.findViewById(R.id.img_item_list_more);
            txt_item_list_product_name = (TextView) v.findViewById(R.id.txt_item_list_product_name);
            txt_item_list_price = (TextView) v.findViewById(R.id.txt_item_list_price);
            img_item_list_product = (ImageView) v.findViewById(R.id.img_item_list_product);
            rltv_item_list_all_product = (RelativeLayout) v.findViewById(R.id.rltv_item_list_all_product);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemAllProducts(Context context) {
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ItemAllProducts.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_all_products, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        holder.img_item_list_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(holder.rltv_item_list_all_product, position);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return 10;
    }


    private void showPopupMenu(View view, final int popUpPosition) {

        final PopupWindow popupWindow = new PopupWindow(mContext); // inflet your layout or diynamic add view
        View view1 = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view1 = inflater.inflate(R.layout.popup_layout, null);
        TextView item = (TextView) view1.findViewById(R.id.button1);
        popupWindow.setFocusable(true);
        TextView buttonLivePic = (TextView) view1.findViewById(R.id.button4);
        buttonLivePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   mContext.startActivity(new Intent(mContext, ProductDetailActivity.class).putExtra("prdId", lis.get(popUpPosition).getPrdId()));

            }
        });



        view1.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int width = view1.getMeasuredWidth();
        int height = view1.getMeasuredHeight();
        popupWindow.setWidth(width);
        popupWindow.setHeight(height);

        //  popupWindow.setHeight(70);
        item.setGravity(Gravity.CENTER);

        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.GRAY));
        popupWindow.setContentView(view1);
        popupWindow.showAsDropDown(view, 0, -125);

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (popupWindow != null)
                    popupWindow.dismiss();
            }
        });
    }
}
