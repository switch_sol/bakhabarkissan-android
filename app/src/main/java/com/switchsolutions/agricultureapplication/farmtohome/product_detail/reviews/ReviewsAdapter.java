package com.switchsolutions.agricultureapplication.farmtohome.product_detail.reviews;

import android.content.Context;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.ProductDetailResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Muhammad Hassan on 5/19/2017.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {


    Context mContext;


    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public AppCompatRatingBar appCompatRatingBar;
        public TextView txt_product_review_time;
        public TextView txt_product_review_detail;
        public TextView txt_all_reviews;
        public TextView txt_product_reviewer_name;

        public ViewHolder(@NonNull View v) {
            super(v);

            appCompatRatingBar = (AppCompatRatingBar) v.findViewById(R.id.rating_prd_rev);
            txt_product_review_time = (TextView) v.findViewById(R.id.txt_product_review_time);
            txt_product_review_detail = (TextView) v.findViewById(R.id.txt_product_review_detail);
            txt_all_reviews = (TextView) v.findViewById(R.id.txt_all_reviews);
            txt_product_reviewer_name = (TextView) v.findViewById(R.id.txt_product_reviewer_name);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    List<ProductDetailResponse> jsonObject;

    public ReviewsAdapter(Context context, List<ProductDetailResponse> jsonObject) {
        mContext = context;
        this.jsonObject = jsonObject;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_reviews_detail, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if (jsonObject.get(position).getReview() == "null" || jsonObject.get(position).getDatetime() == "null" || jsonObject.get(position).getReview() == null || jsonObject.get(position).getReview() == "") {
            holder.txt_product_review_detail.setVisibility(View.GONE);
            holder.txt_product_review_time.setVisibility(View.GONE);
            holder.appCompatRatingBar.setVisibility(View.GONE);
            holder.txt_product_reviewer_name.setVisibility(View.GONE);

        }
        if (position > 0) {
            if (jsonObject.get(position).getUsername().equalsIgnoreCase(jsonObject.get(position - 1).getUsername())) {
                holder.txt_product_review_detail.setVisibility(View.GONE);
                holder.txt_product_review_time.setVisibility(View.GONE);
                holder.appCompatRatingBar.setVisibility(View.GONE);
                holder.txt_product_reviewer_name.setVisibility(View.GONE);

            }
        }

        holder.txt_product_review_detail.setText("" + jsonObject.get(position).getReview());
        if (jsonObject.get(position).getStars() != null && jsonObject.get(position).getStars() != "null" && jsonObject.get(position).getStars() != "")
            holder.appCompatRatingBar.setRating(Float.valueOf(jsonObject.get(position).getStars()));


        if (jsonObject.get(position).getName() != null && jsonObject.get(position).getName() != "") {
            holder.txt_product_reviewer_name.setText("by " + jsonObject.get(position).getName());
        }
        String dtStart = jsonObject.get(position).getDatetime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            if (dtStart != null && dtStart != "" && dtStart != "null") {
                Date date = format.parse(dtStart);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH) + 1;
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int hour = cal.get(Calendar.HOUR_OF_DAY);
                int minute = cal.get(Calendar.MINUTE);

                holder.txt_product_review_time.setText(day + " " + Constants.getMonth(month) + " " + year);
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return jsonObject.size();
    }
}
