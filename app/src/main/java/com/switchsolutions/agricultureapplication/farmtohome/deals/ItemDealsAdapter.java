package com.switchsolutions.agricultureapplication.farmtohome.deals;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.api.ApiClient;
import com.switchsolutions.agricultureapplication.farmtohome.api.IEndPoints;
import com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants;
import com.switchsolutions.agricultureapplication.farmtohome.model.AddToCartRequest;
import com.switchsolutions.agricultureapplication.farmtohome.model.DealsResponse;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Muhammad Hassan on 3/30/2017.
 */

public class ItemDealsAdapter extends RecyclerView.Adapter<ItemDealsAdapter.ViewHolder> implements PopupMenu.OnMenuItemClickListener {

    Context mContext;
    BaseApplication baseApplication;


    @Override
    public boolean onMenuItemClick(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                return true;
            default:
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView img_item_list_more;
        public TextView txt_item_list_product_name;
        public TextView txt_product_count_cat;
        public ImageView img_product_cat;


        public ViewHolder(@NonNull View v) {
            super(v);
            img_item_list_more = (ImageView) v.findViewById(R.id.img_item_list_more);
            img_product_cat = (ImageView) v.findViewById(R.id.img_product_cat);
            txt_item_list_product_name = (TextView) v.findViewById(R.id.txt_item_list_product_name);
            txt_product_count_cat = (TextView) v.findViewById(R.id.txt_product_count_cat);
        }
    }

    List<DealsResponse> dealsResponses;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ItemDealsAdapter(Context context, List<DealsResponse> dealsResponses) {
        mContext = context;
        this.dealsResponses = dealsResponses;

        baseApplication = (BaseApplication) mContext.getApplicationContext();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public ItemDealsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_deals, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);


        holder.img_item_list_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(holder.img_item_list_more, position);
            }
        });

        holder.txt_item_list_product_name.setText("" + dealsResponses.get(position).getEnName());
        holder.txt_product_count_cat.setText("" + dealsResponses.get(position).getDealPrice());
        Glide.with(mContext).load(dealsResponses.get(position).getDealImage()).into(holder.img_product_cat);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dealsResponses.size();
    }


    private void showPopupMenu(@NonNull View view, final int position) {
        // inflate menu

        final PopupWindow popupWindow = new PopupWindow(mContext); // inflet your layout or diynamic add view
        View view1 = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view1 = inflater.inflate(R.layout.popup_layout, null);
        TextView item = (TextView) view1.findViewById(R.id.button1);
        popupWindow.setFocusable(true);

        view1.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int width = view1.getMeasuredWidth();
        int height = view1.getMeasuredHeight();
        popupWindow.setWidth(width);
        popupWindow.setHeight(height);

        item.setGravity(Gravity.CENTER);

        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.GRAY));
        popupWindow.setContentView(view1);
        popupWindow.showAsDropDown(view, 0, -180);

        TextView buttonLivePic = (TextView) view1.findViewById(R.id.button4);

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow != null)
                    popupWindow.dismiss();

                addItemtoCart(position);
            }
        });
    }

    private void addItemtoCart(int pos) {


        IEndPoints iEndPoints = ApiClient.getClient().create(IEndPoints.class);
        AddToCartRequest addToCartRequest = new AddToCartRequest();
        addToCartRequest.setAction("add");
        addToCartRequest.setProductId(dealsResponses.get(pos).getPrdId());
        addToCartRequest.setQuantity("1");
        addToCartRequest.setToken(Constants.token);

        baseApplication.showProgresDialogue("adding item in cart", mContext);

        Observable<String> observable = iEndPoints.addToCartApi(addToCartRequest);
        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        baseApplication.hideProgressDialogue();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        baseApplication.hideProgressDialogue();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onerror fruits");
                    }

                    @Override
                    public void onNext(String jsonObject) {
                        Toast.makeText(mContext, "" + jsonObject.toString(), Toast.LENGTH_SHORT).show();
                        Log.d(Constants.FARM_TO_HOME_LOGS, "add to cart onnext fruits");
                    }
                });
    }

}
