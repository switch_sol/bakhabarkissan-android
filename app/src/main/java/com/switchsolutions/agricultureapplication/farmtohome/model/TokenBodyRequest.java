package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

/**
 * Created by Muhammad Hassan on 5/15/2017.
 */
@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class TokenBodyRequest {

    @SerializedName("token")
    private String mToken;

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String token) {
        mToken = token;
    }

}
