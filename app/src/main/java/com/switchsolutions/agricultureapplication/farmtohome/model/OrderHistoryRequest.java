
package com.switchsolutions.agricultureapplication.farmtohome.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OrderHistoryRequest {

    @SerializedName("mobile")
    private String mMobile;

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

}
