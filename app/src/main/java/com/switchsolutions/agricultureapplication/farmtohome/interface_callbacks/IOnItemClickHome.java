package com.switchsolutions.agricultureapplication.farmtohome.interface_callbacks;

/**
 * Created by Muhammad Hassan on 5/3/2017.
 */

public interface IOnItemClickHome {

    void onItemClick(int position);

    void onItemClick(String itemName);
}
