package com.switchsolutions.agricultureapplication.farmtohome.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.switchsolutions.agricultureapplication.farmtohome.model.LoginResponse;

/**
 * Created by Muhammad Hassan on 5/8/2017.
 */

public class Sharedprefrencesutil {

    private static final String APP_SETTINGS = "FARM_TO_HOME_PREFRENCES";


    private Sharedprefrencesutil() {
    }

    private static SharedPreferences getSharedPreferences(@NonNull Context context) {
        return context.getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
    }

    @Nullable
    public static String getSomeStringValue(@NonNull Context context, String key) {
        return getSharedPreferences(context).getString(key, null);
    }

    public static void setSomeStringValue(@NonNull Context context, String key, String value) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, value);
        editor.apply();
    }


    public static void setLoginData(@NonNull Context context, LoginResponse jsonObject, String key) {
        final SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        Gson gson = new Gson();
        String json = gson.toJson(jsonObject);
        editor.putString(key, json);
        editor.apply();
    }

    public static LoginResponse getLoginData(@NonNull Context context, String key) {
        Gson gson = new Gson();
        String json = getSharedPreferences(context).getString(key, "");
        LoginResponse obj = gson.fromJson(json, LoginResponse.class);
        return obj;

    }

    // other getters/setters
}

