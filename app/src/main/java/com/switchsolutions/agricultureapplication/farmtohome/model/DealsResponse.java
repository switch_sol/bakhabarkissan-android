
package com.switchsolutions.agricultureapplication.farmtohome.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DealsResponse {

    @SerializedName("cat_id")
    private String mCatId;
    @SerializedName("deal_id")
    private String mDealId;
    @SerializedName("deal_image")
    private String mDealImage;
    @SerializedName("deal_price")
    private String mDealPrice;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("en_description")
    private String mEnDescription;
    @SerializedName("en_name")
    private String mEnName;
    @SerializedName("enddate")
    private String mEnddate;
    @SerializedName("grade")
    private String mGrade;
    @SerializedName("id")
    private String mId;
    @SerializedName("in_stock")
    private String mInStock;
    @SerializedName("name")
    private String mName;
    @SerializedName("original_price")
    private String mOriginalPrice;
    @SerializedName("pquantity")
    private String mPquantity;
    @SerializedName("prd_id")
    private String mPrdId;
    @SerializedName("prd_image")
    private String mPrdImage;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("product_id")
    private String mProductId;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("rating")
    private String mRating;
    @SerializedName("sku")
    private String mSku;
    @SerializedName("startdate")
    private String mStartdate;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("unit_id")
    private String mUnitId;
    @SerializedName("ur_description")
    private String mUrDescription;
    @SerializedName("ur_name")
    private String mUrName;

    public String getCatId() {
        return mCatId;
    }

    public void setCatId(String catId) {
        mCatId = catId;
    }

    public String getDealId() {
        return mDealId;
    }

    public void setDealId(String dealId) {
        mDealId = dealId;
    }

    public String getDealImage() {
        return mDealImage;
    }

    public void setDealImage(String dealImage) {
        mDealImage = dealImage;
    }

    public String getDealPrice() {
        return mDealPrice;
    }

    public void setDealPrice(String dealPrice) {
        mDealPrice = dealPrice;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getEnDescription() {
        return mEnDescription;
    }

    public void setEnDescription(String enDescription) {
        mEnDescription = enDescription;
    }

    public String getEnName() {
        return mEnName;
    }

    public void setEnName(String enName) {
        mEnName = enName;
    }

    public String getEnddate() {
        return mEnddate;
    }

    public void setEnddate(String enddate) {
        mEnddate = enddate;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getInStock() {
        return mInStock;
    }

    public void setInStock(String inStock) {
        mInStock = inStock;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOriginalPrice() {
        return mOriginalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        mOriginalPrice = originalPrice;
    }

    public String getPquantity() {
        return mPquantity;
    }

    public void setPquantity(String pquantity) {
        mPquantity = pquantity;
    }

    public String getPrdId() {
        return mPrdId;
    }

    public void setPrdId(String prdId) {
        mPrdId = prdId;
    }

    public String getPrdImage() {
        return mPrdImage;
    }

    public void setPrdImage(String prdImage) {
        mPrdImage = prdImage;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getProductId() {
        return mProductId;
    }

    public void setProductId(String productId) {
        mProductId = productId;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRating() {
        return mRating;
    }

    public void setRating(String rating) {
        mRating = rating;
    }

    public String getSku() {
        return mSku;
    }

    public void setSku(String sku) {
        mSku = sku;
    }

    public String getStartdate() {
        return mStartdate;
    }

    public void setStartdate(String startdate) {
        mStartdate = startdate;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUnitId() {
        return mUnitId;
    }

    public void setUnitId(String unitId) {
        mUnitId = unitId;
    }

    public String getUrDescription() {
        return mUrDescription;
    }

    public void setUrDescription(String urDescription) {
        mUrDescription = urDescription;
    }

    public String getUrName() {
        return mUrName;
    }

    public void setUrName(String urName) {
        mUrName = urName;
    }

}
