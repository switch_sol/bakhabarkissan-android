
package com.switchsolutions.agricultureapplication.farmtohome.model;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SubmitFeedback {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("mobile")
    private String mMobile;
    @SerializedName("order_id")
    private String mOrderId;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getMobile() {
        return mMobile;
    }

    public void setMobile(String mobile) {
        mMobile = mobile;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        mOrderId = orderId;
    }

}
