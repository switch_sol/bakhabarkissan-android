
package com.switchsolutions.agricultureapplication.farmtohome.products.chicken;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ChickenList {


    public String getmEngUnit() {
        return mEngUnit;
    }

    public void setmEngUnit(String mEngUnit) {
        this.mEngUnit = mEngUnit;
    }

    @SerializedName("en_unit")
    private String mEngUnit;

    @SerializedName("cat_id")
    private String mCatId;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("en_description")
    private String mEnDescription;
    @SerializedName("en_name")
    private String mEnName;
    @SerializedName("grade")
    private String mGrade;
    @SerializedName("in_stock")
    private String mInStock;
    @SerializedName("prd_id")
    private String mPrdId;
    @SerializedName("prd_image")
    private String mPrdImage;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("sku")
    private String mSku;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("unit_id")
    private String mUnitId;
    @SerializedName("ur_description")
    private String mUrDescription;
    @SerializedName("ur_name")
    private String mUrName;

    public String getCatId() {
        return mCatId;
    }

    public void setCatId(String catId) {
        mCatId = catId;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getEnDescription() {
        return mEnDescription;
    }

    public void setEnDescription(String enDescription) {
        mEnDescription = enDescription;
    }

    public String getEnName() {
        return mEnName;
    }

    public void setEnName(String enName) {
        mEnName = enName;
    }

    public String getGrade() {
        return mGrade;
    }

    public void setGrade(String grade) {
        mGrade = grade;
    }

    public String getInStock() {
        return mInStock;
    }

    public void setInStock(String inStock) {
        mInStock = inStock;
    }

    public String getPrdId() {
        return mPrdId;
    }

    public void setPrdId(String prdId) {
        mPrdId = prdId;
    }

    public String getPrdImage() {
        return mPrdImage;
    }

    public void setPrdImage(String prdImage) {
        mPrdImage = prdImage;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getSku() {
        return mSku;
    }

    public void setSku(String sku) {
        mSku = sku;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUnitId() {
        return mUnitId;
    }

    public void setUnitId(String unitId) {
        mUnitId = unitId;
    }

    public String getUrDescription() {
        return mUrDescription;
    }

    public void setUrDescription(String urDescription) {
        mUrDescription = urDescription;
    }

    public String getUrName() {
        return mUrName;
    }

    public void setUrName(String urName) {
        mUrName = urName;
    }

}
