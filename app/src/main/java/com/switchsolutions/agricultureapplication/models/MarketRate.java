package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class MarketRate implements Parcelable {

    public static final Creator<MarketRate> CREATOR = new Creator<MarketRate>() {
        @Override
        public MarketRate createFromParcel(Parcel in) {
            return new MarketRate(in);
        }

        @Override
        public MarketRate[] newArray(int size) {
            return new MarketRate[size];
        }
    };

    private static final String FIELD_NAME = "name";
    private static final String FIELD_RATE = "rate";
    private static final String FIELD_DATE = "date";
    private final String name;
    private final String rate;
    private final double date;

    @JsonCreator
    public MarketRate(@JsonProperty(FIELD_NAME) String name,
                      @JsonProperty(FIELD_RATE) String rate,
                      @JsonProperty(FIELD_DATE) double date) {
        this.name = name;
        this.rate = rate;
        this.date = date;
    }

    protected MarketRate(Parcel in) {
        name = in.readString();
        rate = in.readString();
        date = in.readDouble();
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_RATE)
    public String getRate() {
        return rate;
    }

    @JsonGetter(FIELD_DATE)
    public double getDate() {
        return date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(rate);
        parcel.writeDouble(date);
    }
}
