package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Market implements Parcelable {

    public static final Creator<Market> CREATOR = new Creator<Market>() {
        @Override
        public Market createFromParcel(Parcel in) {
            return new Market(in);
        }

        @Override
        public Market[] newArray(int size) {
            return new Market[size];
        }
    };

    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_LAT = "lat";
    private static final String FIELD_LON = "lon";
    private final long id;
    private final String name;
    private final double lat;
    private final double lon;

    @JsonCreator
    public Market(@JsonProperty(FIELD_ID) long id,
                  @JsonProperty(FIELD_NAME) String name,
                  @JsonProperty(FIELD_LAT) double lat,
                  @JsonProperty(FIELD_LON) double lon) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
    }

    protected Market(Parcel in) {
        id = in.readLong();
        name = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_LAT)
    public double getLat() {
        return lat;
    }

    @JsonGetter(FIELD_LON)
    public double getLon() {
        return lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeDouble(lat);
        parcel.writeDouble(lon);
    }
}
