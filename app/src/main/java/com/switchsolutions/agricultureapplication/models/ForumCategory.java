package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ForumCategory implements Parcelable {

    public static final Creator<ForumCategory> CREATOR = new Creator<ForumCategory>() {
        @Override
        public ForumCategory createFromParcel(Parcel in) {
            return new ForumCategory(in);
        }

        @Override
        public ForumCategory[] newArray(int size) {
            return new ForumCategory[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private final long id;
    private final String name;

    @JsonCreator
    public ForumCategory(@JsonProperty(FIELD_ID) long id,
                         @JsonProperty(FIELD_NAME) String name) {
        this.id = id;
        this.name = name;
    }

    protected ForumCategory(Parcel in) {
        id = in.readLong();
        name = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
    }
}
