package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ForumComment implements Parcelable {

    public static final Creator<ForumComment> CREATOR = new Creator<ForumComment>() {
        @Override
        public ForumComment createFromParcel(Parcel in) {
            return new ForumComment(in);
        }

        @Override
        public ForumComment[] newArray(int size) {
            return new ForumComment[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_ANSWER_ID = "answer_id";
    private static final String FIELD_ACCOUNT_ID = "account_id";
    private static final String FIELD_ACCOUNT_NAME = "account_name";
    private final long id;
    private final String text;
    private final double datetime;
    private final long answerId;
    private final long accountId;
    private final String accountName;

    @JsonCreator
    public ForumComment(@JsonProperty(FIELD_ID) long id,
                        @JsonProperty(FIELD_TEXT) String text,
                        @JsonProperty(FIELD_DATETIME) double datetime,
                        @JsonProperty(FIELD_ANSWER_ID) long answerId,
                        @JsonProperty(FIELD_ACCOUNT_ID) long accountId,
                        @JsonProperty(FIELD_ACCOUNT_NAME) String accountName) {
        this.id = id;
        this.text = text;
        this.datetime = datetime;
        this.answerId = answerId;
        this.accountId = accountId;
        this.accountName = accountName;
    }

    protected ForumComment(Parcel in) {
        id = in.readLong();
        text = in.readString();
        datetime = in.readDouble();
        answerId = in.readLong();
        accountId = in.readLong();
        accountName = in.readString();
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public double getDatetime() {
        return datetime;
    }

    public long getAnswerId() {
        return answerId;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(text);
        parcel.writeDouble(datetime);
        parcel.writeLong(answerId);
        parcel.writeLong(accountId);
        parcel.writeString(accountName);
    }
}
