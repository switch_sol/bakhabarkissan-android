package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class ForumQuestion implements Parcelable {

    public static final Creator<ForumQuestion> CREATOR = new Creator<ForumQuestion>() {
        @Override
        public ForumQuestion createFromParcel(Parcel in) {
            return new ForumQuestion(in);
        }

        @Override
        public ForumQuestion[] newArray(int size) {
            return new ForumQuestion[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_IMAGES = "images";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_VOTED = "voted";
    private static final String FIELD_CATEGORY_ID = "category_id";
    private static final String FIELD_ACCOUNT_ID = "account_id";
    private static final String FIELD_ACCOUNT_NAME = "account_name";
    private final long id;
    private final String text;
    private final String[] images;
    private final double datetime;
    private final int score;
    private final int voted;
    private final long categoryId;
    private final long accountId;
    private final String accountName;

    public ForumQuestion(@JsonProperty(FIELD_ID) long id,
                         @JsonProperty(FIELD_TEXT) String text,
                         @JsonProperty(FIELD_IMAGES) String[] images,
                         @JsonProperty(FIELD_DATETIME) double datetime,
                         @JsonProperty(FIELD_SCORE) int score,
                         @JsonProperty(FIELD_VOTED) int voted,
                         @JsonProperty(FIELD_CATEGORY_ID) long categoryId,
                         @JsonProperty(FIELD_ACCOUNT_ID) long accountId,
                         @JsonProperty(FIELD_ACCOUNT_NAME) String accountName) {
        this.id = id;
        this.text = text;
        this.images = images;
        this.datetime = datetime;
        this.score = score;
        this.voted = voted;
        this.categoryId = categoryId;
        this.accountId = accountId;
        this.accountName = accountName;
    }

    protected ForumQuestion(Parcel in) {
        id = in.readLong();
        text = in.readString();
        images = in.createStringArray();
        datetime = in.readDouble();
        score = in.readInt();
        voted = in.readInt();
        categoryId = in.readLong();
        accountId = in.readLong();
        accountName = in.readString();
    }

    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String[] getImages() {
        return images;
    }

    public double getDatetime() {
        return datetime;
    }

    public int getScore() {
        return score;
    }

    public int getVoted() {
        return voted;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public long getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(text);
        parcel.writeStringArray(images);
        parcel.writeDouble(datetime);
        parcel.writeInt(score);
        parcel.writeInt(voted);
        parcel.writeLong(categoryId);
        parcel.writeLong(accountId);
        parcel.writeString(accountName);
    }
}
