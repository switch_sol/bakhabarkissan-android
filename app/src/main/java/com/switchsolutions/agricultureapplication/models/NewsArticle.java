package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class NewsArticle implements Parcelable {

    public static final Creator<NewsArticle> CREATOR = new Creator<NewsArticle>() {
        @Override
        public NewsArticle createFromParcel(Parcel in) {
            return new NewsArticle(in);
        }

        @Override
        public NewsArticle[] newArray(int size) {
            return new NewsArticle[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_SUBHEADING = "subheading";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_SRC_NAME = "src_name";
    private static final String FIELD_SRC_URL = "src_url";
    private final long id;
    private final String title;
    private final String subheading;
    private final String imagePath;
    private final double datetime;
    private final String srcName;
    private final String srcUrl;

    @JsonCreator
    public NewsArticle(@JsonProperty(FIELD_ID) long id,
                       @JsonProperty(FIELD_TITLE) String title,
                       @JsonProperty(FIELD_SUBHEADING) String subheading,
                       @JsonProperty(FIELD_IMAGE_PATH) String imagePath,
                       @JsonProperty(FIELD_DATETIME) double datetime,
                       @JsonProperty(FIELD_SRC_NAME) String srcName,
                       @JsonProperty(FIELD_SRC_URL) String srcUrl) {
        this.id = id;
        this.title = title;
        this.subheading = subheading;
        this.imagePath = imagePath;
        this.datetime = datetime;
        this.srcName = srcName;
        this.srcUrl = srcUrl;
    }

    protected NewsArticle(Parcel in) {
        id = in.readLong();
        title = in.readString();
        subheading = in.readString();
        imagePath = in.readString();
        datetime = in.readDouble();
        srcName = in.readString();
        srcUrl = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_TITLE)
    public String getTitle() {
        return title;
    }

    @JsonGetter(FIELD_SUBHEADING)
    public String getSubheading() {
        return subheading;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @JsonGetter(FIELD_DATETIME)
    public double getDatetime() {
        return datetime;
    }

    @JsonGetter(FIELD_SRC_NAME)
    public String getSrcName() {
        return srcName;
    }

    @JsonGetter(FIELD_SRC_URL)
    public String getSrcUrl() {
        return srcUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(subheading);
        parcel.writeString(imagePath);
        parcel.writeDouble(datetime);
        parcel.writeString(srcName);
        parcel.writeString(srcUrl);
    }
}
