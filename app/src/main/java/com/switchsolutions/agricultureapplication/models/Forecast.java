package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.switchsolutions.agricultureapplication.R;

import java.util.Date;

public final class Forecast implements Parcelable {

    public static final Creator<Forecast> CREATOR = new Creator<Forecast>() {
        @Override
        public Forecast createFromParcel(Parcel in) {
            return new Forecast(in);
        }

        @Override
        public Forecast[] newArray(int size) {
            return new Forecast[size];
        }
    };
    private final Date datetime;
    private final float kelvin;
    private final int humidity;
    private final int weatherCode;
    private final int weatherCodeStringResId;
    private final int weatherCodeDrawableResId;
    private final int cloudCover;
    private final float windSpeed;
    private final float windDirection;
    private final int windDirectionStringRes;

    public Forecast(Date datetime, float kelvin, int humidity, int weatherCode, int cloudCover, float windSpeed, float windDirection) {
        this.datetime = datetime;
        this.kelvin = kelvin;
        this.humidity = humidity;
        this.weatherCode = weatherCode;
        this.weatherCodeStringResId = openWeatherMapCodeToStringResource(weatherCode);
        this.weatherCodeDrawableResId = weatherCodeToImageResource(weatherCodeStringResId);
        this.cloudCover = cloudCover;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
        this.windDirectionStringRes = windDirectionToStringResource(windDirection);
    }

    protected Forecast(Parcel in) {
        datetime = new Date(in.readLong());
        kelvin = in.readFloat();
        humidity = in.readInt();
        weatherCode = in.readInt();
        weatherCodeStringResId = in.readInt();
        weatherCodeDrawableResId = in.readInt();
        cloudCover = in.readInt();
        windSpeed = in.readFloat();
        windDirection = in.readFloat();
        windDirectionStringRes = in.readInt();
    }

    // From: http://openweathermap.org/weather-conditions
    public static int openWeatherMapCodeToStringResource(int code) {
        if (code >= 200 && code <= 232) {
            return R.string.thunderstorm;
        } else if (code >= 300 && code <= 321) {
            return R.string.light_rain;
        } else if (code >= 500 && code <= 531) {
            return R.string.rain;
        } else if (code >= 600 && code <= 622) {
            return R.string.snow;
        } else if (code >= 701 && code <= 781) {
            return R.string.fog;
        } else if (code >= 801 && code <= 802) {
            return R.string.clouds;
        } else if (code >= 803 && code <= 804) {
            return R.string.light_clouds;
        } else {
            return R.string.clear;
        }
    }

    public static int weatherCodeToImageResource(int stringResId) {
        switch (stringResId) {
            case R.string.thunderstorm:
                return R.drawable.art_thunderstorms;
            case R.string.light_rain:
                return R.drawable.art_light_rain;
            case R.string.rain:
                return R.drawable.art_rain;
            case R.string.snow:
                return R.drawable.art_snow;
            case R.string.fog:
                return R.drawable.art_fog;
            case R.string.clouds:
                return R.drawable.art_clouds;
            case R.string.light_clouds:
                return R.drawable.art_light_clouds;
            default:
                return R.drawable.art_clear;
        }
    }

    // This is half-assed.
    public static int windDirectionToStringResource(float direction) {
        if ((direction >= 315 && direction <= 360) || (direction <= 45 && direction >= 0)) {
            return R.string.north;
        } else if (direction > 45 && direction <= 135) {
            return R.string.east;
        } else if (direction > 135 && direction <= 225) {
            return R.string.south;
        } else if (direction > 225 && direction <= 315) {
            return R.string.west;
        }
        return R.string.north;
    }

    public Date getDatetime() {
        return datetime;
    }

    public float getKelvin() {
        return kelvin;
    }

    public int getCelsius() {
        return Math.round(kelvin - 273.0f);
    }

    public int getFahrenheit() {
        return Math.round(((kelvin - 273.0f) * 9.0f / 5.0f) + 32f);
    }

    public int getHumidity() {
        return humidity;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public int getWeatherCodeStringResId() {
        return weatherCodeStringResId;
    }

    public int getWeatherCodeDrawableResId() {
        return weatherCodeDrawableResId;
    }

    public int getCloudCover() {
        return cloudCover;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getWindDirection() {
        return windDirection;
    }

    public int getWindDirectionStringRes() {
        return windDirectionStringRes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(datetime.getTime());
        dest.writeFloat(kelvin);
        dest.writeInt(humidity);
        dest.writeInt(weatherCode);
        dest.writeInt(weatherCodeStringResId);
        dest.writeInt(weatherCodeDrawableResId);
        dest.writeInt(cloudCover);
        dest.writeFloat(windSpeed);
        dest.writeFloat(windDirection);
        dest.writeInt(windDirectionStringRes);
    }
}
