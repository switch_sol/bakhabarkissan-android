package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public final class ProductCompany implements Parcelable {

    public static final Creator<ProductCompany> CREATOR = new Creator<ProductCompany>() {
        @Override
        public ProductCompany createFromParcel(Parcel in) {
            return new ProductCompany(in);
        }

        @Override
        public ProductCompany[] newArray(int size) {
            return new ProductCompany[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_CONTACT_PHONES = "contact_numbers";
    private final long id;
    private final String name;
    private final String imagePath;
    private final List<String> contactPhones;

    @JsonCreator
    public ProductCompany(@JsonProperty(FIELD_ID) long id,
                          @JsonProperty(FIELD_NAME) String name,
                          @JsonProperty(FIELD_IMAGE_PATH) String imagePath,
                          @JsonProperty(FIELD_CONTACT_PHONES) List<String> contactPhones) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
        this.contactPhones = contactPhones;
    }

    protected ProductCompany(Parcel in) {
        id = in.readLong();
        name = in.readString();
        imagePath = in.readString();
        contactPhones = in.createStringArrayList();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @JsonGetter(FIELD_CONTACT_PHONES)
    public List<String> getContactPhones() {
        return contactPhones;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(imagePath);
        parcel.writeStringList(contactPhones);
    }
}
