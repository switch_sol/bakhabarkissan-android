package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Video implements Parcelable {

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_VIDEO_PATH = "video_path";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_AD_VIDEO_PATH = "ad_video_path";
    private static final String FIELD_AD_IMAGE_PATH = "ad_image_path";
    private final long id;
    private final String name;
    private final String videoPath;
    private final String imagePath;
    private final double datetime;
    private final String adVideoPath;
    private final String adImagePath;

    @JsonCreator
    public Video(@JsonProperty(FIELD_ID) long id,
                 @JsonProperty(FIELD_NAME) String name,
                 @JsonProperty(FIELD_VIDEO_PATH) String videoPath,
                 @JsonProperty(FIELD_IMAGE_PATH) String imagePath,
                 @JsonProperty(FIELD_DATETIME) double datetime,
                 @JsonProperty(FIELD_AD_VIDEO_PATH) String adVideoPath,
                 @JsonProperty(FIELD_AD_IMAGE_PATH) String adImagePath) {
        this.id = id;
        this.name = name;
        this.videoPath = videoPath;
        this.imagePath = imagePath;
        this.datetime = datetime;
        this.adVideoPath = adVideoPath;
        this.adImagePath = adImagePath;
    }

    protected Video(Parcel in) {
        id = in.readLong();
        name = in.readString();
        videoPath = in.readString();
        imagePath = in.readString();
        datetime = in.readDouble();
        adVideoPath = in.readString();
        adImagePath = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_VIDEO_PATH)
    public String getVideoPath() {
        return videoPath;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @JsonGetter(FIELD_DATETIME)
    public double getDatetime() {
        return datetime;
    }

    @JsonGetter(FIELD_AD_VIDEO_PATH)
    public String getAdVideoPath() {
        return adVideoPath;
    }

    @JsonGetter(FIELD_AD_IMAGE_PATH)
    public String getAdImagePath() {
        return adImagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(videoPath);
        parcel.writeString(imagePath);
        parcel.writeDouble(datetime);
        parcel.writeString(adVideoPath);
        parcel.writeString(adImagePath);
    }
}
