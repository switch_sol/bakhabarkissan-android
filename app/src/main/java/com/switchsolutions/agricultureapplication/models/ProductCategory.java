package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ProductCategory implements Parcelable {

    public static final Creator<ProductCategory> CREATOR = new Creator<ProductCategory>() {
        @Override
        public ProductCategory createFromParcel(Parcel in) {
            return new ProductCategory(in);
        }

        @Override
        public ProductCategory[] newArray(int size) {
            return new ProductCategory[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private final long id;
    private final String name;
    private final String imagePath;

    @JsonCreator
    public ProductCategory(@JsonProperty(FIELD_ID) long id,
                           @JsonProperty(FIELD_NAME) String name,
                           @JsonProperty(FIELD_IMAGE_PATH) String imagePath) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
    }

    protected ProductCategory(Parcel in) {
        id = in.readLong();
        name = in.readString();
        imagePath = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(imagePath);
    }
}
