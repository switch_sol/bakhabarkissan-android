package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ProductDealer implements Parcelable {

    public static final Creator<ProductDealer> CREATOR = new Creator<ProductDealer>() {
        @Override
        public ProductDealer createFromParcel(Parcel in) {
            return new ProductDealer(in);
        }

        @Override
        public ProductDealer[] newArray(int size) {
            return new ProductDealer[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_LAT = "lat";
    private static final String FIELD_LON = "lon";
    private static final String FIELD_NUMBER = "number";
    private final long id;
    private final String name;
    private final String description;
    private final double lat;
    private final double lon;
    private final String number;

    @JsonCreator
    public ProductDealer(@JsonProperty(FIELD_ID) long id,
                         @JsonProperty(FIELD_NAME) String name,
                         @JsonProperty(FIELD_DESCRIPTION) String description,
                         @JsonProperty(FIELD_LAT) double lat,
                         @JsonProperty(FIELD_LON) double lon,
                         @JsonProperty(FIELD_NUMBER) String number) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.lat = lat;
        this.lon = lon;
        this.number = number;
    }

    protected ProductDealer(Parcel in) {
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        lat = in.readDouble();
        lon = in.readDouble();
        number = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_DESCRIPTION)
    public String getDescription() {
        return description;
    }

    @JsonGetter(FIELD_LAT)
    public double getLat() {
        return lat;
    }

    @JsonGetter(FIELD_LON)
    public double getLon() {
        return lon;
    }

    @JsonGetter(FIELD_NUMBER)
    public String getNumber() {
        return number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeDouble(lat);
        parcel.writeDouble(lon);
        parcel.writeString(number);
    }
}
