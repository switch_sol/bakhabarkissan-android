package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class VideoCategoryChild implements Parcelable {

    public static final Creator<VideoCategoryChild> CREATOR = new Creator<VideoCategoryChild>() {
        @Override
        public VideoCategoryChild createFromParcel(Parcel in) {
            return new VideoCategoryChild(in);
        }

        @Override
        public VideoCategoryChild[] newArray(int size) {
            return new VideoCategoryChild[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_COUNT = "count";
    private static final String FIELD_UPDATED_ON = "updated_on";
    private final long id;
    private final String name;
    private final String imagePath;
    private final int count;
    private final double updatedOn;

    @JsonCreator
    public VideoCategoryChild(@JsonProperty(FIELD_ID) long id,
                              @JsonProperty(FIELD_NAME) String name,
                              @JsonProperty(FIELD_IMAGE_PATH) String imagePath,
                              @JsonProperty(FIELD_COUNT) int count,
                              @JsonProperty(FIELD_UPDATED_ON) double updatedOn) {
        this.id = id;
        this.name = name;
        this.imagePath = imagePath;
        this.count = count;
        this.updatedOn = updatedOn;
    }

    protected VideoCategoryChild(Parcel in) {
        id = in.readLong();
        name = in.readString();
        imagePath = in.readString();
        count = in.readInt();
        updatedOn = in.readDouble();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @JsonGetter(FIELD_COUNT)
    public int getCount() {
        return count;
    }

    @JsonGetter(FIELD_UPDATED_ON)
    public double getUpdatedOn() {
        return updatedOn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(imagePath);
        parcel.writeInt(count);
        parcel.writeDouble(updatedOn);
    }
}
