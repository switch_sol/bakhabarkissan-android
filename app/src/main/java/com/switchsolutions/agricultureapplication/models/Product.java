package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Product implements Parcelable {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_RATE = "rate";
    private static final String FIELD_IMAGE_PATH = "image_path";
    private static final String FIELD_PROMOTED = "promoted";
    private static final String FIELD_COMPANY = "company";
    private final long id;
    private final String name;
    private final String rate;
    private final String imagePath;
    private final boolean promoted;
    private final ProductCompany company;

    @JsonCreator
    public Product(@JsonProperty(FIELD_ID) long id,
                   @JsonProperty(FIELD_NAME) String name,
                   @JsonProperty(FIELD_RATE) String rate,
                   @JsonProperty(FIELD_IMAGE_PATH) String imagePath,
                   @JsonProperty(FIELD_PROMOTED) boolean promoted,
                   @JsonProperty(FIELD_COMPANY) ProductCompany company) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.imagePath = imagePath;
        this.promoted = promoted;
        this.company = company;
    }

    protected Product(Parcel in) {
        id = in.readLong();
        name = in.readString();
        rate = in.readString();
        imagePath = in.readString();
        promoted = in.readByte() != 0;
        company = in.readParcelable(ProductCompany.class.getClassLoader());
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_NAME)
    public String getName() {
        return name;
    }

    @JsonGetter(FIELD_RATE)
    public String getRate() {
        return rate;
    }

    @JsonGetter(FIELD_IMAGE_PATH)
    public String getImagePath() {
        return imagePath;
    }

    @JsonGetter(FIELD_PROMOTED)
    public boolean isPromoted() {
        return promoted;
    }

    @JsonGetter(FIELD_COMPANY)
    public ProductCompany getCompany() {
        return company;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(rate);
        parcel.writeString(imagePath);
        parcel.writeByte((byte) (promoted ? 1 : 0));
        parcel.writeParcelable(company, i);
    }
}
