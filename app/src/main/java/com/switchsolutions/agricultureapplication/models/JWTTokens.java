package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class JWTTokens implements Parcelable {

    public static final Creator<JWTTokens> CREATOR = new Creator<JWTTokens>() {
        @Override
        public JWTTokens createFromParcel(Parcel in) {
            return new JWTTokens(in);
        }

        @Override
        public JWTTokens[] newArray(int size) {
            return new JWTTokens[size];
        }
    };
    private static final String FIELD_REQUEST = "request";
    private static final String FIELD_REFRESH = "refresh";
    private final String requestJwt;
    private final String refreshJwt;

    @JsonCreator
    public JWTTokens(@JsonProperty(FIELD_REQUEST) String requestJwt,
                     @JsonProperty(FIELD_REFRESH) String refreshJwt) {
        this.requestJwt = requestJwt;
        this.refreshJwt = refreshJwt;
    }

    protected JWTTokens(Parcel in) {
        requestJwt = in.readString();
        refreshJwt = in.readString();
    }

    @JsonGetter(FIELD_REQUEST)
    public String getRequestJwt() {
        return requestJwt;
    }

    @JsonGetter(FIELD_REFRESH)
    public String getRefreshJwt() {
        return refreshJwt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(requestJwt);
        parcel.writeString(refreshJwt);
    }
}
