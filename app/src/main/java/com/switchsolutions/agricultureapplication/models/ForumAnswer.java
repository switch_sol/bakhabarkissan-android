package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public final class ForumAnswer implements Parcelable {

    public static final Creator<ForumAnswer> CREATOR = new Creator<ForumAnswer>() {
        @Override
        public ForumAnswer createFromParcel(Parcel in) {
            return new ForumAnswer(in);
        }

        @Override
        public ForumAnswer[] newArray(int size) {
            return new ForumAnswer[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_IMAGES = "images";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_VOTED = "voted";
    private static final String FIELD_QUESTION_ID = "question_id";
    private static final String FIELD_ACCOUNT_ID = "account_id";
    private static final String FIELD_ACCOUNT_NAME = "account_name";
    private final long id;
    private final String text;
    private final List<String> images;
    private final double datetime;
    private final int score;
    private final int voted;
    private final long questionId;
    private final long accountId;
    private final String accountName;

    @JsonCreator
    public ForumAnswer(@JsonProperty(FIELD_ID) long id,
                       @JsonProperty(FIELD_TEXT) String text,
                       @JsonProperty(FIELD_IMAGES) List<String> images,
                       @JsonProperty(FIELD_DATETIME) double datetime,
                       @JsonProperty(FIELD_SCORE) int score,
                       @JsonProperty(FIELD_VOTED) int voted,
                       @JsonProperty(FIELD_QUESTION_ID) long questionId,
                       @JsonProperty(FIELD_ACCOUNT_ID) long accountId,
                       @JsonProperty(FIELD_ACCOUNT_NAME) String accountName) {
        this.id = id;
        this.text = text;
        this.images = images;
        this.datetime = datetime;
        this.score = score;
        this.voted = voted;
        this.questionId = questionId;
        this.accountId = accountId;
        this.accountName = accountName;
    }

    protected ForumAnswer(Parcel in) {
        id = in.readLong();
        text = in.readString();
        images = in.createStringArrayList();
        datetime = in.readDouble();
        score = in.readInt();
        voted = in.readInt();
        questionId = in.readLong();
        accountId = in.readLong();
        accountName = in.readString();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_TEXT)
    public String getText() {
        return text;
    }

    @JsonGetter(FIELD_IMAGES)
    public List<String> getImages() {
        return images;
    }

    @JsonGetter(FIELD_DATETIME)
    public double getDatetime() {
        return datetime;
    }

    @JsonGetter(FIELD_SCORE)
    public int getScore() {
        return score;
    }

    @JsonGetter(FIELD_VOTED)
    public int getVoted() {
        return voted;
    }

    @JsonGetter(FIELD_QUESTION_ID)
    public long getQuestionId() {
        return questionId;
    }

    @JsonGetter(FIELD_ACCOUNT_ID)
    public long getAccountId() {
        return accountId;
    }

    @JsonGetter(FIELD_ACCOUNT_NAME)
    public String getAccountName() {
        return accountName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(text);
        parcel.writeStringList(images);
        parcel.writeDouble(datetime);
        parcel.writeInt(score);
        parcel.writeInt(voted);
        parcel.writeLong(questionId);
        parcel.writeLong(accountId);
        parcel.writeString(accountName);
    }
}
