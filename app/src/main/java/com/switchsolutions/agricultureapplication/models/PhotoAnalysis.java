package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class PhotoAnalysis implements Parcelable {

    private static final String FIELD_ID = "id";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_AUDIO = "audio";
    private static final String FIELD_PHOTOS = "photos";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_STATUS = "status";
    private static final String FIELD_ACCOUNT_ID = "account_id";

    private final long id;
    private final String text;
    private final String audio;
    private final String[] photos;
    private final double datetime;
    private final int status;
    private final long accountId;

    @JsonCreator
    public PhotoAnalysis(@JsonProperty(FIELD_ID) long id,
                         @JsonProperty(FIELD_TEXT) String text,
                         @JsonProperty(FIELD_AUDIO) String audio,
                         @JsonProperty(FIELD_PHOTOS) String[] photos,
                         @JsonProperty(FIELD_DATETIME) double datetime,
                         @JsonProperty(FIELD_STATUS) int status,
                         @JsonProperty(FIELD_ACCOUNT_ID) long accountId) {
        this.id = id;
        this.text = text;
        this.audio = audio;
        this.photos = photos;
        this.datetime = datetime;
        this.status = status;
        this.accountId = accountId;
    }

    protected PhotoAnalysis(Parcel in) {
        id = in.readLong();
        text = in.readString();
        audio = in.readString();
        photos = in.createStringArray();
        datetime = in.readDouble();
        status = in.readInt();
        accountId = in.readLong();
    }

    @JsonGetter(FIELD_ID)
    public long getId() {
        return id;
    }

    @JsonGetter(FIELD_TEXT)
    public String getText() {
        return text;
    }

    @JsonGetter(FIELD_AUDIO)
    public String getAudio() {
        return audio;
    }

    @JsonGetter(FIELD_PHOTOS)
    public String[] getPhotos() {
        return photos;
    }

    @JsonGetter(FIELD_DATETIME)
    public double getDatetime() {
        return datetime;
    }

    @JsonGetter(FIELD_STATUS)
    public int getStatus() {
        return status;
    }

    @JsonGetter(FIELD_ACCOUNT_ID)
    public long getAccountId() {
        return accountId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(text);
        dest.writeString(audio);
        dest.writeStringArray(photos);
        dest.writeDouble(datetime);
        dest.writeInt(status);
        dest.writeLong(accountId);
    }

    public static final Creator<PhotoAnalysis> CREATOR = new Creator<PhotoAnalysis>() {
        @Override
        public PhotoAnalysis createFromParcel(Parcel in) {
            return new PhotoAnalysis(in);
        }

        @Override
        public PhotoAnalysis[] newArray(int size) {
            return new PhotoAnalysis[size];
        }
    };
}
