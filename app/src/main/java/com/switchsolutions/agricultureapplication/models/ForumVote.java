package com.switchsolutions.agricultureapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class ForumVote implements Parcelable {

    public static final Creator<ForumVote> CREATOR = new Creator<ForumVote>() {
        @Override
        public ForumVote createFromParcel(Parcel in) {
            return new ForumVote(in);
        }

        @Override
        public ForumVote[] newArray(int size) {
            return new ForumVote[size];
        }
    };
    private static final String FIELD_ID = "id";
    private static final String FIELD_SCORE = "score";
    private static final String FIELD_VOTED = "voted";
    private final long id;
    private final int score;
    private final int voted;

    @JsonCreator
    public ForumVote(@JsonProperty(FIELD_ID) long id,
                     @JsonProperty(FIELD_SCORE) int score,
                     @JsonProperty(FIELD_VOTED) int voted) {
        this.id = id;
        this.score = score;
        this.voted = voted;
    }

    protected ForumVote(Parcel in) {
        id = in.readLong();
        score = in.readInt();
        voted = in.readInt();
    }

    public long getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public int getVoted() {
        return voted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeInt(score);
        parcel.writeInt(voted);
    }
}
