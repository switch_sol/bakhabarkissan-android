package com.switchsolutions.agricultureapplication.utils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public final class PhoneNumberUtils {

    private static final PhoneNumberUtil PHONE_NUMBER_UTIL = PhoneNumberUtil.getInstance();
    private static final String REGION_PK = "PK";
    private static final List<String> CARRIER_CODES = Arrays.asList(
            "300", "301", "302", "303", "304", "305", "306", "307", "308", "309",   // Mobilink
            "310", "311", "312", "313", "314", "315", "316",                        // Zong
            "320", "321", "322", "323", "324",                                      // Warid
            "331", "332", "333", "334", "335", "336",                               // Ufone
            "340", "341", "342", "343", "344", "345", "346", "347", "348"           // Telenor
    );

    private PhoneNumberUtils() {
    }

    public static String formatNumber(String number) throws NumberParseException {
        Phonenumber.PhoneNumber pn = PHONE_NUMBER_UTIL.parse(number, REGION_PK);
        if (!PHONE_NUMBER_UTIL.isValidNumberForRegion(pn, REGION_PK)) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "Invalid Phone Number for Region.");
        }

        String formattedNumber = PHONE_NUMBER_UTIL.format(pn, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);

        String[] formattedNumberSplit = formattedNumber.split(Pattern.quote(" "));
        if (formattedNumberSplit.length != 3) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "Invalid Phone Number.");
        }

        if (!CARRIER_CODES.contains(formattedNumberSplit[1])) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "Invalid Phone Number.");
        }

        return formattedNumber;
    }

}
