package com.switchsolutions.agricultureapplication.utils;

import java.util.Random;

public final class RandomUtils {

    private static final byte[] ALPHANUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".getBytes();

    private static final Random RANDOM = new Random(System.currentTimeMillis());

    private RandomUtils() {
    }

    public static String randomAlphanumeric(int n) {
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            sb.append(ALPHANUMERIC[RANDOM.nextInt(ALPHANUMERIC.length)]);
        }
        return sb.toString();
    }
}
