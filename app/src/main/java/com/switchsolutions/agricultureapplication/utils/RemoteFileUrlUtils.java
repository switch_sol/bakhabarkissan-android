package com.switchsolutions.agricultureapplication.utils;

import com.switchsolutions.agricultureapplication.Constants;

import okhttp3.HttpUrl;

public final class RemoteFileUrlUtils {

    private static final HttpUrl URL_API = HttpUrl.parse(Constants.URL_AGRICULTURE_API);
    private static final HttpUrl URL_CONTENT = HttpUrl.parse(Constants.URL_AGRICULTURE_CONTENT);

    private static final String IMAGES = "images";
    private static final String THUMBS = "thumbs";
    private static final String VIDEOS = "videos";
    private static final String AUDIOS = "audios";
    private static final String ADS = "ads";

    private RemoteFileUrlUtils() {

    }

    public static String toImageUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(IMAGES)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toThumbUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(THUMBS)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toVideoUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(VIDEOS)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toAudioUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(AUDIOS)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toAdImageUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(ADS)
                .addPathSegment(IMAGES)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toAdThumbUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(ADS)
                .addPathSegment(THUMBS)
                .addPathSegment(filename)
                .build()
                .toString();
    }

    public static String toAdVideoUrl(String filename) {
        return URL_CONTENT
                .newBuilder()
                .addPathSegment(ADS)
                .addPathSegment(VIDEOS)
                .addPathSegment(filename)
                .build()
                .toString();
    }

}
