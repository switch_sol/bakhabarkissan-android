package com.switchsolutions.agricultureapplication.utils;

import android.app.Application;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.ContextThemeWrapper;

import java.util.Locale;

public class LocaleUtils {

    public static final Locale LOCALE_UR = new Locale("ur");
    public static final Locale LOCALE_SD = new Locale("sd");
    public static final Locale LOCALE_PS = new Locale("ps");
    public static final Locale LOCALE_EN = new Locale("en");

    private static Locale sLocale;
    private static String sCode;

    public static Locale getLocale() {
        return sLocale;
    }

    public static void setLocale(String localeCode) {
        sCode = localeCode;
        switch (localeCode) {
            case Languages.UR:
                sLocale = LOCALE_UR;
                break;
            case Languages.SD:
                sLocale = LOCALE_SD;
                break;
            case Languages.PS:
                sLocale = LOCALE_PS;
                break;
            default:
                sLocale = LOCALE_EN;
        }

        Locale.setDefault(sLocale);
    }

    public static String getCode() {
        return sCode;
    }

    public static void updateConfig(ContextThemeWrapper contextThemeWrapper) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration configuration = new Configuration();
            configuration.setLocale(sLocale);
            contextThemeWrapper.applyOverrideConfiguration(configuration);
        }
    }

    public static void updateConfig(Application application, Configuration oldConfiguration) {
        if (sLocale != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration newConfiguration = new Configuration(oldConfiguration);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                newConfiguration.setLocale(sLocale);
            } else {
                newConfiguration.locale = sLocale;
            }
            Resources resources = application.getBaseContext().getResources();
            resources.updateConfiguration(newConfiguration, resources.getDisplayMetrics());
        }
    }

}
