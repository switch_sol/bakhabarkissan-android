package com.switchsolutions.agricultureapplication.utils;

import android.location.Location;

public final class Coordinates {

    private final double lat;
    private final double lon;
    private final long datetime;

    public Coordinates(double lat, double lon, long datetime) {
        this.lat = lat;
        this.lon = lon;
        this.datetime = datetime;
    }

    public Coordinates(Location location) {
        this.lat = location.getLatitude();
        this.lon = location.getLongitude();
        this.datetime = location.getTime();
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public long getDatetime() {
        return datetime;
    }
}
