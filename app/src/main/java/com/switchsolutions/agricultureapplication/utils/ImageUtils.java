package com.switchsolutions.agricultureapplication.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public final class ImageUtils {

    private ImageUtils() {
    }

    public static void compressImage(File src, File dst) throws IOException {
        int rotate;
        ExifInterface exifInterface = new ExifInterface(src.getAbsolutePath());
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                rotate = 90;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                rotate = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotate = 270;
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                rotate = 0;
                break;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);

        Bitmap srcBitmap = BitmapFactory.decodeFile(src.getAbsolutePath());
        int nw;
        int nh;
        if (srcBitmap.getWidth() > srcBitmap.getHeight()) {
            float d = 512f / srcBitmap.getWidth();
            nw = 512;
            nh = Math.round(d * srcBitmap.getHeight());
        } else {
            float d = 512f / srcBitmap.getHeight();
            nh = 512;
            nw = Math.round(d * srcBitmap.getWidth());
        }
        srcBitmap = Bitmap.createScaledBitmap(srcBitmap, nw, nh, false);
        srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, false);
        srcBitmap.compress(Bitmap.CompressFormat.JPEG, 75, new FileOutputStream(dst));
        srcBitmap.recycle();
    }

}
