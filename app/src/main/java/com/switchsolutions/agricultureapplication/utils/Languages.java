package com.switchsolutions.agricultureapplication.utils;

public final class Languages {

    public static final String EN = "en";
    public static final String UR = "ur";
    public static final String PS = "ps";

    // TODO: This is a hack. Needs to be improved.
    public static final String SD = "sd";

    private Languages() {

    }

}
