package com.switchsolutions.agricultureapplication;

import android.content.Context;

import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClientModule;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClientModule;
import com.switchsolutions.agricultureapplication.api.cloud_messaging.CloudMessagingClient;
import com.switchsolutions.agricultureapplication.api.cloud_messaging.CloudMessagingClientModule;
import com.switchsolutions.agricultureapplication.api.crops.CropsClient;
import com.switchsolutions.agricultureapplication.api.crops.CropsClientModule;
import com.switchsolutions.agricultureapplication.api.farming_techniques.FarmingTechniquesClient;
import com.switchsolutions.agricultureapplication.api.farming_techniques.FarmingTechniquesClientModule;
import com.switchsolutions.agricultureapplication.api.forecast.ForecastClient;
import com.switchsolutions.agricultureapplication.api.forecast.ForecastClientModule;
import com.switchsolutions.agricultureapplication.api.forum.ForumClient;
import com.switchsolutions.agricultureapplication.api.forum.ForumClientModule;
import com.switchsolutions.agricultureapplication.api.google.GoogleClient;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientModule;
import com.switchsolutions.agricultureapplication.api.livestock.LivestockClient;
import com.switchsolutions.agricultureapplication.api.livestock.LivestockClientModule;
import com.switchsolutions.agricultureapplication.api.location.LocationClient;
import com.switchsolutions.agricultureapplication.api.location.LocationClientModule;
import com.switchsolutions.agricultureapplication.api.market_rates.MarketRatesClient;
import com.switchsolutions.agricultureapplication.api.market_rates.MarketRatesClientModule;
import com.switchsolutions.agricultureapplication.api.news.NewsClient;
import com.switchsolutions.agricultureapplication.api.news.NewsClientModule;
import com.switchsolutions.agricultureapplication.api.photo_analysis.PhotoAnalysisClient;
import com.switchsolutions.agricultureapplication.api.photo_analysis.PhotoAnalysisClientModule;
import com.switchsolutions.agricultureapplication.api.products.ProductsClient;
import com.switchsolutions.agricultureapplication.api.products.ProductsClientModule;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClientModule;
import com.switchsolutions.agricultureapplication.api.videos.VideosClient;
import com.switchsolutions.agricultureapplication.api.videos.VideosClientModule;
import com.switchsolutions.agricultureapplication.audio.AudioModule;
import com.switchsolutions.agricultureapplication.audio.AudioPlayer;
import com.switchsolutions.agricultureapplication.audio.AudioRecorder;
import com.switchsolutions.agricultureapplication.eventbus.EventBusModule;
import com.switchsolutions.agricultureapplication.network.NetworkModule;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.preferences.PreferencesModule;
import com.switchsolutions.agricultureapplication.storage.Storage;
import com.switchsolutions.agricultureapplication.storage.StorageModule;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                NewsClientModule.class,
                EventBusModule.class,
                PreferencesModule.class,
                ProductsClientModule.class,
                MarketRatesClientModule.class,
                ForecastClientModule.class,
                LocationClientModule.class,
                CropsClientModule.class,
                VideosClientModule.class,
                AnalyticsClientModule.class,
                AccountsClientModule.class,
                StorageModule.class,
                ForumClientModule.class,
                NetworkModule.class,
                RemoteConfigClientModule.class,
                CloudMessagingClientModule.class,
                GoogleClientModule.class,
                LivestockClientModule.class,
                AudioModule.class,
                PhotoAnalysisClientModule.class,
                FarmingTechniquesClientModule.class
        }
)
public interface ApplicationComponent {
    Context context();

    EventBus eventBus();

    Preferences preferences();

    Storage storage();

    AudioRecorder audioRecorder();

    AudioPlayer audioPlayer();

    GoogleClient googleClient();

    AnalyticsClient analyticsClient();

    RemoteConfigClient remoteConfigClient();

    CloudMessagingClient cloudMessagingClient();

    AccountsClient accountsClient();

    NewsClient newsClient();

    ProductsClient productsClient();

    MarketRatesClient marketRatesClient();

    ForecastClient forecastClient();

    LocationClient locationClient();

    CropsClient cropsClient();

    VideosClient videosClient();

    ForumClient forumClient();

    LivestockClient livestockClient();

    PhotoAnalysisClient photoAnalysisClient();

    FarmingTechniquesClient farmingTechniquesClient();
}
