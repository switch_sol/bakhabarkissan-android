package com.switchsolutions.agricultureapplication.widgets;

import android.os.Bundle;

public class DialogFragment extends android.support.v4.app.DialogFragment {

    public void setResult(int requestCode, Bundle bundle) {
        OnDialogResultListener listener = null;
        if (getTargetFragment() != null && getTargetFragment() instanceof OnDialogResultListener) {
            listener = (OnDialogResultListener) getTargetFragment();
        } else if (getParentFragment() != null && getParentFragment() instanceof OnDialogResultListener) {
            listener = (OnDialogResultListener) getParentFragment();
        } else if (getActivity() != null && getActivity() instanceof OnDialogResultListener) {
            listener = (OnDialogResultListener) getActivity();
        }

        if (listener != null) {
            listener.onDialogResult(requestCode, bundle);
        }
    }

    public interface OnDialogResultListener {
        void onDialogResult(int requestCode, Bundle bundle);
    }

}
