package com.switchsolutions.agricultureapplication.widgets;

import android.support.v7.app.AppCompatActivity;

import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

public class BaseActivity extends AppCompatActivity {

    public BaseActivity() {
        LocaleUtils.updateConfig(this);
    }

}
