package com.switchsolutions.agricultureapplication.screens.crops_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView viewTitle;
    private final ImageView viewImage;

    public ViewHolder(View itemView) {
        super(itemView);
        this.viewTitle = (TextView) itemView.findViewById(R.id.crop_title);
        this.viewImage = (ImageView) itemView.findViewById(R.id.crop_image);
    }

    void setTitle(String title) {
        viewTitle.setText(title);
    }

    void setImage(String imagePath) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .centerCrop())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage);
    }
}
