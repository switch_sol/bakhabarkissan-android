package com.switchsolutions.agricultureapplication.screens.video_categories_child_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.screens.videos_list.VideosListActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class VideoCategoriesChildListActivity extends BaseActivity implements VideoCategoriesChildListContract.View {

    private static final String KEY_CATEGORY_PARENT = "category_parent";

    private VideoCategoriesChildListContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;

    public static Intent createIntent(Context context, VideoCategoryParent parentCategory) {
        Intent intent = new Intent(context, VideoCategoriesChildListActivity.class);
        intent.putExtra(KEY_CATEGORY_PARENT, parentCategory);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_categories_child_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));
        viewList.setHasFixedSize(true);

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        VideoCategoryParent parentCategory = getIntent().getParcelableExtra(KEY_CATEGORY_PARENT);
        presenter = new VideoCategoriesChildListPresenter(this, parentCategory, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showVideosListScreen(VideoCategoryParent parentCategory, VideoCategoryChild childCategory) {
        Intent intent = VideosListActivity.createIntent(this, parentCategory, childCategory);
        startActivity(intent);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams swipeParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        swipeParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(swipeParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void setToolbarTitle(String title) {
        viewToolbar.setTitle(title);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyItemRangeInserted(int positionStart, int additionCount) {
        adapter.notifyItemRangeInserted(positionStart, additionCount);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringResId) {
        Snackbar.make(viewList, stringResId, Snackbar.LENGTH_LONG)
                .show();
    }
}
