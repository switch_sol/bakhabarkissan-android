package com.switchsolutions.agricultureapplication.screens.document_narrator;

import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.devbrackets.android.exomedia.EMAudioPlayer;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.html.HTMLElement;

import java.io.IOException;
import java.util.ArrayList;

public class DocumentNarratorFragment extends Fragment implements DocumentNarratorContract.View {

    private static final String KEY_ELEMENTS = "elements";

    private DocumentNarratorContract.Presenter presenter;

    private ImageView viewPauseResumeButton;
    private ProgressBar viewProgressBar;
    private ImageView viewPrevButton;
    private ImageView viewNextButton;

    private EMAudioPlayer audioPlayer;

    public static DocumentNarratorFragment newInstance(ArrayList<HTMLElement> elements) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_ELEMENTS, elements);
        DocumentNarratorFragment fragment = new DocumentNarratorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document_narration, container, false);

        viewPauseResumeButton = (ImageView) view.findViewById(R.id.track_pause_resume);
        viewPauseResumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPauseResumeButtonClicked();
            }
        });

        viewProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        viewPrevButton = (ImageView) view.findViewById(R.id.track_prev);
        viewPrevButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPrevButtonClicked();
            }
        });

        viewNextButton = (ImageView) view.findViewById(R.id.track_next);
        viewNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNextButtonClicked();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<HTMLElement> elements = getArguments().getParcelableArrayList(KEY_ELEMENTS);
        presenter = new DocumentNarratorPresenter(this, elements, BaseApplication.getComponent(getContext()));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        presenter.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        presenter.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroy();
    }

    @Override
    public void setPauseResumeButtonEnabled(boolean enabled) {
        viewPauseResumeButton.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
        viewPauseResumeButton.setEnabled(enabled);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewProgressBar.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setButtonToPause() {
        viewPauseResumeButton.setImageResource(R.drawable.ic_pause_circle_outline_white_48dp);
    }

    @Override
    public void setButtonToResume() {
        viewPauseResumeButton.setImageResource(R.drawable.ic_play_circle_outline_white_48dp);
    }

    @Override
    public void createMediaPlayer() {
        audioPlayer = new EMAudioPlayer(getContext());
        audioPlayer.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                presenter.onTrackStreamPrepared();
            }
        });
        audioPlayer.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                presenter.onTrackStreamFinished();
            }
        });
        audioPlayer.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError() {
                return presenter.onTrackStreamError();
            }
        });
    }

    @Override
    public void prepareTrack(String uriStr) throws IOException {
        Uri uri = Uri.parse(uriStr);
        audioPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        audioPlayer.setDataSource(getContext(), uri);
        audioPlayer.prepareAsync();
    }

    @Override
    public void startTrack() throws IllegalStateException {
        audioPlayer.start();
    }

    @Override
    public void pauseTrack() throws IllegalStateException {
        audioPlayer.pause();
    }

    @Override
    public void stopTrack() throws IllegalStateException {
        audioPlayer.stopPlayback();
    }

    @Override
    public void resetTrack() {
        audioPlayer.reset();
    }

    @Override
    public void releaseMediaPlayer() {
        audioPlayer.release();
        audioPlayer.setOnPreparedListener(null);
        audioPlayer.setOnCompletionListener(null);
        audioPlayer.setOnErrorListener(null);
        audioPlayer = null;
    }
}
