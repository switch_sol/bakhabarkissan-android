package com.switchsolutions.agricultureapplication.screens.ticker_forecast;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private final TickerForecastContract.Presenter presenter;

    public Adapter(TickerForecastContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_ticker_forecast, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        presenter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
