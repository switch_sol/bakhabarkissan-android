package com.switchsolutions.agricultureapplication.screens.account_login_verification;

public final class AccountVerificationEvents {

    private AccountVerificationEvents() {
    }

    public static final class OnVerificationCodeReceived {
        public final String code;

        public OnVerificationCodeReceived(String code) {
            this.code = code;
        }
    }
}
