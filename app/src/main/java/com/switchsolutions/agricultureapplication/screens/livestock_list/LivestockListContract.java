package com.switchsolutions.agricultureapplication.screens.livestock_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.Livestock;

public interface LivestockListContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showLivestockPage(Livestock livestock);

        void showBanner();

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder viewHolder, int position);

        void onListItemClicked(int position);
    }
}
