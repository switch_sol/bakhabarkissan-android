package com.switchsolutions.agricultureapplication.screens.video_container;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.Video;

public interface VideoContainerContract {

    interface View {

        void navigateUp();

        void finish(@StringRes int errorStringRes);

        void showVideoPlaybackScreen(Video video);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();
    }
}
