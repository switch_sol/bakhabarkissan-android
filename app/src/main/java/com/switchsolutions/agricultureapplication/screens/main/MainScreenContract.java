package com.switchsolutions.agricultureapplication.screens.main;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

public interface MainScreenContract {

    interface View {

        void requestSpeechRecognition();

        void showFarmToHome();

        void showLoginScreen();

        void showNewsScreen();

        void showCropsScreen();

        void showLivestockScreen();

        void showModernFarmingTechniquesScreen();

        void showProductsScreen();

        void showForecastScreen();

        void showMarketRatesScreen();

        void showVideosScreen();

        void showVisualAnalysisScreen();

        void showForumScreen();

        void showContactScreen(String number);

        void showWebPageScreen(String url);

        void showForecastTickerScreen();

        void enterForecastTickerScreen();

        void showSettingsScreen();

        void setListAdapter();

        void setGridAdapter();

        void animateBlurIn();

        void animateBlurOut();

        void setMenuItemSwitchViewIcon(@DrawableRes int drawableRes);

        void setMenuItemSwitchViewText(@StringRes int stringRes);

        void animateEntrance();

        void scrollToCleanestPosition();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        boolean onMenuItemSettingsClicked();

        boolean onMenuItemSpeechClicked();

        boolean onMenuItemSwitchViewClicked();

        int getItemCount();

        void onBindViewHolder(GridViewHolder viewHolder, int position);

        void onBindViewHolder(ListViewHolder viewHolder, int position);

        int getSpanSize(int position);

        void onListItemClicked(int position);

        void onSpeechRecognitionResult(String result);

        void onBlurAnimationCompleted();

        void onScrollStateChanged(int newState);
    }

}
