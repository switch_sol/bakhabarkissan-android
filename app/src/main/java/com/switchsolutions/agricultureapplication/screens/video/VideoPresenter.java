package com.switchsolutions.agricultureapplication.screens.video;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class VideoPresenter implements VideoContract.Presenter {

    private final VideoContract.View view;
    private final Video video;
    private final EventBus eventBus;
    private final AnalyticsClient analyticsClient;

    private boolean isAdvertising = true;
    private Subscription subscription;

    VideoPresenter(VideoContract.View view, Video video, ApplicationComponent component) {
        this.view = view;
        this.video = video;
        this.eventBus = component.eventBus();
        this.analyticsClient = component.analyticsClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (video.getAdVideoPath() != null && !"".equals(video.getAdVideoPath())) {
                isAdvertising = true;

                view.setControlsEnabled(false);
                view.setBottomControlsVisible(true);
                view.setBottomControlsEnabled(false);
                view.setBottomControlsText(R.string.sponsored_video);
                view.setVideoImage(RemoteFileUrlUtils.toAdThumbUrl(video.getAdImagePath()), RemoteFileUrlUtils.toAdImageUrl(video.getAdImagePath()));
                view.setVideo(RemoteFileUrlUtils.toAdVideoUrl(video.getAdVideoPath()));
                return;
            }
        }
        isAdvertising = false;

        view.setControlsEnabled(true);
        view.setBottomControlsVisible(false);
        view.setBottomControlsEnabled(false);
        view.setVideoImage(RemoteFileUrlUtils.toThumbUrl(video.getImagePath()), RemoteFileUrlUtils.toVideoUrl(video.getImagePath()));
        view.setVideo(RemoteFileUrlUtils.toVideoUrl(video.getVideoPath()));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    @Override
    public void onVideoPrepared() {
        view.startVideo();

        if (isAdvertising) {
            subscription = Observable
                    .timer(10, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .first()
                    .subscribe(new Action1<Long>() {
                        @Override
                        public void call(Long aLong) {
                            view.setBottomControlsText(R.string.skip);
                            view.setBottomControlsEnabled(true);
                            view.setBottomControlsVisible(true);
                        }
                    });

            analyticsClient.sendViewedVideoAdvertisementEvent(video.getAdVideoPath());
        }
    }

    @Override
    public void onVideoCompleted() {
        if (isAdvertising) {
            isAdvertising = false;

            view.setControlsEnabled(true);
            view.setBottomControlsEnabled(false);
            view.setBottomControlsVisible(false);
            view.setVideoImage(RemoteFileUrlUtils.toThumbUrl(video.getImagePath()), RemoteFileUrlUtils.toVideoUrl(video.getImagePath()));
            view.setVideo(RemoteFileUrlUtils.toVideoUrl(video.getVideoPath()));
        } else {
            eventBus.post(new VideoEvents.OnVideoCompleted());
        }
    }

    @Override
    public boolean onVideoError() {
        return false;
    }

    @Override
    public void onBottomControlClicked() {
        isAdvertising = false;

        view.setControlsEnabled(true);
        view.setBottomControlsVisible(false);
        view.setBottomControlsEnabled(false);
        view.setVideoImage(RemoteFileUrlUtils.toThumbUrl(video.getImagePath()), RemoteFileUrlUtils.toVideoUrl(video.getImagePath()));
        view.setVideo(RemoteFileUrlUtils.toVideoUrl(video.getVideoPath()));
    }
}
