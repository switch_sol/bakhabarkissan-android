package com.switchsolutions.agricultureapplication.screens.product_dealers_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public interface ProductDealersListContract {

    interface View {

        void navigateUp();

        void showLocationSettingsScreen();

        void showLoginScreen();

        void showBanner();

        void showCallScreen(String number);

        void showMapScreen(double lat, double lon);

        void setBottomSheetState(int state);

        void setFocusedDealerDistance(double kilometers);

        void setFocusedDealerName(String name);

        void setFocusedDealerDescription(String description);

        void setFocusedDealerMarker(String dealerName, double dealerLat, double dealerLon);

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);

        void showError(@StringRes int actionStringRes, @StringRes int errorStringRes);

        void hideError();

        void requestPermissions(@NonNull String[] permissions, int requestCode);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        boolean onBackPressed();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder holder, int position);

        void onListItemClicked(int position);

        void onMapReady();

        void onBottomSheetStateChanged(int newState);

        void onCallDealerButtonClicked();

        void onMapClicked(double lat, double lon);

        void onActionButtonClicked();

        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        void onLocationSettingsScreenResult();
    }
}
