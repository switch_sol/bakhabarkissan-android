package com.switchsolutions.agricultureapplication.screens.banner;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

public class BannerFragment extends Fragment implements BannerContract.View {

    private BannerContract.Presenter presenter;

    private ImageView viewImage;

    public static BannerFragment newInstance() {
        Bundle args = new Bundle();
        BannerFragment fragment = new BannerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewImage = (ImageView) inflater.inflate(R.layout.fragment_banner, container, false);
        return viewImage;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter = new BannerPresenter(this, BaseApplication.getComponent(getContext()));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        presenter.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void setImage(String imagePath) {
        Glide.with(getContext())
                .load(RemoteFileUrlUtils.toAdImageUrl(imagePath))
                .thumbnail(Glide.with(getContext())
                        .load(RemoteFileUrlUtils.toAdThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .fitCenter())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .fitCenter()
                .into(viewImage);
    }
}
