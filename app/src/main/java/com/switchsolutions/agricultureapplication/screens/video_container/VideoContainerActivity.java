package com.switchsolutions.agricultureapplication.screens.video_container;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.screens.video.VideoFragment;
import com.switchsolutions.agricultureapplication.screens.videos_list.VideosListActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class VideoContainerActivity extends BaseActivity implements VideoContainerContract.View {

    private static final String KEY_VIDEO = "video";

    private VideoContainerContract.Presenter presenter;

    public static Intent createIntent(Context context, Video video) {
        Intent intent = new Intent(context, VideoContainerActivity.class);
        intent.putExtra(KEY_VIDEO, video);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View decorView = getWindow().getDecorView();
        int decorFlags = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            decorFlags |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        decorView.setSystemUiVisibility(decorFlags);

        setContentView(R.layout.activity_video_container);

        Video video = getIntent().getParcelableExtra(KEY_VIDEO);
        presenter = new VideoContainerPresenter(this, video, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void finish(@StringRes int errorStringRes) {
        Intent intent = VideosListActivity.createResultIntent(errorStringRes);
        setResult(Activity.RESULT_CANCELED, intent);
    }

    @Override
    public void showVideoPlaybackScreen(Video video) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_video, VideoFragment.newInstance(video))
                .commit();
    }
}
