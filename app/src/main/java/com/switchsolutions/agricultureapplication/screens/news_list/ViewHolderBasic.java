package com.switchsolutions.agricultureapplication.screens.news_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

final class ViewHolderBasic extends RecyclerView.ViewHolder {

    private final TextView viewTitle;
    private final TextView viewSubheading;
    private final TextView viewDate;

    ViewHolderBasic(View itemView) {
        super(itemView);

        viewTitle = (TextView) itemView.findViewById(R.id.article_title);
        viewSubheading = (TextView) itemView.findViewById(R.id.article_subheading);
        viewDate = (TextView) itemView.findViewById(R.id.article_date);
    }

    void setTitle(String title) {
        viewTitle.setText(title);
    }

    void setSubheading(String subheading) {
        viewSubheading.setText(subheading);
    }

    void setDate(String date) {
        viewDate.setText(date);
    }

}
