package com.switchsolutions.agricultureapplication.screens.crops_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.crops.CropsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.Crop;
import com.switchsolutions.agricultureapplication.models.CropCategory;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class CropsListScreenPresenter implements CropsListScreenContract.Presenter {

    private static final String SCREEN_NAME = "Crops List";

    private final CropsListScreenContract.View view;
    private final CropCategory cropCategory;
    private final CropsClient cropsClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<Crop> crops;
    private Subscription subscription;

    public CropsListScreenPresenter(CropsListScreenContract.View view, CropCategory cropCategory, ApplicationComponent component) {
        this.view = view;
        this.cropCategory = cropCategory;
        this.cropsClient = component.cropsClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setProgressBarEnabled(true);
        view.setAdapter();
        view.setToolbarTitle(cropCategory.getName());

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return crops != null ? crops.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Crop crop = crops.get(position);

        viewHolder.setTitle(crop.getName());
        viewHolder.setImage(crop.getImagePath());
    }

    @Override
    public void onListItemClicked(int position) {
        Crop crop = crops.get(position);

        analyticsClient.sendCropSelectedEvent(crop.getId());
        view.showCropPageScreen(crop);
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        subscription = cropsClient
                .getCrops(cropCategory.getId())
                .map(new Func1<List<Crop>, ArrayList<Crop>>() {
                    @Override
                    public ArrayList<Crop> call(List<Crop> response) {
                        return new ArrayList<>(response);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Crop>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Crop> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get crops.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleResponse(ArrayList<Crop> response) {
        crops = response;

        view.notifyDataSetChanged();
        view.setProgressBarEnabled(false);
    }
}
