package com.switchsolutions.agricultureapplication.screens.video_container;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.screens.video.VideoEvents;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

final class VideoContainerPresenter implements VideoContainerContract.Presenter {

    private final VideoContainerContract.View view;
    private final Video video;
    private final EventBus eventBus;

    VideoContainerPresenter(VideoContainerContract.View view, Video video, ApplicationComponent component) {
        this.view = view;
        this.video = video;
        this.eventBus = component.eventBus();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            view.showVideoPlaybackScreen(video);
        }

        eventBus.register(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Subscribe
    public void onEvent(VideoEvents.OnVideoError event) {
        view.finish(R.string.an_error_occurred);
    }

    @Subscribe
    public void onEvent(VideoEvents.OnVideoCompleted event) {
        view.navigateUp();
    }
}
