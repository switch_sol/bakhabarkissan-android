package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class AdapterMarketRates extends RecyclerView.Adapter<ViewHolderMarketRate> {

    private final MarketRatesListContract.Presenter presenter;

    AdapterMarketRates(MarketRatesListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ViewHolderMarketRate onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_market_rates_list_market_rate_item, parent, false);
        return new ViewHolderMarketRate(view);
    }

    @Override
    public void onBindViewHolder(ViewHolderMarketRate holder, int position) {
        presenter.onBindMarketRateViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getMarketRatesItemCount();
    }
}
