package com.switchsolutions.agricultureapplication.screens.document_narrator;

import com.switchsolutions.agricultureapplication.html.HTMLElement;

public final class DocumentNarratorEvents {

    private DocumentNarratorEvents() {

    }

    public static final class PrepareTrack {
        public final HTMLElement element;

        public PrepareTrack(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnTrackPrepared {
        public final HTMLElement element;

        public OnTrackPrepared(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnTrackStarted {
        public final HTMLElement element;

        public OnTrackStarted(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnTrackPaused {
        public final HTMLElement element;

        public OnTrackPaused(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnTrackStopped {
        public final HTMLElement element;

        public OnTrackStopped(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnTrackReset {
        public final HTMLElement element;

        public OnTrackReset(HTMLElement element) {
            this.element = element;
        }
    }

    public static final class OnError {
        public final int stringRes;

        public OnError(int stringRes) {
            this.stringRes = stringRes;
        }
    }
}
