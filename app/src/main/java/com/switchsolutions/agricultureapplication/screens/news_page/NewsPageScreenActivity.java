package com.switchsolutions.agricultureapplication.screens.news_page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLRecyclerView;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.NewsArticle;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class NewsPageScreenActivity extends BaseActivity implements NewsPageScreenContract.View {

    private static final String KEY_NEWS_ARTICLE = "news_article";

    private NewsPageScreenContract.Presenter presenter;

    private Toolbar viewToolbar;
    private SwipeRefreshLayout viewSwipeRefreshLayout;
    private HTMLRecyclerView viewContent;

    public static Intent createIntent(Context context, NewsArticle newsArticle) {
        Intent intent = new Intent(context, NewsPageScreenActivity.class);
        intent.putExtra(KEY_NEWS_ARTICLE, newsArticle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_page);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        viewContent = (HTMLRecyclerView) findViewById(R.id.content_view);
        viewContent.setOnElementClickListener(new HTMLRecyclerView.OnElementClickListener() {
            @Override
            public void onElementClicked(HTMLHeader header) {
                presenter.onHTMLElementClicked(header);
            }

            @Override
            public void onElementClicked(HTMLParagraph paragraph) {
                presenter.onHTMLElementClicked(paragraph);
            }

            @Override
            public void onElementClicked(HTMLImage image) {
                presenter.onHTMLElementClicked(image);
            }

            @Override
            public void onElementClicked(HTMLTable table) {
                presenter.onHTMLElementClicked(table);
            }

            @Override
            public void onElementClicked(HTMLUnorderedList unorderedList) {
                presenter.onHTMLElementClicked(unorderedList);
            }

            @Override
            public void onElementClicked(HTMLOrderedList orderedList) {
                presenter.onHTMLElementClicked(orderedList);
            }
        });

        NewsArticle newsArticle = getIntent().getParcelableExtra(KEY_NEWS_ARTICLE);

        presenter = new NewsPageScreenPresenter(this, newsArticle, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void setContent(List<HTMLElement> elements) {
        viewContent.setContent(elements);
    }

    @Override
    public void setContentItemInFocus(int position) {
        viewContent.setFocusedPosition(position);
    }

    @Override
    public void clearContentItemInFocus() {
        viewContent.clearFocusedPosition();
    }

    @Override
    public void setNarrator(ArrayList<HTMLElement> elements) {
        CoordinatorLayout.LayoutParams oldLayoutParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        oldLayoutParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.narrator_container_height);
        viewSwipeRefreshLayout.setLayoutParams(oldLayoutParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_narration, DocumentNarratorFragment.newInstance(elements))
                .commit();
    }

    @Override
    public void scrollToContentPosition(int position) {
        viewContent.smoothScrollToPosition(position);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewContent, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }
}
