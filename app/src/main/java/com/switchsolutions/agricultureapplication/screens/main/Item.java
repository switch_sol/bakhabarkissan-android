package com.switchsolutions.agricultureapplication.screens.main;

import com.switchsolutions.agricultureapplication.R;

enum Item {

    NEWS(R.drawable.image_news, R.string.news, R.string.feature_description_news),
    PRODUCTS(R.drawable.image_products, R.string.products, R.string.feature_description_products),
    CROPS(R.drawable.image_crops, R.string.crops, R.string.feature_description_crops),
    LIVESTOCK(R.drawable.image_livestock, R.string.livestock, R.string.feature_description_livestock),
    MARKET_RATES(R.drawable.image_market_rates, R.string.mandi_rates, R.string.feature_description_mandi_rates),
    FORECAST(R.drawable.image_weather, R.string.weather, R.string.feature_description_weather),
    VIDEOS(R.drawable.image_videos, R.string.videos, R.string.feature_description_videos),
    CONTACT(R.drawable.image_contact_us, R.string.contact_us, R.string.feature_description_contact_us),
    VISUAL_ANALYSIS(R.drawable.image_visual_analysis, R.string.visual_analysis, R.string.feature_description_visual_analysis),
    FARM_TO_HOME(R.drawable.img_farm_banner, R.string.farm_to_home, R.string.feature_description_farm_to_home),
    ABOUT_US(R.drawable.image_about_us, R.string.about, R.string.feature_description_about_us),
    MODERN_FARMING_TECHNIQUES(R.drawable.image_modern_farming_techniques, R.string.modern_farming_techniques, R.string.feature_description_modern_farming_techniques);

    final int imageResId;
    final int titleResId;
    final int descriptionResId;

    Item(int imageResId, int titleResId, int descriptionResId) {
        this.imageResId = imageResId;
        this.titleResId = titleResId;
        this.descriptionResId = descriptionResId;
    }
}
