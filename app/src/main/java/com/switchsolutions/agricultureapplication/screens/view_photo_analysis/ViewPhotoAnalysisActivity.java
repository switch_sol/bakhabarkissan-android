package com.switchsolutions.agricultureapplication.screens.view_photo_analysis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.screens.view_photo_analysis_response.ViewPhotoAnalysisResponseActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.util.Locale;

public class ViewPhotoAnalysisActivity extends BaseActivity implements ViewPhotoAnalysisContract.View {

    private static final String KEY_PHOTO_ANALYSIS = "photo_analysis";

    private ViewPhotoAnalysisContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private TextView viewTag;
    private TextView viewText;
    private ViewGroup viewAudioContainer;
    private TextView viewAudioLength;
    private FloatingActionButton viewAudioFAB;
    private ProgressBar viewAudioProgressBar;
    private RecyclerView viewImagesList;
    private Button viewActionButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo_analysis);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewTag = (TextView) findViewById(R.id.tag);

        viewText = (TextView) findViewById(R.id.issue_text);

        viewAudioContainer = (ViewGroup) findViewById(R.id.audio_container);

        viewAudioFAB = (FloatingActionButton) findViewById(R.id.audio_fab);
        viewAudioFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAudioFABClicked();
            }
        });

        viewAudioProgressBar = (ProgressBar) findViewById(R.id.audio_progress_bar);

        viewAudioLength = (TextView) findViewById(R.id.audio_length);

        viewImagesList = (RecyclerView) findViewById(R.id.images_list);
        viewImagesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        viewImagesList.setHasFixedSize(true);

        viewActionButton = (Button) findViewById(R.id.action_button);
        viewActionButton.setEnabled(false);
        viewActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onActionButtonClicked();
            }
        });

        PhotoAnalysis pa = getIntent().getParcelableExtra(KEY_PHOTO_ANALYSIS);
        presenter = new ViewPhotoAnalysisPresenter(this, BaseApplication.getComponent(this), pa);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        presenter.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
    }

    @Override
    public void showResponsePage(PhotoAnalysis photoAnalysis) {
        Intent intent = ViewPhotoAnalysisResponseActivity.createIntent(this, photoAnalysis);
        startActivity(intent);
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void setTag(@StringRes int stringRes, @ColorRes int colorRes) {
        viewTag.setText(getString(stringRes));
        viewTag.setBackgroundColor(ContextCompat.getColor(this, colorRes));
    }

    @Override
    public void hideAudioContainer() {
        viewAudioContainer.setVisibility(View.GONE);
    }

    @Override
    public void showActionButton() {
        viewActionButton.setEnabled(true);
        viewActionButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void setAudioFABIcon(@DrawableRes int drawableRes) {
        viewAudioFAB.setImageResource(drawableRes);
    }

    @Override
    public void setAudioFABEnabled(boolean enabled) {
        viewAudioFAB.setEnabled(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewImagesList, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void setIssueText(String text) {
        viewText.setText(text);
    }

    @Override
    public void setAudioProgressBarLimit(int n) {
        viewAudioProgressBar.setMax(n);
    }

    @Override
    public void setAudioProgressBar(int i) {
        viewAudioProgressBar.setProgress(i);
    }

    @Override
    public void clearAudioLength() {
        viewAudioLength.setText("");
    }

    @Override
    public void setAudioLength(int i, int n) {
        int im = i / 60;
        int is = i % 60;
        int nm = n / 60;
        int ns = n % 60;
        viewAudioLength.setText(String.format(Locale.US, "%02d:%02d / %02d:%02d", im, is, nm, ns));
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewImagesList.setAdapter(adapter);
    }

    public static Intent createIntent(Context context, PhotoAnalysis photoAnalysis) {
        Intent intent = new Intent(context, ViewPhotoAnalysisActivity.class);
        intent.putExtra(KEY_PHOTO_ANALYSIS, photoAnalysis);
        return intent;
    }
}
