package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public interface MarketRatesListContract {

    interface View {

        void navigateUp();

        void showLocationSettingsScreen();

        void showLoginScreen();

        void showBanner();

        void setBottomSheetState(int state);

        void setMarketsAdapter();

        void notifyMarketsDataSetChanged();

        void notifyMarketsItemRangeInserted(int positionStart, int additionCount);

        void setMarketRatesAdapter();

        void notifyMarketRatesDataSetChanged();

        void setMarketsProgressBarEnabled(boolean enabled);

        void setMarketRatesProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes, boolean actionable);

        void showError(@StringRes int actionStringRes, @StringRes int errorStringRes, boolean actionable);

        void hideError();

        void requestPermissions(@NonNull String[] permissions, int requestCode);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        boolean onBackPressed();

        void onNavigationButtonClicked();

        int getMarketsItemCount();

        void onBindMarketViewHolder(ViewHolderMarket holder, int position);

        void onMarketListItemClicked(int position);

        int getMarketRatesItemCount();

        void onBindMarketRateViewHolder(ViewHolderMarketRate holder, int position);

        void onBottomSheetStateChanged(int newState);

        void onActionButtonClicked();

        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        void onLocationSettingsScreenResult();
    }
}
