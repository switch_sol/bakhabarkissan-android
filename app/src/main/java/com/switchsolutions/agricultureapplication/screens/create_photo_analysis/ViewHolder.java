package com.switchsolutions.agricultureapplication.screens.create_photo_analysis;

import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.switchsolutions.agricultureapplication.R;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final ImageView viewIcon;

    ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.image);
        viewIcon = (ImageView) itemView.findViewById(R.id.icon);
    }

    void setImage(String filepath) {
        if (filepath == null) {
            viewImage.setImageDrawable(null);
        } else {
            Glide.with(itemView.getContext())
                    .load(filepath)
                    .fitCenter()
                    .dontAnimate()
                    .into(viewImage);
        }
    }

    void setIcon(@DrawableRes int drawableRes) {
        if (drawableRes == 0) {
            viewIcon.setImageDrawable(null);
        } else {
            viewIcon.setImageResource(drawableRes);
        }
    }

}
