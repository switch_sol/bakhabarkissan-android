package com.switchsolutions.agricultureapplication.screens.livestock_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.livestock.LivestockClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.Livestock;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class LivestockListPresenter implements LivestockListContract.Presenter {

    private static final int PAGINATION_LIMIT = 10;

    private static final String SCREEN_NAME = "Livestock List";

    private final LivestockListContract.View view;
    private final LivestockClient livestockClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<Livestock> livestockList;
    private Subscription subscription;
    private boolean shouldRequestNext = true;

    LivestockListPresenter(LivestockListContract.View view, ApplicationComponent component) {
        this.view = view;
        this.livestockClient = component.livestockClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setAdapter();

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return livestockList != null ? livestockList.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Livestock livestock = livestockList.get(position);

        viewHolder.setTitle(livestock.getName());
        viewHolder.setImage(livestock.getImagePath());

        if (shouldRequestNext && position == livestockList.size() - 1) {
            requestContent();
        }
    }

    @Override
    public void onListItemClicked(int position) {
        Livestock livestock = livestockList.get(position);

        analyticsClient.sendLivestockSelectedEvent(livestock.getId());

        view.showLivestockPage(livestock);
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        subscription = livestockClient
                .getLivestock(PAGINATION_LIMIT, livestockList != null ? livestockList.size() : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Livestock>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Livestock> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get content.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleResponse(ArrayList<Livestock> response) {
        shouldRequestNext = response.size() == PAGINATION_LIMIT;

        if (livestockList == null) {
            livestockList = response;
            view.notifyDataSetChanged();
        } else {
            livestockList.addAll(response);
            view.notifyItemRangeInserted(livestockList.size() - response.size(), response.size());
        }

        view.setProgressBarEnabled(false);
    }
}
