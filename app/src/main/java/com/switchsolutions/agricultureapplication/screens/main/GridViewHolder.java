package com.switchsolutions.agricultureapplication.screens.main;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.switchsolutions.agricultureapplication.R;

final class GridViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewTitle;

    GridViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.image);
        viewTitle = (TextView) itemView.findViewById(R.id.title);
    }

    public void setImage(@DrawableRes int drawableRes) {
        Glide.with(itemView.getContext())
                .load(drawableRes)
                .centerCrop()
                .into(viewImage);
    }

    public void setTitle(@StringRes int stringRes) {
        viewTitle.setText(stringRes);
    }
}
