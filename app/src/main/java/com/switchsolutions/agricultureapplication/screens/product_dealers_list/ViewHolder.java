package com.switchsolutions.agricultureapplication.screens.product_dealers_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView viewProductDealerName;
    private final TextView viewProductDealerDescription;
    private final TextView viewProductDealerNumber;

    ViewHolder(View itemView) {
        super(itemView);

        viewProductDealerName = (TextView) itemView.findViewById(R.id.product_dealer_name);
        viewProductDealerDescription = (TextView) itemView.findViewById(R.id.product_dealer_description);
        viewProductDealerNumber = (TextView) itemView.findViewById(R.id.product_dealer_number);
    }

    void setName(String name) {
        viewProductDealerName.setText(name);
    }

    void setDescription(String description) {
        viewProductDealerDescription.setText(description);
    }

    void setNumber(String number) {
        viewProductDealerNumber.setText(number);
    }

}
