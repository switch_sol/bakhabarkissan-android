package com.switchsolutions.agricultureapplication.screens.main;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class GridAdapter extends RecyclerView.Adapter<GridViewHolder> {

    private final MainScreenContract.Presenter presenter;

    GridAdapter(MainScreenContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final TypedArray styledAttributes = parent.getContext().getTheme().obtainStyledAttributes(new int[] {android.R.attr.actionBarSize});
        int th = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        int bh = parent.getContext().getResources().getDimensionPixelSize(R.dimen.ticker_container_height);
        int h = Resources.getSystem().getDisplayMetrics().heightPixels;

        int sh = 0;
        int shrid = parent.getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (shrid > 0) {
            sh = parent.getContext().getResources().getDimensionPixelSize(shrid);
        }

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.viewholder_main_grid_basic_item, parent, false);

        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
        lp.height = (h - th - bh - sh) / 3;
        view.setLayoutParams(lp);

        final GridViewHolder viewHolder = new GridViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, int position) {
        presenter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

}
