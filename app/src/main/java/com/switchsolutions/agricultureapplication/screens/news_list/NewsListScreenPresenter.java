package com.switchsolutions.agricultureapplication.screens.news_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.news.NewsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.NewsArticle;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Languages;
import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

final class NewsListScreenPresenter implements NewsListScreenContract.Presenter {

    private static final String SCREEN_NAME = "News List";

    private static final int PAGINATION_LIMIT = 10;

    private final NewsListScreenContract.View view;
    private final NewsClient newsClient;
    private final RemoteConfigClient remoteConfigClient;
    private final AnalyticsClient analyticsClient;
    private final Preferences preferences;

    private DateFormat dateFormat;

    private ArrayList<NewsArticle> newsArticles;
    private Subscription subscription;
    private boolean shouldRequestNext = true;

    NewsListScreenPresenter(NewsListScreenContract.View view, ApplicationComponent component) {
        this.view = view;
        this.newsClient = component.newsClient();
        this.remoteConfigClient = component.remoteConfigClient();
        this.analyticsClient = component.analyticsClient();
        this.preferences = component.preferences();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (preferences.getLanguage().equals(Languages.EN)) {
            dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, LocaleUtils.LOCALE_EN);
        } else {
            dateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, LocaleUtils.LOCALE_UR);
        }

        view.setProgressBarEnabled(true);
        onRequestRefresh();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onRequestRefresh() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        subscription = newsClient
                .getNews(PAGINATION_LIMIT, 0)
                .map(new Func1<List<NewsArticle>, ArrayList<NewsArticle>>() {
                    @Override
                    public ArrayList<NewsArticle> call(List<NewsArticle> newsArticles) {
                        return new ArrayList<NewsArticle>(newsArticles);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<NewsArticle>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<NewsArticle> response) {
                        handleFreshResponse(response);
                    }
                });
    }

    @Override
    public int getItemViewType(int position) {
        return newsArticles.get(position).getImagePath() == null ? Adapter.VIEW_TYPE_BASIC : Adapter.VIEW_TYPE_IMAGE;
    }

    @Override
    public int getItemCount() {
        return newsArticles == null ? 0 : newsArticles.size();
    }

    @Override
    public void onBindViewHolder(ViewHolderBasic viewHolder, int position) {
        NewsArticle article = newsArticles.get(position);
        viewHolder.setTitle(article.getTitle());
        viewHolder.setSubheading(article.getSubheading());
        viewHolder.setDate(dateFormat.format(new Date((long) article.getDatetime() * 1000)));

        if (shouldRequestNext && position == newsArticles.size() - 1) {
            requestNextData();
        }
    }

    @Override
    public void onBindViewHolder(ViewHolderImage viewHolder, int position) {
        NewsArticle article = newsArticles.get(position);
        viewHolder.setImage(article.getImagePath());
        viewHolder.setTitle(article.getTitle());
        viewHolder.setSubheading(article.getSubheading());
        viewHolder.setDate(dateFormat.format(new Date((long) article.getDatetime() * 1000)));

        if (shouldRequestNext && position == newsArticles.size() - 1) {
            requestNextData();
        }
    }

    @Override
    public void onListItemClicked(int position) {
        NewsArticle article = newsArticles.get(position);

        analyticsClient.sendNewsSelectedEvent(article.getId());
        view.showArticleScreen(newsArticles.get(position));
    }

    void requestNextData() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        subscription = newsClient
                .getNews(PAGINATION_LIMIT, newsArticles.size())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<NewsArticle>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(List<NewsArticle> response) {
                        handleNextResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get news articles.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleFreshResponse(ArrayList<NewsArticle> response) {
        if (newsArticles == null) {
            newsArticles = response;
            view.setAdapter();
        } else {
            newsArticles = response;
            view.notifyDataSetChanged();
        }
        view.setProgressBarEnabled(false);

        shouldRequestNext = response.size() == PAGINATION_LIMIT;
    }

    void handleNextResponse(List<NewsArticle> response) {
        newsArticles.addAll(response);
        view.notifyItemRangeInserted(newsArticles.size() - response.size(), response.size());
        view.setProgressBarEnabled(false);

        shouldRequestNext = response.size() == PAGINATION_LIMIT;
    }
}
