package com.switchsolutions.agricultureapplication.screens.ticker_forecast;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public interface TickerForecastContract {

    interface View {

        void showLocationSettingsScreen();

        void setErrorContainerVisible(boolean visible);

        void setRetryButtonEnabled(boolean enabled);

        void setErrorMessage(@StringRes int actionStringRes, @StringRes int errorStringRes);

        void setListVisible(boolean visible);

        void setProgressBarVisible(boolean visible);

        void setAdapter();

        void smoothScrollToPosition(int position);

        void requestPermissions(@NonNull String[] permissions, int requestCode);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onResume();

        void onPause();

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onActionButtonClicked();

        void onTickerClicked();

        void onBindViewHolder(ViewHolder holder, int position);

        int getItemCount();

        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        void onLocationSettingsScreenResult();
    }

}
