package com.switchsolutions.agricultureapplication.screens.document_narrator;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.ArrayList;

import timber.log.Timber;

public class DocumentNarratorPresenter implements DocumentNarratorContract.Presenter {

    private static final String KEY_CURRENT_TRACK_INDEX = "current_track_index";

    private final DocumentNarratorContract.View view;
    private final ArrayList<HTMLElement> elements;
    private final EventBus eventBus;

    private int currentTrackIndex = 0;
    private AudioState audioState = AudioState.NOTHING;

    public DocumentNarratorPresenter(DocumentNarratorContract.View view, ArrayList<HTMLElement> elements, ApplicationComponent component) {
        this.view = view;
        this.elements = elements;

        this.eventBus = component.eventBus();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setButtonToResume();
        view.setPauseResumeButtonEnabled(true);
        view.setProgressBarEnabled(false);

        if (savedInstanceState != null) {
            currentTrackIndex = savedInstanceState.getInt(KEY_CURRENT_TRACK_INDEX, 0);
        }

        eventBus.register(this);
    }

    @Override
    public void onStart() {
        view.createMediaPlayer();
    }

    @Override
    public void onStop() {
        if (audioState == AudioState.STARTED || audioState == AudioState.PAUSED) {
            stopTrack();
        }

        if (audioState == AudioState.STOPPED) {
            resetTrack();
        }

        view.releaseMediaPlayer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(KEY_CURRENT_TRACK_INDEX, currentTrackIndex);
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

    @Override
    public void onPrevButtonClicked() {
        if (currentTrackIndex <= 0) {
            return;
        }

        if (audioState == AudioState.STARTED || audioState == AudioState.PAUSED) {
            stopTrack();
        }

        if (audioState == AudioState.PREPARING || audioState == AudioState.STOPPED) {
            resetTrack();
        }

        prepareTrack(currentTrackIndex - 1);
    }

    @Override
    public void onPauseResumeButtonClicked() {
        switch (audioState) {
            case NOTHING:
            case FINISHED:
                prepareTrack(currentTrackIndex);
                break;
            case STARTED:
                pauseTrack();
                break;
            case PAUSED:
                startTrack();
                break;
        }
    }

    @Override
    public void onNextButtonClicked() {
        if (currentTrackIndex >= elements.size() - 1) {
            return;
        }

        if (audioState == AudioState.STARTED || audioState == AudioState.PAUSED) {
            stopTrack();
        }

        if (audioState == AudioState.PREPARING || audioState == AudioState.STOPPED) {
            resetTrack();
        }

        prepareTrack(currentTrackIndex + 1);
    }

    @Override
    public void onTrackStreamPrepared() {
        startTrack();

        eventBus.post(new DocumentNarratorEvents.OnTrackPrepared(elements.get(currentTrackIndex)));
    }

    @Override
    public void onTrackStreamFinished() {
        resetTrack();

        if (currentTrackIndex < elements.size() - 1) {
            prepareTrack(currentTrackIndex + 1);
        }
    }

    @Override
    public boolean onTrackStreamError() {
        Timber.d("An error occurred with the audio player.");
        return false;
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.PrepareTrack event) {
        if (audioState == AudioState.PREPARING) {
            return;
        }

        if (currentTrackIndex >= elements.size() - 1) {
            return;
        }

        if (audioState == AudioState.STARTED || audioState == AudioState.PAUSED) {
            stopTrack();
        }

        if (audioState == AudioState.STOPPED) {
            resetTrack();
        }

        for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i) == event.element) {
                prepareTrack(i);
                return;
            }
        }
    }

    void prepareTrack(int newIndex) {
        try {
            view.prepareTrack(RemoteFileUrlUtils.toAudioUrl(elements.get(newIndex).getAudioPath()));
            view.setPauseResumeButtonEnabled(false);
            view.setProgressBarEnabled(true);
            audioState = AudioState.PREPARING;
            currentTrackIndex = newIndex;
        } catch (IOException e) {
            Timber.e(e, "Unable to prepare track.");
        }
    }

    void startTrack() {
        try {
            view.startTrack();
            view.setButtonToPause();
            view.setPauseResumeButtonEnabled(true);
            view.setProgressBarEnabled(false);
            audioState = AudioState.STARTED;

            eventBus.post(new DocumentNarratorEvents.OnTrackStarted(elements.get(currentTrackIndex)));
        } catch (IllegalStateException e) {
            Timber.e(e, "Unable to start track.");
        }
    }

    void pauseTrack() {
        try {
            view.pauseTrack();
            view.setButtonToResume();
            view.setPauseResumeButtonEnabled(true);
            view.setProgressBarEnabled(false);
            audioState = AudioState.PAUSED;

            eventBus.post(new DocumentNarratorEvents.OnTrackPaused(elements.get(currentTrackIndex)));
        } catch (IllegalStateException e) {
            Timber.e(e, "Unable to pause track.");
        }
    }

    void stopTrack() {
        try {
            view.stopTrack();
            view.setButtonToResume();
            view.setPauseResumeButtonEnabled(true);
            view.setProgressBarEnabled(false);
            audioState = AudioState.STOPPED;

            eventBus.post(new DocumentNarratorEvents.OnTrackStopped(elements.get(currentTrackIndex)));
        } catch (IllegalStateException e) {
            Timber.e(e, "Unable to stop track.");
        }
    }

    void resetTrack() {
        try {
            view.resetTrack();
            view.setButtonToResume();
            view.setPauseResumeButtonEnabled(true);
            view.setProgressBarEnabled(false);
            audioState = AudioState.FINISHED;

            eventBus.post(new DocumentNarratorEvents.OnTrackReset(elements.get(currentTrackIndex)));
        } catch (IllegalStateException e) {
            Timber.e(e, "Unable to reset track.");
        }
    }
}
