package com.switchsolutions.agricultureapplication.screens.videos_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.Video;

public interface VideosListContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showVideoScreen(Video video);

        void showBanner();

        void setToolbarTitle(String title);

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle saveInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onVideoContainerScreenResult(int errorStringRes);

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder holder, int position);

        void onListItemClicked(int position);
    }
}
