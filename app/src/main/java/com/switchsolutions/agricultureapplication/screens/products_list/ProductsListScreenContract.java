package com.switchsolutions.agricultureapplication.screens.products_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;

public interface ProductsListScreenContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showProductScreen(ProductCategory category, Product product);

        void showBanner();

        void setToolbarTitle(String title);

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringResId);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder holder, int position);

        void onListItemClicked(int position);
    }
}
