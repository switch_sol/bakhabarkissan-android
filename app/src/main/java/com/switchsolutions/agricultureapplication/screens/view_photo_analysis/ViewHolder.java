package com.switchsolutions.agricultureapplication.screens.view_photo_analysis;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.switchsolutions.agricultureapplication.R;

import java.io.File;

class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;

    ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.image);
    }

    void setImage(File file) {
        if (file == null) {
            viewImage.setImageDrawable(null);
        } else {
            Glide.with(itemView.getContext())
                    .load(file)
                    .fitCenter()
                    .dontAnimate()
                    .into(viewImage);
        }
    }

}
