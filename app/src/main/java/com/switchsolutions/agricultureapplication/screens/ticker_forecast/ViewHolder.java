package com.switchsolutions.agricultureapplication.screens.ticker_forecast;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView viewDate;
    private final TextView viewDescription;
    private final ImageView viewIcon;
    private final TextView viewTemperature;

    public ViewHolder(View itemView) {
        super(itemView);

        viewDate = (TextView) itemView.findViewById(R.id.forecast_date);
        viewDescription = (TextView) itemView.findViewById(R.id.forecast_description);
        viewIcon = (ImageView) itemView.findViewById(R.id.forecast_icon);
        viewTemperature = (TextView) itemView.findViewById(R.id.forecast_temperature);
    }

    public void setDate(String formattedDate) {
        viewDate.setText(formattedDate);
    }

    public void setDescription(@StringRes int stringResId) {
        viewDescription.setText(stringResId);
    }

    public void setIcon(@DrawableRes int drawableResId) {
        Glide.with(itemView.getContext())
                .load(drawableResId)
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .into(viewIcon);
    }

    public void setTemperature(int temperature) {
        viewTemperature.setText(itemView.getContext().getString(R.string.format_temperature, temperature));
    }

}
