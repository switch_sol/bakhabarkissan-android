package com.switchsolutions.agricultureapplication.screens.forecast_list;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

final class ExpandedItemViewHolder extends RecyclerView.ViewHolder {

    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("cccc, K a", Locale.getDefault());

    private final TextView viewDate;
    private final TextView viewDescription;
    private final ImageView viewIcon;
    private final TextView viewTemperature;
    private final TextView viewHumidity;
    private final TextView viewCloudCover;
    private final TextView viewWindSpeed;
    private final TextView viewWindDirection;

    public ExpandedItemViewHolder(View itemView) {
        super(itemView);

        viewDate = (TextView) itemView.findViewById(R.id.date);
        viewDescription = (TextView) itemView.findViewById(R.id.description);
        viewIcon = (ImageView) itemView.findViewById(R.id.icon);
        viewTemperature = (TextView) itemView.findViewById(R.id.temperature);
        viewHumidity = (TextView) itemView.findViewById(R.id.humidity_value);
        viewCloudCover = (TextView) itemView.findViewById(R.id.cloud_value);
        viewWindSpeed = (TextView) itemView.findViewById(R.id.wind_speed_value);
        viewWindDirection = (TextView) itemView.findViewById(R.id.wind_direction_value);
    }

    public void setDate(Date date) {
        viewDate.setText(dateFormat.format(date));
    }

    public void setDescription(@StringRes int stringResId) {
        viewDescription.setText(stringResId);
    }

    public void setIcon(@DrawableRes int drawableResId) {
        viewIcon.setImageResource(drawableResId);
    }

    public void setTemperature(int temperature) {
        viewTemperature.setText(itemView.getContext().getString(R.string.format_temperature, temperature));
    }

    public void setHumidity(int humidity) {
        viewHumidity.setText(itemView.getContext().getString(R.string.format_percent, humidity));
    }

    public void setCloudCover(int cloudCover) {
        viewCloudCover.setText(itemView.getContext().getString(R.string.format_percent, cloudCover));
    }

    public void setWindSpeed(int speed) {
        viewWindSpeed.setText(itemView.getContext().getString(R.string.format_speed_kph, speed));
    }

    public void setWindDirection(@StringRes int direction) {
        viewWindDirection.setText(direction);
    }
}
