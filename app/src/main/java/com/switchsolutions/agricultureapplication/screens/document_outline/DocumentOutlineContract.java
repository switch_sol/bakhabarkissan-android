package com.switchsolutions.agricultureapplication.screens.document_outline;

import android.os.Bundle;

public interface DocumentOutlineContract {

    interface View {

        void setAdapter();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onListItemClicked(int position);

        int getItemViewType(int position);

        void onBindViewHolder(ViewHolder viewHolder, int position);

        int getItemCount();
    }
}
