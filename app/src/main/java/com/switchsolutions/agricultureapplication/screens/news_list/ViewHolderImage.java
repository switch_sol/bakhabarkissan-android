package com.switchsolutions.agricultureapplication.screens.news_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolderImage extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewTitle;
    private final TextView viewSubheading;
    private final TextView viewDate;

    ViewHolderImage(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.article_image);
        viewTitle = (TextView) itemView.findViewById(R.id.article_title);
        viewSubheading = (TextView) itemView.findViewById(R.id.article_subheading);
        viewDate = (TextView) itemView.findViewById(R.id.article_date);
    }

    void setImage(String imagePath) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .skipMemoryCache(BuildConfig.DEBUG)
                        .centerCrop())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .skipMemoryCache(BuildConfig.DEBUG)
                .centerCrop()
                .into(viewImage);
    }

    void setTitle(String title) {
        viewTitle.setText(title);
    }

    void setSubheading(String subheading) {
        viewSubheading.setText(subheading);
    }

    void setDate(String date) {
        viewDate.setText(date);
    }
}
