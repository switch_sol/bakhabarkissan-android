package com.switchsolutions.agricultureapplication.screens.account_login_verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class AccountVerificationActivity extends BaseActivity implements AccountVerificationContract.View {

    private static final String KEY_NUMBER = "number";

    private AccountVerificationContract.Presenter presenter;

    private Toolbar viewToolbar;
    private TextView viewNumber;
    private TextInputLayout viewCodeInputLayout;
    private EditText viewCodeInput;
    private FloatingActionButton viewFAB;
    private ProgressBar viewProgressBar;

    public static Intent createIntent(Context context, String number) {
        Intent intent = new Intent(context, AccountVerificationActivity.class);
        intent.putExtra(KEY_NUMBER, number);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);

        viewNumber = (TextView) findViewById(R.id.expected_number);

        viewCodeInputLayout = (TextInputLayout) findViewById(R.id.code_input_layout);
        viewCodeInput = (EditText) findViewById(R.id.code_input);
        viewCodeInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    presenter.onFABClicked();
                    return true;
                }
                return false;
            }
        });

        viewFAB = (FloatingActionButton) findViewById(R.id.fab);
        if (viewFAB != null) {
            viewFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onFABClicked();
                }
            });
        }

        viewProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        String number = getIntent().getStringExtra(KEY_NUMBER);
        presenter = new AccountVerificationPresenter(this, BaseApplication.getComponent(this), number);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showMainScreen() {
        Intent intent = MainScreenActivity.createIntent(this, true);
        startActivity(intent);
    }

    @Override
    public void setNumber(String number) {
        viewNumber.setText(number);
    }

    @Override
    public String getCode() {
        return viewCodeInput.getText().toString();
    }

    @Override
    public void setCode(String code) {
        viewCodeInput.setText(code);
    }

    @Override
    public void showCodeError(@StringRes int stringRes) {
        viewCodeInputLayout.setError(getString(stringRes));
    }

    @Override
    public void setInputEnabled(boolean enabled) {
        viewCodeInput.setFocusable(enabled);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewProgressBar.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewFAB, stringRes, Snackbar.LENGTH_LONG).show();
    }
}
