package com.switchsolutions.agricultureapplication.screens.ticker_forecast;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.forecast.ForecastClient;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionFailedException;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionSuspendedException;
import com.switchsolutions.agricultureapplication.api.location.LocationClient;
import com.switchsolutions.agricultureapplication.api.location.LocationNotFoundException;
import com.switchsolutions.agricultureapplication.api.location.LocationProviderDisabledException;
import com.switchsolutions.agricultureapplication.models.Forecast;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Coordinates;
import com.switchsolutions.agricultureapplication.utils.Languages;
import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class TickerForecastPresenter implements TickerForecastContract.Presenter {

    private static final String KEY_CURRENT_POSITION = "current_position";

    private static final int RC_LOCATION_PERMISSION = 10;

    private final TickerForecastContract.View view;
    private final LocationClient locationClient;
    private final ForecastClient forecastClient;
    private final Preferences preferences;

    private SimpleDateFormat dateFormat;

    private ArrayList<Forecast> forecastList = new ArrayList<>();
    private Subscription locationSubscription;
    private Subscription forecastSubscription;
    private Subscription timerSubscription;
    private long currentPosition = 0;

    public TickerForecastPresenter(TickerForecastContract.View view, ApplicationComponent component) {
        this.view = view;
        this.locationClient = component.locationClient();
        this.forecastClient = component.forecastClient();
        this.preferences = component.preferences();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (preferences.getLanguage().equals(Languages.EN)) {
            dateFormat = new SimpleDateFormat("cccc, K a", LocaleUtils.LOCALE_EN);
        } else {
            dateFormat = new SimpleDateFormat("cccc, K a", LocaleUtils.LOCALE_UR);
        }

        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getLong(KEY_CURRENT_POSITION);
        }

        requestLocation();
    }

    @Override
    public void onResume() {
        timerSubscription = Observable
                .interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (forecastList.size() > 0) {
                            int newPosition = (int) ++currentPosition;
                            view.smoothScrollToPosition(newPosition);
                        }
                    }
                });
    }

    @Override
    public void onPause() {
        if (timerSubscription != null && !timerSubscription.isUnsubscribed()) {
            timerSubscription.unsubscribe();
            timerSubscription = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putLong(KEY_CURRENT_POSITION, currentPosition);
    }

    @Override
    public void onDestroy() {
        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) {
            forecastSubscription.unsubscribe();
            forecastSubscription = null;
        }

        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            locationSubscription.unsubscribe();
            locationSubscription = null;
        }
    }

    @Override
    public void onActionButtonClicked() {
        requestContent();
    }

    @Override
    public void onTickerClicked() {

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Forecast f = forecastList.get(position % forecastList.size());
        holder.setDate(dateFormat.format(f.getDatetime()));
        holder.setDescription(f.getWeatherCodeStringResId());
        holder.setIcon(f.getWeatherCodeDrawableResId());
        holder.setTemperature(f.getCelsius());
    }

    @Override
    public int getItemCount() {
        return forecastList.size() == 0 ? 0 : 10_000_000;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RC_LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                } else {
                    setError(R.string.solve, R.string.location_permission_required_for_forecast);
                }
                break;
            default:
                // Nothing
        }
    }

    @Override
    public void onLocationSettingsScreenResult() {
        requestContent();
    }

    void requestContent() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) {
            return;
        }

        if (!locationClient.hasPermission()) {
            view.requestPermissions(new String[]{ACCESS_FINE_LOCATION}, RC_LOCATION_PERMISSION);
            return;
        }

        if (!locationClient.hasAccess()) {
            view.showLocationSettingsScreen();
            return;
        }

        requestLocation();
    }

    void requestLocation() {
        try {
            view.setListVisible(false);
            view.setProgressBarVisible(true);
            view.setErrorContainerVisible(false);
            view.setErrorMessage(0, 0);
            view.setRetryButtonEnabled(false);

            locationSubscription = locationClient
                    .getLocation()
                    .timeout(10, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Coordinates>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            handleError(e);
                        }

                        @Override
                        public void onNext(Coordinates coordinates) {
                            if (coordinates != null)
                                handleLocationResponse(coordinates);
                        }
                    });
        } catch (Exception e) {

        }
    }

    void requestForecast(Coordinates coordinates) {


        try {
            forecastSubscription = forecastClient
                    .getForecast(coordinates.getLat(), coordinates.getLon())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ArrayList<Forecast>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            handleError(e);
                        }

                        @Override
                        public void onNext(ArrayList<Forecast> response) {
                            handleForecastResponse(response);
                        }
                    });
        } catch (Exception e) {

        }
    }

    void setError(@StringRes int actionStringRes, @StringRes int errorStringRes) {
        view.setListVisible(false);
        view.setProgressBarVisible(false);
        view.setErrorContainerVisible(true);
        view.setErrorMessage(actionStringRes, errorStringRes);
        view.setRetryButtonEnabled(true);
    }

    void handleError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else if (e instanceof GoogleClientConnectionSuspendedException) {
            handleError((GoogleClientConnectionSuspendedException) e);
        } else if (e instanceof GoogleClientConnectionFailedException) {
            handleError((GoogleClientConnectionFailedException) e);
        } else if (e instanceof LocationProviderDisabledException) {
            handleError((LocationProviderDisabledException) e);
        } else if (e instanceof SecurityException) {
            handleError((SecurityException) e);
        } else if (e instanceof TimeoutException) {
            handleError((TimeoutException) e);
        } else if (e instanceof LocationNotFoundException) {
            handleError((LocationNotFoundException) e);
        } else {
            Timber.e(e, "Unable to get your location.");

            setError(R.string.retry, R.string.unable_to_get_your_location);
        }
    }

    void handleError(SocketTimeoutException e) {
        setError(R.string.retry, R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        setError(R.string.retry, R.string.please_check_your_internet_connection);
    }

    void handleError(GoogleClientConnectionSuspendedException e) {
        setError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(GoogleClientConnectionFailedException e) {
        setError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationProviderDisabledException e) {
        setError(R.string.solve, R.string.please_enable_location_access);
    }

    void handleError(SecurityException e) {
        setError(R.string.solve, R.string.location_permission_required_for_forecast);
    }

    void handleError(TimeoutException e) {
        setError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationNotFoundException e) {
        setError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleLocationResponse(Coordinates coordinates) {
        requestForecast(coordinates);
    }

    void handleForecastResponse(ArrayList<Forecast> response) {
        forecastList = response;

        view.setListVisible(true);
        view.setProgressBarVisible(false);
        view.setErrorContainerVisible(false);
        view.setErrorMessage(0, 0);
        view.setRetryButtonEnabled(false);
        view.setAdapter();
    }
}
