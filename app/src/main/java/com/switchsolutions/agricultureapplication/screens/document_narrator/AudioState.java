package com.switchsolutions.agricultureapplication.screens.document_narrator;

public enum AudioState {
    NOTHING,
    PREPARING,
    STARTED,
    PAUSED,
    STOPPED,
    FINISHED
}
