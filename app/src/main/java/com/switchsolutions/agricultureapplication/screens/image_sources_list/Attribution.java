package com.switchsolutions.agricultureapplication.screens.image_sources_list;

import com.switchsolutions.agricultureapplication.R;

enum Attribution {

    IMAGE_CONTACT_US(R.string.attribution_image_title_contact_us, R.string.attribution_image_source_contact_us, R.drawable.image_contact_us),
    IMAGE_CROPS(R.string.attribution_image_title_crops, R.string.attribution_image_source_crops, R.drawable.image_crops),
    IMAGE_MARKET_RATES(R.string.attribution_image_title_market_rates, R.string.attribution_image_source_market_rates, R.drawable.image_market_rates),
    IMAGE_NEWS(R.string.attribution_image_title_news, R.string.attribution_image_source_news, R.drawable.image_news),
    IMAGE_PRODUCTS(R.string.attribution_image_title_products, R.string.attribution_image_source_products, R.drawable.image_products),
    IMAGE_VIDEOS(R.string.attribution_image_title_videos, R.string.attribution_image_source_videos, R.drawable.image_videos),
    IMAGE_WEATHER(R.string.attribution_image_title_weather, R.string.attribution_image_source_weather, R.drawable.image_weather),
    IMAGE_LIVESTOCK(R.string.attribution_image_title_livestock, R.string.attribution_image_source_livestock, R.drawable.image_livestock),
    IMAGE_PHOTO_ANALYSIS(R.string.attribution_image_title_photo_analysis, R.string.attribution_image_source_photo_analysis, R.drawable.image_visual_analysis),
    ICON_TREE(R.string.attribution_icon_title_tree, R.string.attribution_icon_source_tree, R.drawable.ic_tree_white_24dp),
    IMAGE_MODERN_FARMING_TECHNIQUES(R.string.attribution_image_title_modern_farming_techniques, R.string.attribution_image_source_modern_farming_techniques, R.drawable.image_modern_farming_techniques);

    final int titleRes;
    final int urlRes;
    final int drawableRes;

    Attribution(int titleRes, int urlRes, int drawableRes) {
        this.titleRes = titleRes;
        this.urlRes = urlRes;
        this.drawableRes = drawableRes;
    }

}
