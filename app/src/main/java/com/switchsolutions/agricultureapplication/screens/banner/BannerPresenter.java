package com.switchsolutions.agricultureapplication.screens.banner;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BannerPresenter implements BannerContract.Presenter {

    private static final String SCREEN_NAME = "Banner";

    private static int currentIndex = 0;

    private final BannerContract.View view;
    private final RemoteConfigClient remoteConfigClient;
    private final AnalyticsClient analyticsClient;

    private String[] banners;
    private Subscription subscription;

    BannerPresenter(BannerContract.View view, ApplicationComponent component) {
        this.view = view;
        this.remoteConfigClient = component.remoteConfigClient();
        this.analyticsClient = component.analyticsClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        banners = remoteConfigClient.getBannerAds();

        setImage();

        analyticsClient.sendScreenEvent(SCREEN_NAME);
    }

    @Override
    public void onResume() {
        subscription = Observable
                .interval(15, TimeUnit.SECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Long aLong) {
                        currentIndex++;

                        setImage();
                    }
                });
    }

    @Override
    public void onPause() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    void setImage() {
        if (banners.length > 0) {
            String imagePath = banners[currentIndex % banners.length];
            view.setImage(imagePath);

            analyticsClient.sendViewedBannerAdvertisementEvent(imagePath);
        }
    }
}
