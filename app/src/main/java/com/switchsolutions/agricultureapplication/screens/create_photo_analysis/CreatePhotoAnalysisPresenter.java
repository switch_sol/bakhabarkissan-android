package com.switchsolutions.agricultureapplication.screens.create_photo_analysis;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.audio.AudioPlayer;
import com.switchsolutions.agricultureapplication.audio.AudioRecorder;
import com.switchsolutions.agricultureapplication.storage.Storage;
import com.switchsolutions.agricultureapplication.utils.ImageUtils;
import com.switchsolutions.agricultureapplication.utils.RandomUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import timber.log.Timber;

public class CreatePhotoAnalysisPresenter implements CreatePhotoAnalysisContract.Presenter {

    private static final int LIMIT_AUDIOS = 120;
    private static final int LIMIT_IMAGES = 10;
    private static final int LIMIT_TEXT = 2048;

    private static final String KEY_TEXT = "text";
    private static final String KEY_IMAGES = "images";
    private static final String KEY_HAS_RECORDED = "has_recorded";
    private static final String KEY_AUDIO_RECORDED_SECONDS = "audio_recorded_seconds";

    private static final String FILENAME_TEMP_AUDIO = "pa_audio.3gp";
    private static final String FILENAME_TEMP_IMAGE = "pa_image.jpg";

    private final CreatePhotoAnalysisContract.View view;
    private final AudioRecorder audioRecorder;
    private final AudioPlayer audioPlayer;
    private final Storage storage;

    private ArrayList<String> images = new ArrayList<>();
    private boolean hasRecorded = false;
    private boolean isRecording = false;
    private boolean isPlaying = false;
    private int audioRecordedSeconds = 0;
    private int audioPlayedSeconds = 0;

    private final AudioRecorder.Listener audioRecorderListener = new AudioRecorder.Listener() {
        @Override
        public void onAudioRecorderSecond(long seconds) {
            audioRecordedSeconds = (int) seconds;
            view.setAudioLength(audioRecordedSeconds, LIMIT_AUDIOS);
            view.setAudioProgressBar(audioRecordedSeconds);

            if (seconds >= LIMIT_AUDIOS) {
                stopRecording();
            }
        }

        @Override
        public void onAudioRecorderStateChanged(int state) {
            switch (state) {
                case AudioRecorder.STATE_STARTED:
                    Timber.d("Recorder State Started");

                    view.setAudioFABEnabled(true);
                    setStateToRecording();
                    break;
                case AudioRecorder.STATE_STOPPED:
                    Timber.d("Recorder State Stopped");

                    view.setAudioFABEnabled(true);
                    setStateToRecorded();
                    break;
            }
        }

        @Override
        public void onAudioRecorderError(int what, int extra) {
            Timber.d("Audio Recorder Error: %d, %d", what, extra);

            view.showError(R.string.an_error_occurred_recording_audio);
        }

        @Override
        public void onAudioRecorderInfo(int what, int extra) {
            Timber.d("Audio Recorder Info: %d, %d", what, extra);

            view.showError(R.string.an_error_occurred_recording_audio);
        }
    };

    private final AudioPlayer.Listener audioPlayerListener = new AudioPlayer.Listener() {
        @Override
        public void onAudioPlayerCompleted() {
            view.setAudioFABEnabled(true);
            setStateToRecorded();
        }

        @Override
        public void onAudioPlayerSecond(long seconds) {
            audioPlayedSeconds = (int) seconds;
            view.setAudioLength(audioPlayedSeconds, audioRecordedSeconds);
            view.setAudioProgressBar(audioPlayedSeconds);
        }

        @Override
        public void onAudioPlayerStateChanged(int state) {
            switch (state) {
                case AudioPlayer.STATE_STARTED:
                    Timber.d("Player State Started.");

                    view.setAudioFABEnabled(true);
                    setStateToPlaying();
                    break;
                case AudioPlayer.STATE_STOPPED:
                    Timber.d("Player State Stopped.");

                    view.setAudioFABEnabled(true);
                    setStateToRecorded();
                    break;
            }
        }
    };

    CreatePhotoAnalysisPresenter(CreatePhotoAnalysisContract.View view, ApplicationComponent component) {
        this.view = view;
        this.audioRecorder = component.audioRecorder();
        this.audioPlayer = component.audioPlayer();
        this.storage = component.storage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            images = savedInstanceState.getStringArrayList(KEY_IMAGES);
            hasRecorded = savedInstanceState.getBoolean(KEY_HAS_RECORDED);
            audioRecordedSeconds = savedInstanceState.getInt(KEY_AUDIO_RECORDED_SECONDS);

            view.setIssueText(savedInstanceState.getString(KEY_TEXT));
        } else {
            images = new ArrayList<>();
            hasRecorded = false;
        }

        view.setIssueTextLimit(LIMIT_TEXT);
        view.setIssueTextCount(view.getIssueText().length(), LIMIT_TEXT);
        view.setAudioLength(audioRecordedSeconds, LIMIT_AUDIOS);
        view.setAudioProgressBar(audioRecordedSeconds);
        view.setAudioProgressBarLimit(LIMIT_AUDIOS);
        view.setImagesCount(images.size(), LIMIT_IMAGES);

        view.setAdapter();

        audioRecorder.setListener(audioRecorderListener);
        audioPlayer.setListener(audioPlayerListener);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {
        if (audioRecorder.getState() != AudioRecorder.STATE_RESET) {
            audioRecorder.stop();
            audioRecorder.reset();
        }

        if (audioPlayer.getState() != AudioPlayer.STATE_RESET) {
            audioPlayer.stop();
            audioPlayer.reset();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_TEXT, view.getIssueText());
        outState.putStringArrayList(KEY_IMAGES, images);
        outState.putBoolean(KEY_HAS_RECORDED, hasRecorded);
        outState.putInt(KEY_AUDIO_RECORDED_SECONDS, audioRecordedSeconds);
    }

    @Override
    public void onDestroy() {
        audioRecorder.setListener(null);
        audioPlayer.setListener(null);
    }

    @Override
    public void onRecorderPermissionResult(boolean granted) {
        if (granted) {
            startRecording();
        } else {
            view.showError(R.string.permission_to_record_audio_required);
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onAudioFABClicked() {
        if (!hasRecorded && !isRecording && !view.hasPermission(Manifest.permission.RECORD_AUDIO)) {
            view.requestRecorderPermission();
            return;
        }

        if (!hasRecorded && !isRecording) {
            startRecording();
            return;
        }

        if (!hasRecorded && isRecording) {
            stopRecording();
            return;
        }

        if (hasRecorded && !isPlaying) {
            startPlayback();
            return;
        }

        if (hasRecorded && isPlaying) {
            stopPlayback();
            return;
        }
    }

    @Override
    public void onAudioDeleteClicked() {
        File file = storage.getAudioFile(FILENAME_TEMP_AUDIO);
        if (file != null) {
            file.delete();
        }

        if (isPlaying) {
            stopPlayback();
        }

        resetState();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (images.size() == LIMIT_IMAGES) {
            holder.setImage(images.get(position));
            holder.setIcon(R.drawable.ic_cancel_white_36dp);
        } else if (position < images.size()) {
            holder.setImage(images.get(position));
            holder.setIcon(R.drawable.ic_cancel_white_36dp);
        } else {
            holder.setImage(null);
            holder.setIcon(R.drawable.ic_add_box_white_36dp);
        }
    }

    @Override
    public int getItemCount() {
        return images.size() < LIMIT_IMAGES ? images.size()+1 : images.size();
    }

    @Override
    public void onListItemClicked(int position) {
        if (position == images.size() && images.size() != LIMIT_IMAGES) {
            File file = storage.getImageFile(FILENAME_TEMP_IMAGE);
            if (file != null) {
                view.showImageCaptureScreen(file);
            } else {
                // TODO
            }
        } else {
            File file = storage.getImageFile(images.get(position));
            if (file != null && file.exists()) {
                file.delete();
            }
            images.remove(position);
            view.setImagesCount(images.size(), LIMIT_IMAGES);
            view.notifyDataSetChanged();
        }
    }

    @Override
    public void onImageCaptureResult(int resultCode) {
        if (resultCode == Activity.RESULT_OK) {
            File srcFile = storage.getImageFile(FILENAME_TEMP_IMAGE);
            File dstFile = storage.getImageFile(RandomUtils.randomAlphanumeric(12) + ".jpg");
            if (srcFile != null && dstFile != null) {
                try {
                    ImageUtils.compressImage(srcFile, dstFile);
                    System.gc();

                    images.add(dstFile.getAbsolutePath());

                    view.setImagesCount(images.size(), LIMIT_IMAGES);
                    view.notifyDataSetChanged();
                } catch (IOException e) {
                    Timber.e(e, "Unable to compress image.");
                }
            } else {
                // TODO
            }
        } else {
            // TODO
        }
    }

    @Override
    public void onIssueTextChanged(int n) {
        view.setIssueTextCount(n, LIMIT_TEXT);
    }

    @Override
    public void onSubmitButtonClicked() {
        String text = view.getIssueText();

        String audio = null;
        if (hasRecorded) {
            File audioFile = storage.getAudioFile(FILENAME_TEMP_AUDIO);
            if (audioFile != null) {
                audio = audioFile.getAbsolutePath();
            }
        }

        ArrayList<String> images = null;
        if (this.images != null) {
            images = new ArrayList<>(this.images);
        }

        view.startUploadIntent(text, audio, images);
        view.navigateUp();
    }

    void startRecording() {
        File file = storage.getAudioFile(FILENAME_TEMP_AUDIO);
        if (file != null) {
            try {
                view.setAudioFABEnabled(false);
                audioRecorder.prepare(file);
                audioRecorder.start();
            } catch (IOException e) {
                Timber.e(e, "Unable to start recording.");
            }
        }
    }

    void stopRecording() {
        view.setAudioFABEnabled(false);
        audioRecorder.stop();
        audioRecorder.reset();
    }

    void startPlayback() {
        File file = storage.getAudioFile(FILENAME_TEMP_AUDIO);
        if (file != null) {
            try {
                view.setAudioFABEnabled(false);
                audioPlayer.prepare(file);
                audioPlayer.start();
            } catch (IOException e) {
                Timber.e(e, "Unable to start playback.");
            }
        }
    }

    void stopPlayback() {
        view.setAudioFABEnabled(false);
        audioPlayer.stop();
        audioPlayer.reset();
    }

    void resetState() {
        hasRecorded = false;
        isRecording = false;
        isPlaying = false;

        view.setAudioLength(0, LIMIT_AUDIOS);
        view.setAudioProgressBar(0);
        view.setAudioProgressBarLimit(LIMIT_AUDIOS);

        view.setAudioFABIcon(R.drawable.ic_keyboard_voice_white_24dp);
        view.setAudioDeleteButtonEnabled(false);
    }

    void setStateToRecording() {
        hasRecorded = false;
        isRecording = true;
        isPlaying = false;

        view.setAudioLength(0, LIMIT_AUDIOS);
        view.setAudioProgressBar(0);
        view.setAudioProgressBarLimit(LIMIT_AUDIOS);

        view.setAudioFABIcon(R.drawable.ic_stop_white_24dp);
    }

    void setStateToRecorded() {
        hasRecorded = true;
        isRecording = false;
        isPlaying = false;

        view.setAudioLength(0, audioRecordedSeconds);
        view.setAudioProgressBar(0);
        view.setAudioProgressBarLimit(audioRecordedSeconds);

        view.setAudioFABIcon(R.drawable.ic_play_arrow_white_24dp);

        view.setAudioDeleteButtonEnabled(true);
    }

    void setStateToPlaying() {
        hasRecorded = true;
        isRecording = false;
        isPlaying = true;

        view.setAudioLength(0, audioRecordedSeconds);
        view.setAudioProgressBar(0);
        view.setAudioProgressBarLimit(audioRecordedSeconds);

        view.setAudioFABIcon(R.drawable.ic_stop_white_24dp);
    }

}
