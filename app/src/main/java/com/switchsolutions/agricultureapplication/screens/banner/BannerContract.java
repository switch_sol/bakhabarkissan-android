package com.switchsolutions.agricultureapplication.screens.banner;

import android.os.Bundle;

public interface BannerContract {

    interface View {

        void setImage(String imagePath);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onResume();

        void onPause();

        void onSaveInstanceState(Bundle outState);

        void onDestroy();
    }

}
