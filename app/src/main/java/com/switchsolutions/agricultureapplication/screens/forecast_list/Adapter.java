package com.switchsolutions.agricultureapplication.screens.forecast_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int VIEW_TYPE_COLLAPSED = 1;
    static final int VIEW_TYPE_EXPANDED = 2;

    private final ForecastListContract.Presenter presenter;

    public Adapter(ForecastListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        return presenter.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_COLLAPSED:
                return onCreateCollapsedItemViewHolder(parent);
            case VIEW_TYPE_EXPANDED:
                return onCreateExpandedItemViewHolder(parent);
            default:
                return null;
        }
    }

    private CollapsedItemViewHolder onCreateCollapsedItemViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_forecast_collapsed_item, parent, false);
        final CollapsedItemViewHolder viewHolder = new CollapsedItemViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    private ExpandedItemViewHolder onCreateExpandedItemViewHolder(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_forecast_expanded_item, parent, false);
        final ExpandedItemViewHolder viewHolder = new ExpandedItemViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (presenter.getItemViewType(position)) {
            case VIEW_TYPE_COLLAPSED:
                presenter.onBindViewHolder((CollapsedItemViewHolder) holder, position);
                break;
            case VIEW_TYPE_EXPANDED:
                presenter.onBindViewHolder((ExpandedItemViewHolder) holder, position);
                break;
            default:
                // Nothing.
        }
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
