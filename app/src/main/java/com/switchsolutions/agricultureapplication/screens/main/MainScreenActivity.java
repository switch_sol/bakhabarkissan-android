package com.switchsolutions.agricultureapplication.screens.main;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.farmtohome.home.HomeFarmActivity;
import com.switchsolutions.agricultureapplication.farmtohome.main.MainFarmActivity;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.crop_categories_list.CropCategoriesListScreenActivity;
import com.switchsolutions.agricultureapplication.screens.farming_techniques_list.FarmingTechniquesListActivity;
import com.switchsolutions.agricultureapplication.screens.forecast_list.ForecastListActivity;
import com.switchsolutions.agricultureapplication.screens.livestock_list.LivestockListActivity;
import com.switchsolutions.agricultureapplication.screens.market_rates_list.MarketRatesListActivity;
import com.switchsolutions.agricultureapplication.screens.news_list.NewsListScreenActivity;
import com.switchsolutions.agricultureapplication.screens.photo_analyses_list.PhotoAnalysesListActivity;
import com.switchsolutions.agricultureapplication.screens.product_categories_list.ProductCategoriesListScreenActivity;
import com.switchsolutions.agricultureapplication.screens.settings_container.SettingsContainerActivity;
import com.switchsolutions.agricultureapplication.screens.ticker_forecast.TickerForecastFragment;
import com.switchsolutions.agricultureapplication.screens.video_categories_parent_list.VideoCategoriesParentListActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;
import com.switchsolutions.agricultureapplication.widgets.BlurView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

public class MainScreenActivity extends BaseActivity implements MainScreenContract.View {

    private static final int RC_RECOGNITION_CODE = 8498;

    private static final String KEY_FROM_LOGIN = "from_login";

    private MainScreenContract.Presenter presenter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private BlurView viewBlur;
    private View viewForecastContainer;

    private RecyclerView.Adapter adapter;

    public static Intent createIntent(Context context) {
        return createIntent(context, false);
    }

    public static Intent createIntent(Context context, boolean fromLogin) {
        Intent intent = new Intent(context, MainScreenActivity.class);
        intent.putExtra(KEY_FROM_LOGIN, fromLogin);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (viewToolbar != null) {
            viewToolbar.inflateMenu(R.menu.menu_main_container);
            viewToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.action_settings:
                            return presenter.onMenuItemSettingsClicked();
                        case R.id.action_speech:
                            return presenter.onMenuItemSpeechClicked();
                        case R.id.action_switch_view:
                            return presenter.onMenuItemSwitchViewClicked();
                        default:
                            return false;
                    }
                }
            });
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return presenter.getSpanSize(position);
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(gridLayoutManager);
        viewList.setHasFixedSize(true);
        viewList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                presenter.onScrollStateChanged(newState);
            }
        });

        viewBlur = (BlurView) findViewById(R.id.blur_view);

        viewForecastContainer = findViewById(R.id.fragment_container_forecast_ticker);

        boolean fromLogin = getIntent().getBooleanExtra(KEY_FROM_LOGIN, false);

        ApplicationComponent component = BaseApplication.getComponent(this);
        presenter = new MainScreenPresenter(this, component, fromLogin);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RC_RECOGNITION_CODE: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String text = result.get(0);
                    presenter.onSpeechRecognitionResult(text);
                }
                break;
            }
        }
    }

    @Override
    public void requestSpeechRecognition() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.ACTION_GET_LANGUAGE_DETAILS, "en-UK");
        try {
            startActivityForResult(intent, RC_RECOGNITION_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(this, R.string.an_error_occurred, Toast.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showNewsScreen() {
        Intent intent = NewsListScreenActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showCropsScreen() {
        Intent intent = CropCategoriesListScreenActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showLivestockScreen() {
        Intent intent = LivestockListActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showModernFarmingTechniquesScreen() {
        Intent intent = FarmingTechniquesListActivity.Companion.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showProductsScreen() {
        Intent intent = ProductCategoriesListScreenActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showForecastScreen() {
        Intent intent = ForecastListActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showMarketRatesScreen() {
        Intent intent = MarketRatesListActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showVideosScreen() {
        Intent intent = VideoCategoriesParentListActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showVisualAnalysisScreen() {
        Intent intent = PhotoAnalysesListActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showForumScreen() {

    }

    @Override
    public void showContactScreen(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        startActivity(intent);
    }

    @Override
    public void showWebPageScreen(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void showForecastTickerScreen() {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .add(R.id.fragment_container_forecast_ticker, TickerForecastFragment.newInstance())
                .commit();
    }

    @Override
    public void enterForecastTickerScreen() {
        try {
            viewForecastContainer.animate()
                    .translationY(0f)
                    .setDuration(350)
                    .setInterpolator(new OvershootInterpolator(0.5f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showSettingsScreen() {
        Intent intent = SettingsContainerActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public void showFarmToHome() {

        Intent intent = new Intent(this, HomeFarmActivity.class);
        startActivity(intent);

    }

    @Override
    public void setListAdapter() {
        adapter = new ListAdapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void setGridAdapter() {
        adapter = new GridAdapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void animateBlurIn() {
        viewBlur.setAlpha(0f);
        viewBlur.setBlurSource(viewList, 10, 20);
        viewBlur.animate()
                .alpha(1f)
                .setDuration(250)
                .setInterpolator(new OvershootInterpolator(0.5f))
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animation.removeAllListeners();
                        presenter.onBlurAnimationCompleted();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        animation.removeAllListeners();
                        presenter.onBlurAnimationCompleted();
                    }
                });
    }

    @Override
    public void animateBlurOut() {
        viewBlur.setAlpha(1f);
        viewBlur.animate()
                .alpha(0f)
                .setDuration(250)
                .setInterpolator(new AnticipateInterpolator(0.5f))
                .setListener(new AnimatorListenerAdapter() {
                });
    }

    @Override
    public void setMenuItemSwitchViewIcon(@DrawableRes int drawableRes) {
        MenuItem menuItem = viewToolbar.getMenu().findItem(R.id.action_switch_view);
        menuItem.setIcon(drawableRes);
    }

    @Override
    public void setMenuItemSwitchViewText(@StringRes int stringRes) {
        MenuItem menuItem = viewToolbar.getMenu().findItem(R.id.action_switch_view);
        menuItem.setTitle(stringRes);
    }

    @Override
    public void animateEntrance() {

        try {
            long step = 750;
            long incr = 0;

            viewToolbar.setAlpha(0f);
            viewToolbar.setTranslationY(-getResources().getDimensionPixelSize(R.dimen.dp64));
            viewToolbar.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setStartDelay(incr)
                    .setDuration(step)
                    .setInterpolator(new OvershootInterpolator(0.5f));

            viewList.setAlpha(0f);
            viewList.setTranslationY(getResources().getDimensionPixelSize(R.dimen.dp320));
            viewList.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setStartDelay(incr += step / 2)
                    .setDuration(step)
                    .setInterpolator(new OvershootInterpolator(0.5f));

            viewForecastContainer.setAlpha(0f);
            viewForecastContainer.setTranslationY(getResources().getDimensionPixelSize(R.dimen.ticker_container_height));
            viewForecastContainer.animate()
                    .alpha(1f)
                    .translationY(0f)
                    .setStartDelay(incr += step / 2)
                    .setDuration(step)
                    .setInterpolator(new OvershootInterpolator(0.5f));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void scrollToCleanestPosition() {
        int i;
        int j;
        View va;
        if (viewList.getLayoutManager() instanceof LinearLayoutManager) {
            i = ((LinearLayoutManager) viewList.getLayoutManager()).findFirstVisibleItemPosition();
            j = ((LinearLayoutManager) viewList.getLayoutManager()).findLastCompletelyVisibleItemPosition();
        } else {
            i = ((GridLayoutManager) viewList.getLayoutManager()).findFirstVisibleItemPosition();
            j = ((GridLayoutManager) viewList.getLayoutManager()).findLastCompletelyVisibleItemPosition();
        }
        va = viewList.getLayoutManager().findViewByPosition(i);

        Rect ra = new Rect();
        va.getHitRect(ra);

        if (j + 1 != adapter.getItemCount()) {
            if (ra.bottom < va.getHeight() / 2) {
                viewList.smoothScrollBy(0, ra.bottom);
            } else {
                viewList.smoothScrollBy(0, ra.top);
            }
        }
    }
}
