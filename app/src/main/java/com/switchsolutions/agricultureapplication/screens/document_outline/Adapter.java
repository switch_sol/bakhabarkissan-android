package com.switchsolutions.agricultureapplication.screens.document_outline;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

final class Adapter extends RecyclerView.Adapter<ViewHolder> {

    private final DocumentOutlineContract.Presenter presenter;

    Adapter(DocumentOutlineContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        return presenter.getItemViewType(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TextView view = new TextView(parent.getContext());
        view.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
        view.setSingleLine(false);
        view.setTextColor(ContextCompat.getColor(parent.getContext(), R.color.black_primary_text));
        view.setClickable(true);
        view.setMaxLines(2);
        view.setEllipsize(TextUtils.TruncateAt.END);

        TypedValue outValue = new TypedValue();
        parent.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        view.setBackgroundResource(outValue.resourceId);

        int dp8 = parent.getContext().getResources().getDimensionPixelSize(R.dimen.dp8);
        ViewCompat.setPaddingRelative(view, dp8 * 2 * viewType, dp8, dp8 * 2, dp8);

        view.setLayoutParams(new ViewGroup.LayoutParams(MATCH_PARENT, WRAP_CONTENT));

        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        presenter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
