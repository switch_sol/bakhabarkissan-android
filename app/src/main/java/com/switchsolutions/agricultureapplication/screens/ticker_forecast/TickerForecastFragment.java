package com.switchsolutions.agricultureapplication.screens.ticker_forecast;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.widgets.TickerLayoutManager;

public class TickerForecastFragment extends Fragment implements TickerForecastContract.View {

    private static final int RC_LOCATION_SETTINGS = 13;

    private TickerForecastContract.Presenter presenter;

    private View viewTickerContainer;
    private RecyclerView viewTicker;
    private View viewErrorContainer;
    private TextView viewActionText;
    private TextView viewErrorText;
    private ProgressBar viewProgressBar;

    private Adapter adapter;

    public static TickerForecastFragment newInstance() {
        Bundle args = new Bundle();
        TickerForecastFragment fragment = new TickerForecastFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ticker_forecast, container, false);

        try {
            viewTickerContainer = view.findViewById(R.id.ticker_container);
            viewTickerContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onTickerClicked();
                }
            });
            viewTicker = (RecyclerView) view.findViewById(R.id.ticker);
            viewTicker.setLayoutManager(new TickerLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            viewTicker.setHasFixedSize(true);

            viewErrorContainer = view.findViewById(R.id.error_container);
            viewActionText = (TextView) view.findViewById(R.id.button_action);
            viewActionText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onActionButtonClicked();
                }
            });
            viewErrorText = (TextView) view.findViewById(R.id.error_message);

            viewProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

            return view;
        } catch (Exception e) {

        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ApplicationComponent component = BaseApplication.getComponent(getContext());
        presenter = new TickerForecastPresenter(this, component);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        presenter.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_LOCATION_SETTINGS) {
            presenter.onLocationSettingsScreenResult();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void showLocationSettingsScreen() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, RC_LOCATION_SETTINGS);
    }

    @Override
    public void setErrorContainerVisible(boolean visible) {
        viewErrorContainer.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setRetryButtonEnabled(boolean enabled) {
        viewActionText.setEnabled(enabled);
    }

    @Override
    public void setErrorMessage(@StringRes int actionStringRes, @StringRes int errorStringRes) {
        try {
            viewActionText.setText(actionStringRes);
            viewErrorText.setText(errorStringRes);
        } catch (Resources.NotFoundException e) {
            viewActionText.setText("");
            viewErrorText.setText("");
        }
    }

    @Override
    public void setListVisible(boolean visible) {
        viewTickerContainer.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setProgressBarVisible(boolean visible) {
        viewProgressBar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewTicker.setAdapter(adapter);
    }

    @Override
    public void smoothScrollToPosition(int position) {
        viewTicker.smoothScrollToPosition(position);
    }
}
