package com.switchsolutions.agricultureapplication.screens.account_login;

import android.os.Bundle;
import android.support.annotation.StringRes;

public interface AccountLoginContract {

    interface View {

        void showMainScreen();

//        void showVerificationScreen(String number);

        String getName();

        String getNumber();

        void showLanguagePicker();

        void restartApplication();

        void setInputEnabled(boolean enabled);

        void setProgressBarEnabled(boolean enabled);

        void showNameError(@StringRes int stringRes);

        void showNumberError(@StringRes int stringRes);

        void showError(@StringRes int stringRes);

        boolean hasPermission(String permission);

        void requestSMSPermission();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigateNextButtonClicked();

        void onSMSPermissionResult(boolean granted);

        void onLanguagePickerResult(String string);
    }
}
