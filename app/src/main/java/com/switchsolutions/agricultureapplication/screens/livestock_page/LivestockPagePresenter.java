package com.switchsolutions.agricultureapplication.screens.livestock_page;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.livestock.LivestockClient;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.Livestock;
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorEvents;
import com.switchsolutions.agricultureapplication.screens.document_outline.DocumentOutlineEvents;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class LivestockPagePresenter implements LivestockPageContract.Presenter {

    private static final String SCREEN_NAME = "Livestock Page";

    private final LivestockPageContract.View view;
    private final Livestock livestock;
    private final LivestockClient livestockClient;
    private final EventBus eventBus;
    private final AnalyticsClient analyticsClient;

    private Subscription contentSubscription;
    private Subscription outlineSubscription;
    private Subscription narratorSubscription;

    LivestockPagePresenter(LivestockPageContract.View view, Livestock livestock, ApplicationComponent component) {
        this.view = view;
        this.livestock = livestock;
        this.livestockClient = component.livestockClient();
        this.eventBus = component.eventBus();
        this.analyticsClient = component.analyticsClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setToolbarTitle(livestock.getName());

        requestContent();

        eventBus.register(this);

        analyticsClient.sendScreenEvent(SCREEN_NAME);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);

        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            contentSubscription.unsubscribe();
        }

        if (outlineSubscription != null && !outlineSubscription.isUnsubscribed()) {
            outlineSubscription.unsubscribe();
        }

        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            narratorSubscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public boolean onOutlineButtonClicked() {
        view.showOutline();
        return true;
    }

    @Override
    public void onHTMLElementClicked(HTMLHeader header) {
        if (header.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(header));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLParagraph paragraph) {
        if (paragraph.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(paragraph));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLImage image) {

    }

    @Override
    public void onHTMLElementClicked(HTMLTable table) {
        if (table.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(table));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLOrderedList orderedList) {
        if (orderedList.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(orderedList));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLUnorderedList unorderedList) {
        if (unorderedList.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(unorderedList));
        }
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackPrepared event) {
        view.setContentItemInFocus(event.element.getIndex());
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackStarted event) {

    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackPaused event) {

    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackStopped event) {

    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackReset event) {
        view.clearContentItemInFocus();
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnError event) {
        view.showError(event.stringRes);
    }

    @Subscribe
    public void onEvent(DocumentOutlineEvents.OnHeaderSelected event) {
        view.scrollToContentPosition(event.header.getIndex());
        view.hideOutline();
    }

    void requestContent() {
        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        contentSubscription = livestockClient
                .getLivestockPage(livestock.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<HTMLElement> response) {
                        handleContentResponse(response);
                    }
                });
    }

    void handleContentResponse(ArrayList<HTMLElement> elements) {
        view.setContent(elements);
        view.setProgressBarEnabled(false);

        prepareOutline(elements);
        prepareNarrator(elements);
    }

    void prepareOutline(final List<HTMLElement> srcElements) {
        if (outlineSubscription != null && !outlineSubscription.isUnsubscribed()) {
            return;
        }

        outlineSubscription = Observable
                .fromCallable(new Callable<ArrayList<HTMLHeader>>() {
                    @Override
                    public ArrayList<HTMLHeader> call() throws Exception {
                        ArrayList<HTMLHeader> dstElements = new ArrayList<>();
                        for (HTMLElement element : srcElements) {
                            if (element instanceof HTMLHeader) {
                                dstElements.add((HTMLHeader) element);
                            }
                        }
                        return dstElements;
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLHeader>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<HTMLHeader> response) {
                        handleOutlineResponse(response);
                    }
                });
    }

    void handleOutlineResponse(ArrayList<HTMLHeader> response) {
        view.setOutline(response);
    }

    void prepareNarrator(final List<HTMLElement> srcElements) {
        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            return;
        }

        narratorSubscription = Observable
                .fromCallable(new Callable<ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call() throws Exception {
                        ArrayList<HTMLElement> dstElements = new ArrayList<>();
                        for (HTMLElement element : srcElements) {
                            if (element.getAudioPath() != null) {
                                dstElements.add(element);
                            }
                        }
                        return dstElements;
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<HTMLElement> response) {
                        handleNarratorResponse(response);
                    }
                });
    }

    void handleNarratorResponse(ArrayList<HTMLElement> response) {
        if (response.size() > 0) {
            view.setNarrator(response);
        }
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get livestock page.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        Timber.d(e, "HttpException Occurred.");

        view.showError(R.string.an_error_occurred);
    }

    void handleError(SocketTimeoutException e) {
        Timber.d(e, "SocketTimeoutException Occurred");

        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        Timber.d(e, "ConnectException Occurred");

        view.showError(R.string.please_check_your_internet_connection);
    }
}
