package com.switchsolutions.agricultureapplication.screens.account_login_verification;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.cloud_messaging.CloudMessagingClient;
import com.switchsolutions.agricultureapplication.models.JWTTokens;
import com.switchsolutions.agricultureapplication.preferences.Preferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class AccountVerificationPresenter implements AccountVerificationContract.Presenter {

    private static final String SCREEN_NAME = "Account Verification";
    private static final String REGEX_NUMBERS = "[^0-9a-zA-Z]";

    private final AccountVerificationContract.View view;
    private final AccountsClient accountsClient;
    private final Preferences preferences;
    private final CloudMessagingClient cloudMessagingClient;
    private final AnalyticsClient analyticsClient;
    private final EventBus eventBus;

    private final String number;

    private Subscription subscription;

    public AccountVerificationPresenter(AccountVerificationContract.View view, ApplicationComponent component, String number) {
        this.view = view;
        this.preferences = component.preferences();
        this.accountsClient = component.accountsClient();
        this.cloudMessagingClient = component.cloudMessagingClient();
        this.analyticsClient = component.analyticsClient();
        this.eventBus = component.eventBus();

        this.number = number;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setNumber(number);
        view.setInputEnabled(true);
        view.setProgressBarEnabled(false);

        eventBus.register(this);

        analyticsClient.sendScreenEvent(SCREEN_NAME);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
            subscription = null;
        }
    }

    @Override
    public void onFABClicked() {
        String code = view.getCode().replaceAll(REGEX_NUMBERS, "");

        if (code.length() == 6) {
            requestVerifyAccount(view.getCode());
        } else {
            view.showCodeError(R.string.invalid_code);
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onEvent(AccountVerificationEvents.OnVerificationCodeReceived event) {
        view.setCode(event.code);

        eventBus.removeStickyEvent(event);

        requestVerifyAccount(event.code);
    }

    void requestVerifyAccount(String code) {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        view.setInputEnabled(false);
        subscription = accountsClient
                .verifyAccount(code)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JWTTokens>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            handleHttpException((HttpException) e);
                            return;
                        }

                        if (e instanceof SocketTimeoutException) {
                            handleSocketTimeoutException((SocketTimeoutException) e);
                            return;
                        }

                        if (e instanceof ConnectException) {
                            handleConnectException((ConnectException) e);
                            return;
                        }

                        handleGenericException(e);
                    }

                    @Override
                    public void onNext(JWTTokens jwtTokens) {
                        preferences.setIsFirstRun(false);
                        cloudMessagingClient.subscribeToGlobals();
                        view.showMainScreen();
                    }
                });
    }

    void handleHttpException(HttpException e) {
        switch (e.code()) {
            case 401:
                view.showError(R.string.invalid_code);
                break;
            default:
                view.showError(R.string.an_unknown_error_occurred);
                break;
        }

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleSocketTimeoutException(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleConnectException(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleGenericException(Throwable e) {
        Timber.e(e, "Unable to create account.");
        view.showError(R.string.an_unknown_error_occurred);

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }
}
