package com.switchsolutions.agricultureapplication.screens.account_login;

import android.os.Bundle;

import com.google.i18n.phonenumbers.NumberParseException;
import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.cloud_messaging.CloudMessagingClient;
import com.switchsolutions.agricultureapplication.models.JWTTokens;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Languages;
import com.switchsolutions.agricultureapplication.utils.PhoneNumberUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class AccountLoginPresenter implements AccountLoginContract.Presenter {

    private static final String SCREEN_NAME = "Account Login";
    private static final String REGEX_ALPHA = "[^A-Za-z ]";
    private static final String REGEX_ALPHA_NO_SPACES = "[^A-Za-z]";

    private final AccountLoginContract.View view;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final AnalyticsClient analyticsClient;
    private final CloudMessagingClient cloudMessagingClient;

    private String requestedName;
    private String requestedNumber;

    private Subscription subscription;

    public AccountLoginPresenter(AccountLoginContract.View view, ApplicationComponent component) {
        this.view = view;
        this.preferences = component.preferences();
        this.accountsClient = component.accountsClient();
        this.analyticsClient = component.analyticsClient();
        this.cloudMessagingClient = component.cloudMessagingClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (!preferences.getLanguagePicked()) {
            view.showLanguagePicker();
        }

        analyticsClient.sendScreenEvent(SCREEN_NAME);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigateNextButtonClicked() {
        String name = view.getName();
        name = name.replaceAll(REGEX_ALPHA, "");
        if (name.replaceAll(REGEX_ALPHA_NO_SPACES, "").length() <= 3) {
            view.showNameError(R.string.invalid_name);
            return;
        }

        String number = view.getNumber();
        try {
            number = PhoneNumberUtils.formatNumber(number);
        } catch (NumberParseException e) {
            view.showNumberError(R.string.invalid_number);
            return;
        }

        requestedName = name;
        requestedNumber = number;

        requestDemoCreateAccount();

//        if (!view.hasPermission(Manifest.permission.RECEIVE_SMS)) {
//            view.requestSMSPermission();
//        } else {
//            requestCreateAccount();
//        }

    }

    @Override
    public void onSMSPermissionResult(boolean granted) {
//        requestCreateAccount();
    }

    @Override
    public void onLanguagePickerResult(String string) {
        switch (string) {
            case Languages.EN:
            case Languages.PS:
            case Languages.SD:
            case Languages.UR:
                preferences.setLanguage(string);
                preferences.setLanguagePicked(true);
                view.restartApplication();
                break;
        }
    }

    //    void requestCreateAccount() {
//        if (subscription != null && !subscription.isUnsubscribed()) {
//            return;
//        }
//
//        view.setInputEnabled(false);
//        view.setProgressBarEnabled(true);
//        subscription = accountsClient
//                .createAccount(requestedName, requestedNumber)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<Void>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        if (e instanceof HttpException) {
//                            handleHttpException((HttpException) e);
//                            return;
//                        }
//
//                        if (e instanceof SocketTimeoutException) {
//                            handleSocketTimeoutException((SocketTimeoutException) e);
//                            return;
//                        }
//
//                        if (e instanceof ConnectException) {
//                            handleConnectException((ConnectException) e);
//                            return;
//                        }
//
//                        handleGenericException(e);
//                    }
//
//                    @Override
//                    public void onNext(Void response) {
//                        view.showVerificationScreen(requestedNumber);
//
//                        view.setProgressBarEnabled(false);
//                        view.setInputEnabled(true);
//                    }
//                });
//    }

    void requestDemoCreateAccount() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setInputEnabled(false);
        view.setProgressBarEnabled(true);
        subscription = accountsClient
                .createAccountDemo(requestedName, requestedNumber)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JWTTokens>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            handleHttpException((HttpException) e);
                            return;
                        }

                        if (e instanceof SocketTimeoutException) {
                            handleSocketTimeoutException((SocketTimeoutException) e);
                            return;
                        }

                        if (e instanceof ConnectException) {
                            handleConnectException((ConnectException) e);
                            return;
                        }

                        handleGenericException(e);
                    }

                    @Override
                    public void onNext(JWTTokens jwtTokens) {
                        preferences.setIsFirstRun(false);
                        cloudMessagingClient.subscribeToGlobals();
                        view.showMainScreen();
                    }
                });
    }

    void handleHttpException(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showError(R.string.account_banned);
                break;
            default:
                view.showError(R.string.an_unknown_error_occurred);
                break;
        }

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleSocketTimeoutException(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleConnectException(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);

        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
    }

    void handleGenericException(Throwable e) {
        Timber.e(e, "Unable to create account.");


        view.setProgressBarEnabled(false);
        view.setInputEnabled(true);
        view.showError(R.string.an_unknown_error_occurred);
    }
}
