package com.switchsolutions.agricultureapplication.screens.news_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.NewsArticle;

public interface NewsListScreenContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showArticleScreen(NewsArticle article);

        void showBanner();

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onRequestRefresh();

        int getItemViewType(int position);

        int getItemCount();

        void onBindViewHolder(ViewHolderBasic viewHolder, int position);

        void onBindViewHolder(ViewHolderImage viewHolder, int position);

        void onListItemClicked(int position);
    }
}
