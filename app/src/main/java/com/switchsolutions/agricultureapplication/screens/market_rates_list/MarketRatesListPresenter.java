package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionFailedException;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionSuspendedException;
import com.switchsolutions.agricultureapplication.api.location.LocationClient;
import com.switchsolutions.agricultureapplication.api.location.LocationNotFoundException;
import com.switchsolutions.agricultureapplication.api.location.LocationProviderDisabledException;
import com.switchsolutions.agricultureapplication.api.market_rates.MarketRatesClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.Market;
import com.switchsolutions.agricultureapplication.models.MarketRate;
import com.switchsolutions.agricultureapplication.utils.Coordinates;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public final class MarketRatesListPresenter implements MarketRatesListContract.Presenter {

    private static final String SCREEN_NAME = "Market Rates List";

    private static final int RC_LOCATION_PERMISSION = 30;
    private static final int PAGINATION_LIMIT = 10;

    private final MarketRatesListContract.View view;
    private final MarketRatesClient marketRatesClient;
    private final LocationClient locationClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<Market> marketsList;
    private ArrayList<MarketRate> marketRatesList;
    private Subscription locationSubscription;
    private Subscription marketsListSubscription;
    private Subscription marketRatesListSubscription;
    private int bottomSheetState;

    MarketRatesListPresenter(MarketRatesListContract.View view, ApplicationComponent component) {
        this.view = view;
        this.marketRatesClient = component.marketRatesClient();
        this.locationClient = component.locationClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setBottomSheetState(bottomSheetState = BottomSheetBehavior.STATE_HIDDEN);

        view.setMarketsAdapter();
        view.setMarketRatesAdapter();

        requestLocation();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            locationSubscription.unsubscribe();
        }

        if (marketsListSubscription != null && !marketsListSubscription.isUnsubscribed()) {
            marketsListSubscription.unsubscribe();
        }

        if (marketRatesListSubscription != null && !marketRatesListSubscription.isUnsubscribed()) {
            marketRatesListSubscription.unsubscribe();
        }
    }

    @Override
    public boolean onBackPressed() {
        if (bottomSheetState == BottomSheetBehavior.STATE_EXPANDED || bottomSheetState == BottomSheetBehavior.STATE_COLLAPSED) {
            view.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return true;
        }
        return false;
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getMarketsItemCount() {
        return marketsList != null ? marketsList.size() : 0;
    }

    @Override
    public void onBindMarketViewHolder(ViewHolderMarket holder, int position) {
        Market market = marketsList.get(position);

        holder.setName(market.getName());
    }

    @Override
    public void onMarketListItemClicked(int position) {
        view.setBottomSheetState(BottomSheetBehavior.STATE_COLLAPSED);

        Market market = marketsList.get(position);
        requestMarketRatesList(market);

        analyticsClient.sendMarketRatesSelectedEvent(market.getId());
    }

    @Override
    public int getMarketRatesItemCount() {
        return marketRatesList != null ? marketRatesList.size() : 0;
    }

    @Override
    public void onBindMarketRateViewHolder(ViewHolderMarketRate holder, int position) {
        MarketRate marketRate = marketRatesList.get(position);

        holder.setName(marketRate.getName());
        holder.setRate(marketRate.getRate());
    }

    @Override
    public void onBottomSheetStateChanged(int newState) {
        bottomSheetState = newState;
    }

    @Override
    public void onActionButtonClicked() {
        requestContent();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RC_LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                } else {
                    view.showError(R.string.solve, R.string.location_permission_required_for_forecast, true);
                }
                break;
            default:
                // Nothing
        }
    }

    @Override
    public void onLocationSettingsScreenResult() {
        requestContent();
    }

    void requestContent() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        if (marketsListSubscription != null && !marketsListSubscription.isUnsubscribed()) {
            return;
        }

        if (!locationClient.hasPermission()) {
            view.requestPermissions(new String[]{ACCESS_FINE_LOCATION}, RC_LOCATION_PERMISSION);
            return;
        }

        if (!locationClient.hasAccess()) {
            view.showLocationSettingsScreen();
            return;
        }

        requestLocation();
    }

    void requestLocation() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        view.setMarketsProgressBarEnabled(true);
        locationSubscription = locationClient
                .getLocation()
                .timeout(10, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Coordinates>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e, true);
                        view.setMarketsProgressBarEnabled(false);
                    }

                    @Override
                    public void onNext(Coordinates response) {
                        handleLocationResponse(response);
                    }
                });
    }

    void requestMarketsList(Coordinates coordinates) {
        if (marketsListSubscription != null && !marketsListSubscription.isUnsubscribed()) {
            return;
        }

        view.setMarketsProgressBarEnabled(true);
        marketsListSubscription = marketRatesClient
                .getMarketsNearby(coordinates.getLat(), coordinates.getLon(), PAGINATION_LIMIT, marketsList != null ? marketsList.size() : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Market>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e, true);
                        view.setMarketsProgressBarEnabled(false);
                    }

                    @Override
                    public void onNext(ArrayList<Market> response) {
                        handleMarketsListResponse(response);
                    }
                });
    }

    void requestMarketRatesList(Market market) {
        if (marketRatesListSubscription != null && !marketRatesListSubscription.isUnsubscribed()) {
            marketRatesListSubscription.unsubscribe();
        }

        marketRatesList = null;
        view.notifyMarketRatesDataSetChanged();
        view.setMarketRatesProgressBarEnabled(true);
        marketRatesListSubscription = marketRatesClient
                .getMarketRates(market.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<MarketRate>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e, false);
                        view.setMarketRatesProgressBarEnabled(false);
                        view.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
                    }

                    @Override
                    public void onNext(ArrayList<MarketRate> response) {
                        handleMarketRatesListResponse(response);
                    }
                });
    }

    void handleError(Throwable e, boolean actionable) {
        if (e instanceof HttpException) {
            handleError((HttpException) e, actionable);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e, actionable);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e, actionable);
        } else if (e instanceof GoogleClientConnectionSuspendedException) {
            handleError((GoogleClientConnectionSuspendedException) e, actionable);
        } else if (e instanceof GoogleClientConnectionFailedException) {
            handleError((GoogleClientConnectionFailedException) e, actionable);
        } else if (e instanceof LocationProviderDisabledException) {
            handleError((LocationProviderDisabledException) e, actionable);
        } else if (e instanceof SecurityException) {
            handleError((SecurityException) e, actionable);
        } else if (e instanceof TimeoutException) {
            handleError((TimeoutException) e, actionable);
        } else if (e instanceof LocationNotFoundException) {
            handleError((LocationNotFoundException) e, actionable);
        } else {
            Timber.e(e, "Unable to get content.");

            view.showError(R.string.retry, R.string.an_error_occurred, actionable);
        }
    }

    void handleError(HttpException e, boolean actionable) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred, actionable);
                break;
        }
    }

    void handleError(SocketTimeoutException e, boolean actionable) {
        view.showError(R.string.unable_to_contact_server, actionable);
    }

    void handleError(ConnectException e, boolean actionable) {
        view.showError(R.string.please_check_your_internet_connection, actionable);
    }

    void handleError(GoogleClientConnectionSuspendedException e, boolean actionable) {
        view.showError(R.string.unable_to_get_your_location, actionable);
    }

    void handleError(GoogleClientConnectionFailedException e, boolean actionable) {
        view.showError(R.string.unable_to_get_your_location, actionable);
    }

    void handleError(LocationProviderDisabledException e, boolean actionable) {
        view.showError(actionable ? R.string.solve : -1, R.string.please_enable_location_access, actionable);
    }

    void handleError(SecurityException e, boolean actionable) {
        view.showError(actionable ? R.string.solve : -1, R.string.location_permission_required_for_forecast, actionable);
    }

    void handleError(TimeoutException e, boolean actionable) {
        view.showError(R.string.unable_to_get_your_location, actionable);
    }

    void handleError(LocationNotFoundException e, boolean actionable) {
        view.showError(R.string.unable_to_get_your_location, actionable);
    }

    void handleLocationResponse(Coordinates coordinates) {
        requestMarketsList(coordinates);
    }

    void handleMarketsListResponse(ArrayList<Market> response) {
        if (marketsList == null) {
            marketsList = response;
            view.notifyMarketsDataSetChanged();
        } else {
            marketsList.addAll(response);
            view.notifyMarketsItemRangeInserted(marketsList.size() - response.size(), response.size());
        }

        view.setMarketsProgressBarEnabled(false);
    }

    void handleMarketRatesListResponse(ArrayList<MarketRate> response) {
        marketRatesList = response;
        view.notifyMarketRatesDataSetChanged();
        view.setMarketRatesProgressBarEnabled(false);
    }
}
