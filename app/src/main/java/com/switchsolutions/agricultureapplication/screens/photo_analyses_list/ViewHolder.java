package com.switchsolutions.agricultureapplication.screens.photo_analyses_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.switchsolutions.agricultureapplication.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

final class ViewHolder extends RecyclerView.ViewHolder {

    private static final java.text.DateFormat DATE_FORMAT = SimpleDateFormat.getDateTimeInstance();

    private final TextView viewTag;
    private final TextView viewDate;
    private final ImageView viewImage1;
    private final ImageView viewImage2;
    private final ImageView viewImage3;

    ViewHolder(View itemView) {
        super(itemView);

        viewTag = (TextView) itemView.findViewById(R.id.tag);
        viewDate = (TextView) itemView.findViewById(R.id.date);
        viewImage1 = (ImageView) itemView.findViewById(R.id.image1);
        viewImage2 = (ImageView) itemView.findViewById(R.id.image2);
        viewImage3 = (ImageView) itemView.findViewById(R.id.image3);
    }

    void setDate(Date date) {
        viewDate.setText(DATE_FORMAT.format(date));
    }

    void setImage1(File f) {
        if (f == null) {
            viewImage1.setImageDrawable(null);
        } else {
            Glide.with(itemView.getContext())
                    .load(f)
                    .fitCenter()
                    .into(viewImage1);
        }
    }

    void setImage2(File f) {
        if (f == null) {
            viewImage2.setImageDrawable(null);
        } else {
            Glide.with(itemView.getContext())
                    .load(f)
                    .fitCenter()
                    .into(viewImage2);
        }
    }

    void setImage3(File f) {
        if (f == null) {
            viewImage3.setImageDrawable(null);
        } else {
            Glide.with(itemView.getContext())
                    .load(f)
                    .fitCenter()
                    .into(viewImage3);

        }
    }

    void setStatusCancelled() {
        viewTag.setBackgroundResource(R.drawable.rounded_corners_red);
        viewTag.setText(R.string.cancelled);
    }

    void setStatusInProgress() {
        viewTag.setBackgroundResource(R.drawable.rounded_corners_blue);
        viewTag.setText(R.string.in_progress);
    }

    void setStatusCompleted() {
        viewTag.setBackgroundResource(R.drawable.rounded_corners_green);
        viewTag.setText(R.string.solved);
    }

}
