package com.switchsolutions.agricultureapplication.screens.products_list;

import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewProductImage;
    private final TextView viewCompanyName;
    private final TextView viewProductName;
    private final TextView viewProductRate;
    private final ImageView viewPromotionIcon;

    private final BitmapImageViewTarget bitmapImageViewTarget;
    private final int selectableItemBackgroundResId;

    ViewHolder(final View itemView) {
        super(itemView);

        viewProductImage = (ImageView) itemView.findViewById(R.id.product_image);
        viewCompanyName = (TextView) itemView.findViewById(R.id.company_name);
        viewProductName = (TextView) itemView.findViewById(R.id.product_name);
        viewProductRate = (TextView) itemView.findViewById(R.id.product_rate);
        viewPromotionIcon = (ImageView) itemView.findViewById(R.id.promotion_icon);

        TypedValue typedValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, typedValue, true);
        selectableItemBackgroundResId = typedValue.resourceId;

        bitmapImageViewTarget = new BitmapImageViewTarget(viewProductImage) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable rbd = RoundedBitmapDrawableFactory.create(itemView.getContext().getResources(), resource);
                rbd.setCircular(true);
                viewProductImage.setImageDrawable(rbd);
            }
        };
    }

    void setPromoted(boolean promoted) {
        if (promoted) {
            itemView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.md_yellow_100));
            viewPromotionIcon.setVisibility(View.VISIBLE);
        } else {
            itemView.setBackgroundResource(selectableItemBackgroundResId);
            viewPromotionIcon.setVisibility(View.INVISIBLE);
        }
    }

    void setImage(String filename) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(filename))
                .asBitmap()
                .skipMemoryCache(BuildConfig.DEBUG)
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .into(bitmapImageViewTarget);
    }

    void setCompanyName(String name) {
        viewCompanyName.setText(name);
    }

    void setProductName(String name) {
        viewProductName.setText(name);
    }

    void setProductRate(String rate) {
        viewProductRate.setText(rate);
    }
}
