package com.switchsolutions.agricultureapplication.screens.settings_container;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;

public class SettingsContainerPresenter implements SettingsContainerContract.Presenter {

    private final SettingsContainerContract.View view;

    public SettingsContainerPresenter(SettingsContainerContract.View view, ApplicationComponent component) {
        this.view = view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            view.showSettingsScreen();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }
}
