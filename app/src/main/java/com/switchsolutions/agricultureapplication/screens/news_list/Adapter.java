package com.switchsolutions.agricultureapplication.screens.news_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final int VIEW_TYPE_BASIC = 1;
    static final int VIEW_TYPE_IMAGE = 2;

    private final NewsListScreenContract.Presenter presenter;

    Adapter(NewsListScreenContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public int getItemViewType(int position) {
        return presenter.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_BASIC:
                return onCreateViewHolderBasic(parent);
            case VIEW_TYPE_IMAGE:
                return onCreateViewHolderImage(parent);
            default:
                return null;
        }
    }

    private ViewHolderBasic onCreateViewHolderBasic(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_news_list_basic_item, parent, false);
        final ViewHolderBasic viewHolder = new ViewHolderBasic(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    private ViewHolderImage onCreateViewHolderImage(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_news_list_image_item, parent, false);
        final ViewHolderImage viewHolder = new ViewHolderImage(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_TYPE_BASIC:
                presenter.onBindViewHolder((ViewHolderBasic) holder, position);
                break;
            case VIEW_TYPE_IMAGE:
                presenter.onBindViewHolder((ViewHolderImage) holder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
