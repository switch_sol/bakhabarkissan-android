package com.switchsolutions.agricultureapplication.screens.forecast_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

public interface ForecastListContract {

    interface View {

        void navigateUp();

        void showLocationSettingsScreen();

        void showBanner();

        void setAdapter();

        void notifyItemChanged(int position);

        void notifyDataSetChanged();

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int actionStringRes, @StringRes int errorStringRes);

        void hideError();

        void requestPermissions(@NonNull String[] permissions, int requestCode);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemViewType(int position);

        void onBindViewHolder(CollapsedItemViewHolder viewHolder, int position);

        void onBindViewHolder(ExpandedItemViewHolder viewHolder, int position);

        int getItemCount();

        void onListItemClicked(int position);

        void onActionButtonClicked();

        void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults);

        void onLocationSettingsScreenResult();
    }
}
