package com.switchsolutions.agricultureapplication.screens.videos_list;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.screens.video_container.VideoContainerActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class VideosListActivity extends BaseActivity implements VideosListContract.View {

    private static final int RC_VIDEO_CONTAINER = 5;

    private static final String KEY_CATEGORY_PARENT = "category_parent";
    private static final String KEY_CATEGORY_CHILD = "category_child";
    private static final String KEY_ERROR_STRING_RES = "error_string_res";

    private VideosListContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;

    public static Intent createIntent(Context context, VideoCategoryParent vcp, VideoCategoryChild vcc) {
        Intent intent = new Intent(context, VideosListActivity.class);
        intent.putExtra(KEY_CATEGORY_PARENT, vcp);
        intent.putExtra(KEY_CATEGORY_CHILD, vcc);
        return intent;
    }

    public static Intent createResultIntent(int errorStringRes) {
        Intent intent = new Intent();
        intent.putExtra(KEY_ERROR_STRING_RES, errorStringRes);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));
        viewList.setHasFixedSize(true);

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        VideoCategoryParent vcp = getIntent().getParcelableExtra(KEY_CATEGORY_PARENT);
        VideoCategoryChild vcc = getIntent().getParcelableExtra(KEY_CATEGORY_CHILD);
        presenter = new VideosListPresenter(this, vcp, vcc, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RC_VIDEO_CONTAINER:
                if (resultCode == Activity.RESULT_CANCELED && data != null) {
                    presenter.onVideoContainerScreenResult(data.getIntExtra(KEY_ERROR_STRING_RES, 0));
                }
                break;
        }
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showVideoScreen(Video video) {
        Intent intent = VideoContainerActivity.createIntent(this, video);
        startActivityForResult(intent, RC_VIDEO_CONTAINER);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams swipeParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        swipeParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(swipeParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void setToolbarTitle(String title) {
        viewToolbar.setTitle(title);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyItemRangeInserted(int positionStart, int additionCount) {
        adapter.notifyItemRangeInserted(positionStart, additionCount);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewList, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }

}
