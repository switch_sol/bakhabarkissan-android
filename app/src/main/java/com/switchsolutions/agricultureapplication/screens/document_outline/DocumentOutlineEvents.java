package com.switchsolutions.agricultureapplication.screens.document_outline;

import com.switchsolutions.agricultureapplication.html.HTMLHeader;

public final class DocumentOutlineEvents {

    private DocumentOutlineEvents() {

    }

    public static final class OnHeaderSelected {
        public final HTMLHeader header;

        public OnHeaderSelected(HTMLHeader header) {
            this.header = header;
        }
    }
}
