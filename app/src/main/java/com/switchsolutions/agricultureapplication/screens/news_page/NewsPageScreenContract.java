package com.switchsolutions.agricultureapplication.screens.news_page;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;

import java.util.ArrayList;
import java.util.List;

public interface NewsPageScreenContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void setContent(List<HTMLElement> elements);

        void setContentItemInFocus(int position);

        void clearContentItemInFocus();

        void setNarrator(ArrayList<HTMLElement> elements);

        void scrollToContentPosition(int position);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onHTMLElementClicked(HTMLHeader header);

        void onHTMLElementClicked(HTMLParagraph paragraph);

        void onHTMLElementClicked(HTMLImage image);

        void onHTMLElementClicked(HTMLTable table);

        void onHTMLElementClicked(HTMLOrderedList orderedList);

        void onHTMLElementClicked(HTMLUnorderedList unorderedList);
    }
}
