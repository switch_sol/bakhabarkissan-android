package com.switchsolutions.agricultureapplication.screens.document_outline;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class DocumentOutlinePresenter implements DocumentOutlineContract.Presenter {

    private final DocumentOutlineContract.View view;
    private final List<HTMLHeader> headers;

    private final EventBus eventBus;

    DocumentOutlinePresenter(DocumentOutlineContract.View view, List<HTMLHeader> headers, ApplicationComponent component) {
        this.view = view;
        this.headers = headers;

        this.eventBus = component.eventBus();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setAdapter();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onListItemClicked(int position) {
        eventBus.post(new DocumentOutlineEvents.OnHeaderSelected(headers.get(position)));
    }

    @Override
    public int getItemViewType(int position) {
        return headers.get(position).getHeaderSize();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setContent(headers.get(position));
    }

    @Override
    public int getItemCount() {
        return headers.size();
    }
}
