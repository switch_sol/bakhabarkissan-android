package com.switchsolutions.agricultureapplication.screens.video_categories_child_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.api.videos.VideosClient;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Languages;
import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class VideoCategoriesChildListPresenter implements VideoCategoriesChildListContract.Presenter {

    private static final String SCREEN_NAME = "Video Categories Child List";

    private static final int PAGINATION_LIMIT = 10;

    private final VideoCategoriesChildListContract.View view;
    private final VideoCategoryParent parentCategory;
    private final VideosClient videosClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;
    private final Preferences preferences;

    private DateFormat dateFormat;

    private ArrayList<VideoCategoryChild> categoriesList;
    private Subscription subscription;
    private boolean shouldRequestMore = true;

    VideoCategoriesChildListPresenter(VideoCategoriesChildListContract.View view, VideoCategoryParent parentCategory, ApplicationComponent component) {
        this.view = view;
        this.parentCategory = parentCategory;
        this.videosClient = component.videosClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
        this.preferences = component.preferences();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (preferences.getLanguage().equals(Languages.EN)) {
            dateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT, LocaleUtils.LOCALE_EN);
        } else {
            dateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT, LocaleUtils.LOCALE_UR);
        }

        view.setAdapter();
        view.setToolbarTitle(parentCategory.getName());

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return categoriesList != null ? categoriesList.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VideoCategoryChild category = categoriesList.get(position);

        holder.setName(category.getName());
        holder.setImage(category.getImagePath());
        holder.setCount(category.getCount());
        holder.setUpdatedOn(dateFormat.format(new Date((long) category.getUpdatedOn() * 1000L)));

        if (shouldRequestMore && position == categoriesList.size() - 1) {
            requestContent();
        }
    }

    @Override
    public void onListItemClicked(int position) {
        VideoCategoryChild childCategory = categoriesList.get(position);

        analyticsClient.sendVideosChildCategorySelectedEvent(childCategory.getId());
        view.showVideosListScreen(parentCategory, childCategory);
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        subscription = videosClient
                .getCategories(parentCategory.getId(), PAGINATION_LIMIT, getItemCount())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<VideoCategoryChild>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<VideoCategoryChild> response) {
                        handleCategoriesResponse(response);
                    }
                });
    }

    void handleCategoriesResponse(ArrayList<VideoCategoryChild> response) {
        shouldRequestMore = response.size() == PAGINATION_LIMIT;

        if (categoriesList == null) {
            categoriesList = response;
            view.notifyDataSetChanged();
        } else {
            categoriesList.addAll(response);
            view.notifyItemRangeInserted(categoriesList.size() - response.size(), response.size());
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else {
            Timber.e(e, "Unable to get video parent categories list.");

            view.showError(R.string.an_error_occurred);
        }
        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }
}
