package com.switchsolutions.agricultureapplication.screens.settings_container;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.screens.settings.SettingsFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class SettingsContainerActivity extends BaseActivity implements SettingsContainerContract.View {

    private SettingsContainerContract.Presenter presenter;

    public static Intent createIntent(Context context) {
        return new Intent(context, SettingsContainerActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    presenter.onNavigationButtonClicked();
                }
            });
        }

        presenter = new SettingsContainerPresenter(this, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showSettingsScreen() {
        getFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container_settings, SettingsFragment.Companion.newInstance())
                .commit();
    }
}
