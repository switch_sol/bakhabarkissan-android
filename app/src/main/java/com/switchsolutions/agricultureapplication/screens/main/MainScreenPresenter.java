package com.switchsolutions.agricultureapplication.screens.main;

import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.Constants;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.preferences.Preferences;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class MainScreenPresenter implements MainScreenContract.Presenter {

    private static final String SCREEN_NAME = "Main";

    private static final Item[] ITEMS = new Item[]{
            Item.NEWS,
            Item.CROPS,
            Item.LIVESTOCK,
            Item.FARM_TO_HOME,
            Item.MODERN_FARMING_TECHNIQUES,
            Item.PRODUCTS,
            Item.MARKET_RATES,
            Item.FORECAST,
            Item.VIDEOS,
            Item.VISUAL_ANALYSIS,
            Item.CONTACT,
            Item.ABOUT_US
    };

    private static final List<Pair<String, Item>> KEYWORDS = new ArrayList<>();

    static {
        KEYWORDS.add(Pair.create("news", Item.NEWS));
        KEYWORDS.add(Pair.create("crops", Item.CROPS));
        KEYWORDS.add(Pair.create("livestock", Item.LIVESTOCK));
        KEYWORDS.add(Pair.create("farmtohome", Item.FARM_TO_HOME));
        KEYWORDS.add(Pair.create("modern farming techniques", Item.MODERN_FARMING_TECHNIQUES));
        KEYWORDS.add(Pair.create("farming techniques", Item.MODERN_FARMING_TECHNIQUES));
        KEYWORDS.add(Pair.create("products", Item.PRODUCTS));
        KEYWORDS.add(Pair.create("market", Item.MARKET_RATES));
        KEYWORDS.add(Pair.create("mandi", Item.MARKET_RATES));
        KEYWORDS.add(Pair.create("forecast", Item.FORECAST));
        KEYWORDS.add(Pair.create("weather", Item.FORECAST));
        KEYWORDS.add(Pair.create("videos", Item.VIDEOS));
        KEYWORDS.add(Pair.create("about", Item.ABOUT_US));
        KEYWORDS.add(Pair.create("contact", Item.CONTACT));
        KEYWORDS.add(Pair.create("photo", Item.VISUAL_ANALYSIS));
        KEYWORDS.add(Pair.create("visual analysis", Item.VISUAL_ANALYSIS));
    }

    private final MainScreenContract.View view;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;
    private final boolean fromLogin;

    private Subscription updateFCMTokenSubscription;

    public MainScreenPresenter(MainScreenContract.View view, ApplicationComponent component, boolean fromLogin) {
        this.view = view;
        this.preferences = component.preferences();
        this.accountsClient = component.accountsClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
        this.fromLogin = fromLogin;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (preferences.isFirstRun()) {
           view.showLoginScreen();
            return;
        }

        if (savedInstanceState == null) {
            view.showForecastTickerScreen();
            view.animateEntrance();
        }

        if (fromLogin) {
            updateFCMToken();
        }

        setSwitchViewMenuIconState();

        if (preferences.getMainViewPreferGrid()) {
            view.setGridAdapter();
        } else {
            view.setListAdapter();
        }

        try {
            analyticsClient.sendScreenEvent(SCREEN_NAME);
        } catch (Exception e) {

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (updateFCMTokenSubscription != null && !updateFCMTokenSubscription.isUnsubscribed()) {
            updateFCMTokenSubscription.unsubscribe();
        }
    }

    @Override
    public boolean onMenuItemSettingsClicked() {
        view.showSettingsScreen();
        return true;
    }

    @Override
    public boolean onMenuItemSpeechClicked() {
        view.requestSpeechRecognition();
        return true;
    }

    @Override
    public boolean onMenuItemSwitchViewClicked() {
        preferences.setMainViewPreferGrid(!preferences.getMainViewPreferGrid());
        setSwitchViewMenuIconState();

        view.animateBlurIn();

        return true;
    }

    @Override
    public void onSpeechRecognitionResult(String result) {
        double m = -1;
        Item r = null;
        for (Pair<String, Item> p : KEYWORDS) {
            double sim = similarity(result, p.first);
            if (sim > m) {
                m = sim;
                r = p.second;
            }
        }
        onItemClicked(r);
    }

    @Override
    public void onBlurAnimationCompleted() {
        if (preferences.getMainViewPreferGrid()) {
            view.setGridAdapter();
        } else {
            view.setListAdapter();
        }

        view.animateBlurOut();
    }

    @Override
    public int getItemCount() {
        return ITEMS.length;
    }

    @Override
    public void onBindViewHolder(ListViewHolder viewHolder, int position) {
        Item item = ITEMS[position];
        viewHolder.setImage(item.imageResId);
        viewHolder.setTitle(item.titleResId);
        viewHolder.setDescription(item.descriptionResId);
    }

    @Override
    public void onBindViewHolder(GridViewHolder viewHolder, int position) {
        Item item = ITEMS[position];
        viewHolder.setImage(item.imageResId);
        viewHolder.setTitle(item.titleResId);
    }

    @Override
    public int getSpanSize(int position) {
        if (preferences.getMainViewPreferGrid()) {
            if (ITEMS.length % 2 != 0 && position == ITEMS.length - 1) {
                return 2;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }

    @Override
    public void onListItemClicked(int position) {
        onItemClicked(ITEMS[position]);
    }

    @Override
    public void onScrollStateChanged(int newState) {
        if (newState == RecyclerView.SCROLL_STATE_IDLE) {
            view.scrollToCleanestPosition();
        }
    }

    void setSwitchViewMenuIconState() {
        if (preferences.getMainViewPreferGrid()) {
            view.setMenuItemSwitchViewIcon(R.drawable.ic_view_agenda_white_24dp);
            view.setMenuItemSwitchViewText(R.string.list);
        } else {
            view.setMenuItemSwitchViewIcon(R.drawable.ic_view_module_white_24dp);
            view.setMenuItemSwitchViewText(R.string.grid);
        }
    }

    void onItemClicked(Item item) {
        switch (item) {
            case NEWS:
                view.showNewsScreen();
                break;
            case FARM_TO_HOME:
                view.showFarmToHome();
                break;
            case CROPS:
                view.showCropsScreen();
                break;
            case LIVESTOCK:
                view.showLivestockScreen();
                break;
            case MODERN_FARMING_TECHNIQUES:
                view.showModernFarmingTechniquesScreen();
                break;
            case PRODUCTS:
                view.showProductsScreen();
                break;
            case MARKET_RATES:
                view.showMarketRatesScreen();
                break;
            case FORECAST:
                view.showForecastScreen();
                break;
            case VIDEOS:
                view.showVideosScreen();
                break;
            case VISUAL_ANALYSIS:
                view.showVisualAnalysisScreen();
                break;
            case CONTACT:
                analyticsClient.sendContactUsSelectedEvent();
                view.showContactScreen(Constants.SHORTCODE);
                break;
            case ABOUT_US:
                analyticsClient.sendAboutUsSelectedEvent();
                view.showWebPageScreen(Constants.URL_SPONSOR);
                break;
        }
    }

    void updateFCMToken() {
        String newToken = FirebaseInstanceId.getInstance().getToken();

        if (newToken == null) {
            return;
        }

        Timber.d("Updating FCM Token: %s", newToken);
        updateFCMTokenSubscription = accountsClient
                .updateFCMToken(newToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            Timber.d("Unable to update FCM token.");
                        } else if (e instanceof SocketTimeoutException) {
                            Timber.d("Unable to update FCM token.");
                        } else if (e instanceof ConnectException) {
                            Timber.d("Unable to update FCM token.");
                        } else {
                            Timber.e(e, "Unable to update FCM token.");
                        }
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        Timber.d("Updated FCM Token.");
                    }
                });
    }

    //this function takes two strings and finds its similarity
    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2;
            shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) {
            return 1.0; /* both strings are zero length */
        }
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
    }

    //this is the sub function of the string similarity function
    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

}
