package com.switchsolutions.agricultureapplication.screens.videos_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.api.videos.VideosClient;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Languages;
import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class VideosListPresenter implements VideosListContract.Presenter {

    private static final String SCREEN_NAME = "Videos List";

    private static final int PAGINATION_LIMIT = 10;

    private final VideosListContract.View view;
    private final VideoCategoryParent videoCategoryParent;
    private final VideoCategoryChild videoCategoryChild;
    private final VideosClient videosClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;
    private final Preferences preferences;

    private DateFormat dateFormat;
    private ArrayList<Video> videosList;
    private Subscription subscription;
    private boolean shouldRequestMore = true;

    VideosListPresenter(VideosListContract.View view, VideoCategoryParent videoCategoryParent, VideoCategoryChild videoCategoryChild, ApplicationComponent component) {
        this.view = view;
        this.videoCategoryParent = videoCategoryParent;
        this.videoCategoryChild = videoCategoryChild;
        this.videosClient = component.videosClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
        this.preferences = component.preferences();
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        if (preferences.getLanguage().equals(Languages.EN)) {
            dateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT, LocaleUtils.LOCALE_EN);
        } else {
            dateFormat = SimpleDateFormat.getDateInstance(DateFormat.DEFAULT, LocaleUtils.LOCALE_UR);
        }

        view.setToolbarTitle(videoCategoryChild.getName());
        view.setAdapter();

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (saveInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onVideoContainerScreenResult(int errorStringRes) {
        view.showError(errorStringRes);
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return videosList != null ? videosList.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Video video = videosList.get(position);

        holder.setImage(video.getImagePath());
        holder.setName(video.getName());
        holder.setUploadedOn(dateFormat.format(new Date((long) video.getDatetime() * 1000)));

        if (shouldRequestMore && position == videosList.size() - 1) {
            requestContent();
        }
    }

    @Override
    public void onListItemClicked(int position) {
        Video video = videosList.get(position);

        analyticsClient.sendVideoSelectedEvent(video.getId());
        view.showVideoScreen(video);
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        subscription = videosClient
                .getVideos(videoCategoryParent.getId(), videoCategoryChild.getId(), PAGINATION_LIMIT, videosList != null ? videosList.size() : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Video>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Video> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleResponse(ArrayList<Video> response) {
        shouldRequestMore = response.size() == PAGINATION_LIMIT;

        if (videosList == null) {
            videosList = response;
            view.notifyDataSetChanged();
        } else {
            videosList.addAll(response);
            view.notifyItemRangeInserted(videosList.size() - response.size(), response.size());
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else {
            Timber.e(e, "Unable to get video parent categories list.");

            view.showError(R.string.an_error_occurred);
        }
        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }
}
