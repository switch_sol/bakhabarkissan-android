package com.switchsolutions.agricultureapplication.screens.document_narrator;

import android.os.Bundle;

import java.io.IOException;

public interface DocumentNarratorContract {

    interface View {

        void setPauseResumeButtonEnabled(boolean enabled);

        void setProgressBarEnabled(boolean enabled);

        void setButtonToPause();

        void setButtonToResume();

        void createMediaPlayer();

        void prepareTrack(String uriStr) throws IOException;

        void startTrack() throws IllegalStateException;

        void pauseTrack() throws IllegalStateException;

        void stopTrack() throws IllegalStateException;

        void resetTrack() throws IllegalStateException;

        void releaseMediaPlayer() throws IllegalStateException;
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onStart();

        void onStop();

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onPrevButtonClicked();

        void onPauseResumeButtonClicked();

        void onNextButtonClicked();

        void onTrackStreamPrepared();

        void onTrackStreamFinished();

        boolean onTrackStreamError();
    }
}
