package com.switchsolutions.agricultureapplication.screens.forecast_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class ForecastListActivity extends BaseActivity implements ForecastListContract.View {

    private static final int RC_LOCATION_SETTINGS = 13;

    private ForecastListContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;
    private Snackbar viewSnackbar;

    public static Intent createIntent(Context context) {
        return new Intent(context, ForecastListActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));
        viewList.setHasFixedSize(false);

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        viewSnackbar = Snackbar.make(viewList, "", Snackbar.LENGTH_INDEFINITE);

        presenter = new ForecastListPresenter(this, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_LOCATION_SETTINGS) {
            presenter.onLocationSettingsScreenResult();
        }
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLocationSettingsScreen() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, RC_LOCATION_SETTINGS);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        params.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(params);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void notifyItemChanged(int position) {
        adapter.notifyItemChanged(position);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int actionStringRes, @StringRes int errorStringRes) {
        viewSnackbar
                .setText(errorStringRes)
                .setAction(actionStringRes, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.onActionButtonClicked();
                    }
                })
                .show();
    }

    @Override
    public void hideError() {
        viewSnackbar.dismiss();
    }
}
