package com.switchsolutions.agricultureapplication.screens.document_outline;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;

import java.util.ArrayList;

public class DocumentOutlineFragment extends Fragment implements DocumentOutlineContract.View {

    private static final String KEY_HEADERS = "headers";

    private DocumentOutlineContract.Presenter presenter;

    private RecyclerView viewList;

    private Adapter adapter;

    public static DocumentOutlineFragment newInstance(ArrayList<HTMLHeader> headers) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_HEADERS, headers);
        DocumentOutlineFragment fragment = new DocumentOutlineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document_outline, container, false);

        viewList = (RecyclerView) view.findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(getContext()));
        viewList.setHasFixedSize(true);
        viewList.setOverScrollMode(View.OVER_SCROLL_NEVER);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<HTMLHeader> headers = getArguments().getParcelableArrayList(KEY_HEADERS);
        presenter = new DocumentOutlinePresenter(this, headers, BaseApplication.getComponent(getContext()));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

}
