package com.switchsolutions.agricultureapplication.screens.product_dealers_list;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.util.Locale;

public class ProductDealersListActivity extends BaseActivity implements ProductDealersListContract.View {

    private static final int RC_LOCATION_SETTINGS = 13;

    private static final String KEY_PRODUCT_CATEGORY = "product_category";

    private ProductDealersListContract.Presenter presenter;

    private Adapter adapter;
    private BottomSheetBehavior bottomSheetBehavior;
    private GoogleMap googleMap;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;
    private View viewBottomSheetContainer;
    private TextView viewProductDealerDistance;
    private TextView viewProductDealerName;
    private TextView viewProductDealerDescription;
    private Button viewProductDealerNumber;
    private Snackbar viewSnackbar;

    public static Intent createIntent(Context context, ProductCategory productCategory) {
        Intent intent = new Intent(context, ProductDealersListActivity.class);
        intent.putExtra(KEY_PRODUCT_CATEGORY, productCategory);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_dealers_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));
        viewList.setHasFixedSize(false);

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        viewBottomSheetContainer = findViewById(R.id.container_bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(viewBottomSheetContainer);
        bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (presenter != null) {
                    presenter.onBottomSheetStateChanged(newState);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        viewProductDealerDistance = (TextView) viewBottomSheetContainer.findViewById(R.id.product_dealer_distance);
        viewProductDealerName = (TextView) viewBottomSheetContainer.findViewById(R.id.product_dealer_name);
        viewProductDealerDescription = (TextView) viewBottomSheetContainer.findViewById(R.id.product_dealer_description);
        viewProductDealerNumber = (Button) viewBottomSheetContainer.findViewById(R.id.button_call_dealer);
        viewProductDealerNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onCallDealerButtonClicked();
            }
        });

        viewSnackbar = Snackbar.make(viewList, "", Snackbar.LENGTH_INDEFINITE);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.container_map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap gm) {
                googleMap = gm;
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.getUiSettings().setMapToolbarEnabled(false);
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        presenter.onMapClicked(latLng.latitude, latLng.longitude);
                    }
                });
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        LatLng latLng = marker.getPosition();
                        presenter.onMapClicked(latLng.latitude, latLng.longitude);
                        return true;
                    }
                });

                presenter.onMapReady();
            }
        });

        ProductCategory productCategory = getIntent().getParcelableExtra(KEY_PRODUCT_CATEGORY);
        presenter = new ProductDealersListPresenter(this, productCategory, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void onBackPressed() {
        if (!presenter.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_LOCATION_SETTINGS) {
            presenter.onLocationSettingsScreenResult();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLocationSettingsScreen() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, RC_LOCATION_SETTINGS);
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams swipeParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        swipeParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(swipeParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void showCallScreen(String number) {
        String uri = String.format("tel:%s", number);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public void showMapScreen(double lat, double lon) {
        String uri = String.format(Locale.ENGLISH, "geo:%f,%f", lat, lon);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {

        }
    }

    @Override
    public void setBottomSheetState(int state) {
        bottomSheetBehavior.setState(state);
    }

    @Override
    public void setFocusedDealerDistance(double kilometers) {
        viewProductDealerDistance.setText(getString(R.string.fmt_distance_kilometers, kilometers));
    }

    @Override
    public void setFocusedDealerName(String name) {
        viewProductDealerName.setText(name);
        viewProductDealerNumber.setText(getString(R.string.fmt_call_company, name));
    }

    @Override
    public void setFocusedDealerDescription(String description) {
        viewProductDealerDescription.setText(description);
    }

    @Override
    public void setFocusedDealerMarker(String dealerName, double dealerLat, double dealerLon) {
        googleMap.clear();

        LatLng dealerCoordinates = new LatLng(dealerLat, dealerLon);
        MarkerOptions dealerMarkerOptions = new MarkerOptions()
                .position(dealerCoordinates)
                .title(dealerName)
                .draggable(false);
        googleMap.addMarker(dealerMarkerOptions);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dealerCoordinates, 17.5f));
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyItemRangeInserted(int positionStart, int additionCount) {
        adapter.notifyItemRangeInserted(positionStart, additionCount);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        showError(R.string.retry, stringRes);
    }

    @Override
    public void showError(@StringRes int actionStringRes, @StringRes int errorStringRes) {
        viewSnackbar
                .setText(errorStringRes)
                .setAction(actionStringRes, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.onActionButtonClicked();
                    }
                })
                .show();
    }

    @Override
    public void hideError() {
        viewSnackbar.dismiss();
    }
}
