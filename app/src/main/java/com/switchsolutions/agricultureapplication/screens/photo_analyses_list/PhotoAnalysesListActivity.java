package com.switchsolutions.agricultureapplication.screens.photo_analyses_list;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.screens.create_photo_analysis.CreatePhotoAnalysisActivity;
import com.switchsolutions.agricultureapplication.screens.view_photo_analysis.ViewPhotoAnalysisActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class PhotoAnalysesListActivity extends BaseActivity implements PhotoAnalysesListContract.View {

    private static final int RC_PERMISSION_STORAGE = 25;

    private PhotoAnalysesListContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private FloatingActionButton viewFAB;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_analyses_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));

        viewFAB = (FloatingActionButton) findViewById(R.id.fab);
        viewFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onFABClicked();
            }
        });

        presenter = new PhotoAnalysesListPresenter(this, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSION_STORAGE:
                presenter.onStoragePermissionResult(grantResults[0] == PackageManager.PERMISSION_GRANTED);
                break;
        }
    }

    @Override
    public void showViewPhotoAnalysisScreen(PhotoAnalysis photoAnalysis) {
        Intent intent = ViewPhotoAnalysisActivity.createIntent(this, photoAnalysis);
        startActivity(intent);
    }

    @Override
    public void showCreatePhotoAnalysisScreen() {
        Intent intent = CreatePhotoAnalysisActivity.createIntent(this);
        startActivity(intent);
    }

    @Override
    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, RC_PERMISSION_STORAGE);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showError(@StringRes int stringRes) {

    }

    public static Intent createIntent(Context context) {
        return new Intent(context, PhotoAnalysesListActivity.class);
    }
}
