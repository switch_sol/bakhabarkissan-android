package com.switchsolutions.agricultureapplication.screens.image_sources_list;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewTitle;

    public ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.image);
        viewTitle = (TextView) itemView.findViewById(R.id.title);
    }

    public void setImage(@DrawableRes int drawableRes) {
        Glide.with(itemView.getContext())
                .load(drawableRes)
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage);
    }

    public void setTitle(@StringRes int stringRes) {
        viewTitle.setText(stringRes);
    }

}
