package com.switchsolutions.agricultureapplication.screens.view_photo_analysis;

import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;

public interface ViewPhotoAnalysisContract {

    interface View {

        void showResponsePage(PhotoAnalysis photoAnalysis);

        void navigateUp();

        void setTag(@StringRes int stringRes, @ColorRes int colorRes);

        void hideAudioContainer();

        void showActionButton();

        void setAudioFABIcon(@DrawableRes int drawableRes);

        void setAudioFABEnabled(boolean enabled);

        void showError(@StringRes int stringRes);

        void setIssueText(String text);

        void setAudioProgressBarLimit(int n);

        void setAudioProgressBar(int i);

        void clearAudioLength();

        void setAudioLength(int i, int n);

        void setAdapter();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onResume();

        void onPause();

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onAudioFABClicked();

        void onBindViewHolder(ViewHolder holder, int position);

        int getItemCount();

        void onImageItemClicked(int position);

        void onActionButtonClicked();
    }
}
