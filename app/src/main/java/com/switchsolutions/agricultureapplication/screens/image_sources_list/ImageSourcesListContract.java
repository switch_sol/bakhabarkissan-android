package com.switchsolutions.agricultureapplication.screens.image_sources_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

public class ImageSourcesListContract {

    interface View {

        void navigateUp();

        void showWebPageScreen(@StringRes int stringRes);

        void setAdapter();
    }

    interface Presenter {

        void onCreate(Bundle onSavedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onItemClicked(int position);

        void onBindViewHolder(ViewHolder viewHolder, int position);

        int getItemCount();
    }
}
