package com.switchsolutions.agricultureapplication.screens.image_sources_list;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class ImageSourcesListActivity extends BaseActivity implements ImageSourcesListContract.View {

    private ImageSourcesListContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;

    public static Intent createIntent(Context context) {
        return new Intent(context, ImageSourcesListActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_sources_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));
        viewList.setHasFixedSize(true);

        presenter = new ImageSourcesListPresenter(this);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showWebPageScreen(@StringRes int stringRes) {
        String url = getString(stringRes);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }
}
