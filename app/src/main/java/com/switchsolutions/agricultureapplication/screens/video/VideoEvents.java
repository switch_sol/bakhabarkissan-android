package com.switchsolutions.agricultureapplication.screens.video;

public final class VideoEvents {

    private VideoEvents() {
    }

    public static final class OnVideoError {

    }

    public static final class OnVideoCompleted {

    }
}
