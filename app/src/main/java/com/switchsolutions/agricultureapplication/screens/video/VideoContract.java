package com.switchsolutions.agricultureapplication.screens.video;

import android.os.Bundle;

public interface VideoContract {

    interface View {

        void setControlsEnabled(boolean enabled);

        void setVideoImage(String thumbPath, String imagePath);

        void setVideo(String videoPath);

        void startVideo();

        void stopVideo();

        void seekVideo(int milliseconds);

        void setBottomControlsVisible(boolean visible);

        void setBottomControlsText(int stringRes);

        void setBottomControlsEnabled(boolean enabled);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onVideoPrepared();

        void onVideoCompleted();

        boolean onVideoError();

        void onBottomControlClicked();
    }

}
