package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

final class AdapterMarkets extends RecyclerView.Adapter<ViewHolderMarket> {

    private final MarketRatesListContract.Presenter presenter;

    AdapterMarkets(MarketRatesListContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public ViewHolderMarket onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.viewholder_market_rates_list_market_item, parent, false);
        final ViewHolderMarket viewHolder = new ViewHolderMarket(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onMarketListItemClicked(viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderMarket holder, int position) {
        presenter.onBindMarketViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getMarketsItemCount();
    }
}
