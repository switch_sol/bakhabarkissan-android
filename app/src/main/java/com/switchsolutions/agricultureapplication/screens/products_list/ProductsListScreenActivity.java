package com.switchsolutions.agricultureapplication.screens.products_list;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.screens.product_page.ProductPageScreenActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class ProductsListScreenActivity extends BaseActivity implements ProductsListScreenContract.View {

    private static final String KEY_PRODUCT_CATEGORY = "product_category";

    private ProductsListScreenContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private RecyclerView viewList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;

    public static Intent createIntent(Context context, ProductCategory productCategory) {
        Intent intent = new Intent(context, ProductsListScreenActivity.class);
        intent.putExtra(KEY_PRODUCT_CATEGORY, productCategory);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewList = (RecyclerView) findViewById(R.id.list_view);
        viewList.setLayoutManager(new LinearLayoutManager(this));

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        ProductCategory productCategory = getIntent().getParcelableExtra(KEY_PRODUCT_CATEGORY);
        presenter = new ProductsListScreenPresenter(this, productCategory, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showProductScreen(ProductCategory category, Product product) {
        Intent intent = ProductPageScreenActivity.createIntent(this, category, product);
        startActivity(intent);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams swipeParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        swipeParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(swipeParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void setToolbarTitle(String title) {
        viewToolbar.setTitle(title);
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewList.setAdapter(adapter);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notifyItemRangeInserted(int positionStart, int additionCount) {
        adapter.notifyItemRangeInserted(positionStart, additionCount);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringResId) {
        Snackbar.make(viewList, stringResId, Snackbar.LENGTH_LONG)
                .show();
    }

}
