package com.switchsolutions.agricultureapplication.screens.product_dealers_list;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionFailedException;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionSuspendedException;
import com.switchsolutions.agricultureapplication.api.location.LocationClient;
import com.switchsolutions.agricultureapplication.api.location.LocationNotFoundException;
import com.switchsolutions.agricultureapplication.api.location.LocationProviderDisabledException;
import com.switchsolutions.agricultureapplication.api.products.ProductsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.models.ProductDealer;
import com.switchsolutions.agricultureapplication.utils.Coordinates;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ProductDealersListPresenter implements ProductDealersListContract.Presenter {

    private static final String SCREEN_NAME = "Product Dealers List";

    private static final int RC_LOCATION_PERMISSION = 30;
    private static final int PAGINATION_LIMIT = 10;

    private final ProductDealersListContract.View view;
    private final ProductCategory productCategory;
    private final LocationClient locationClient;
    private final ProductsClient productsClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private Coordinates currentLocation;
    private ArrayList<ProductDealer> productDealersList;
    private Subscription locationSubscription;
    private Subscription productDealersSubscription;
    private boolean isMapReady = false;
    private int bottomSheetState;
    private ProductDealer focusedProductDealer;

    ProductDealersListPresenter(ProductDealersListContract.View view, ProductCategory productCategory, ApplicationComponent component) {
        this.view = view;
        this.productCategory = productCategory;
        this.locationClient = component.locationClient();
        this.productsClient = component.productsClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setBottomSheetState(bottomSheetState = BottomSheetBehavior.STATE_HIDDEN);

        requestLocation();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            locationSubscription.unsubscribe();
        }

        if (productDealersSubscription != null && !productDealersSubscription.isUnsubscribed()) {
            productDealersSubscription.unsubscribe();
        }
    }

    @Override
    public boolean onBackPressed() {
        if (bottomSheetState == BottomSheetBehavior.STATE_EXPANDED || bottomSheetState == BottomSheetBehavior.STATE_COLLAPSED) {
            view.setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);
            return true;
        }
        return false;
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return productDealersList != null ? productDealersList.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProductDealer productDealer = productDealersList.get(position);

        holder.setName(productDealer.getName());
        holder.setDescription(productDealer.getDescription());
        holder.setNumber(productDealer.getNumber());
    }

    @Override
    public void onListItemClicked(int position) {
        focusedProductDealer = productDealersList.get(position);

        view.setBottomSheetState(BottomSheetBehavior.STATE_COLLAPSED);
        view.setFocusedDealerDistance(distanceBetween(currentLocation.getLat(), currentLocation.getLon(), focusedProductDealer.getLat(), focusedProductDealer.getLon()));
        view.setFocusedDealerName(focusedProductDealer.getName());
        view.setFocusedDealerDescription(focusedProductDealer.getDescription());

        if (isMapReady) {
            String dealerName = focusedProductDealer.getName();
            double dealerLat = focusedProductDealer.getLat();
            double dealerLon = focusedProductDealer.getLon();
            view.setFocusedDealerMarker(dealerName, dealerLat, dealerLon);
        }

        analyticsClient.sendProductDealerSelectedEvent(focusedProductDealer.getId());
    }

    @Override
    public void onMapReady() {
        isMapReady = true;

        if (focusedProductDealer != null) {
            String dealerName = focusedProductDealer.getName();
            double dealerLat = focusedProductDealer.getLat();
            double dealerLon = focusedProductDealer.getLon();
            view.setFocusedDealerMarker(dealerName, dealerLat, dealerLon);
        }
    }

    @Override
    public void onBottomSheetStateChanged(int newState) {
        bottomSheetState = newState;
    }

    @Override
    public void onCallDealerButtonClicked() {
        if (focusedProductDealer != null) {
            String number = focusedProductDealer.getNumber();

            analyticsClient.sendCallProductDealerEvent(number);
            view.showCallScreen(number);
        }
    }

    @Override
    public void onMapClicked(double lat, double lon) {
        view.showMapScreen(lat, lon);
    }

    @Override
    public void onActionButtonClicked() {
        requestContent();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RC_LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                } else {
                    view.showError(R.string.solve, R.string.location_permission_required_for_forecast);
                }
                break;
            default:
                // Nothing
        }
    }

    @Override
    public void onLocationSettingsScreenResult() {
        requestContent();
    }

    void requestContent() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        if (productDealersSubscription != null && !productDealersSubscription.isUnsubscribed()) {
            return;
        }

        if (!locationClient.hasPermission()) {
            view.requestPermissions(new String[]{ACCESS_FINE_LOCATION}, RC_LOCATION_PERMISSION);
            return;
        }

        if (!locationClient.hasAccess()) {
            view.showLocationSettingsScreen();
            return;
        }

        requestLocation();
    }

    void requestLocation() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        locationSubscription = locationClient
                .getLocation()
                .timeout(10, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Coordinates>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(Coordinates response) {
                        handleLocationResponse(response);
                    }
                });
    }

    void requestProductDealers() {
        if (productDealersSubscription != null && !productDealersSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        productDealersSubscription = productsClient
                .getProductDealers(productCategory.getId(), currentLocation.getLat(), currentLocation.getLon(), PAGINATION_LIMIT, productDealersList != null ? productDealersList.size() : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<ProductDealer>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<ProductDealer> response) {
                        handleContentResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else if (e instanceof GoogleClientConnectionSuspendedException) {
            handleError((GoogleClientConnectionSuspendedException) e);
        } else if (e instanceof GoogleClientConnectionFailedException) {
            handleError((GoogleClientConnectionFailedException) e);
        } else if (e instanceof LocationProviderDisabledException) {
            handleError((LocationProviderDisabledException) e);
        } else if (e instanceof SecurityException) {
            handleError((SecurityException) e);
        } else if (e instanceof TimeoutException) {
            handleError((TimeoutException) e);
        } else if (e instanceof LocationNotFoundException) {
            handleError((LocationNotFoundException) e);
        } else {
            Timber.e(e, "Unable to get content.");

            view.showError(R.string.retry, R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.retry, R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.retry, R.string.please_check_your_internet_connection);
    }

    void handleError(GoogleClientConnectionSuspendedException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(GoogleClientConnectionFailedException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationProviderDisabledException e) {
        view.showError(R.string.solve, R.string.please_enable_location_access);
    }

    void handleError(SecurityException e) {
        view.showError(R.string.solve, R.string.location_permission_required_for_forecast);
    }

    void handleError(TimeoutException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationNotFoundException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleLocationResponse(Coordinates response) {
        currentLocation = response;

        requestProductDealers();
    }

    void handleContentResponse(ArrayList<ProductDealer> response) {
        if (productDealersList == null) {
            productDealersList = response;
            view.setAdapter();
        } else {
            productDealersList.addAll(response);
            view.notifyItemRangeInserted(productDealersList.size() - response.size(), response.size());
        }
        view.hideError();
        view.setProgressBarEnabled(false);
    }

    double distanceBetween(double lat1, double lon1, double lat2, double lon2) {
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (6371 * c * 1000) * 0.001;
    }

}
