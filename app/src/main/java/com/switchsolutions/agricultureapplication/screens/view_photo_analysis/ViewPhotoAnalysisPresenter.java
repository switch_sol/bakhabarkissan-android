package com.switchsolutions.agricultureapplication.screens.view_photo_analysis;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.audio.AudioPlayer;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.io.File;
import java.io.IOException;

public class ViewPhotoAnalysisPresenter implements ViewPhotoAnalysisContract.Presenter {

    private final ViewPhotoAnalysisContract.View view;
    private final PhotoAnalysis photoAnalysis;
    private final AudioPlayer audioPlayer;
    private final Storage storage;

    private final AudioPlayer.Listener audioPlayerListener = new AudioPlayer.Listener() {
        @Override
        public void onAudioPlayerCompleted() {
            audioPlayer.reset();

            view.setAudioFABIcon(R.drawable.ic_play_arrow_white_24dp);
            view.setAudioProgressBar(0);
            view.clearAudioLength();
        }

        @Override
        public void onAudioPlayerSecond(long seconds) {
            view.setAudioLength((int) seconds, audioPlayer.getLength());
        }

        @Override
        public void onAudioPlayerStateChanged(int state) {
            switch (state) {
                case AudioPlayer.STATE_STARTED:
                    view.setAudioFABEnabled(true);
                    view.setAudioFABIcon(R.drawable.ic_stop_white_24dp);
                    view.setAudioProgressBarLimit(audioPlayer.getLength());
                    break;
                case AudioPlayer.STATE_STOPPED:
                    view.setAudioFABEnabled(true);
                    view.setAudioFABIcon(R.drawable.ic_play_arrow_white_24dp);
                    view.setAudioProgressBar(0);
                    view.setAudioProgressBarLimit(0);
                    view.clearAudioLength();
                    break;
            }
        }
    };

    ViewPhotoAnalysisPresenter(ViewPhotoAnalysisContract.View view, ApplicationComponent component, PhotoAnalysis photoAnalysis) {
        this.view = view;
        this.photoAnalysis = photoAnalysis;

        this.audioPlayer = component.audioPlayer();
        this.storage = component.storage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (photoAnalysis.getStatus() == 0) {
            view.setTag(R.string.in_progress, R.color.md_blue_500);
        } else if (photoAnalysis.getStatus() < 0) {
            view.setTag(R.string.cancelled, R.color.md_red_500);
        } else {
            view.setTag(R.string.solved, R.color.md_green_500);
            view.showActionButton();
        }

        view.setIssueText(photoAnalysis.getText());

        if (photoAnalysis.getAudio() == null || photoAnalysis.getAudio().equals("")) {
            view.hideAudioContainer();
        }

        view.setAdapter();

        audioPlayer.setListener(audioPlayerListener);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {
        if (audioPlayer.getState() != AudioPlayer.STATE_RESET) {
            audioPlayer.stop();
            audioPlayer.reset();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        audioPlayer.setListener(null);
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onAudioFABClicked() {
        if (photoAnalysis.getAudio() != null && !photoAnalysis.getAudio().equals("")) {
            if (audioPlayer.getState() == AudioPlayer.STATE_RESET) {
                File file = storage.getAudioFile(photoAnalysis.getAudio());
                if (file != null) {
                    try {
                        audioPlayer.prepare(file);
                        audioPlayer.start();
                    } catch (IOException e) {
                        view.showError(R.string.an_error_occurred);
                    }
                }
            } else {
                audioPlayer.stop();
                audioPlayer.reset();
            }
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setImage(storage.getImageFile(photoAnalysis.getPhotos()[position]));
    }

    @Override
    public int getItemCount() {
        return photoAnalysis.getPhotos() != null ? photoAnalysis.getPhotos().length : 0;
    }

    @Override
    public void onImageItemClicked(int position) {

    }

    @Override
    public void onActionButtonClicked() {
        view.showResponsePage(photoAnalysis);
    }
}
