package com.switchsolutions.agricultureapplication.screens.view_photo_analysis_response;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.photo_analysis.PhotoAnalysisClient;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorEvents;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

class ViewPhotoAnalysisResponsePresenter implements ViewPhotoAnalysisResponseContract.Presenter {

    private final ViewPhotoAnalysisResponseContract.View view;
    private final PhotoAnalysis photoAnalysis;
    private final PhotoAnalysisClient photoAnalysisClient;
    private final EventBus eventBus;

    private Subscription contentSubscription;
    private Subscription narratorSubscription;

    ViewPhotoAnalysisResponsePresenter(ViewPhotoAnalysisResponseContract.View view, PhotoAnalysis photoAnalysis, ApplicationComponent component) {
        this.view = view;
        this.photoAnalysis = photoAnalysis;
        this.photoAnalysisClient = component.photoAnalysisClient();
        this.eventBus = component.eventBus();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestContent();

        eventBus.register(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);

        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            contentSubscription.unsubscribe();
            contentSubscription = null;
        }

        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            narratorSubscription.unsubscribe();
            narratorSubscription = null;
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onHTMLElementClicked(HTMLHeader header) {
        if (header.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(header));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLParagraph paragraph) {
        if (paragraph.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(paragraph));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLImage image) {

    }

    @Override
    public void onHTMLElementClicked(HTMLTable table) {
        if (table.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(table));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLOrderedList orderedList) {
        if (orderedList.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(orderedList));
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLUnorderedList unorderedList) {
        if (unorderedList.getAudioPath() != null) {
            eventBus.post(new DocumentNarratorEvents.PrepareTrack(unorderedList));
        }
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackPrepared event) {
        view.setContentItemInFocus(event.element.getIndex());
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnTrackReset event) {
        view.clearContentItemInFocus();
    }

    @Subscribe
    public void onEvent(DocumentNarratorEvents.OnError event) {
        view.showError(event.stringRes);
    }

    void requestContent() {
        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        contentSubscription = photoAnalysisClient
                .getResponse(photoAnalysis.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(List<HTMLElement> response) {
                        view.setContent(response);
                        view.setProgressBarEnabled(false);

                        prepareNarrator(response);
                    }
                });
    }

    void prepareNarrator(final List<HTMLElement> elements) {
        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            narratorSubscription.unsubscribe();
        }

        narratorSubscription = Observable
                .from(elements)
                .filter(new Func1<HTMLElement, Boolean>() {
                    @Override
                    public Boolean call(HTMLElement htmlElement) {
                        return htmlElement.getAudioPath() != null;
                    }
                })
                .toList()
                .map(new Func1<List<HTMLElement>, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(List<HTMLElement> elements) {
                        return new ArrayList<>(elements);
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Error");
                    }

                    @Override
                    public void onNext(ArrayList<HTMLElement> elements) {
                        if (elements.size() > 0) {
                            view.setNarrator(elements);
                        }
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get photo analysis page.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        view.showError(R.string.an_error_occurred);
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }
}
