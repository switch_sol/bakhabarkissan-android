package com.switchsolutions.agricultureapplication.screens.video;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnErrorListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.listener.VideoControlsButtonListener;
import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.devbrackets.android.exomedia.ui.widget.VideoControls;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.Video;

public class VideoFragment extends Fragment implements VideoContract.View {

    private static final String KEY_VIDEO = "video";

    private VideoContract.Presenter presenter;

    private EMVideoView viewVideo;
    private TextView viewBottomControls;

    public static VideoFragment newInstance(Video video) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_VIDEO, video);
        VideoFragment fragment = new VideoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);

        viewVideo = (EMVideoView) view.findViewById(R.id.video_view);
        viewVideo.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared() {
                presenter.onVideoPrepared();
            }
        });
        viewVideo.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                presenter.onVideoCompleted();
            }
        });
        viewVideo.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError() {
                return presenter.onVideoError();
            }
        });

        viewBottomControls = (TextView) view.findViewById(R.id.ad_controls);
        viewBottomControls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onBottomControlClicked();
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Video video = getArguments().getParcelable(KEY_VIDEO);
        presenter = new VideoPresenter(this, video, BaseApplication.getComponent(getContext()));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void setControlsEnabled(final boolean enabled) {
        viewVideo.setFocusable(enabled);

        VideoControls videoControls = viewVideo.getVideoControls();
        if (videoControls != null) {
            videoControls.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);

            videoControls.setFastForwardButtonEnabled(false);
            videoControls.setFastForwardButtonRemoved(true);
            videoControls.setNextButtonEnabled(false);
            videoControls.setNextButtonRemoved(true);
            videoControls.setPreviousButtonEnabled(false);
            videoControls.setPreviousButtonRemoved(true);
            videoControls.setRewindButtonEnabled(false);
            videoControls.setRewindButtonRemoved(true);

            videoControls.setButtonListener(new VideoControlsButtonListener() {
                @Override
                public boolean onPlayPauseClicked() {
                    return !enabled;
                }

                @Override
                public boolean onPreviousClicked() {
                    return !enabled;
                }

                @Override
                public boolean onNextClicked() {
                    return !enabled;
                }

                @Override
                public boolean onRewindClicked() {
                    return !enabled;
                }

                @Override
                public boolean onFastForwardClicked() {
                    return !enabled;
                }
            });
        }
    }

    @Override
    public void setVideoImage(String thumbPath, String imagePath) {
        Glide.with(this)
                .load(imagePath)
                .thumbnail(Glide.with(this)
                        .load(thumbPath)
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .fitCenter())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .fitCenter()
                .into(viewVideo.getPreviewImageView());
    }

    @Override
    public void setVideo(String videoPath) {
        viewVideo.setVideoURI(Uri.parse(videoPath));
    }

    @Override
    public void startVideo() {
        viewVideo.start();
    }

    @Override
    public void stopVideo() {
        viewVideo.stopPlayback();
    }

    @Override
    public void seekVideo(int milliseconds) {
        viewVideo.seekTo(milliseconds);
    }

    @Override
    public void setBottomControlsVisible(boolean visible) {
        viewBottomControls.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setBottomControlsText(int stringRes) {
        viewBottomControls.setText(stringRes);
    }

    @Override
    public void setBottomControlsEnabled(boolean enabled) {
        viewBottomControls.setEnabled(enabled);
    }
}
