package com.switchsolutions.agricultureapplication.screens.video_categories_parent_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;

public interface VideoCategoriesParentListContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showChildCategoriesListScreen(VideoCategoryParent parentCategory);

        void showBanner();

        void setAdapter();

        void notifyDataSetChanged();

        void notifyItemRangeInserted(int positionStart, int additionCount);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringResId);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder holder, int position);

        void onListItemClicked(int position);
    }
}
