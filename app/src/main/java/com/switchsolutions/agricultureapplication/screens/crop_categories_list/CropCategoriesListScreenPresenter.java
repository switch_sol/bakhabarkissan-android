package com.switchsolutions.agricultureapplication.screens.crop_categories_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.crops.CropsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.CropCategory;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class CropCategoriesListScreenPresenter implements CropCategoriesListScreenContract.Presenter {

    private static final String SCREEN_NAME = "Crop Categories List";

    private final CropCategoriesListScreenContract.View view;
    private final CropsClient cropsClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<CropCategory> cropCategories;
    private Subscription subscription;

    CropCategoriesListScreenPresenter(CropCategoriesListScreenContract.View view, ApplicationComponent component) {
        this.view = view;
        this.cropsClient = component.cropsClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setProgressBarEnabled(true);
        view.setAdapter();

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return cropCategories != null ? cropCategories.size() : 0;
    }

    @Override
    public void onListItemClicked(int position) {
        CropCategory cropCategory = cropCategories.get(position);
        analyticsClient.sendCropCategorySelectedEvent(cropCategory.getId());
        view.showCropsListScreen(cropCategory);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        CropCategory cropCategory = cropCategories.get(position);

        viewHolder.setImage(cropCategory.getImagePath());
        viewHolder.setTitle(cropCategory.getName());
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        subscription = cropsClient
                .getCategories()
                .map(new Func1<List<CropCategory>, ArrayList<CropCategory>>() {
                    @Override
                    public ArrayList<CropCategory> call(List<CropCategory> response) {
                        return new ArrayList<>(response);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<CropCategory>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<CropCategory> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get news articles.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleResponse(ArrayList<CropCategory> response) {
        cropCategories = response;

        view.notifyDataSetChanged();
        view.setProgressBarEnabled(false);
    }
}
