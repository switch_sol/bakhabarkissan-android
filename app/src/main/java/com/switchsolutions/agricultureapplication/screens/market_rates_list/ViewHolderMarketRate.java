package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

final class ViewHolderMarketRate extends RecyclerView.ViewHolder {

    private final TextView viewName;
    private final TextView viewRate;

    ViewHolderMarketRate(View itemView) {
        super(itemView);

        viewName = (TextView) itemView.findViewById(R.id.item_name);
        viewRate = (TextView) itemView.findViewById(R.id.item_rate);
    }

    void setName(String name) {
        viewName.setText(name);
    }

    void setRate(String rate) {
        viewRate.setText(rate);
    }
}
