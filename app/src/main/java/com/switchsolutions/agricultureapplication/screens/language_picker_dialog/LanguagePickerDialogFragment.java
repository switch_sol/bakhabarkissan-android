package com.switchsolutions.agricultureapplication.screens.language_picker_dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.widgets.DialogFragment;

// TODO: Convert to MVP
public class LanguagePickerDialogFragment extends DialogFragment {

    private static final String KEY_REQUEST_CODE = "request_code";

    public static final String KEY_LANGUAGE = "language";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] keys = getResources().getStringArray(R.array.supported_languages);
        final String[] vals = getResources().getStringArray(R.array.supported_languages_values);

        return new AlertDialog.Builder(getContext())
                .setTitle(R.string.choose_language)
                .setItems(keys, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Bundle bundle = new Bundle();
                        bundle.putString(KEY_LANGUAGE, vals[which]);
                        setResult(getArguments().getInt(KEY_REQUEST_CODE, -1), bundle);
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .create();
    }

    public static LanguagePickerDialogFragment newInstance(int requestCode) {
        Bundle args = new Bundle();
        args.putInt(KEY_REQUEST_CODE, requestCode);
        LanguagePickerDialogFragment fragment = new LanguagePickerDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
