package com.switchsolutions.agricultureapplication.screens.photo_analyses_list;

import android.Manifest;
import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.photo_analysis.PhotoAnalysisClient;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.util.Date;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class PhotoAnalysesListPresenter implements PhotoAnalysesListContract.Presenter {

    private static final String KEY_IS_FIRST_RUN = "is_first_run";

    private final PhotoAnalysesListContract.View view;
    private final PhotoAnalysisClient photoAnalysisClient;
    private final Storage storage;

    private List<PhotoAnalysis> items;
    private Subscription subscription;
    private boolean isFirstRun = true;

    PhotoAnalysesListPresenter(PhotoAnalysesListContract.View view, ApplicationComponent component) {
        this.view = view;

        this.photoAnalysisClient = component.photoAnalysisClient();
        this.storage = component.storage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            isFirstRun = savedInstanceState.getBoolean(KEY_IS_FIRST_RUN, true);
        }

        if (!view.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            view.requestStoragePermission();
        } else {
            requestContent();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onFABClicked() {
        view.showCreatePhotoAnalysisScreen();
    }

    @Override
    public void onStoragePermissionResult(boolean granted) {
        if (granted) {
            requestContent();
        } else {
            view.showError(R.string.storage_permission_required);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PhotoAnalysis pa = items.get(position);

        long dt = (long) pa.getDatetime() * 1000;
        holder.setDate(new Date(dt));

        holder.setImage1(null);
        holder.setImage2(null);
        holder.setImage3(null);
        if (pa.getPhotos() != null) {
            int n = pa.getPhotos().length;
            if (n > 0) {
                holder.setImage1(storage.getImageFile(pa.getPhotos()[0]));
            }
            if (n > 1) {
                holder.setImage2(storage.getImageFile(pa.getPhotos()[1]));
            }
            if (n > 2) {
                holder.setImage3(storage.getImageFile(pa.getPhotos()[2]));
            }
        }

        if (pa.getStatus() == 0) {
            holder.setStatusInProgress();
        } else if (pa.getStatus() < 0) {
            holder.setStatusCancelled();
        } else if (pa.getStatus() > 0) {
            holder.setStatusCompleted();
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public void onListItemClicked(int position) {
        view.showViewPhotoAnalysisScreen(items.get(position));
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        subscription = photoAnalysisClient
                .getLocalList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<PhotoAnalysis>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Unable to get photo analyses from list.");

                        view.showError(R.string.an_error_occurred);
                    }

                    @Override
                    public void onNext(List<PhotoAnalysis> response) {
                        items = response;

                        view.setAdapter();

                        if (response.size() == 0 && isFirstRun) {
                            isFirstRun = false;
                            view.showCreatePhotoAnalysisScreen();
                        }
                    }
                });
    }

}
