package com.switchsolutions.agricultureapplication.screens.account_login;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.processphoenix.ProcessPhoenix;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.screens.language_picker_dialog.LanguagePickerDialogFragment;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;
import com.switchsolutions.agricultureapplication.widgets.DialogFragment;

public class AccountLoginActivity extends BaseActivity implements AccountLoginContract.View,
        ActivityCompat.OnRequestPermissionsResultCallback, DialogFragment.OnDialogResultListener {

    public static final int RC_LANGUAGE_PICKED = 34;
    public static final int RC_PERMISSION_SMS = 37;

    private AccountLoginContract.Presenter presenter;

    private TextInputLayout viewNameLayout;
    private EditText viewName;
    private TextInputLayout viewNumberLayout;
    private EditText viewNumber;
    private FloatingActionButton viewFAB;
    private ProgressBar viewProgressBar;

    public static Intent createIntent(Context context) {
        return new Intent(context, AccountLoginActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_login);

        viewNameLayout = (TextInputLayout) findViewById(R.id.account_name_layout);
        viewName = (EditText) findViewById(R.id.account_name);

        viewNumberLayout = (TextInputLayout) findViewById(R.id.account_number_layout);
        viewNumber = (EditText) findViewById(R.id.account_number);
        viewNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    presenter.onNavigateNextButtonClicked();
                    return true;
                }
                return false;
            }
        });

        viewFAB = (FloatingActionButton) findViewById(R.id.fab);
        viewFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNavigateNextButtonClicked();
            }
        });

        viewProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        presenter = new AccountLoginPresenter(this, BaseApplication.getComponent(this));
       // showMainScreen();
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSION_SMS:
                presenter.onSMSPermissionResult(grantResults[0] == PackageManager.PERMISSION_GRANTED);
                break;
        }
    }

    @Override
    public void onDialogResult(int requestCode, Bundle bundle) {
        switch (requestCode) {
            case RC_LANGUAGE_PICKED:
                presenter.onLanguagePickerResult(bundle.getString(LanguagePickerDialogFragment.KEY_LANGUAGE));
                break;
        }
    }

    @Override
    public void showMainScreen() {
        Intent intent = MainScreenActivity.createIntent(this, true);
        startActivity(intent);
    }

//    @Override
//    public void showVerificationScreen(String number) {
//        Intent intent = AccountVerificationActivity.createIntent(this, number);
//        startActivity(intent);
//    }


    @Override
    public void showLanguagePicker() {
        DialogFragment df = LanguagePickerDialogFragment.newInstance(RC_LANGUAGE_PICKED);
        df.show(getSupportFragmentManager(), null);
    }

    @Override
    public String getName() {
        return viewName.getText().toString();
    }

    @Override
    public String getNumber() {
        return viewNumber.getText().toString();
    }

    @Override
    public void restartApplication() {
        Intent intent = createIntent(this);
        ProcessPhoenix.triggerRebirth(this, intent);
    }

    @Override
    public void setInputEnabled(boolean enabled) {
        viewName.setEnabled(enabled);
        viewNumber.setEnabled(enabled);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewProgressBar.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showNameError(@StringRes int stringRes) {
        viewNameLayout.setError(getString(stringRes));
    }

    @Override
    public void showNumberError(@StringRes int stringRes) {
        viewNumberLayout.setError(getString(stringRes));
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewFAB, stringRes, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestSMSPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, RC_PERMISSION_SMS);
    }
}
