package com.switchsolutions.agricultureapplication.screens.crop_page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLRecyclerView;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.Crop;
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorFragment;
import com.switchsolutions.agricultureapplication.screens.document_outline.DocumentOutlineFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class CropPageScreenActivity extends BaseActivity implements CropPageScreenContract.View {

    private static final String KEY_CROP = "crop";

    private CropPageScreenContract.Presenter presenter;

    private DrawerLayout viewDrawer;
    private Toolbar viewToolbar;
    private SwipeRefreshLayout viewSwipeRefreshLayout;
    private HTMLRecyclerView viewContent;

    public static Intent createIntent(Context context, Crop crop) {
        Intent intent = new Intent(context, CropPageScreenActivity.class);
        intent.putExtra(KEY_CROP, crop);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_page);

        viewDrawer = (DrawerLayout) findViewById(R.id.container_drawer);
        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.inflateMenu(R.menu.menu_crop_page);
        viewToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_outline:
                        return presenter.onOutlineButtonClicked();
                    default:
                        return false;
                }
            }
        });
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        viewContent = (HTMLRecyclerView) findViewById(R.id.content_view);
        viewContent.setOnElementClickListener(new HTMLRecyclerView.OnElementClickListener() {
            @Override
            public void onElementClicked(HTMLHeader header) {
                presenter.onHTMLElementClicked(header);
            }

            @Override
            public void onElementClicked(HTMLParagraph paragraph) {
                presenter.onHTMLElementClicked(paragraph);
            }

            @Override
            public void onElementClicked(HTMLImage image) {
                presenter.onHTMLElementClicked(image);
            }

            @Override
            public void onElementClicked(HTMLTable table) {
                presenter.onHTMLElementClicked(table);
            }

            @Override
            public void onElementClicked(HTMLUnorderedList unorderedList) {
                presenter.onHTMLElementClicked(unorderedList);
            }

            @Override
            public void onElementClicked(HTMLOrderedList orderedList) {
                presenter.onHTMLElementClicked(orderedList);
            }
        });

        Crop crop = getIntent().getParcelableExtra(KEY_CROP);
        presenter = new CropPageScreenPresenter(this, crop, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void setToolbarTitle(String title) {
        viewToolbar.setTitle(title);
    }

    @Override
    public void setContent(List<HTMLElement> elements) {
        viewContent.setContent(elements);
    }

    @Override
    public void setContentItemInFocus(int position) {
        viewContent.setFocusedPosition(position);
    }

    @Override
    public void clearContentItemInFocus() {
        viewContent.clearFocusedPosition();
    }

    @Override
    public void setOutline(ArrayList<HTMLHeader> headers) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_outline, DocumentOutlineFragment.newInstance(headers))
                .commit();
    }

    @Override
    public void showOutline() {
        viewDrawer.openDrawer(GravityCompat.END);
    }

    @Override
    public void hideOutline() {
        viewDrawer.closeDrawer(GravityCompat.END);
    }

    @Override
    public void setNarrator(ArrayList<HTMLElement> elements) {
        CoordinatorLayout.LayoutParams oldLayoutParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        oldLayoutParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.narrator_container_height);
        viewSwipeRefreshLayout.setLayoutParams(oldLayoutParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_narration, DocumentNarratorFragment.newInstance(elements))
                .commit();
    }

    @Override
    public void scrollToContentPosition(int position) {
        viewContent.smoothScrollToPosition(position);
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewContent, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }
}
