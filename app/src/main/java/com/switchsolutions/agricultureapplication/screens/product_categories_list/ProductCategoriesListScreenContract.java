package com.switchsolutions.agricultureapplication.screens.product_categories_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.ProductCategory;

public interface ProductCategoriesListScreenContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showProductsListScreen(ProductCategory productCategory);

        void showBanner();

        void setAdapter();

        void notifyDataSetChanged();

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringResId);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder holder, int position);

        void onListItemClicked(int position);
    }

}
