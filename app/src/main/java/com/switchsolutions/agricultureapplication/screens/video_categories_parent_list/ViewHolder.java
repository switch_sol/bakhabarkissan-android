package com.switchsolutions.agricultureapplication.screens.video_categories_parent_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewTitle;
    private final TextView viewCount;
    private final TextView viewUpdatedOn;

    ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.category_image);
        viewTitle = (TextView) itemView.findViewById(R.id.category_title);
        viewCount = (TextView) itemView.findViewById(R.id.category_count);
        viewUpdatedOn = (TextView) itemView.findViewById(R.id.category_updated_on);
    }

    void setImage(String imagePath) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .centerCrop())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage);
    }

    void setName(String title) {
        viewTitle.setText(title);
    }

    void setCount(int count) {
        viewCount.setText(itemView.getResources().getString(R.string.fmt_videos_count, count));
    }

    void setUpdatedOn(String dateString) {
        viewUpdatedOn.setText(itemView.getResources().getString(R.string.fmt_updated_on, dateString));
    }
}
