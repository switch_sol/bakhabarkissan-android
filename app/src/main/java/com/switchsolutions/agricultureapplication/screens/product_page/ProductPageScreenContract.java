package com.switchsolutions.agricultureapplication.screens.product_page;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.ProductCategory;

import java.util.ArrayList;
import java.util.List;

public interface ProductPageScreenContract {

    interface View {

        void navigateUp();

        void showProductDealersScreen(ProductCategory productCategory);

        void showCallScreen(String number);

        void setCompanyImage(String imagePath);

        void setProductImage(String imagePath);

        void setProductName(String productName);

        void setProductRate(String productRate);

        void setCompanyName(String companyName);

        void setContent(List<HTMLElement> elements);

        void setContentItemInFocus(int position);

        void clearContentItemInFocus();

        void setNarrator(ArrayList<HTMLElement> elements);

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        boolean onShowDealerLocationsButtonClicked();

        void onCallButtonClicked();

        void onHTMLElementClicked(HTMLHeader header);

        void onHTMLElementClicked(HTMLParagraph paragraph);

        void onHTMLElementClicked(HTMLImage image);

        void onHTMLElementClicked(HTMLTable table);

        void onHTMLElementClicked(HTMLOrderedList orderedList);

        void onHTMLElementClicked(HTMLUnorderedList unorderedList);
    }

}
