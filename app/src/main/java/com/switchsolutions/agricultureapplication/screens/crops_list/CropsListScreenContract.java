package com.switchsolutions.agricultureapplication.screens.crops_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.Crop;

public interface CropsListScreenContract {

    interface View {

        void navigateUp();

        void showLoginScreen();

        void showCropPageScreen(Crop crop);

        void showBanner();

        void setToolbarTitle(String title);

        void setAdapter();

        void notifyDataSetChanged();

        void setProgressBarEnabled(boolean enabled);

        void showError(@StringRes int stringResId);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        int getItemCount();

        void onBindViewHolder(ViewHolder viewHolder, int position);

        void onListItemClicked(int position);
    }
}
