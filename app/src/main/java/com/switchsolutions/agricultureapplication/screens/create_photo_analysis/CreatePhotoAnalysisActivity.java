package com.switchsolutions.agricultureapplication.screens.create_photo_analysis;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.services.UploadPhotoAnalysisService;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class CreatePhotoAnalysisActivity extends BaseActivity implements CreatePhotoAnalysisContract.View, ActivityCompat.OnRequestPermissionsResultCallback {

    private static final int RC_IMAGE_CAPTURE = 10;
    private static final int RC_PERMISSION_RECORD_AUDIO = 11;

    private CreatePhotoAnalysisContract.Presenter presenter;

    private Adapter adapter;

    private Toolbar viewToolbar;
    private TextView viewTextCounter;
    private EditText viewText;
    private FloatingActionButton viewAudioFAB;
    private ProgressBar viewAudioProgressBar;
    private TextView viewAudioLength;
    private ImageView viewAudioDelete;
    private TextView viewImagesCounter;
    private RecyclerView viewImagesList;
    private Button viewSubmit;

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            presenter.onIssueTextChanged(s.length());
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_photo_analysis);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewTextCounter = (TextView) findViewById(R.id.issue_length);

        viewText = (EditText) findViewById(R.id.issue_text);
        viewText.addTextChangedListener(textWatcher);

        viewAudioFAB = (FloatingActionButton) findViewById(R.id.audio_fab);
        viewAudioFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAudioFABClicked();
            }
        });

        viewAudioProgressBar = (ProgressBar) findViewById(R.id.audio_progress_bar);

        viewAudioLength = (TextView) findViewById(R.id.audio_length);

        viewAudioDelete = (ImageView) findViewById(R.id.audio_delete);
        viewAudioDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAudioDeleteClicked();
            }
        });

        viewImagesCounter = (TextView) findViewById(R.id.images_counter);

        viewImagesList = (RecyclerView) findViewById(R.id.images_list);
        viewImagesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        viewImagesList.setHasFixedSize(true);

        viewSubmit = (Button) findViewById(R.id.submit_button);
        viewSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSubmitButtonClicked();
            }
        });

        presenter = new CreatePhotoAnalysisPresenter(this, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        presenter.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case RC_IMAGE_CAPTURE:
                presenter.onImageCaptureResult(resultCode);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case RC_PERMISSION_RECORD_AUDIO:
                presenter.onRecorderPermissionResult(grantResults[0] == PackageManager.PERMISSION_GRANTED);
                break;
        }
    }

    @Override
    public void showImageCaptureScreen(File file) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            Uri uri = Uri.fromFile(file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, RC_IMAGE_CAPTURE);
        }
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void setAudioDeleteButtonEnabled(boolean enabled) {
        viewAudioDelete.setEnabled(enabled);
        viewAudioDelete.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setAudioFABIcon(@DrawableRes int drawableRes) {
        viewAudioFAB.setImageResource(drawableRes);
    }

    @Override
    public void setAudioFABEnabled(boolean enabled) {
        viewAudioFAB.setEnabled(enabled);
    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewImagesList, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public void setAdapter() {
        adapter = new Adapter(presenter);
        viewImagesList.setAdapter(adapter);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean hasPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void setIssueTextCount(int i, int n) {
        viewTextCounter.setText(String.format(Locale.US, "%d / %d", i, n));
    }

    @Override
    public void setAudioLength(int i, int n) {
        int im = i / 60;
        int is = i % 60;
        int nm = n / 60;
        int ns = n % 60;
        viewAudioLength.setText(String.format(Locale.US, "%02d:%02d / %02d:%02d", im, is, nm, ns));
    }

    @Override
    public void setImagesCount(int i, int n) {
        viewImagesCounter.setText(String.format(Locale.US, "%02d / %02d", i, n));
    }

    @Override
    public void setIssueTextLimit(int n) {
        viewText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(n)});
    }

    @Override
    public String getIssueText() {
        return viewText.getText().toString();
    }

    @Override
    public void setIssueText(String text) {
        viewText.removeTextChangedListener(textWatcher);
        viewText.setText(text);
        viewText.addTextChangedListener(textWatcher);
    }

    @Override
    public void setAudioProgressBarLimit(int n) {
        viewAudioProgressBar.setMax(n);
    }

    @Override
    public void setAudioProgressBar(int i) {
        viewAudioProgressBar.setProgress(i);
    }

    @Override
    public void requestRecorderPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, RC_PERMISSION_RECORD_AUDIO);
    }

    @Override
    public void startUploadIntent(String text, String audio, ArrayList<String> images) {
        Intent intent = UploadPhotoAnalysisService.createIntent(this, text, audio, images);
        startService(intent);
    }

    public static Intent createIntent(Context context) {
        return new Intent(context, CreatePhotoAnalysisActivity.class);
    }

}
