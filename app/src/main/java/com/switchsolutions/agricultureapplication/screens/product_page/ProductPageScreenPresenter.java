package com.switchsolutions.agricultureapplication.screens.product_page;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.products.ProductsClient;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ProductPageScreenPresenter implements ProductPageScreenContract.Presenter {

    private static final String SCREEN_NAME = "Product Page";

    private final ProductPageScreenContract.View view;
    private final ProductCategory category;
    private final Product product;
    private final ProductsClient productsClient;
    private final AnalyticsClient analyticsClient;

    private Subscription contentSubscription;
    private Subscription narratorSubscription;

    ProductPageScreenPresenter(ProductPageScreenContract.View view, ProductCategory category, Product product, ApplicationComponent component) {
        this.view = view;
        this.category = category;
        this.product = product;
        this.productsClient = component.productsClient();
        this.analyticsClient = component.analyticsClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setProductImage(product.getImagePath());
        view.setCompanyImage(product.getCompany().getImagePath());
        view.setProductName(product.getName());
        view.setProductRate(product.getRate());
        view.setCompanyName(product.getCompany().getName());

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            contentSubscription.unsubscribe();
        }

        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            narratorSubscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public boolean onShowDealerLocationsButtonClicked() {
        analyticsClient.sendProductDealersCategorySelectedEvent(category.getId());
        view.showProductDealersScreen(category);
        return true;
    }

    @Override
    public void onCallButtonClicked() {
        List<String> numbers = product.getCompany().getContactPhones();
        if (numbers != null && numbers.size() > 0) {
            String number = numbers.get(0);
            analyticsClient.sendCallProductCompanyEvent(number);
            view.showCallScreen(number);
        }
    }

    @Override
    public void onHTMLElementClicked(HTMLHeader header) {

    }

    @Override
    public void onHTMLElementClicked(HTMLParagraph paragraph) {

    }

    @Override
    public void onHTMLElementClicked(HTMLImage image) {

    }

    @Override
    public void onHTMLElementClicked(HTMLTable table) {

    }

    @Override
    public void onHTMLElementClicked(HTMLOrderedList orderedList) {

    }

    @Override
    public void onHTMLElementClicked(HTMLUnorderedList unorderedList) {

    }

    void requestContent() {
        if (contentSubscription != null && !contentSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        contentSubscription = productsClient
                .getProductPage(product.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<HTMLElement> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get crops.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleResponse(ArrayList<HTMLElement> elements) {
        view.setContent(elements);

        view.setProgressBarEnabled(false);
    }

    void prepareNarrator(final List<HTMLElement> elements) {
        if (narratorSubscription != null && !narratorSubscription.isUnsubscribed()) {
            narratorSubscription.unsubscribe();
        }

        narratorSubscription = Observable
                .fromCallable(new Callable<ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call() throws Exception {
                        ArrayList<HTMLElement> audios = new ArrayList<>();
                        for (HTMLElement element : elements) {
                            if (element.getAudioPath() != null) {
                                audios.add(element);
                            }
                        }
                        return audios;
                    }
                })
                .filter(new Func1<ArrayList<HTMLElement>, Boolean>() {
                    @Override
                    public Boolean call(ArrayList<HTMLElement> elements) {
                        return elements.size() > 0;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<HTMLElement>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ArrayList<HTMLElement> response) {
                        view.setNarrator(response);
                    }
                });
    }
}
