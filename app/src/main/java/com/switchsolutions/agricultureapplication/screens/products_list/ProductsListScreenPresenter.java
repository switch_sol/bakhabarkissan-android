package com.switchsolutions.agricultureapplication.screens.products_list;

import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.products.ProductsClient;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class ProductsListScreenPresenter implements ProductsListScreenContract.Presenter {

    private static final String SCREEN_NAME = "Products List";

    private static final int PAGINATION_LIMIT = 10;

    private final ProductsListScreenContract.View view;
    private final ProductCategory productCategory;
    private final ProductsClient productsClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<Product> products;
    private Subscription subscription;

    ProductsListScreenPresenter(ProductsListScreenContract.View view, ProductCategory productCategory, ApplicationComponent component) {
        this.view = view;
        this.productCategory = productCategory;
        this.productsClient = component.productsClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setAdapter();
        view.setToolbarTitle(productCategory.getName());

        requestContent();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemCount() {
        return products != null ? products.size() : 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product product = products.get(position);

        holder.setPromoted(product.isPromoted());
        holder.setImage(product.getImagePath());
        holder.setCompanyName(product.getCompany().getName());
        holder.setProductName(product.getName());
        holder.setProductRate(product.getRate());
    }

    @Override
    public void onListItemClicked(int position) {
        Product product = products.get(position);
        analyticsClient.sendProductSelectedEvent(product.getId());
        view.showProductScreen(productCategory, product);
    }

    void requestContent() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        subscription = productsClient
                .getProducts(productCategory.getId(), PAGINATION_LIMIT, products != null ? products.size() : 0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Product>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Product> response) {
                        handleResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof HttpException) {
            handleError((HttpException) e);
        } else if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else {
            Timber.e(e, "Unable to get crops.");

            view.showError(R.string.an_error_occurred);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(HttpException e) {
        switch (e.code()) {
            case 403:
                view.showLoginScreen();
                break;
            default:
                view.showError(R.string.an_error_occurred);
                break;
        }
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.please_check_your_internet_connection);
    }

    void handleResponse(ArrayList<Product> response) {
        if (products == null) {
            products = response;
            view.notifyDataSetChanged();
        } else {
            products.addAll(response);
            view.notifyItemRangeInserted(products.size() - response.size(), response.size());
        }
        view.setProgressBarEnabled(false);
    }
}
