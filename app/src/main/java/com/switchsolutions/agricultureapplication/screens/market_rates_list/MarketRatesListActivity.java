package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.screens.account_login.AccountLoginActivity;
import com.switchsolutions.agricultureapplication.screens.banner.BannerFragment;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

public class MarketRatesListActivity extends BaseActivity implements MarketRatesListContract.View {

    private static final int RC_LOCATION_SETTINGS = 13;

    private MarketRatesListContract.Presenter presenter;

    private AdapterMarkets adapterMarkets;
    private AdapterMarketRates adapterMarketRates;
    private BottomSheetBehavior bottomSheetBehavior;

    private Toolbar viewToolbar;
    private RecyclerView viewMarketsList;
    private SwipeRefreshLayout viewSwipeRefreshLayout;
    private View viewBottomSheetContainer;
    private RecyclerView viewMarketRatesList;
    private ProgressBar viewMarketRatesListProgressBar;
    private Snackbar viewSnackbar;

    public static Intent createIntent(Context context) {
        return new Intent(context, MarketRatesListActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_market_rates_list);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });

        viewMarketsList = (RecyclerView) findViewById(R.id.markets_list);
        viewMarketsList.setLayoutManager(new LinearLayoutManager(this));
        viewMarketsList.setHasFixedSize(true);

        viewSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        viewBottomSheetContainer = findViewById(R.id.container_bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(viewBottomSheetContainer);
        bottomSheetBehavior.setPeekHeight(BottomSheetBehavior.PEEK_HEIGHT_AUTO);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (presenter != null) {
                    presenter.onBottomSheetStateChanged(newState);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        viewMarketRatesList = (RecyclerView) findViewById(R.id.market_rates_list);
        viewMarketRatesList.setLayoutManager(new LinearLayoutManager(this));
        viewMarketRatesList.setHasFixedSize(true);

        viewMarketRatesListProgressBar = (ProgressBar) findViewById(R.id.market_rates_list_progress_bar);

        viewSnackbar = Snackbar.make(viewMarketsList, "", Snackbar.LENGTH_INDEFINITE);

        presenter = new MarketRatesListPresenter(this, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_LOCATION_SETTINGS) {
            presenter.onLocationSettingsScreenResult();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        presenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        if (!presenter.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showLocationSettingsScreen() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, RC_LOCATION_SETTINGS);
    }

    @Override
    public void showLoginScreen() {
        Intent intent = AccountLoginActivity.createIntent(this);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void showBanner() {
        CoordinatorLayout.LayoutParams swipeParams = (CoordinatorLayout.LayoutParams) viewSwipeRefreshLayout.getLayoutParams();
        swipeParams.bottomMargin = getResources().getDimensionPixelSize(R.dimen.bottom_banner_container_height);
        viewSwipeRefreshLayout.setLayoutParams(swipeParams);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_banner, BannerFragment.newInstance())
                .commit();
    }

    @Override
    public void setBottomSheetState(int state) {
        bottomSheetBehavior.setState(state);
    }

    @Override
    public void setMarketsAdapter() {
        adapterMarkets = new AdapterMarkets(presenter);
        viewMarketsList.setAdapter(adapterMarkets);
    }

    @Override
    public void notifyMarketsDataSetChanged() {
        adapterMarkets.notifyDataSetChanged();
    }

    @Override
    public void notifyMarketsItemRangeInserted(int positionStart, int additionCount) {
        adapterMarkets.notifyItemRangeInserted(positionStart, additionCount);
    }

    @Override
    public void setMarketRatesAdapter() {
        adapterMarketRates = new AdapterMarketRates(presenter);
        viewMarketRatesList.setAdapter(adapterMarketRates);
    }

    @Override
    public void notifyMarketRatesDataSetChanged() {
        adapterMarketRates.notifyDataSetChanged();
    }

    @Override
    public void setMarketsProgressBarEnabled(boolean enabled) {
        viewSwipeRefreshLayout.setEnabled(enabled);
        viewSwipeRefreshLayout.setRefreshing(enabled);
    }

    @Override
    public void setMarketRatesProgressBarEnabled(boolean enabled) {
        viewMarketRatesListProgressBar.setVisibility(enabled ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void showError(@StringRes int stringRes, boolean actionable) {
        showError(actionable ? R.string.retry : -1, stringRes, actionable);
    }

    @Override
    public void showError(@StringRes int actionStringRes, @StringRes int errorStringRes, final boolean actionable) {
        String actionString;
        try {
            actionString = getString(actionStringRes);
        } catch (Resources.NotFoundException e) {
            actionString = "";
        }

        viewSnackbar
                .setText(errorStringRes)
                .setAction(actionString, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (actionable) {
                            presenter.onActionButtonClicked();
                        }
                    }
                })
                .show();
    }

    @Override
    public void hideError() {
        viewSnackbar.dismiss();
    }
}
