package com.switchsolutions.agricultureapplication.screens.image_sources_list;

import android.os.Bundle;

public class ImageSourcesListPresenter implements ImageSourcesListContract.Presenter {

    private static final Attribution[] ATTRIBUTIONS = {
            Attribution.IMAGE_CONTACT_US,
            Attribution.IMAGE_CROPS,
            Attribution.IMAGE_MARKET_RATES,
            Attribution.IMAGE_NEWS,
            Attribution.IMAGE_PRODUCTS,
            Attribution.IMAGE_VIDEOS,
            Attribution.IMAGE_WEATHER,
            Attribution.IMAGE_LIVESTOCK,
            Attribution.IMAGE_PHOTO_ANALYSIS,
            Attribution.ICON_TREE,
            Attribution.IMAGE_MODERN_FARMING_TECHNIQUES
    };

    private final ImageSourcesListContract.View view;

    ImageSourcesListPresenter(ImageSourcesListContract.View view) {
        this.view = view;
    }

    @Override
    public void onCreate(Bundle onSavedInstanceState) {
        view.setAdapter();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public void onItemClicked(int position) {
        view.showWebPageScreen(ATTRIBUTIONS[position].urlRes);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        viewHolder.setTitle(ATTRIBUTIONS[position].titleRes);
        viewHolder.setImage(ATTRIBUTIONS[position].drawableRes);
    }

    @Override
    public int getItemCount() {
        return ATTRIBUTIONS.length;
    }
}
