package com.switchsolutions.agricultureapplication.screens.document_outline;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.html.HTMLHeader;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView viewText;

    ViewHolder(View itemView) {
        super(itemView);

        this.viewText = (TextView) itemView;
    }

    void setContent(HTMLHeader header) {
        viewText.setText(Html.fromHtml(header.getText()));
    }
}
