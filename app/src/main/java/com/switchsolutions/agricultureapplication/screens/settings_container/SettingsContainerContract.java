package com.switchsolutions.agricultureapplication.screens.settings_container;

import android.os.Bundle;

public interface SettingsContainerContract {

    interface View {

        void navigateUp();

        void showSettingsScreen();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();
    }
}
