package com.switchsolutions.agricultureapplication.screens.create_photo_analysis;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

import java.io.File;
import java.util.ArrayList;

public interface CreatePhotoAnalysisContract {

    interface View {

        void startUploadIntent(String text, String audio, ArrayList<String> images);

        void showImageCaptureScreen(File file);

        void navigateUp();

        void setAudioDeleteButtonEnabled(boolean enabled);

        void setAudioFABIcon(@DrawableRes int drawableRes);

        void setAudioFABEnabled(boolean enabled);

        void showError(@StringRes int stringRes);

        String getIssueText();

        void setIssueText(String text);

        void setIssueTextCount(int i, int n);

        void setIssueTextLimit(int n);

        void setAudioLength(int i, int n);

        void setImagesCount(int i, int n);

        void setAudioProgressBarLimit(int n);

        void setAudioProgressBar(int i);

        void setAdapter();

        void notifyDataSetChanged();

        boolean hasPermission(String permission);

        void requestRecorderPermission();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onResume();

        void onPause();

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onAudioFABClicked();

        void onAudioDeleteClicked();

        void onBindViewHolder(ViewHolder holder, int position);

        int getItemCount();

        void onListItemClicked(int position);

        void onImageCaptureResult(int resultCode);

        void onIssueTextChanged(int n);

        void onRecorderPermissionResult(boolean granted);

        void onSubmitButtonClicked();
    }

}
