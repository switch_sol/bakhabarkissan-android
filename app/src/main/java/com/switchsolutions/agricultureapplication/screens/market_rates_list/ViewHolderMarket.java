package com.switchsolutions.agricultureapplication.screens.market_rates_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

final class ViewHolderMarket extends RecyclerView.ViewHolder {

    private final TextView viewName;

    ViewHolderMarket(View itemView) {
        super(itemView);

        viewName = (TextView) itemView.findViewById(R.id.market_name);
    }

    void setName(String name) {
        viewName.setText(name);
    }
}
