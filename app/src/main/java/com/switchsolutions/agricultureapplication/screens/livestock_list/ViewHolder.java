package com.switchsolutions.agricultureapplication.screens.livestock_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewTitle;

    ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.livestock_image);
        viewTitle = (TextView) itemView.findViewById(R.id.livestock_title);
    }

    void setImage(String imagePath) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .centerCrop())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage);
    }

    void setTitle(String title) {
        viewTitle.setText(title);
    }
}
