package com.switchsolutions.agricultureapplication.screens.photo_analyses_list;

import android.os.Bundle;
import android.support.annotation.StringRes;

import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;

public interface PhotoAnalysesListContract {

    interface View {

        void showViewPhotoAnalysisScreen(PhotoAnalysis photoAnalysis);

        void showCreatePhotoAnalysisScreen();

        boolean hasPermission(String permission);

        void requestStoragePermission();

        void setAdapter();

        void showError(@StringRes int stringRes);

        void navigateUp();
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onNavigationButtonClicked();

        void onFABClicked();

        void onStoragePermissionResult(boolean granted);

        void onBindViewHolder(ViewHolder holder, int position);

        int getItemCount();

        void onListItemClicked(int position);
    }

}
