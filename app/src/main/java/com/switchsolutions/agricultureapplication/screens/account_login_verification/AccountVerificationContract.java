package com.switchsolutions.agricultureapplication.screens.account_login_verification;

import android.os.Bundle;
import android.support.annotation.StringRes;

public interface AccountVerificationContract {

    interface View {

        void navigateUp();

        void showMainScreen();

        void setNumber(String number);

        void setInputEnabled(boolean enabled);

        void setProgressBarEnabled(boolean enabled);

        String getCode();

        void setCode(String code);

        void showCodeError(@StringRes int stringRes);

        void showError(@StringRes int stringRes);
    }

    interface Presenter {

        void onCreate(Bundle savedInstanceState);

        void onSaveInstanceState(Bundle outState);

        void onDestroy();

        void onFABClicked();
    }
}
