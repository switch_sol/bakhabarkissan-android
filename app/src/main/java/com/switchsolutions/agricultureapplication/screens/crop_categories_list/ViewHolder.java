package com.switchsolutions.agricultureapplication.screens.crop_categories_list;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.Constants;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final TextView viewTitle;
    private final ImageView viewImage;

    public ViewHolder(View itemView) {
        super(itemView);
        this.viewTitle = (TextView) itemView.findViewById(R.id.category_title);
        this.viewImage = (ImageView) itemView.findViewById(R.id.category_image);
    }

    void setTitle(String title) {
        viewTitle.setText(title);
    }

    void setImage(String imagePath) {
        Log.d(com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants.FARM_TO_HOME_LOGS, "img path:" + imagePath);
        Log.d(com.switchsolutions.agricultureapplication.farmtohome.farmconstants.Constants.FARM_TO_HOME_LOGS, "img path gen:" + RemoteFileUrlUtils.toImageUrl(imagePath));

        Glide.with(itemView.getContext())
                //.load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .load("http://115.186.147.181:8181/v1/images/" + imagePath)
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .centerCrop())

                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop().listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                Log.e("IMAGE_EXCEPTION", "Exception " + e.toString());
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        })
                .into(viewImage);
    }
}
