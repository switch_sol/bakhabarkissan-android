package com.switchsolutions.agricultureapplication.screens.product_page;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.html.HTMLHeader;
import com.switchsolutions.agricultureapplication.html.HTMLImage;
import com.switchsolutions.agricultureapplication.html.HTMLOrderedList;
import com.switchsolutions.agricultureapplication.html.HTMLParagraph;
import com.switchsolutions.agricultureapplication.html.HTMLRecyclerView;
import com.switchsolutions.agricultureapplication.html.HTMLTable;
import com.switchsolutions.agricultureapplication.html.HTMLUnorderedList;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.screens.document_narrator.DocumentNarratorFragment;
import com.switchsolutions.agricultureapplication.screens.product_dealers_list.ProductDealersListActivity;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;
import com.switchsolutions.agricultureapplication.widgets.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class ProductPageScreenActivity extends BaseActivity implements ProductPageScreenContract.View {

    private static final String KEY_PRODUCT_CATEGORY = "product_category";
    private static final String KEY_PRODUCT = "product";

    private ProductPageScreenContract.Presenter presenter;

    private Toolbar viewToolbar;
    private ImageView viewProductImage;
    private ImageView viewCompanyImage;
    private TextView viewProductName;
    private TextView viewProductRate;
    private Button viewCallCompanyButton;
    private HTMLRecyclerView viewContent;

    public static Intent createIntent(Context context, ProductCategory category, Product product) {
        Intent intent = new Intent(context, ProductPageScreenActivity.class);
        intent.putExtra(KEY_PRODUCT_CATEGORY, category);
        intent.putExtra(KEY_PRODUCT, product);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_page);

        viewToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onNavigationButtonClicked();
            }
        });
        viewToolbar.inflateMenu(R.menu.menu_product_page);
        viewToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_map:
                        return presenter.onShowDealerLocationsButtonClicked();
                    default:
                        return false;
                }
            }
        });

        viewProductImage = (ImageView) findViewById(R.id.product_image);

        viewCompanyImage = (ImageView) findViewById(R.id.company_image);

        viewProductName = (TextView) findViewById(R.id.product_name);

        viewProductRate = (TextView) findViewById(R.id.product_rate);

        viewCallCompanyButton = (Button) findViewById(R.id.button_call_company);
        viewCallCompanyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onCallButtonClicked();
            }
        });

        viewContent = (HTMLRecyclerView) findViewById(R.id.list_view);
        viewContent.setOnElementClickListener(new HTMLRecyclerView.OnElementClickListener() {
            @Override
            public void onElementClicked(HTMLHeader header) {
                presenter.onHTMLElementClicked(header);
            }

            @Override
            public void onElementClicked(HTMLParagraph paragraph) {
                presenter.onHTMLElementClicked(paragraph);
            }

            @Override
            public void onElementClicked(HTMLImage image) {
                presenter.onHTMLElementClicked(image);
            }

            @Override
            public void onElementClicked(HTMLTable table) {
                presenter.onHTMLElementClicked(table);
            }

            @Override
            public void onElementClicked(HTMLUnorderedList unorderedList) {
                presenter.onHTMLElementClicked(unorderedList);
            }

            @Override
            public void onElementClicked(HTMLOrderedList orderedList) {
                presenter.onHTMLElementClicked(orderedList);
            }
        });

        ProductCategory category = getIntent().getParcelableExtra(KEY_PRODUCT_CATEGORY);
        Product product = getIntent().getParcelableExtra(KEY_PRODUCT);
        presenter = new ProductPageScreenPresenter(this, category, product, BaseApplication.getComponent(this));
        presenter.onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        presenter.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        presenter.onDestroy();
        presenter = null;
    }

    @Override
    public void navigateUp() {
        finish();
    }

    @Override
    public void showProductDealersScreen(ProductCategory productCategory) {
        Intent intent = ProductDealersListActivity.createIntent(this, productCategory);
        startActivity(intent);
    }

    @Override
    public void showCallScreen(String number) {
        String uri = String.format("tel:%s", number);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    public void setCompanyImage(String imagePath) {
        Glide.with(this)
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(this)
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .fitCenter())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .fitCenter()
                .into(viewCompanyImage);
    }

    @Override
    public void setProductImage(String imagePath) {
        Glide.with(this)
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(this)
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .fitCenter())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .fitCenter()
                .into(viewProductImage);
    }

    @Override
    public void setProductName(String productName) {
        viewProductName.setText(productName);
    }

    @Override
    public void setProductRate(String productRate) {
        viewProductRate.setText(productRate);
    }

    @Override
    public void setCompanyName(String companyName) {
        viewCallCompanyButton.setText(getString(R.string.fmt_call_company, companyName));
    }

    @Override
    public void setContent(List<HTMLElement> elements) {
        viewContent.setContent(elements);
    }

    @Override
    public void setContentItemInFocus(int position) {
        viewContent.setFocusedPosition(position);
    }

    @Override
    public void clearContentItemInFocus() {
        viewContent.clearFocusedPosition();
    }

    @Override
    public void setNarrator(ArrayList<HTMLElement> elements) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_narration, DocumentNarratorFragment.newInstance(elements))
                .commit();
    }

    @Override
    public void setProgressBarEnabled(boolean enabled) {

    }

    @Override
    public void showError(@StringRes int stringRes) {
        Snackbar.make(viewContent, stringRes, Snackbar.LENGTH_LONG)
                .show();
    }
}
