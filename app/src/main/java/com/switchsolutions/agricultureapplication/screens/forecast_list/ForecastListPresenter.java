package com.switchsolutions.agricultureapplication.screens.forecast_list;

import android.content.pm.PackageManager;
import android.os.Bundle;

import com.switchsolutions.agricultureapplication.ApplicationComponent;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.forecast.ForecastClient;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionFailedException;
import com.switchsolutions.agricultureapplication.api.google.GoogleClientConnectionSuspendedException;
import com.switchsolutions.agricultureapplication.api.location.LocationClient;
import com.switchsolutions.agricultureapplication.api.location.LocationNotFoundException;
import com.switchsolutions.agricultureapplication.api.location.LocationProviderDisabledException;
import com.switchsolutions.agricultureapplication.api.remote_config.RemoteConfigClient;
import com.switchsolutions.agricultureapplication.models.Forecast;
import com.switchsolutions.agricultureapplication.utils.Coordinates;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ForecastListPresenter implements ForecastListContract.Presenter {

    private static final String SCREEN_NAME = "Forecast List";

    private static final int RC_LOCATION_PERMISSION = 30;

    private final ForecastListContract.View view;
    private final LocationClient locationClient;
    private final ForecastClient forecastClient;
    private final AnalyticsClient analyticsClient;
    private final RemoteConfigClient remoteConfigClient;

    private ArrayList<Forecast> forecastList;
    private Subscription locationSubscription;
    private Subscription forecastSubscription;
    private int selectedPosition = -1;

    public ForecastListPresenter(ForecastListContract.View view, ApplicationComponent component) {
        this.view = view;
        this.locationClient = component.locationClient();
        this.forecastClient = component.forecastClient();
        this.analyticsClient = component.analyticsClient();
        this.remoteConfigClient = component.remoteConfigClient();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        view.setAdapter();

        requestLocation();

        analyticsClient.sendScreenEvent(SCREEN_NAME);

        if (savedInstanceState == null && remoteConfigClient.hasBannerAds()) {
            view.showBanner();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onDestroy() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            locationSubscription.unsubscribe();
        }

        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) {
            forecastSubscription.unsubscribe();
        }
    }

    @Override
    public void onNavigationButtonClicked() {
        view.navigateUp();
    }

    @Override
    public int getItemViewType(int position) {
        return selectedPosition == position ? Adapter.VIEW_TYPE_EXPANDED : Adapter.VIEW_TYPE_COLLAPSED;
    }

    @Override
    public void onBindViewHolder(CollapsedItemViewHolder viewHolder, int position) {
        Forecast forecast = forecastList.get(position);
        viewHolder.setDate(forecast.getDatetime());
        viewHolder.setDescription(forecast.getWeatherCodeStringResId());
        viewHolder.setIcon(forecast.getWeatherCodeDrawableResId());
        viewHolder.setTemperature(forecast.getCelsius());
    }

    @Override
    public void onBindViewHolder(ExpandedItemViewHolder viewHolder, int position) {
        Forecast forecast = forecastList.get(position);
        viewHolder.setDate(forecast.getDatetime());
        viewHolder.setDescription(forecast.getWeatherCodeStringResId());
        viewHolder.setIcon(forecast.getWeatherCodeDrawableResId());
        viewHolder.setTemperature(forecast.getCelsius());
        viewHolder.setHumidity(forecast.getHumidity());
        viewHolder.setCloudCover(forecast.getCloudCover());
        viewHolder.setWindSpeed(Math.round(forecast.getWindSpeed())); // TODO: Fix units
        viewHolder.setWindDirection(forecast.getWindDirectionStringRes());
    }

    @Override
    public int getItemCount() {
        return forecastList != null ? forecastList.size() : 0;
    }

    @Override
    public void onListItemClicked(int position) {
        if (selectedPosition == -1) {
            selectedPosition = position;
            view.notifyItemChanged(position);
            return;
        }

        if (selectedPosition == position) {
            selectedPosition = -1;
            view.notifyItemChanged(position);
            return;
        }

        int oldPosition = selectedPosition;
        selectedPosition = position;
        view.notifyItemChanged(oldPosition);
        view.notifyItemChanged(selectedPosition);
    }

    @Override
    public void onActionButtonClicked() {
        requestContent();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RC_LOCATION_PERMISSION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    requestLocation();
                } else {
                    view.showError(R.string.solve, R.string.location_permission_required_for_forecast);
                }
                break;
            default:
                // Nothing
        }
    }

    @Override
    public void onLocationSettingsScreenResult() {
        requestContent();
    }

    void requestContent() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        if (forecastSubscription != null && !forecastSubscription.isUnsubscribed()) {
            return;
        }

        if (!locationClient.hasPermission()) {
            view.requestPermissions(new String[]{ACCESS_FINE_LOCATION}, RC_LOCATION_PERMISSION);
            return;
        }

        if (!locationClient.hasAccess()) {
            view.showLocationSettingsScreen();
            return;
        }

        requestLocation();
    }

    void requestLocation() {
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            return;
        }

        view.setProgressBarEnabled(true);
        locationSubscription = locationClient
                .getLocation()
                .timeout(10, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Coordinates>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(Coordinates coordinates) {
                        handleLocationResponse(coordinates);
                    }
                });
    }

    void requestForecast(Coordinates coordinates) {
        forecastSubscription = forecastClient
                .getForecast(coordinates.getLat(), coordinates.getLon())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Forecast>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onNext(ArrayList<Forecast> response) {
                        handleForecastResponse(response);
                    }
                });
    }

    void handleError(Throwable e) {
        if (e instanceof SocketTimeoutException) {
            handleError((SocketTimeoutException) e);
        } else if (e instanceof ConnectException) {
            handleError((ConnectException) e);
        } else if (e instanceof GoogleClientConnectionSuspendedException) {
            handleError((GoogleClientConnectionSuspendedException) e);
        } else if (e instanceof GoogleClientConnectionFailedException) {
            handleError((GoogleClientConnectionFailedException) e);
        } else if (e instanceof LocationProviderDisabledException) {
            handleError((LocationProviderDisabledException) e);
        } else if (e instanceof SecurityException) {
            handleError((SecurityException) e);
        } else if (e instanceof TimeoutException) {
            handleError((TimeoutException) e);
        } else if (e instanceof LocationNotFoundException) {
            handleError((LocationNotFoundException) e);
        } else {
            Timber.e(e, "Unable to get your location.");

            view.showError(R.string.retry, R.string.unable_to_get_your_location);
        }

        view.setProgressBarEnabled(false);
    }

    void handleError(SocketTimeoutException e) {
        view.showError(R.string.retry, R.string.unable_to_contact_server);
    }

    void handleError(ConnectException e) {
        view.showError(R.string.retry, R.string.please_check_your_internet_connection);
    }

    void handleError(GoogleClientConnectionSuspendedException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(GoogleClientConnectionFailedException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationProviderDisabledException e) {
        view.showError(R.string.solve, R.string.please_enable_location_access);
    }

    void handleError(SecurityException e) {
        view.showError(R.string.solve, R.string.location_permission_required_for_forecast);
    }

    void handleError(TimeoutException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleError(LocationNotFoundException e) {
        view.showError(R.string.retry, R.string.unable_to_get_your_location);
    }

    void handleLocationResponse(Coordinates coordinates) {
        requestForecast(coordinates);
    }

    void handleForecastResponse(ArrayList<Forecast> response) {
        forecastList = response;

        view.hideError();
        view.setAdapter();
        view.setProgressBarEnabled(false);
    }

}
