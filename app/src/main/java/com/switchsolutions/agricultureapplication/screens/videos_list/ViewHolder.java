package com.switchsolutions.agricultureapplication.screens.videos_list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

final class ViewHolder extends RecyclerView.ViewHolder {

    private final ImageView viewImage;
    private final TextView viewName;
    private final TextView viewUploadedOn;

    ViewHolder(View itemView) {
        super(itemView);

        viewImage = (ImageView) itemView.findViewById(R.id.video_image);
        viewName = (TextView) itemView.findViewById(R.id.video_name);
        viewUploadedOn = (TextView) itemView.findViewById(R.id.video_uploaded_on);
    }

    void setImage(String imagePath) {
        Glide.with(itemView.getContext())
                .load(RemoteFileUrlUtils.toImageUrl(imagePath))
                .thumbnail(Glide.with(itemView.getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(imagePath))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .centerCrop())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .centerCrop()
                .into(viewImage);
    }

    void setName(String title) {
        viewName.setText(title);
    }

    void setUploadedOn(String uploadedOn) {
        viewUploadedOn.setText(uploadedOn);
    }
}
