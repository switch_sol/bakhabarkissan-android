package com.switchsolutions.agricultureapplication.services;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.switchsolutions.agricultureapplication.BaseApplication;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class FirebaseUpdateTokenService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        if (BaseApplication.getComponent(this).preferences().getRequestToken() == null) {
            return;
        }

        Timber.d("Updating FCM Token.");

        String newToken = FirebaseInstanceId.getInstance().getToken();

        BaseApplication.getComponent(this)
                .accountsClient()
                .updateFCMToken(newToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof HttpException) {
                            Timber.d("Unable to update FCM token.");
                        } else if (e instanceof SocketTimeoutException) {
                            Timber.d("Unable to update FCM token.");
                        } else if (e instanceof ConnectException) {
                            Timber.d("Unable to update FCM token.");
                        } else {
                            Timber.e(e, "Unable to get crops.");
                        }
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        Timber.d("Updated FCM Token.");
                    }
                });
    }

}
