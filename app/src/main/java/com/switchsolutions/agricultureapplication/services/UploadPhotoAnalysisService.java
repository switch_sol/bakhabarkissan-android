package com.switchsolutions.agricultureapplication.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;
import com.switchsolutions.agricultureapplication.screens.photo_analyses_list.PhotoAnalysesListActivity;

import java.io.File;
import java.util.ArrayList;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;

public class UploadPhotoAnalysisService extends IntentService {

    private static final int NOTIFICATION_ID = 4835;

    public static final String KEY_TEXT = "text";
    public static final String KEY_AUDIO = "audio";
    public static final String KEY_IMAGES = "images";

    public UploadPhotoAnalysisService() {
        super(UploadPhotoAnalysisService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final String text = intent.getStringExtra(KEY_TEXT);
        final String audioPath = intent.getStringExtra(KEY_AUDIO);
        final ArrayList<String> imagePaths = intent.getStringArrayListExtra(KEY_IMAGES);

        File audio = null;
        if (audioPath != null) {
            audio = new File(audioPath);
        }

        File[] images = null;
        if (imagePaths != null) {
            images = new File[imagePaths.size()];
            for (int i = 0; i < imagePaths.size(); i++) {
                images[i] = new File(imagePaths.get(i));
            }
        }

        showUploadingNotification();
        BaseApplication.getComponent(this)
                .photoAnalysisClient()
                .upload(text, audio, images)
                .subscribe(new Subscriber<PhotoAnalysis>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        int err = R.string.an_error_occurred;
                        if (e instanceof HttpException) {
                            HttpException httpe = (HttpException) e;
                            switch (httpe.code()) {
                                case 429:
                                    err = R.string.please_try_again_in_5_minutes;
                                    break;
                            }
                        }

                        showRetryNotification(err, text, audioPath, imagePaths);
                    }

                    @Override
                    public void onNext(PhotoAnalysis photoAnalysis) {
                        showCompletedNotification();


                    }
                });
    }

    void showUploadingNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_cloud_upload_white_24dp);
        builder.setContentTitle(getString(R.string.uploading_for_photo_analysis));
        builder.setContentText(getString(R.string.please_wait));
        builder.setProgress(0, 0, true);

        showNotification(builder.build());
    }

    void showRetryNotification(@StringRes int stringRes, String text, String audioPath, ArrayList<String> imagePaths) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_error_white_24dp);
        builder.setContentTitle(getString(R.string.cant_upload_for_photo_analysis));
        builder.setContentText(getString(stringRes));

        Intent intent = createIntent(this, text, audioPath, imagePaths);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.addAction(R.drawable.ic_refresh_white_24dp, getString(R.string.retry), pendingIntent);

        showNotification(builder.build());
    }

    void showCompletedNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_cloud_upload_white_24dp);
        builder.setContentTitle(getString(R.string.uploaded_for_photo_analysis));
        builder.setContentText(getString(R.string.a_solution_will_be_provided_for_you_soon));

        TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(this);
        taskStackBuilder.addParentStack(MainScreenActivity.class);
        taskStackBuilder.addNextIntent(PhotoAnalysesListActivity.createIntent(this));
        PendingIntent pendingIntent = taskStackBuilder.getPendingIntent(0, PendingIntent.FLAG_CANCEL_CURRENT);

        builder.setContentIntent(pendingIntent);

        showNotification(builder.build());
    }

    void showNotification(Notification notification) {
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.notify(NOTIFICATION_ID, notification);
    }

    public static Intent createIntent(Context context, String text, String audio, ArrayList<String> images) {
        Intent intent = new Intent(context, UploadPhotoAnalysisService.class);
        intent.putExtra(KEY_TEXT, text);
        intent.putExtra(KEY_AUDIO, audio);
        intent.putStringArrayListExtra(KEY_IMAGES, images);
        return intent;
    }

}
