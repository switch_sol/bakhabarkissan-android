package com.switchsolutions.agricultureapplication.services;

import android.app.PendingIntent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.api.cloud_messaging.CloudMessagingClient;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.receivers.AppUpdateNotificationClickReceiver;
import com.switchsolutions.agricultureapplication.receivers.GlobalNotificationClickReceiver;
import com.switchsolutions.agricultureapplication.receivers.NewsNotificationClickReceiver;
import com.switchsolutions.agricultureapplication.receivers.PhotoAnalysisNotificationClickReceiver;
import com.switchsolutions.agricultureapplication.utils.Languages;

import java.util.Map;

import rx.Subscriber;
import timber.log.Timber;

public class FirebaseNotificationService extends FirebaseMessagingService {

    private static final int ID_GLOBAL = 300;
    private static final int ID_NEWS = 310;
    private static final int ID_PHOTO_ANALYSIS = 320;
    private static final int ID_APP_UPDATE = 330;

    private static final String KEY_TOPIC = "topic";
    private static final String KEY_TITLE = "title";
    private static final String KEY_TITLE_EN = "title_en";
    private static final String KEY_TITLE_UR = "title_ur";
    private static final String KEY_TEXT = "text";
    private static final String KEY_TEXT_EN = "text_en";
    private static final String KEY_TEXT_UR = "text_ur";
    private static final String KEY_ID = "id";
    private static final String KEY_VERSION = "version";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Map<String, String> data = remoteMessage.getData();
        switch (data.get(KEY_TOPIC)) {
            case CloudMessagingClient.TOPIC_GLOBAL:
                handleGlobalNotification(data);
                return;
            case CloudMessagingClient.TOPIC_NEWS:
                handleNewsNotification(data);
                return;
            case CloudMessagingClient.TOPIC_PHOTO_ANALYSIS:
                handlePhotoAnalysis(data);
                return;
            case CloudMessagingClient.TOPIC_APPUPDATE:
                handleAppUpdateNotification(data);
                return;
        }
    }

    private void handleGlobalNotification(Map<String, String> data) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, GlobalNotificationClickReceiver.createIntent(), PendingIntent.FLAG_UPDATE_CURRENT);

        switch (BaseApplication.getComponent(this).preferences().getLanguage()) {
            case Languages.UR:
                createNotification(data.get(KEY_TITLE_UR), data.get(KEY_TEXT_UR), ID_GLOBAL, pendingIntent, false);
                break;
            default:
                createNotification(data.get(KEY_TITLE_EN), data.get(KEY_TEXT_EN), ID_GLOBAL, pendingIntent, false);
                break;
        }

        AnalyticsClient analytics = BaseApplication.getComponent(this).analyticsClient();
        analytics.sendReceivedGlobalNotification();
    }

    private void handleNewsNotification(Map<String, String> data) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, NewsNotificationClickReceiver.createIntent(), PendingIntent.FLAG_UPDATE_CURRENT);

        switch (BaseApplication.getComponent(this).preferences().getLanguage()) {
            case Languages.UR:
                createNotification(data.get(KEY_TITLE_UR), data.get(KEY_TEXT_UR), ID_NEWS, pendingIntent, false);
                break;
            default:
                createNotification(data.get(KEY_TITLE_EN), data.get(KEY_TEXT_EN), ID_NEWS, pendingIntent, false);
                break;
        }

        BaseApplication.getComponent(this).analyticsClient().sendReceivedNewsNotification();
    }

    private void handlePhotoAnalysis(final Map<String, String> data) {
        final long id = Long.valueOf(data.get(KEY_ID));
        final String title = data.get(KEY_TITLE);
        final String text = data.get(KEY_TEXT);

        BaseApplication.getComponent(this).photoAnalysisClient()
                .update(id)
                .subscribe(new Subscriber<PhotoAnalysis>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Unable to update photo analysis status.");
                    }

                    @Override
                    public void onNext(PhotoAnalysis photoAnalysis) {
                        PendingIntent pendingIntent = PendingIntent.getBroadcast(FirebaseNotificationService.this, 0, PhotoAnalysisNotificationClickReceiver.createIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
                        createNotification(title, text, ID_PHOTO_ANALYSIS, pendingIntent, true);
                    }
                });
    }

    private void handleAppUpdateNotification(Map<String, String> data) {
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, AppUpdateNotificationClickReceiver.createIntent(), PendingIntent.FLAG_UPDATE_CURRENT);

        if (Integer.valueOf(data.get(KEY_VERSION)) > BuildConfig.VERSION_CODE){
            switch (BaseApplication.getComponent(this).preferences().getLanguage()) {
                case Languages.UR:
                    createNotification(data.get(KEY_TITLE_UR), data.get(KEY_TEXT_UR), ID_APP_UPDATE, pendingIntent, false);
                    break;
                default:
                    createNotification(data.get(KEY_TITLE_EN), data.get(KEY_TEXT_EN), ID_APP_UPDATE, pendingIntent, false);
                    break;
            }
        }

        BaseApplication.getComponent(this).analyticsClient().sendReceivedAppUpdateNotification();
    }

    private void createNotification(String title, String text, int id, PendingIntent pendingIntent, boolean alert) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_tree_white_24dp);
        builder.setContentTitle(title);
        builder.setContentText(text);
        builder.setLights(0xffffffff, 1000, 5000);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(text));

        if (alert) {
            builder.setVibrate(new long[]{500, 500});
            builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        }

        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.notify(id, builder.build());
    }

}
