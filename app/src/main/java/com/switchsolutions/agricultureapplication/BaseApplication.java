package com.switchsolutions.agricultureapplication;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;
import com.switchsolutions.agricultureapplication.stetho.StethoInitializer;
import com.switchsolutions.agricultureapplication.utils.LocaleUtils;

import timber.log.Timber;

public class BaseApplication extends Application {

    private ApplicationComponent component;

    public static int cart_count = 0;

    @Nullable
    private ProgressDialog mProgressDialog;


    public static ApplicationComponent getComponent(Context context) {
        return ((BaseApplication) context.getApplicationContext()).component;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        prepareTimber();
        prepareStetho();
        prepareComponent();
        prepareLocale();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        LocaleUtils.updateConfig(this, newConfig);
    }

    private void prepareTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new Timber.Tree() {
                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                    if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                        return;
                    }

                    FirebaseCrash.log(message);

                    if (t != null) {
                        FirebaseCrash.report(t);
                    }
                }
            });
        }
    }

    private void prepareStetho() {
        StethoInitializer.initialize(this);
    }

    private void prepareComponent() {
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    private void prepareLocale() {
        String locale = component.preferences().getLanguage();
        LocaleUtils.setLocale(locale);
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
    }


    public void showProgresDialogue(String mess, Context context) {

        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(true);
                mProgressDialog.setMessage("" + mess);
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialogue() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
