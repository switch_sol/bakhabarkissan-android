package com.switchsolutions.agricultureapplication.network;

public interface NetworkUtility {

    boolean isInternetConnected();
}
