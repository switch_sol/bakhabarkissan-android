package com.switchsolutions.agricultureapplication.network;

import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.Constants;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.stetho.StethoInitializer;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.io.IOException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Module
public class NetworkModule {

    public static final String PROVIDER_AGRICULTURE_API = "agriculture.api";
    public static final String PROVIDER_AGRICULTURE_CONTENT = "agriculture.content";
    public static final String PROVIDER_FORECAST = "forecast";

    private static final long CACHE_SIZE = 10 * 1024 * 5;

    @Provides
    @Singleton
    public NetworkUtility providesNetworkUtility(NetworkUtilityImpl network) {
        return network;
    }

    @Provides
    @Singleton
    @Named(PROVIDER_FORECAST)
    public OkHttpClient providesForecastOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInitializer.createInterceptor());
        }
        return builder.build();
    }

    @Provides
    @Singleton
    @Named(PROVIDER_FORECAST)
    public Retrofit providesForecastRetrofit(@Named(PROVIDER_FORECAST) OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Constants.URL_FORECAST)
                .build();
    }

    @Provides
    @Singleton
    @Named(PROVIDER_AGRICULTURE_API)
    public OkHttpClient providesAgricultureApiOkHttpClient(Storage storage, final Preferences preferences) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cache(new Cache(storage.getCacheDir(), CACHE_SIZE));

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request reqRequest = chain.request();

                String requestToken = preferences.getRequestToken();
                if (requestToken != null && reqRequest.header("Authorization") == null) {
                    reqRequest = reqRequest.newBuilder()
                            .addHeader("Authorization", requestToken)
                            .build();
                }

                return chain.proceed(reqRequest);
            }
        });
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInitializer.createInterceptor());
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS));
        }
        return builder.build();
    }

    @Provides
    @Singleton
    @Named(PROVIDER_AGRICULTURE_CONTENT)
    public OkHttpClient providesAgricultureContentOkHttpClient(Storage storage) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.cache(new Cache(storage.getCacheDir(), CACHE_SIZE));
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(StethoInitializer.createInterceptor());
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS));
        }
        return builder.build();
    }

    @Provides
    @Singleton
    @Named(PROVIDER_AGRICULTURE_API)
    public Retrofit providesAgricultureApiRetrofit(@Named(PROVIDER_AGRICULTURE_API) OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Constants.URL_AGRICULTURE_API)
                .build();
    }

    @Provides
    @Singleton
    @Named(PROVIDER_AGRICULTURE_CONTENT)
    public Retrofit providesAgricultureContentRetrofit(@Named(PROVIDER_AGRICULTURE_API) OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(Constants.URL_AGRICULTURE_CONTENT)
                .build();
    }

}
