package com.switchsolutions.agricultureapplication.audio;

import android.media.MediaRecorder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

final class AudioRecorderImpl implements AudioRecorder, MediaRecorder.OnErrorListener, MediaRecorder.OnInfoListener {

    private final MediaRecorder recorder;

    private int state = STATE_RESET;
    private AudioRecorder.Listener listener;
    private Subscription subscription;

    @Inject
    AudioRecorderImpl() {
        this.recorder = new MediaRecorder();
        this.recorder.setOnErrorListener(this);
        this.recorder.setOnInfoListener(this);
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public void prepare(File file) throws IOException {
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorder.setAudioSamplingRate(8000);
        recorder.setAudioEncodingBitRate(8000);

        if (file == null) {
            throw new IOException("Unable to create audio file");
        }
        recorder.setOutputFile(file.getAbsolutePath());

        recorder.prepare();
        state = STATE_PREPARED;

        announceStateChange();
    }

    @Override
    public void start() throws IllegalStateException {
        recorder.start();
        state = STATE_STARTED;

        announceStateChange();

        subscription = Observable
                .interval(1, TimeUnit.SECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (listener != null) {
                            listener.onAudioRecorderSecond(aLong);
                        }
                    }
                });
    }

    @Override
    public void stop() throws IllegalStateException {
        recorder.stop();
        state = STATE_STOPPED;

        subscription.unsubscribe();

        announceStateChange();
    }

    @Override
    public void reset() throws IllegalStateException {
        recorder.reset();
        state = STATE_RESET;

        announceStateChange();
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onError(MediaRecorder mr, int what, int extra) {
        state = STATE_RESET;

        try {
            recorder.reset();
        } catch (Exception e) {

        }

        if (listener != null) {
            switch (what) {
                case MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN:
                case MediaRecorder.MEDIA_ERROR_SERVER_DIED:
                    listener.onAudioRecorderError(what, extra);
            }
        }
    }

    @Override
    public void onInfo(MediaRecorder mr, int what, int extra) {
        state = STATE_RESET;

        try {
            recorder.reset();
        } catch (Exception e) {

        }

        if (listener != null) {
            switch (what) {
                case MediaRecorder.MEDIA_RECORDER_INFO_UNKNOWN:
                case MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:
                case MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:
                    listener.onAudioRecorderInfo(what, extra);
            }
        }
    }

    private void announceStateChange() {
        if (listener != null) {
            listener.onAudioRecorderStateChanged(state);
        }
    }
}
