package com.switchsolutions.agricultureapplication.audio;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AudioModule {

    @Provides
    @Singleton
    public AudioRecorder providesAudioRecorder(AudioRecorderImpl audioRecorder) {
        return audioRecorder;
    }

    @Provides
    @Singleton
    public AudioPlayer providesAudioPlayer(AudioPlayerImpl audioPlayer) {
        return audioPlayer;
    }

}
