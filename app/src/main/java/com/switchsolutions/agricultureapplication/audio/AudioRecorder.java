package com.switchsolutions.agricultureapplication.audio;

import java.io.File;
import java.io.IOException;

public interface AudioRecorder {

    int STATE_RESET = 0;
    int STATE_PREPARED = 1;
    int STATE_STARTED = 2;
    int STATE_STOPPED = 3;

    int getState();

    void prepare(File file) throws IOException;

    void start() throws IllegalStateException;

    void stop() throws IllegalStateException;

    void reset() throws IllegalStateException;

    void setListener(Listener listener);

    interface Listener {
        void onAudioRecorderSecond(long seconds);
        void onAudioRecorderStateChanged(int state);
        void onAudioRecorderError(int what, int extra);
        void onAudioRecorderInfo(int what, int extra);
    }
}
