package com.switchsolutions.agricultureapplication.audio;

import android.support.annotation.NonNull;

import java.io.File;
import java.io.IOException;

public interface AudioPlayer {

    int STATE_RESET = 0;
    int STATE_PREPARED = 1;
    int STATE_STARTED = 2;
    int STATE_PAUSED = 3;
    int STATE_STOPPED = 4;

    int getState();

    int getLength();

    void prepare(@NonNull File file) throws IOException;

    void start() throws IllegalStateException;

    void pause() throws IllegalStateException;

    void stop() throws IllegalStateException;

    void reset() throws IllegalStateException;

    void setListener(Listener listener);

    interface Listener {
        void onAudioPlayerCompleted();
        void onAudioPlayerSecond(long seconds);
        void onAudioPlayerStateChanged(int state);
    }
}
