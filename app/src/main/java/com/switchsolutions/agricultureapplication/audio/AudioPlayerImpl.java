package com.switchsolutions.agricultureapplication.audio;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

final class AudioPlayerImpl implements AudioPlayer, MediaPlayer.OnCompletionListener {

    private final MediaPlayer mediaPlayer;

    private int state = STATE_RESET;
    private Subscription subscription;
    private Listener listener;

    @Inject
    AudioPlayerImpl() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(this);
    }

    @Override
    public int getState() {
        return state;
    }

    @Override
    public int getLength() {
        return (int) TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getDuration());
    }

    @Override
    public void prepare(@NonNull File file) throws IOException {
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setDataSource(file.getAbsolutePath());
        mediaPlayer.prepare();
        state = STATE_PREPARED;

        if (listener != null) {
            listener.onAudioPlayerStateChanged(state);
        }
    }

    @Override
    public void start() throws IllegalStateException {
        mediaPlayer.start();
        state = STATE_STARTED;

        subscription = Observable
                .interval(1001, TimeUnit.MILLISECONDS, Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Long>() {
                    @Override
                    public void call(Long aLong) {
                        if (listener != null) {
                            listener.onAudioPlayerSecond(TimeUnit.MILLISECONDS.toSeconds(mediaPlayer.getCurrentPosition()));
                        }
                    }
                });

        if (listener != null) {
            listener.onAudioPlayerStateChanged(state);
        }
    }

    @Override
    public void pause() throws IllegalStateException {
        mediaPlayer.pause();
        state = STATE_PAUSED;

        if (listener != null) {
            listener.onAudioPlayerStateChanged(state);
        }
    }

    @Override
    public void stop() throws IllegalStateException {
        mediaPlayer.stop();
        state = STATE_STOPPED;

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        if (listener != null) {
            listener.onAudioPlayerStateChanged(state);
        }
    }

    @Override
    public void reset() throws IllegalStateException {
        mediaPlayer.reset();
        state = STATE_RESET;

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        if (listener != null) {
            listener.onAudioPlayerStateChanged(state);
        }
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        state = STATE_RESET;

        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }

        try {
            mediaPlayer.reset();
        } catch (Exception e) {

        }

        if (listener != null) {
            listener.onAudioPlayerCompleted();
        }
    }
}
