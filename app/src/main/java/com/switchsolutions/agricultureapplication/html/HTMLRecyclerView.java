package com.switchsolutions.agricultureapplication.html;

import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public final class HTMLRecyclerView extends RecyclerView {

    private Adapter adapter;

    public HTMLRecyclerView(Context context) {
        super(context);
        init();
    }

    public HTMLRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HTMLRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setLayoutManager(new LayoutManager(getContext()));
        setHasFixedSize(false);
        setAdapter(adapter = new Adapter());
        setItemAnimator(null);
    }

    public void setContent(List<HTMLElement> elements) {
        adapter.elements = elements;
        adapter.notifyDataSetChanged();
    }

    public void setOnElementClickListener(OnElementClickListener onElementClickListener) {
        this.adapter.onElementClickListener = onElementClickListener;
    }

    public void clearFocusedPosition() {
        int focusedPosition = adapter.focusedPosition;
        adapter.focusedPosition = -1;
        adapter.notifyItemChanged(focusedPosition);
    }

    public void setFocusedPosition(int positionFocused) {
        int focusedPosition = adapter.focusedPosition;
        adapter.focusedPosition = -1;
        if (focusedPosition != -1) {
            this.adapter.notifyItemChanged(focusedPosition);
        }
        this.adapter.focusedPosition = positionFocused;
        this.adapter.notifyItemChanged(positionFocused);
    }

    public interface OnElementClickListener {
        void onElementClicked(HTMLHeader header);

        void onElementClicked(HTMLParagraph paragraph);

        void onElementClicked(HTMLImage image);

        void onElementClicked(HTMLTable table);

        void onElementClicked(HTMLUnorderedList unorderedList);

        void onElementClicked(HTMLOrderedList orderedList);
    }

    private static final class LayoutManager extends LinearLayoutManager {

        private SmoothScroller smoothScroller;

        public LayoutManager(Context context) {
            super(context);
            init(context);
        }

        public LayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
            init(context);
        }

        public LayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
            init(context);
        }

        private void init(Context context) {
            smoothScroller = new SmoothScroller(context);
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
            smoothScroller.setTargetPosition(position);
            startSmoothScroll(smoothScroller);
        }

        private final class SmoothScroller extends LinearSmoothScroller {

            private SmoothScroller(Context context) {
                super(context);
            }

            @Nullable
            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return LayoutManager.this.computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected int getVerticalSnapPreference() {
                return SNAP_TO_START;
            }
        }

    }

    private static final class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_HEADER = 1;
        private static final int VIEW_TYPE_PARAGRAPH = 2;
        private static final int VIEW_TYPE_IMAGE = 3;
        private static final int VIEW_TYPE_TABLE = 4;
        private static final int VIEW_TYPE_UNORDERED_LIST = 5;
        private static final int VIEW_TYPE_ORDERED_LIST = 6;
        private static final int VIEW_TYPE_HORIZONTAL_LINE = 7;

        private List<HTMLElement> elements;
        private OnElementClickListener onElementClickListener;
        private int focusedPosition = -1;

        @Override
        public int getItemViewType(int position) {
            if (elements.get(position) instanceof HTMLHeader) {
                return VIEW_TYPE_HEADER;
            }

            if (elements.get(position) instanceof HTMLParagraph) {
                return VIEW_TYPE_PARAGRAPH;
            }

            if (elements.get(position) instanceof HTMLImage) {
                return VIEW_TYPE_IMAGE;
            }

            if (elements.get(position) instanceof HTMLTable) {
                return VIEW_TYPE_TABLE;
            }

            if (elements.get(position) instanceof HTMLUnorderedList) {
                return VIEW_TYPE_UNORDERED_LIST;
            }

            if (elements.get(position) instanceof HTMLOrderedList) {
                return VIEW_TYPE_ORDERED_LIST;
            }

            if (elements.get(position) instanceof HTMLHorizontalLine) {
                return VIEW_TYPE_HORIZONTAL_LINE;
            }

            return super.getItemViewType(position);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_HEADER) {
                final HTMLHeaderView view = HTMLHeaderView.create(parent.getContext());
                final HTMLHeaderViewHolder viewHolder = new HTMLHeaderViewHolder(view);

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLHeader) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_PARAGRAPH) {
                final HTMLParagraphView view = HTMLParagraphView.create(parent.getContext());
                final HTMLParagraphViewHolder viewHolder = new HTMLParagraphViewHolder(view);

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLParagraph) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_IMAGE) {
                final HTMLImageView view = HTMLImageView.create(parent.getContext());
                final HTMLImageViewHolder viewHolder = new HTMLImageViewHolder(view);

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLImage) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_TABLE) {
                final HTMLTableView view = HTMLTableView.create(parent.getContext());
                final HTMLTableViewHolder viewHolder = new HTMLTableViewHolder(view);
                viewHolder.setIsRecyclable(false);

                view.setClickable(true);
                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLTable) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_UNORDERED_LIST) {
                final HTMLUnorderedListView view = HTMLUnorderedListView.create(parent.getContext());
                final HTMLUnorderedListViewHolder viewHolder = new HTMLUnorderedListViewHolder(view);
                viewHolder.setIsRecyclable(false);

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLUnorderedList) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_ORDERED_LIST) {
                final HTMLOrderedListView view = HTMLOrderedListView.create(parent.getContext());
                final HTMLOrderedListViewHolder viewHolder = new HTMLOrderedListViewHolder(view);
                viewHolder.setIsRecyclable(false);

                view.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onElementClickListener != null) {
                            onElementClickListener.onElementClicked((HTMLOrderedList) elements.get(viewHolder.getAdapterPosition()));
                        }
                    }
                });

                return viewHolder;
            }

            if (viewType == VIEW_TYPE_HORIZONTAL_LINE) {
                return new HTMLHorizontalLineViewHolder(HTMLHorizontalLineView.create(parent.getContext()));
            }

            return null;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (getItemViewType(position) == VIEW_TYPE_HEADER) {
                HTMLHeader item = (HTMLHeader) elements.get(position);
                HTMLHeaderViewHolder viewHolder = (HTMLHeaderViewHolder) holder;
                viewHolder.setContent(item);
                viewHolder.setFocused(position == focusedPosition);
                return;
            }

            if (getItemViewType(position) == VIEW_TYPE_PARAGRAPH) {
                HTMLParagraph item = (HTMLParagraph) elements.get(position);
                HTMLParagraphViewHolder viewHolder = (HTMLParagraphViewHolder) holder;
                viewHolder.setContent(item);
                viewHolder.setFocused(position == focusedPosition);
                return;
            }

            if (getItemViewType(position) == VIEW_TYPE_IMAGE) {
                HTMLImage item = (HTMLImage) elements.get(position);
                HTMLImageViewHolder viewHolder = (HTMLImageViewHolder) holder;
                viewHolder.setContent(item);
                return;
            }

            if (getItemViewType(position) == VIEW_TYPE_TABLE) {
                HTMLTable item = (HTMLTable) elements.get(position);
                HTMLTableViewHolder viewHolder = (HTMLTableViewHolder) holder;
                if (!viewHolder.isContentSet()) {
                    viewHolder.setContent(item);
                }
                return;
            }

            if (getItemViewType(position) == VIEW_TYPE_UNORDERED_LIST) {
                HTMLUnorderedList item = (HTMLUnorderedList) elements.get(position);
                HTMLUnorderedListViewHolder viewHolder = (HTMLUnorderedListViewHolder) holder;
                viewHolder.setFocused(position == focusedPosition);
                if (!viewHolder.hasChildren()) {
                    viewHolder.setContent(item);
                }
                return;
            }

            if (getItemViewType(position) == VIEW_TYPE_ORDERED_LIST) {
                HTMLOrderedList item = (HTMLOrderedList) elements.get(position);
                HTMLOrderedListViewHolder viewHolder = (HTMLOrderedListViewHolder) holder;
                viewHolder.setFocused(position == focusedPosition);
                if (!viewHolder.hasChildren()) {
                    viewHolder.setContent(item);
                }
                return;
            }
        }

        @Override
        public int getItemCount() {
            return elements != null ? elements.size() : 0;
        }
    }
}
