package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import org.jsoup.nodes.Element;

public abstract class HTMLElement implements Parcelable {

    private final String audioPath;
    private final int index;

    HTMLElement(Element element, int index) {
        this.audioPath = element.attr("audio").equals("") ? null : element.attr("audio");
        this.index = index;
    }

    HTMLElement(Parcel in) {
        this.audioPath = in.readString();
        this.index = in.readInt();
    }

    @Nullable
    public static HTMLElement from(Element element, int position) {
        if (HTMLHeader.isHeader(element)) {
            return new HTMLHeader(element, position);
        }

        if (HTMLParagraph.isParagraph(element)) {
            return new HTMLParagraph(element, position);
        }

        if (HTMLImage.isImage(element)) {
            return new HTMLImage(element, position);
        }

        if (HTMLTable.isTable(element)) {
            return new HTMLTable(element, position);
        }

        if (HTMLUnorderedList.isUnorderedList(element)) {
            return new HTMLUnorderedList(element, position);
        }

        if (HTMLOrderedList.isOrderedList(element)) {
            return new HTMLOrderedList(element, position);
        }

        if (HTMLHorizontalLine.isHorizontalLine(element)) {
            return new HTMLHorizontalLine(element, position);
        }

        return null;
    }

    public String getAudioPath() {
        return audioPath;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(audioPath);
        parcel.writeInt(index);
    }
}
