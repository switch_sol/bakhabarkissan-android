package com.switchsolutions.agricultureapplication.html;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class HTMLTableViewHolder extends RecyclerView.ViewHolder {

    private final HTMLTableView tableView;
    private boolean isContentSet = false;

    HTMLTableViewHolder(View itemView) {
        super(itemView);

        tableView = (HTMLTableView) itemView;
    }

    void setContent(HTMLTable table) {
        tableView.setContent(table);

        isContentSet = true;
    }

    boolean isContentSet() {
        return isContentSet;
    }
}
