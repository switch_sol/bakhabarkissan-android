package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

public final class HTMLHeader extends HTMLElement {

    public static final Parcelable.Creator<HTMLHeader> CREATOR = new Creator<HTMLHeader>() {
        @Override
        public HTMLHeader createFromParcel(Parcel parcel) {
            return new HTMLHeader(parcel);
        }

        @Override
        public HTMLHeader[] newArray(int i) {
            return new HTMLHeader[i];
        }
    };
    private final String text;
    private final int headerSize;
    private final float textSize;

    HTMLHeader(Element element, int position) {
        super(element, position);
        this.text = element.html();
        this.headerSize = parseHeaderSize(element);
        this.textSize = parseTextSize(element);
    }

    HTMLHeader(Parcel in) {
        super(in);

        this.text = in.readString();
        this.headerSize = in.readInt();
        this.textSize = in.readFloat();
    }

    static boolean isHeader(Element element) {
        String tagName = element.tagName().toLowerCase();
        return tagName.equals("h1")
                || tagName.equals("h2")
                || tagName.equals("h3")
                || tagName.equals("h4")
                || tagName.equals("h5")
                || tagName.equals("h6");
    }

    static int parseHeaderSize(Element element) {
        switch (element.tagName().toLowerCase()) {
            case "h1":
                return 1;
            case "h2":
                return 2;
            case "h3":
                return 3;
            case "h4":
                return 4;
            case "h5":
                return 5;
            default:
                return 6;
        }
    }

    static float parseTextSize(Element element) {
        switch (element.tagName().toLowerCase()) {
            case "h1":
                return 22.00f;
            case "h2":
                return 20.80f;
            case "h3":
                return 19.60f;
            case "h4":
                return 18.40f;
            case "h5":
                return 17.20f;
            default:
                return 16.00f;
        }
    }

    public String getText() {
        return text;
    }

    public int getHeaderSize() {
        return headerSize;
    }

    public float getTextSize() {
        return textSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(text);
        parcel.writeInt(headerSize);
        parcel.writeFloat(textSize);
    }

}
