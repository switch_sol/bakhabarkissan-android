package com.switchsolutions.agricultureapplication.html;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.switchsolutions.agricultureapplication.R;

final class HTMLOrderedListViewHolder extends RecyclerView.ViewHolder {

    private final HTMLOrderedListView orderedListView;

    private final int selectableItemBackgroundResId;

    HTMLOrderedListViewHolder(View itemView) {
        super(itemView);

        orderedListView = (HTMLOrderedListView) itemView;

        TypedValue outValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        selectableItemBackgroundResId = outValue.resourceId;
    }

    void setContent(HTMLOrderedList orderedList) {
        orderedListView.setContent(orderedList);
    }

    boolean hasChildren() {
        return orderedListView.getChildCount() > 0;
    }

    void setFocused(boolean focused) {
        if (focused) {
            orderedListView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.md_blue_50));
        } else {
            orderedListView.setBackgroundResource(selectableItemBackgroundResId);
        }
    }
}
