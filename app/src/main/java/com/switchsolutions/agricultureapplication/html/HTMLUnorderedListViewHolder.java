package com.switchsolutions.agricultureapplication.html;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.switchsolutions.agricultureapplication.R;

final class HTMLUnorderedListViewHolder extends RecyclerView.ViewHolder {

    private final HTMLUnorderedListView unorderedListView;

    private final int selectableItemBackgroundResId;

    HTMLUnorderedListViewHolder(View itemView) {
        super(itemView);

        unorderedListView = (HTMLUnorderedListView) itemView;

        TypedValue outValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        selectableItemBackgroundResId = outValue.resourceId;
    }

    void setContent(HTMLUnorderedList unorderedList) {
        unorderedListView.setContent(unorderedList);
    }

    boolean hasChildren() {
        return unorderedListView.getChildCount() > 0;
    }

    void setFocused(boolean focused) {
        if (focused) {
            unorderedListView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.md_blue_50));
        } else {
            unorderedListView.setBackgroundResource(selectableItemBackgroundResId);
        }
    }
}
