package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public final class HTMLUnorderedList extends HTMLElement {

    public static final Parcelable.Creator<HTMLUnorderedList> CREATOR = new Creator<HTMLUnorderedList>() {
        @Override
        public HTMLUnorderedList createFromParcel(Parcel parcel) {
            return new HTMLUnorderedList(parcel);
        }

        @Override
        public HTMLUnorderedList[] newArray(int i) {
            return new HTMLUnorderedList[i];
        }
    };
    private final List<String> rows;

    HTMLUnorderedList(Element element, int position) {
        super(element, position);

        this.rows = new ArrayList<>();
        for (Element e : element.select("li")) {
            rows.add(e.html());
        }
    }

    HTMLUnorderedList(Parcel in) {
        super(in);

        this.rows = new ArrayList<>();
        in.readStringList(this.rows);
    }

    static boolean isUnorderedList(Element element) {
        return element.tagName().toLowerCase().equals("ul");
    }

    public List<String> getRows() {
        return rows;
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeStringList(rows);
    }
}
