package com.switchsolutions.agricultureapplication.html;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayout;
import android.text.Html;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

import java.util.List;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

final class HTMLTableView extends NestedScrollView {

    private HorizontalScrollView viewHorizontalScrollView;
    private GridLayout viewGridLayout;

    public HTMLTableView(Context context) {
        super(context);
        init();
    }

    public HTMLTableView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HTMLTableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        viewHorizontalScrollView = new HorizontalScrollView(getContext());
        viewHorizontalScrollView.setLayoutParams(new LayoutParams(MATCH_PARENT, WRAP_CONTENT));
        viewHorizontalScrollView.setHorizontalScrollBarEnabled(false);
        viewHorizontalScrollView.setVerticalScrollBarEnabled(false);
        addView(viewHorizontalScrollView);

        viewGridLayout = new GridLayout(getContext());
        viewGridLayout.setLayoutParams(new LayoutParams(WRAP_CONTENT, WRAP_CONTENT));
        viewGridLayout.setUseDefaultMargins(true);

        int dp16 = getContext().getResources().getDimensionPixelSize(R.dimen.dp16);
        viewGridLayout.setPadding(dp16, dp16, dp16, dp16);

        viewHorizontalScrollView.addView(viewGridLayout);

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.md_green_100));
    }

    void setContent(HTMLTable table) {
        viewGridLayout.removeAllViews();

        int r = 0;
        for (List<String> re : table.getRows()) {
            int c = 0;
            for (String ce : re) {
                GridLayout.LayoutParams lp = new GridLayout.LayoutParams();
                lp.rowSpec = GridLayout.spec(r);
                lp.columnSpec = GridLayout.spec(c);

                TextView tv = new TextView(getContext());
                tv.setLayoutParams(lp);
                tv.setMinWidth(getContext().getResources().getDimensionPixelSize(R.dimen.dp48));
                tv.setMaxWidth(getContext().getResources().getDimensionPixelSize(R.dimen.dp192));
                tv.setTextColor(ContextCompat.getColor(getContext(), R.color.black_primary_text));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14f);
                tv.setSingleLine(false);
                tv.setLineSpacing(0f, 1f);
                tv.setText(Html.fromHtml(ce));

                viewGridLayout.addView(tv);
                c++;
            }
            r++;
        }

        viewGridLayout.requestLayout();
    }

    static HTMLTableView create(Context context) {
        HTMLTableView view = new HTMLTableView(context);

        FrameLayout.LayoutParams layoutParams = new LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        view.setLayoutParams(layoutParams);

        return view;
    }

}
