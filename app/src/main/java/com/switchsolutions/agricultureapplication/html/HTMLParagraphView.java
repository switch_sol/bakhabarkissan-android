package com.switchsolutions.agricultureapplication.html;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

final class HTMLParagraphView extends TextView {

    HTMLParagraphView(Context context) {
        super(context);
        init();
    }

    HTMLParagraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    HTMLParagraphView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    HTMLParagraphView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    static HTMLParagraphView create(Context context) {
        HTMLParagraphView view = new HTMLParagraphView(context);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        int dp16 = context.getResources().getDimensionPixelSize(R.dimen.dp16);
        layoutParams.topMargin = dp16;
        layoutParams.bottomMargin = dp16;
        view.setLayoutParams(layoutParams);

        return view;
    }

    private void init() {
        setTextColor(ContextCompat.getColor(getContext(), R.color.black_primary_text));
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
        setLineSpacing(0f, 1.5f);
        setSingleLine(false);

        setClickable(true);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        setBackgroundResource(outValue.resourceId);

        int dp16 = getContext().getResources().getDimensionPixelSize(R.dimen.dp16);
        setPadding(dp16, 0, dp16, 0);
    }

    void setContent(HTMLParagraph paragraph) {
        setText(Html.fromHtml(paragraph.getText()));
    }
}
