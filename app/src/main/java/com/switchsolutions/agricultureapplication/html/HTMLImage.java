package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

public final class HTMLImage extends HTMLElement {

    public static final Parcelable.Creator<HTMLImage> CREATOR = new Creator<HTMLImage>() {
        @Override
        public HTMLImage createFromParcel(Parcel parcel) {
            return new HTMLImage(parcel);
        }

        @Override
        public HTMLImage[] newArray(int i) {
            return new HTMLImage[i];
        }
    };
    private final String imagePath;

    HTMLImage(Element element, int position) {
        super(element, position);

        this.imagePath = element.attr("src");
    }

    HTMLImage(Parcel in) {
        super(in);

        this.imagePath = in.readString();
    }

    static boolean isImage(Element element) {
        return element.tagName().toLowerCase().equals("img");
    }

    public String getImagePath() {
        return imagePath;
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(imagePath);
    }
}
