package com.switchsolutions.agricultureapplication.html;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.switchsolutions.agricultureapplication.R;

final class HTMLHeaderViewHolder extends RecyclerView.ViewHolder {

    private final HTMLHeaderView headerView;

    private final int selectableItemBackgroundResId;

    HTMLHeaderViewHolder(View itemView) {
        super(itemView);

        this.headerView = (HTMLHeaderView) itemView;

        TypedValue outValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        selectableItemBackgroundResId = outValue.resourceId;
    }

    void setContent(HTMLHeader header) {
        headerView.setContent(header);
    }

    void setFocused(boolean focused) {
        if (focused) {
            headerView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.md_blue_50));
        } else {
            headerView.setBackgroundResource(selectableItemBackgroundResId);
        }
    }
}
