package com.switchsolutions.agricultureapplication.html;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayout;
import android.text.Html;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.switchsolutions.agricultureapplication.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

final class HTMLOrderedListView extends GridLayout {

    HTMLOrderedListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    HTMLOrderedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    HTMLOrderedListView(Context context) {
        super(context);
        init();
    }

    static HTMLOrderedListView create(Context context) {
        HTMLOrderedListView view = new HTMLOrderedListView(context);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        int dp16 = context.getResources().getDimensionPixelSize(R.dimen.dp16);
        layoutParams.topMargin = dp16;
        layoutParams.bottomMargin = dp16;
        view.setLayoutParams(layoutParams);

        return view;
    }

    static String digitsToString(int n, String[] options, boolean isRTL) {
        if (n == 0) return options[0];

        StringBuilder sb = new StringBuilder();
        while (n > 0) {
            sb.append(options[n % 10]);
            n = n / 10;
        }

        return isRTL ? sb.toString() : sb.reverse().toString();
    }

    private void init() {
        setUseDefaultMargins(true);

        setClickable(true);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        setBackgroundResource(outValue.resourceId);

        int dp16 = getContext().getResources().getDimensionPixelSize(R.dimen.dp16);
        setPadding(dp16, 0, dp16, 0);
    }

    void setContent(HTMLOrderedList orderedList) {
        removeAllViews();

        String[] options = getResources().getStringArray(R.array.digits);
        boolean isRTL = ViewCompat.getLayoutDirection(this) == ViewCompat.LAYOUT_DIRECTION_RTL;

        int r = 0;
        for (String re : orderedList.getRows()) {
            LayoutParams lp = new LayoutParams();
            lp.width = 0;
            lp.rowSpec = GridLayout.spec(r, 1f);
            lp.columnSpec = GridLayout.spec(0, 1f);
            lp.topMargin = getResources().getDimensionPixelSize(R.dimen.dp8);
            lp.setGravity(Gravity.FILL_HORIZONTAL);

            TextView tv = new TextView(getContext());
            tv.setLayoutParams(lp);
            tv.setTextColor(ContextCompat.getColor(getContext(), R.color.black_primary_text));
            tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f);
            tv.setLineSpacing(0f, 1.25f);
            tv.setSingleLine(false);
            tv.setText(String.format("%s. %s", digitsToString(r + 1, options, isRTL), Html.fromHtml(re)));

            addView(tv);
            r++;
        }
    }
}
