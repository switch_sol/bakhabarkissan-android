package com.switchsolutions.agricultureapplication.html;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.switchsolutions.agricultureapplication.R;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

final class HTMLHorizontalLineView extends View {

    HTMLHorizontalLineView(Context context) {
        super(context);
        init();
    }

    HTMLHorizontalLineView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    HTMLHorizontalLineView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    HTMLHorizontalLineView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    static HTMLHorizontalLineView create(Context context) {
        final HTMLHorizontalLineView view = new HTMLHorizontalLineView(context);

        int dp1 = context.getResources().getDimensionPixelSize(R.dimen.dp1);
        int dp16 = context.getResources().getDimensionPixelSize(R.dimen.dp16);

        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(MATCH_PARENT, dp1);
        MarginLayoutParamsCompat.setMarginStart(layoutParams, dp16);
        MarginLayoutParamsCompat.setMarginEnd(layoutParams, dp16);
        layoutParams.topMargin = dp16;
        layoutParams.bottomMargin = dp16;
        view.setLayoutParams(layoutParams);

        return view;
    }

    private void init() {
        setBackgroundResource(R.color.black_dividers);
    }

}
