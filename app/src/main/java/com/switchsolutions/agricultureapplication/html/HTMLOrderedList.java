package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public final class HTMLOrderedList extends HTMLElement {

    public static final Parcelable.Creator<HTMLOrderedList> CREATOR = new Creator<HTMLOrderedList>() {
        @Override
        public HTMLOrderedList createFromParcel(Parcel parcel) {
            return new HTMLOrderedList(parcel);
        }

        @Override
        public HTMLOrderedList[] newArray(int i) {
            return new HTMLOrderedList[i];
        }
    };
    private final List<String> rows;

    HTMLOrderedList(Element element, int position) {
        super(element, position);

        this.rows = new ArrayList<>();
        for (Element e : element.select("li")) {
            rows.add(e.html());
        }
    }

    HTMLOrderedList(Parcel in) {
        super(in);

        this.rows = new ArrayList<>();
        in.readStringList(this.rows);
    }

    static boolean isOrderedList(Element element) {
        return element.tagName().toLowerCase().equals("ol");
    }

    public List<String> getRows() {
        return rows;
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeStringList(rows);
    }
}
