package com.switchsolutions.agricultureapplication.html;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.R;
import com.switchsolutions.agricultureapplication.utils.RemoteFileUrlUtils;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

final class HTMLImageView extends ImageView {

    HTMLImageView(Context context) {
        super(context);
        init();
    }

    HTMLImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    HTMLImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    HTMLImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    static HTMLImageView create(Context context) {
        final HTMLImageView view = new HTMLImageView(context);

        int height = context.getResources().getDimensionPixelSize(R.dimen.dp256);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(MATCH_PARENT, height);
        int dp16 = context.getResources().getDimensionPixelSize(R.dimen.dp16);
        layoutParams.topMargin = dp16;
        layoutParams.bottomMargin = dp16;
        view.setLayoutParams(layoutParams);

        return view;
    }

    private void init() {
        setClickable(true);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        setBackgroundResource(outValue.resourceId);
    }

    void setContent(HTMLImage image) {
        Glide.clear(this);

        Glide.with(getContext())
                .load(RemoteFileUrlUtils.toImageUrl(image.getImagePath()))
                .thumbnail(Glide.with(getContext())
                        .load(RemoteFileUrlUtils.toThumbUrl(image.getImagePath()))
                        .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                        .dontAnimate()
                        .fitCenter())
                .diskCacheStrategy(BuildConfig.DEBUG ? DiskCacheStrategy.NONE : DiskCacheStrategy.ALL)
                .dontAnimate()
                .fitCenter()
                .into(this);
    }
}
