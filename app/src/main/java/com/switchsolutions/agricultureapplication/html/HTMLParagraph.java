package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

public final class HTMLParagraph extends HTMLElement {

    public static final Parcelable.Creator<HTMLParagraph> CREATOR = new Creator<HTMLParagraph>() {
        @Override
        public HTMLParagraph createFromParcel(Parcel parcel) {
            return new HTMLParagraph(parcel);
        }

        @Override
        public HTMLParagraph[] newArray(int i) {
            return new HTMLParagraph[i];
        }
    };
    private final String text;

    HTMLParagraph(Element element, int position) {
        super(element, position);

        this.text = element.html();
    }

    HTMLParagraph(Parcel in) {
        super(in);

        this.text = in.readString();
    }

    static boolean isParagraph(Element element) {
        return element.tagName().toLowerCase().equals("p");
    }

    public String getText() {
        return text;
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeString(text);
    }
}
