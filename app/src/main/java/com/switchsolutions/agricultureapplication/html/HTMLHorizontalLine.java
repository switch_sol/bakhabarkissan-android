package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

public final class HTMLHorizontalLine extends HTMLElement {

    public static final Parcelable.Creator<HTMLHorizontalLine> CREATOR = new Creator<HTMLHorizontalLine>() {
        @Override
        public HTMLHorizontalLine createFromParcel(Parcel parcel) {
            return new HTMLHorizontalLine(parcel);
        }

        @Override
        public HTMLHorizontalLine[] newArray(int i) {
            return new HTMLHorizontalLine[i];
        }
    };

    HTMLHorizontalLine(Element element, int position) {
        super(element, position);
    }

    HTMLHorizontalLine(Parcel in) {
        super(in);
    }

    static boolean isHorizontalLine(Element element) {
        return element.tagName().toLowerCase().equals("hr");
    }
}
