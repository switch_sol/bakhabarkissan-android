package com.switchsolutions.agricultureapplication.html;

import android.os.Parcel;
import android.os.Parcelable;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

public final class HTMLTable extends HTMLElement {

    public static final Parcelable.Creator<HTMLTable> CREATOR = new Creator<HTMLTable>() {
        @Override
        public HTMLTable createFromParcel(Parcel parcel) {
            return new HTMLTable(parcel);
        }

        @Override
        public HTMLTable[] newArray(int i) {
            return new HTMLTable[i];
        }
    };
    private final StringListList rows;

    HTMLTable(Element element, int position) {
        super(element, position);

        this.rows = new StringListList();
        for (Element re : element.select("tr")) {
            List<String> r = new ArrayList<>();
            for (Element ce : re.select("td")) {
                r.add(ce.html());
            }
            rows.add(r);
        }
    }

    HTMLTable(Parcel in) {
        super(in);

        this.rows = in.readParcelable(StringListList.class.getClassLoader());
    }

    static boolean isTable(Element element) {
        return element.tagName().toLowerCase().equals("table");
    }

    public List<List<String>> getRows() {
        return rows;
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);

        parcel.writeParcelable(rows, i);
    }

    private static final class StringListList extends ArrayList<List<String>> implements Parcelable {

        public static final Creator<StringListList> CREATOR = new Creator<StringListList>() {
            @Override
            public StringListList createFromParcel(Parcel in) {
                return new StringListList(in);
            }

            @Override
            public StringListList[] newArray(int size) {
                return new StringListList[size];
            }
        };

        private StringListList() {
            super();
        }

        private StringListList(Parcel in) {
            int n = in.readInt();
            for (int i = 0; i < n; i++) {
                List<String> ls = new ArrayList<>();
                in.readStringList(ls);
                add(ls);
            }
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(size());
            for (List<String> s : this) {
                parcel.writeStringList(s);
            }
        }

    }

}
