package com.switchsolutions.agricultureapplication.html;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.switchsolutions.agricultureapplication.R;

final class HTMLParagraphViewHolder extends RecyclerView.ViewHolder {

    private final HTMLParagraphView paragraphView;

    private final int selectableItemBackgroundResId;

    HTMLParagraphViewHolder(View itemView) {
        super(itemView);

        paragraphView = (HTMLParagraphView) itemView;

        TypedValue outValue = new TypedValue();
        itemView.getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        selectableItemBackgroundResId = outValue.resourceId;
    }

    void setContent(HTMLParagraph paragraph) {
        paragraphView.setContent(paragraph);
    }

    void setFocused(boolean focused) {
        if (focused) {
            paragraphView.setBackgroundColor(ContextCompat.getColor(itemView.getContext(), R.color.md_blue_50));
        } else {
            paragraphView.setBackgroundResource(selectableItemBackgroundResId);
        }
    }
}
