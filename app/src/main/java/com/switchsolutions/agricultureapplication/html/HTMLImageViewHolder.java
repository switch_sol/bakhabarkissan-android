package com.switchsolutions.agricultureapplication.html;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class HTMLImageViewHolder extends RecyclerView.ViewHolder {

    private final HTMLImageView imageView;

    HTMLImageViewHolder(View itemView) {
        super(itemView);

        imageView = (HTMLImageView) itemView;
    }

    void setContent(HTMLImage image) {
        imageView.setContent(image);
    }
}
