package com.switchsolutions.agricultureapplication.html;

import android.support.v7.widget.RecyclerView;
import android.view.View;

final class HTMLHorizontalLineViewHolder extends RecyclerView.ViewHolder {

    HTMLHorizontalLineViewHolder(View itemView) {
        super(itemView);
    }

}
