package com.switchsolutions.agricultureapplication.storage;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;

public interface LruCache {

    long getLastModified(String key);

    boolean containsKey(String key);

    <T> T readJSON(String key, ObjectReader objectReader) throws IOException;

    void writeJSON(String key, ObjectWriter objectWriter, Object value) throws IOException;

    String readString(String key) throws IOException;

    void writeString(String key, String value) throws IOException;
}
