package com.switchsolutions.agricultureapplication.storage;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.Nullable;

import com.switchsolutions.agricultureapplication.preferences.Preferences;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.inject.Inject;

class StorageImpl implements Storage {

    private static final String DIRNAME_ROOT = "BKK";
    private static final String DIRNAME_AUDIOS = "audios";
    private static final String DIRNAME_PHOTOS = "photos";
    private static final String DIRNAME_ANALYSES = "analyses";

    private final Context context;
    private final Preferences preferences;

    @Inject
    StorageImpl(Context context, Preferences preferences) {
        this.context = context;
        this.preferences = preferences;
    }

    @Override
    public File getCacheDir() {
        return context.getApplicationContext().getCacheDir();
    }

    @Override
    public File getCacheFile(String filename) {
        return new File(getCacheDir(), filename);
    }

    @Override
    public LruCache getLruCache(String directoryName, long maxSize) throws IOException {
        return new LruCacheImpl(new File(context.getFilesDir(), directoryName), maxSize);
    }

    @Nullable
    @Override
    public File getAudioFile(String filename) {
        File dirRoot = new File(Environment.getExternalStorageDirectory(), DIRNAME_ROOT);
        File dirAudios = new File(dirRoot, DIRNAME_AUDIOS);
        if (!dirAudios.exists() && !dirAudios.mkdirs()) {
            return null;
        }
        return new File(dirAudios, filename);
    }

    @Nullable
    @Override
    public File putAudioFile(File srcFile, String dstFilename) throws IOException {
        File dirRoot = new File(Environment.getExternalStorageDirectory(), DIRNAME_ROOT);
        File dirAudios = new File(dirRoot, DIRNAME_AUDIOS);
        if (!dirAudios.exists() && !dirAudios.mkdirs()) {
            return null;
        }

        File dstFile = new File(dirAudios, dstFilename);

        copyFile(srcFile, dstFile);

        return dstFile;
    }

    @Nullable
    @Override
    public File getImageFile(String filename) {
        File dirRoot = new File(Environment.getExternalStorageDirectory(), DIRNAME_ROOT);
        File dirPhotos = new File(dirRoot, DIRNAME_PHOTOS);
        if (!dirPhotos.exists() && !dirPhotos.mkdirs()) {
            return null;
        }
        return new File(dirPhotos, filename);
    }

    @Nullable
    @Override
    public File putImageFile(File srcFile, String dstFilename) throws IOException {
        File dirRoot = new File(Environment.getExternalStorageDirectory(), DIRNAME_ROOT);
        File dirPhotos = new File(dirRoot, DIRNAME_PHOTOS);
        if (!dirPhotos.exists() && !dirPhotos.mkdirs()) {
            return null;
        }

        File dstFile = new File(dirPhotos, dstFilename);

        copyFile(srcFile, dstFile);

        return dstFile;
    }

    @Nullable
    @Override
    public File getPhotoAnalysesDir() {
        File dir = new File(new File(Environment.getExternalStorageDirectory(), DIRNAME_ROOT), DIRNAME_ANALYSES);
        if (!dir.exists() && !dir.mkdirs()) {
            return null;
        }

        return dir;
    }

    @Override
    public void moveFile(File src, File dst) throws IOException {
        src.renameTo(dst);
    }

    @Override
    public void copyFile(File src, File dst) throws IOException {
        FileInputStream i = null;
        FileOutputStream o = null;
        try {
            i = new FileInputStream(src);
            o = new FileOutputStream(dst);

            FileChannel ic = i.getChannel();
            FileChannel oc = o.getChannel();

            ic.transferTo(0, ic.size(), oc);
        } finally {
            if (i != null) {
                try {
                    i.close();
                } catch (IOException ignored) {

                }
            }

            if (o != null) {
                try {
                    o.close();
                } catch (IOException ignored) {

                }
            }
        }
    }

}
