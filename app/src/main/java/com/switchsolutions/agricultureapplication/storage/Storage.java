package com.switchsolutions.agricultureapplication.storage;

import android.support.annotation.Nullable;

import java.io.File;
import java.io.IOException;

public interface Storage {

    File getCacheDir();

    File getCacheFile(String filename);

    LruCache getLruCache(String directoryName, long maxSize) throws IOException;

    @Nullable
    File getAudioFile(String filename);

    @Nullable
    File putAudioFile(File srcFile, String dstFilename) throws IOException;

    @Nullable
    File getImageFile(String filename);

    @Nullable
    File putImageFile(File srcFile, String dstFilename) throws IOException;

    @Nullable
    File getPhotoAnalysesDir();

    void moveFile(File src, File dst) throws IOException;

    void copyFile(File src, File dst) throws IOException;
}
