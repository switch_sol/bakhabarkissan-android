package com.switchsolutions.agricultureapplication.storage;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.jakewharton.disklrucache.DiskLruCache;
import com.switchsolutions.agricultureapplication.BuildConfig;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class LruCacheImpl implements LruCache {

    private final DiskLruCache cache;

    LruCacheImpl(File directory, long maxSize) throws IOException {
        this.cache = DiskLruCache.open(directory, BuildConfig.VERSION_CODE, 1, maxSize);
    }

    @Override
    public long getLastModified(String key) {
        return cache.getDirectory().lastModified();
    }

    @Override
    public boolean containsKey(String key) {
        DiskLruCache.Snapshot snapshot = null;
        try {
            snapshot = cache.get(key);
            return snapshot != null;
        } catch (IOException e) {
            return false;
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }
    }

    @Override
    public <T> T readJSON(String key, ObjectReader objectReader) throws IOException {
        DiskLruCache.Snapshot snapshot = cache.get(key);

        InputStream inputStream = snapshot.getInputStream(0);
        try {
            return objectReader.readValue(inputStream);
        } finally {
            snapshot.close();
        }
    }

    @Override
    public void writeJSON(String key, ObjectWriter objectWriter, Object value) throws IOException {
        DiskLruCache.Editor editor = cache.edit(key);

        OutputStream outputStream;
        try {
            outputStream = editor.newOutputStream(0);
        } catch (IOException e) {
            try {
                editor.abort();
            } catch (IOException e1) {

            }

            throw e;
        }

        try {
            objectWriter.writeValue(outputStream, value);
        } catch (IOException e) {
            try {
                editor.abort();
            } catch (IOException e1) {

            }

            throw e;
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {

            }
        }

        editor.commit();
    }

    @Override
    public String readString(String key) throws IOException {
        DiskLruCache.Snapshot snapshot = cache.get(key);

        try {
            return snapshot.getString(0);
        } finally {
            snapshot.close();
        }
    }

    @Override
    public void writeString(String key, String value) throws IOException {
        DiskLruCache.Editor editor = cache.edit(key);

        try {
            editor.set(0, value);
        } catch (IOException e) {
            try {
                editor.abort();
            } catch (IOException e1) {

            }
            throw e;
        }

        editor.commit();
    }
}
