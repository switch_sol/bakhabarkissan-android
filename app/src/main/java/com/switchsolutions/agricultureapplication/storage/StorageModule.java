package com.switchsolutions.agricultureapplication.storage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    public Storage providesStorageModule(StorageImpl storage) {
        return storage;
    }

}
