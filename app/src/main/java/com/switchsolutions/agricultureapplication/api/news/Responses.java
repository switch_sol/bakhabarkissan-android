package com.switchsolutions.agricultureapplication.api.news;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.NewsArticle;

import java.util.List;

final class Responses {
    static final class NewsList {
        @JsonProperty("result")
        List<NewsArticle> result;
    }
}
