package com.switchsolutions.agricultureapplication.api.news;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    interface API {

        @GET("v1/news")
        Observable<Responses.NewsList> getNews(
                @Query("lang") String lang,
                @Query("limit") int limit,
                @Query("offset") int offset
        );
    }

    interface Content {

        @GET("pages/{lang}/news/{news-id}.html")
        Observable<ResponseBody> getNewsPage(
                @Path("lang") String lang,
                @Path("news-id") long newsId
        );
    }
}
