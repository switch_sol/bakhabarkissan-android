package com.switchsolutions.agricultureapplication.api.remote_config;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RemoteConfigClientModule {

    @Provides
    @Singleton
    public RemoteConfigClient providesRemoteConfigClient(RemoteConfigClientImpl remoteConfigClient) {
        return remoteConfigClient;
    }

}
