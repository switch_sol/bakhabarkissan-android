package com.switchsolutions.agricultureapplication.api.videos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;

import java.util.List;

final class Responses {

    static final class CategoriesParentList {

        @JsonProperty("result")
        public List<VideoCategoryParent> result;
    }

    static final class CategoriesChildList {

        @JsonProperty("result")
        public List<VideoCategoryChild> result;
    }

    static final class VideosList {

        @JsonProperty("result")
        public List<Video> result;
    }

}
