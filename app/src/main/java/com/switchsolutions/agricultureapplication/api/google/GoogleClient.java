package com.switchsolutions.agricultureapplication.api.google;

import com.switchsolutions.agricultureapplication.utils.Coordinates;

import rx.Observable;

public interface GoogleClient {

    Observable<Coordinates> getLocation();
}
