package com.switchsolutions.agricultureapplication.api.photo_analysis;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.Storage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import timber.log.Timber;

final class PhotoAnalysisClientImpl implements PhotoAnalysisClient {

    private static final ObjectReader reader;
    private static final ObjectWriter writer;
    static {
        ObjectMapper mapper = new ObjectMapper();
        reader = mapper.readerFor(PhotoAnalysis.class);
        writer = mapper.writerFor(PhotoAnalysis.class);
    }

    private final Endpoints.API apiEndpoints;
    private final Endpoints.Content contentEndpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;
    private final Storage storage;

    @Inject
    PhotoAnalysisClientImpl(Endpoints.API apiEndpoints, Endpoints.Content contentEndpoints, Preferences preferences, AccountsClient accountsClient, NetworkUtility networkUtility, Storage storage) {
        this.apiEndpoints = apiEndpoints;
        this.contentEndpoints = contentEndpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;
        this.storage = storage;
    }

    @Override
    public Observable<PhotoAnalysis> upload(final @NonNull String text, final @Nullable File audio, final @Nullable File... images) {
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("text", text);

        if (audio != null) {
            builder.addFormDataPart("audio", audio.getName(), RequestBody.create(MediaType.parse("video/3gpp"), audio));
        }

        if (images != null) {
            for (File image : images) {
                builder.addFormDataPart("images", image.getName(), RequestBody.create(MediaType.parse("image/jpeg"), image));
            }
        }

        return apiEndpoints.createPhotoAnalysisRequest(builder.build())
                .compose(new TokensRefreshTransformer<Responses.PhotoAnalysis>(accountsClient))
                .map(new Func1<Responses.PhotoAnalysis, PhotoAnalysis>() {
                    @Override
                    public PhotoAnalysis call(Responses.PhotoAnalysis response) {
                        return response.result;
                    }
                })
                .doOnNext(new Action1<PhotoAnalysis>() {
                    @Override
                    public void call(PhotoAnalysis photoAnalysis) {
                        String filename = String.format(Locale.US, "%d.json", photoAnalysis.getId());
                        File file = new File(storage.getPhotoAnalysesDir(), filename);
                        try {
                            writer.writeValue(file, photoAnalysis);

                            if (audio != null) {
                                File audioFile = storage.getAudioFile(photoAnalysis.getAudio());
                                storage.moveFile(audio, audioFile);
                            }

                            if (images != null) {
                                int a = images.length;
                                int b = photoAnalysis.getPhotos() != null ? photoAnalysis.getPhotos().length : 0;
                                int n = Math.min(a, b);

                                for (int i = 0; i < n; i++) {
                                    File dstFile = storage.getImageFile(photoAnalysis.getPhotos()[i]);
                                    storage.moveFile(images[i], dstFile);
                                }
                            }
                        } catch (IOException e) {
                            Timber.e(e, "Unable to save photo analysis response.");

                            throw new RuntimeException(e);
                        }
                    }
                });
    }

    @Override
    public Observable<PhotoAnalysis> update(long id) {
        return apiEndpoints
                .getPhotoAnalysis(id)
                .compose(new TokensRefreshTransformer<Responses.PhotoAnalysis>(accountsClient))
                .map(new Func1<Responses.PhotoAnalysis, PhotoAnalysis>() {
                    @Override
                    public PhotoAnalysis call(Responses.PhotoAnalysis response) {
                        return response.result;
                    }
                })
                .doOnNext(new Action1<PhotoAnalysis>() {
                    @Override
                    public void call(PhotoAnalysis photoAnalysis) {
                        String filename = String.format(Locale.US, "%d.json", photoAnalysis.getId());
                        File file = new File(storage.getPhotoAnalysesDir(), filename);
                        try {
                            writer.writeValue(file, photoAnalysis);
                        } catch (IOException e) {
                            Timber.e(e, "Unable to update photo analysis status.");

                            throw new RuntimeException(e);
                        }
                    }
                });
    }

    @Override
    public Observable<List<PhotoAnalysis>> getLocalList() {
        return Observable
                .fromCallable(new Callable<List<File>>() {
                    @Override
                    public List<File> call() throws Exception {
                        File dir = storage.getPhotoAnalysesDir();
                        List<File> files;
                        if (dir != null) {
                            files = Arrays.asList(dir.listFiles(new FilenameFilter() {
                                @Override
                                public boolean accept(File dir, String name) {
                                    return name.endsWith("json");
                                }
                            }));
                        } else {
                            files = new ArrayList<>();
                        }

                        return files;
                    }
                })
                .map(new Func1<List<File>, List<PhotoAnalysis>>() {
                    @Override
                    public List<PhotoAnalysis> call(List<File> files) {
                        List<PhotoAnalysis> pas = new ArrayList<>(files.size());
                        try {
                            for (File f : files) {
                                PhotoAnalysis pa = reader.readValue(f);
                                pas.add(pa);
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        return pas;
                    }
                })
                .map(new Func1<List<PhotoAnalysis>, List<PhotoAnalysis>>() {
                    @Override
                    public List<PhotoAnalysis> call(List<PhotoAnalysis> items) {
                        Collections.sort(items, new Comparator<PhotoAnalysis>() {
                            @Override
                            public int compare(PhotoAnalysis o1, PhotoAnalysis o2) {
                                return Double.valueOf(o1.getDatetime()).compareTo(o2.getDatetime());
                            }
                        });

                        Collections.reverse(items);

                        return items;
                    }
                });
    }

    @Override
    public Observable<ArrayList<HTMLElement>> getResponse(long id) {
        String filename = String.format(Locale.US, "%d_response.html", id);
        File file = new File(storage.getPhotoAnalysesDir(), filename);
        if (file.exists()) {
            return getCachedResponse(id);
        } else {
            return getFreshResponse(id);
        }
    }

    Observable<ArrayList<HTMLElement>> getFreshResponse(final long id) {
        return contentEndpoints
                .getPhotoAnalysisResponsePage(id)
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        try {
                            return responseBody.string();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            responseBody.close();
                        }
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        String filename = String.format(Locale.US, "%d_response.html", id);
                        File file = new File(storage.getPhotoAnalysesDir(), filename);

                        FileOutputStream outputStream = null;
                        try {
                            outputStream = new FileOutputStream(file);
                            outputStream.write(s.getBytes(Charset.forName("UTF-8")));
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            if (outputStream != null) {
                                try {
                                    outputStream.close();
                                } catch (IOException ignored) {

                                }
                            }
                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String response) {
                        return Jsoup.parse(response, "");
                    }
                })
                .map(new Func1<Document, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(Document document) {
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                });
    }

    Observable<ArrayList<HTMLElement>> getCachedResponse(final long id) {
        return Observable
                .fromCallable(new Callable<ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call() throws Exception {
                        String filename = String.format(Locale.US, "%d_response.html", id);
                        File file = new File(storage.getPhotoAnalysesDir(), filename);

                        Document document = Jsoup.parse(file, null);
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                });
    }
}
