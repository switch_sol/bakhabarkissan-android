package com.switchsolutions.agricultureapplication.api.forecast;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
class Response {

    @JsonProperty("list")
    List<Forecast> forecasts;

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Forecast {

        @JsonProperty("dt")
        long dt;

        @JsonProperty("main")
        Main main;

        @JsonProperty("weather")
        List<Weather> weather;

        @JsonProperty("clouds")
        Clouds clouds;

        @JsonProperty("wind")
        Wind wind = new Wind();

        @JsonProperty("rain")
        Rain rain = new Rain();
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Main {

        @JsonProperty("temp")
        float temp;

        @JsonProperty("humidity")
        int humidity;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Weather {

        @JsonProperty("id")
        int id;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Clouds {

        @JsonProperty("all")
        int all;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Wind {

        @JsonProperty("speed")
        float speed = 0f;

        @JsonProperty("deg")
        float deg = 0f;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Rain {

        @JsonProperty("3h")
        float amount = 0f;
    }
}
