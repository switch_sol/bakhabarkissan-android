package com.switchsolutions.agricultureapplication.api.google;

public final class GoogleClientConnectionSuspendedException extends RuntimeException {

    private final int code;

    GoogleClientConnectionSuspendedException(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
