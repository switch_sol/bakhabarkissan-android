package com.switchsolutions.agricultureapplication.api.location;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocationClientModule {

    @Provides
    @Singleton
    public LocationClient providesLocationClient(LocationClientImpl locationClient) {
        return locationClient;
    }
}
