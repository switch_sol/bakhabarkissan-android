package com.switchsolutions.agricultureapplication.api.videos;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    @GET("v1/videos/categories")
    Observable<Responses.CategoriesParentList> getCategories(
            @Query("lang") String lang,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("v1/videos/categories/{parent-category-id}")
    Observable<Responses.CategoriesChildList> getCategories(
            @Path("parent-category-id") long parentCategoryId,
            @Query("lang") String lang,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("v1/videos/categories/{parent-category-id}/{child-category-id}/videos")
    Observable<Responses.VideosList> getVideos(
            @Path("parent-category-id") long parentCategoryId,
            @Path("child-category-id") long childCategoryId,
            @Query("lang") String lang,
            @Query("limit") int limit,
            @Query("offset") int offset
    );
}
