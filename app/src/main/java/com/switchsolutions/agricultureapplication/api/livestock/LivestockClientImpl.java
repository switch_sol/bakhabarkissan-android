package com.switchsolutions.agricultureapplication.api.livestock;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.Livestock;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

final class LivestockClientImpl implements LivestockClient {

    private static final long CACHE_SIZE = 1024 * 1024; // 1MB

    private static final String CACHE_DIRNAME = "cache_livestock";

    private final Endpoints.API apiEndpoints;
    private final Endpoints.Content contentEndpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    private final ObjectReader livestockListReader;
    private final ObjectWriter livestockListWriter;

    @Nullable
    private final LruCache cache;

    @Inject
    LivestockClientImpl(Endpoints.API apiEndpoints, Endpoints.Content contentEndpoints, Preferences preferences, AccountsClient accountsClient, NetworkUtility networkUtility, Storage storage) {
        this.apiEndpoints = apiEndpoints;
        this.contentEndpoints = contentEndpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        ObjectMapper objectMapper = new ObjectMapper();
        livestockListReader = objectMapper.readerFor(Responses.LivestockList.class);
        livestockListWriter = objectMapper.writerFor(Responses.LivestockList.class);

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    @Override
    public Observable<ArrayList<Livestock>> getLivestock(int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getLivestockFresh(limit, offset, preferences.getLanguageNormalized());
        } else {
            return getLivestockCached(limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getLivestockFresh(limit, offset, preferences.getLanguageNormalized()))
                    .first();
        }
    }

    @Override
    public Observable<ArrayList<Livestock>> getLivestockCached(final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<Livestock>>() {
                    @Override
                    public ArrayList<Livestock> call() throws Exception {
                        String key = getCacheKeyLivestock(lang, limit, offset);

                        Responses.LivestockList response;
                        try {
                            response = cache.readJSON(key, livestockListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<Livestock>>empty());
    }

    @Override
    public Observable<ArrayList<Livestock>> getLivestockFresh(final int limit, final int offset, final String lang) {
        return apiEndpoints
                .getLivestock(lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.LivestockList>(accountsClient))
                .doOnNext(new Action1<Responses.LivestockList>() {
                    @Override
                    public void call(Responses.LivestockList response) {
                        if (cache == null) return;

                        String key = getCacheKeyLivestock(lang, limit, offset);
                        try {
                            cache.writeJSON(key, livestockListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.LivestockList, ArrayList<Livestock>>() {
                    @Override
                    public ArrayList<Livestock> call(Responses.LivestockList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<HTMLElement>> getLivestockPage(long livestockId) {
        if (networkUtility.isInternetConnected()) {
            return getLivestockPageFresh(livestockId, preferences.getLanguageNormalized());
        } else {
            return getLivestockPageCached(livestockId, preferences.getLanguageNormalized())
                    .concatWith(getLivestockPageFresh(livestockId, preferences.getLanguageNormalized()))
                    .first();
        }
    }

    @Override
    public Observable<ArrayList<HTMLElement>> getLivestockPageCached(final long livestockId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<Document>() {
                    @Override
                    public Document call() throws Exception {
                        String key = getCacheKeyLivestockPage(lang, livestockId);
                        String response;
                        try {
                            response = cache.readString(key);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        return Jsoup.parse(response, "");
                    }
                })
                .map(new Func1<Document, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(Document document) {
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<HTMLElement>>empty());
    }

    @Override
    public Observable<ArrayList<HTMLElement>> getLivestockPageFresh(final long livestockId, final String lang) {
        return contentEndpoints
                .getLivestockPage(lang, livestockId)
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        try {
                            return responseBody.string();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            responseBody.close();
                        }
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String response) {
                        if (cache == null) return;

                        String key = getCacheKeyLivestockPage(lang, livestockId);
                        try {
                            cache.writeString(key, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String response) {
                        return Jsoup.parse(response, "");
                    }
                })
                .map(new Func1<Document, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(Document document) {
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                });
    }

    String getCacheKeyLivestock(String lang, int limit, int offset) {
        return String.format(Locale.US, "%s_livestock_list_%d_%d", lang, limit, offset);
    }

    String getCacheKeyLivestockPage(String lang, long livestockId) {
        return String.format(Locale.US, "%s_livestock_page_%d", lang, livestockId);
    }
}
