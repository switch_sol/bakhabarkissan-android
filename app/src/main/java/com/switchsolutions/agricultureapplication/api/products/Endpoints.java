package com.switchsolutions.agricultureapplication.api.products;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    interface API {

        @GET("v1/products/categories")
        Observable<Responses.CategoriesList> getCategories(
                @Query("lang") String lang
        );

        @GET("v1/products/categories/{category-id}/products")
        Observable<Responses.ProductsList> getProducts(
                @Path("category-id") long categoryId,
                @Query("lang") String lang,
                @Query("limit") int limit,
                @Query("offset") int offset
        );

        @GET("v1/products/categories/{category-id}/dealers/nearby")
        Observable<Responses.DealersList> getProductDealers(
                @Path("category-id") long categoryId,
                @Query("lat") double lat,
                @Query("lon") double lon,
                @Query("lang") String lang,
                @Query("limit") int limit,
                @Query("offset") int offset
        );

    }

    interface Content {

        @GET("pages/{lang}/products/{product-id}.html")
        Observable<ResponseBody> getProductPage(
                @Path("lang") String lang,
                @Path("product-id") long productId
        );

    }

}
