package com.switchsolutions.agricultureapplication.api.photo_analysis;

import com.switchsolutions.agricultureapplication.network.NetworkModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class PhotoAnalysisClientModule {

    @Provides
    @Singleton
    Endpoints.API providesApiEndpoints(@Named(NetworkModule.PROVIDER_AGRICULTURE_API) Retrofit retrofit) {
        return retrofit.create(Endpoints.API.class);
    }

    @Provides
    @Singleton
    Endpoints.Content providesContentEndpoints(@Named(NetworkModule.PROVIDER_AGRICULTURE_CONTENT) Retrofit retrofit) {
        return retrofit.create(Endpoints.Content.class);
    }

    @Provides
    @Singleton
    PhotoAnalysisClient providesClient(PhotoAnalysisClientImpl photoAnalysisClient) {
        return photoAnalysisClient;
    }

}
