package com.switchsolutions.agricultureapplication.api.cloud_messaging;

public interface CloudMessagingClient {

    String TOPIC_GLOBAL = "global";
    String TOPIC_NEWS = "news";
    String TOPIC_APPUPDATE = "appupdate";
    String TOPIC_PHOTO_ANALYSIS = "photo_analysis";

    void subscribeToGlobals();

    void subscribeToGlobalTopic();

    void subscribeToNewsTopic();

    void subscribeToAppUpdateTopic();
}
