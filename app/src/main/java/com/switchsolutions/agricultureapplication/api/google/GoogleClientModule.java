package com.switchsolutions.agricultureapplication.api.google;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GoogleClientModule {

    @Provides
    @Singleton
    public GoogleClient providesGoogleClient(GoogleClientImpl googleClient) {
        return googleClient;
    }

}
