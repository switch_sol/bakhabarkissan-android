package com.switchsolutions.agricultureapplication.api.accounts;

import com.switchsolutions.agricultureapplication.models.JWTTokens;

import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Func1;

public final class TokensRefreshTransformer<T> implements Observable.Transformer<T, T> {

    private final AccountsClient accountsClient;

    public TokensRefreshTransformer(AccountsClient accountsClient) {
        this.accountsClient = accountsClient;
    }

    @Override
    public Observable<T> call(final Observable<T> observable) {
        return observable
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends T>>() {
                    @Override
                    public Observable<? extends T> call(Throwable throwable) {
                        if (throwable instanceof HttpException) {
                            return accountsClient
                                    .refreshTokens()
                                    .flatMap(new Func1<JWTTokens, Observable<T>>() {
                                        @Override
                                        public Observable<T> call(JWTTokens jwtTokens) {
                                            return observable;
                                        }
                                    });
                        }

                        return Observable.error(throwable);
                    }
                });
    }

}
