package com.switchsolutions.agricultureapplication.api.remote_config;

public interface RemoteConfigClient {

    String KEY_BANNER_ADS = "banner_ads";

    boolean hasBannerAds();

    String[] getBannerAds();

}
