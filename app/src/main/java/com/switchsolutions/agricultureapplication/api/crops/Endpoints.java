package com.switchsolutions.agricultureapplication.api.crops;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    interface API {

        @GET("v1/crops/categories")
        Observable<Responses.CategoriesList> getCategories(
                @Query("lang") String lang
        );

        @GET("v1/crops/categories/{crop-category-id}")
        Observable<Responses.CropsList> getCrops(
                @Path("crop-category-id") long cropCategoryId,
                @Query("lang") String lang
        );

    }

    interface Content {

        @GET("pages/{lang}/crops/{crop-id}.html")
        Observable<ResponseBody> getCropPage(
                @Path("lang") String lang,
                @Path("crop-id") long cropId
        );

    }

}
