package com.switchsolutions.agricultureapplication.api.analytics;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.switchsolutions.agricultureapplication.Constants;

import javax.inject.Inject;

class AnalyticsClientImpl implements AnalyticsClient {

    private final Tracker tracker;

    @Inject
    AnalyticsClientImpl(Context context) {
        this.tracker = GoogleAnalytics.getInstance(context).newTracker(Constants.ANALYTICS_TRACKER);
        FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void sendScreenEvent(String screenName) {
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void sendNewsSelectedEvent(long articleId) {
        sendEvent(CATEGORY_NEWS, ACTION_SELECTED_NEWS_ARTICLE, articleId);
    }

    @Override
    public void sendCropCategorySelectedEvent(long cropCategoryId) {
        sendEvent(CATEGORY_CROPS, ACTION_SELECTED_CROP_CATEGORY, cropCategoryId);
    }

    @Override
    public void sendCropSelectedEvent(long cropId) {
        sendEvent(CATEGORY_CROPS, ACTION_SELECTED_CROP, cropId);
    }

    @Override
    public void sendLivestockSelectedEvent(long livestockId) {
        sendEvent(CATEGORY_LIVESTOCK, ACTION_SELECTED_LIVESTOCK, livestockId);
    }

    @Override
    public void sendProductCategorySelectedEvent(long productCategoryId) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_SELECTED_PRODUCT_CATEGORY, productCategoryId);
    }

    @Override
    public void sendProductSelectedEvent(long productId) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_SELECTED_PRODUCT, productId);
    }

    @Override
    public void sendCallProductCompanyEvent(String number) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_CALL_PRODUCT_COMPANY, number);
    }

    @Override
    public void sendProductDealersCategorySelectedEvent(long productCategoryId) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_SELECTED_PRODUCT_DEALERS_CATEGORY, productCategoryId);
    }

    @Override
    public void sendProductDealerSelectedEvent(long dealerId) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_SELECTED_PRODUCT_DEALER, dealerId);
    }

    @Override
    public void sendCallProductDealerEvent(String number) {
        sendEvent(CATEGORY_PRODUCTS, ACTION_CALL_PRODUCT_DEALER, number);
    }

    @Override
    public void sendMarketRatesSelectedEvent(long marketId) {
        sendEvent(CATEGORY_MARKETS, ACTION_SELECTED_MARKET_RATES, marketId);
    }

    @Override
    public void sendVideosParentCategorySelectedEvent(long parentCategoryId) {
        sendEvent(CATEGORY_VIDEOS, ACTION_SELECTED_VIDEO_CATEGORIES_PARENT, parentCategoryId);
    }

    @Override
    public void sendVideosChildCategorySelectedEvent(long childCategoryId) {
        sendEvent(CATEGORY_VIDEOS, ACTION_SELECTED_VIDEO_CATEGORIES_CHILD, childCategoryId);
    }

    @Override
    public void sendVideoSelectedEvent(long videoId) {
        sendEvent(CATEGORY_VIDEOS, ACTION_SELECTED_VIDEO, videoId);
    }

    @Override
    public void sendContactUsSelectedEvent() {
        sendEvent(CATEGORY_MENU, ACTION_SELECTED_CONTACT_US);
    }

    @Override
    public void sendAboutUsSelectedEvent() {
        sendEvent(CATEGORY_MENU, ACTION_SELECTED_ABOUT_US);
    }

    @Override
    public void sendLanguageSelectedEvent(String language) {
        sendEvent(CATEGORY_PREFERENCES, ACTION_SELECTED_LANGUAGE, language);
    }

    @Override
    public void sendViewedBannerAdvertisementEvent(String image) {
        sendEvent(CATEGORY_ADVERTISEMENTS, ACTION_VIEWED_BANNER_ADVERTISEMENT, image);
    }

    @Override
    public void sendViewedVideoAdvertisementEvent(String video) {
        sendEvent(CATEGORY_ADVERTISEMENTS, ACTION_VIEWED_VIDEO_ADVERTISEMENT, video);
    }

    @Override
    public void sendReceivedGlobalNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_RECEIVED, LABEL_NOTIFICATION_GLOBAL);
    }

    @Override
    public void sendClickedGlobalNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_GLOBAL);
    }

    @Override
    public void sendTimeToClickGlobalNotification(long milliseconds) {
        sendTimingEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_GLOBAL, milliseconds);
    }

    @Override
    public void sendReceivedNewsNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_RECEIVED, LABEL_NOTIFICATION_NEWS);
    }

    @Override
    public void sendClickedNewsNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_NEWS);
    }

    @Override
    public void sendTimeToClickNewsNotification(long milliseconds) {
        sendTimingEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_NEWS, milliseconds);
    }

    @Override
    public void sendReceivedPhotoAnalysisNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_RECEIVED, LABEL_NOTIFICATION_PHOTO_ANALYSIS);
    }

    @Override
    public void sendClickedPhotoAnalysisNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_PHOTO_ANALYSIS);
    }

    @Override
    public void sendTimeToClickPhotoAnalysisNotification(long milliseconds) {
        sendTimingEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_PHOTO_ANALYSIS, milliseconds);
    }

    @Override
    public void sendReceivedAppUpdateNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_RECEIVED, LABEL_NOTIFICATION_APP_UPDATE);
    }

    @Override
    public void sendClickedAppUpdateNotification() {
        sendEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_APP_UPDATE);
    }

    @Override
    public void sendTimeToClickAppUpdateNotification(long milliseconds) {
        sendTimingEvent(CATEGORY_NOTIFICATION, ACTION_NOTIFICATION_CLICKED, LABEL_NOTIFICATION_APP_UPDATE, milliseconds);
    }

    private void sendEvent(String category, String action) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .build());
    }

    private void sendEvent(String category, String action, long value) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setValue(value)
                .build());
    }

    private void sendEvent(String category, String action, String label) {
        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    private void sendTimingEvent(String category, String action, String label, long milliseconds) {
        tracker.send(new HitBuilders.TimingBuilder()
                .setCategory(category)
                .setVariable(action)
                .setLabel(label)
                .setValue(milliseconds)
                .build());
    }
}
