package com.switchsolutions.agricultureapplication.api.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.content.ContextCompat;

import com.switchsolutions.agricultureapplication.api.google.GoogleClient;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.utils.Coordinates;

import java.util.NoSuchElementException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

class LocationClientImpl implements LocationClient {

    private final Context context;
    private final GoogleClient googleClient;
    private final Preferences preferences;

    @Inject
    LocationClientImpl(Context context, GoogleClient googleClient, Preferences preferences) {
        this.context = context;
        this.googleClient = googleClient;
        this.preferences = preferences;
    }

    @Override
    public Observable<Coordinates> getLocation() {
        try {
            return Observable
                    .fromCallable(new Callable<Coordinates>() {
                        @Override
                        public Coordinates call() {
                            return preferences.getLastLocation();
                        }
                    })
                    .filter(new Func1<Coordinates, Boolean>() {
                        @Override
                        public Boolean call(Coordinates coordinates) {
                            if (coordinates != null) {
                                long oldMinutes = TimeUnit.MILLISECONDS.toMinutes(coordinates.getDatetime());
                                long newMinutes = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis());
                                if (newMinutes - oldMinutes <= 30) {
                                    return true;
                                }
                            }
                            return false;
                        }
                    })
                    .first()
                    .onErrorResumeNext(new Func1<Throwable, Observable<? extends Coordinates>>() {
                        @Override
                        public Observable<? extends Coordinates> call(Throwable throwable) {
                            if (throwable instanceof NoSuchElementException) {
                                return googleClient.getLocation();
                            } else {
                                return Observable.error(throwable);
                            }
                        }
                    })
                    .doOnNext(new Action1<Coordinates>() {
                        @Override
                        public void call(Coordinates coordinates) {
                            preferences.setLastLocation(coordinates);
                        }
                    })
                    .take(1)
                    .onErrorResumeNext(new Func1<Throwable, Observable<? extends Coordinates>>() {
                        @Override
                        public Observable<? extends Coordinates> call(Throwable throwable) {
                            Coordinates coordinates = preferences.getLastLocation();
                            if (coordinates != null) {
                                long oldHours = TimeUnit.MILLISECONDS.toHours(coordinates.getDatetime());
                                long newHours = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis());
                                if (newHours - oldHours <= 3) {
                                    return Observable.just(coordinates);
                                }
                            }

                            if (!hasPermission()) {
                                return Observable.error(new SecurityException("Location provider requires ACCESS_FINE_LOCATION permission.", throwable));
                            }

                            if (!hasAccess()) {
                                return Observable.error(new LocationProviderDisabledException(throwable));
                            }

                            return Observable.error(throwable);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean hasAccess() {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gpsEnabled;
        try {
            gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            gpsEnabled = false;
        }

        boolean networkEnabled;
        try {
            networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            networkEnabled = false;
        }

        return gpsEnabled || networkEnabled;
    }

    @Override
    public boolean hasPermission() {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
}
