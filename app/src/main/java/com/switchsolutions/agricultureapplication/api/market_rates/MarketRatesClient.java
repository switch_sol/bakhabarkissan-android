package com.switchsolutions.agricultureapplication.api.market_rates;

import com.switchsolutions.agricultureapplication.models.Market;
import com.switchsolutions.agricultureapplication.models.MarketRate;

import java.util.ArrayList;

import rx.Observable;

public interface MarketRatesClient {

    Observable<ArrayList<Market>> getMarketsNearby(double lat, double lon, int limit, int offset);

    Observable<ArrayList<Market>> getMarketsNearbyCached(int limit, int offset, String lang);

    Observable<ArrayList<Market>> getMarketsNearbyFresh(double lat, double lon, int limit, int offset, String lang);

    Observable<ArrayList<MarketRate>> getMarketRates(long marketId);

    Observable<ArrayList<MarketRate>> getMarketRatesCached(long marketId, String lang);

    Observable<ArrayList<MarketRate>> getMarketRatesFresh(long marketId, String lang);
}
