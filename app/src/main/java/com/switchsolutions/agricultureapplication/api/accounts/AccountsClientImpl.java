package com.switchsolutions.agricultureapplication.api.accounts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import com.switchsolutions.agricultureapplication.models.JWTTokens;
import com.switchsolutions.agricultureapplication.preferences.Preferences;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

class AccountsClientImpl implements AccountsClient {

    private final Endpoints endpoints;
    private final Preferences preferences;
    private final Context context;

    @Inject
    AccountsClientImpl(Endpoints endpoints, Preferences preferences, Context context) {
        this.endpoints = endpoints;
        this.preferences = preferences;
        this.context = context;
    }

//    @Override
//    public Observable<Void> createAccount(String name, String number) {
//        return endpoints
//                .createAccount(getSecureID(), name, number);
//    }

    @Override
    public Observable<JWTTokens> createAccountDemo(String name, String number) {
        return endpoints
                .createAccountDemo(getSecureID(), name, number)
                .map(new Func1<Responses.Tokens, JWTTokens>() {
                    @Override
                    public JWTTokens call(Responses.Tokens response) {
                        return response.result;
                    }
                })
                .doOnNext(new Action1<JWTTokens>() {
                    @Override
                    public void call(JWTTokens jwtTokens) {
                        preferences.setRequestToken(jwtTokens.getRequestJwt());
                        preferences.setRefreshToken(jwtTokens.getRefreshJwt());
                    }
                });
    }

    @Override
    public Observable<JWTTokens> verifyAccount(String code) {
        RequestBody codeBody = RequestBody.create(MediaType.parse("text/plain"), code);

        return endpoints
                .verifyAccount(codeBody)
                .map(new Func1<Responses.Tokens, JWTTokens>() {
                    @Override
                    public JWTTokens call(Responses.Tokens response) {
                        return response.result;
                    }
                })
                .doOnNext(new Action1<JWTTokens>() {
                    @Override
                    public void call(JWTTokens jwtTokens) {
                        preferences.setRequestToken(jwtTokens.getRequestJwt());
                        preferences.setRefreshToken(jwtTokens.getRefreshJwt());
                    }
                });
    }

    @Override
    public Observable<JWTTokens> refreshTokens() {
        return endpoints
                .refreshToken(preferences.getRefreshToken())
                .map(new Func1<Responses.Tokens, JWTTokens>() {
                    @Override
                    public JWTTokens call(Responses.Tokens response) {
                        return response.result;
                    }
                })
                .doOnNext(new Action1<JWTTokens>() {
                    @Override
                    public void call(JWTTokens jwtTokens) {
                        preferences.setRequestToken(jwtTokens.getRequestJwt());
                        preferences.setRefreshToken(jwtTokens.getRefreshJwt());
                    }
                });
    }

    @Override
    public Observable<Void> updateFCMToken(String token) {
        RequestBody tokenBody = RequestBody.create(MediaType.parse("text/plain"), token);

        return endpoints
                .updateFCMToken(tokenBody)
                .compose(new TokensRefreshTransformer<Void>(this));
    }

    @SuppressLint("HardwareIds")
    String getSecureID() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
