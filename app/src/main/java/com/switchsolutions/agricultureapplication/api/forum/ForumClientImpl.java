package com.switchsolutions.agricultureapplication.api.forum;

import com.switchsolutions.agricultureapplication.models.ForumAnswer;
import com.switchsolutions.agricultureapplication.models.ForumCategory;
import com.switchsolutions.agricultureapplication.models.ForumComment;
import com.switchsolutions.agricultureapplication.models.ForumQuestion;
import com.switchsolutions.agricultureapplication.models.ForumVote;
import com.switchsolutions.agricultureapplication.preferences.Preferences;

import java.util.ArrayList;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.functions.Func1;

final class ForumClientImpl implements ForumClient {

    private final Endpoints endpoints;
    private final Preferences preferences;

    @Inject
    ForumClientImpl(Endpoints endpoints, Preferences preferences) {
        this.endpoints = endpoints;
        this.preferences = preferences;
    }

    @Override
    public Observable<ArrayList<ForumCategory>> getCategories() {
        return endpoints
                .getCategories(preferences.getLanguage())
                .map(new Func1<Responses.Categories, ArrayList<ForumCategory>>() {
                    @Override
                    public ArrayList<ForumCategory> call(Responses.Categories response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ForumQuestion> createQuestion(long categoryId, String text) {
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);

        return endpoints
                .createQuestion(categoryId, textBody)
                .map(new Func1<Responses.Question, ForumQuestion>() {
                    @Override
                    public ForumQuestion call(Responses.Question response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumAnswer> createAnswer(long categoryId, long questionId, String text) {
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);

        return endpoints
                .createAnswer(categoryId, questionId, textBody)
                .map(new Func1<Responses.Answer, ForumAnswer>() {
                    @Override
                    public ForumAnswer call(Responses.Answer response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumComment> createComment(long categoryId, long questionId, long answerId, String text) {
        RequestBody textBody = RequestBody.create(MediaType.parse("text/plain"), text);

        return endpoints
                .createComment(categoryId, questionId, answerId, textBody)
                .map(new Func1<Responses.Comment, ForumComment>() {
                    @Override
                    public ForumComment call(Responses.Comment response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ArrayList<ForumQuestion>> getQuestionsNew(long categoryId, int limit, int offset) {
        return endpoints
                .getQuestionsNew(categoryId, limit, offset)
                .map(new Func1<Responses.Questions, ArrayList<ForumQuestion>>() {
                    @Override
                    public ArrayList<ForumQuestion> call(Responses.Questions response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<ForumQuestion>> getQuestionsPopular(long categoryId, int limit, int offset) {
        return endpoints
                .getQuestionsPopular(categoryId, limit, offset)
                .map(new Func1<Responses.Questions, ArrayList<ForumQuestion>>() {
                    @Override
                    public ArrayList<ForumQuestion> call(Responses.Questions response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<ForumAnswer>> getAnswers(long categoryId, long questionId) {
        return endpoints
                .getAnswers(categoryId, questionId)
                .map(new Func1<Responses.Answers, ArrayList<ForumAnswer>>() {
                    @Override
                    public ArrayList<ForumAnswer> call(Responses.Answers response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<ForumComment>> getComments(long categoryId, long questionId, long answerId) {
        return endpoints
                .getComments(categoryId, questionId, answerId)
                .map(new Func1<Responses.Comments, ArrayList<ForumComment>>() {
                    @Override
                    public ArrayList<ForumComment> call(Responses.Comments response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ForumVote> incrementQuestionVote(long categoryId, long questionId) {
        return endpoints
                .incrementQuestionVote(categoryId, questionId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumVote> decrementQuestionVote(long categoryId, long questionId) {
        return endpoints
                .decrementQuestionVote(categoryId, questionId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumVote> deleteQuestionVote(long categoryId, long questionId) {
        return endpoints
                .deleteQuestionVote(categoryId, questionId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumVote> incrementAnswerVote(long categoryId, long questionId, long answerId) {
        return endpoints
                .incrementAnswerVote(categoryId, questionId, answerId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumVote> decrementAnswerVote(long categoryId, long questionId, long answerId) {
        return endpoints
                .decrementAnswerVote(categoryId, questionId, answerId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<ForumVote> deleteAnswerVote(long categoryId, long questionId, long answerId) {
        return endpoints
                .deleteAnswerVote(categoryId, questionId, answerId)
                .map(new Func1<Responses.Vote, ForumVote>() {
                    @Override
                    public ForumVote call(Responses.Vote response) {
                        return response.result;
                    }
                });
    }
}
