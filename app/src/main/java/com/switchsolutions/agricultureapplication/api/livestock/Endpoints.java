package com.switchsolutions.agricultureapplication.api.livestock;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    interface API {

        @GET("v1/livestock")
        Observable<Responses.LivestockList> getLivestock(
                @Query("lang") String lang,
                @Query("limit") int limit,
                @Query("offset") int offset
        );
    }

    interface Content {

        @GET("pages/{lang}/livestock/{livestock-id}.html")
        Observable<ResponseBody> getLivestockPage(
                @Path("lang") String lang,
                @Path("livestock-id") long livestockId
        );
    }
}
