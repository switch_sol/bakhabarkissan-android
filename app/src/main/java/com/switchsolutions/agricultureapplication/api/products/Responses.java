package com.switchsolutions.agricultureapplication.api.products;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.models.ProductDealer;

import java.util.List;

final class Responses {

    static final class CategoriesList {

        @JsonProperty("result")
        public List<ProductCategory> result;
    }

    static final class ProductsList {

        @JsonProperty("result")
        public List<Product> result;
    }

    static final class DealersList {

        @JsonProperty("result")
        public List<ProductDealer> result;
    }
}
