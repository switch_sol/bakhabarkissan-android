package com.switchsolutions.agricultureapplication.api.crops;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.Crop;
import com.switchsolutions.agricultureapplication.models.CropCategory;

import java.util.List;

final class Responses {

    static final class CategoriesList {

        @JsonProperty("result")
        public List<CropCategory> result;
    }

    static final class CropsList {

        @JsonProperty("result")
        public List<Crop> result;
    }

}
