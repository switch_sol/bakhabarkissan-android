package com.switchsolutions.agricultureapplication.api.remote_config;

import javax.inject.Inject;

final class RemoteConfigClientImpl implements RemoteConfigClient {

    @Inject
    RemoteConfigClientImpl() {
    }

    @Override
    public boolean hasBannerAds() {
        return false;
    }

    @Override
    public String[] getBannerAds() {
        return new String[]{};
    }
}
