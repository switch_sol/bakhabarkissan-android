package com.switchsolutions.agricultureapplication.api.location;

public final class LocationProviderDisabledException extends RuntimeException {

    public LocationProviderDisabledException(Throwable cause) {
        super(cause);
    }
}
