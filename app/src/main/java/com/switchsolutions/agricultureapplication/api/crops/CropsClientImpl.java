package com.switchsolutions.agricultureapplication.api.crops;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.models.Crop;
import com.switchsolutions.agricultureapplication.models.CropCategory;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;
import com.switchsolutions.agricultureapplication.utils.Languages;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

final class CropsClientImpl implements CropsClient {

    private static final long CACHE_SIZE = 1024 * 1024 * 5; // 5 MB

    private static final String CACHE_DIRNAME = "cache_crops";

    private static final ObjectReader CATEGORIES_LIST_READER;
    private static final ObjectWriter CATEGORIES_LIST_WRITER;

    private static final ObjectReader CROPS_LIST_READER;
    private static final ObjectWriter CROPS_LIST_WRITER;

    static {
        ObjectMapper mapper = new ObjectMapper();

        CATEGORIES_LIST_READER = mapper.readerFor(Responses.CategoriesList.class);
        CATEGORIES_LIST_WRITER = mapper.writerFor(Responses.CategoriesList.class);

        CROPS_LIST_READER = mapper.readerFor(Responses.CropsList.class);
        CROPS_LIST_WRITER = mapper.writerFor(Responses.CropsList.class);
    }

    private final Endpoints.API apiEndpoints;
    private final Endpoints.Content contentEndpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    @Nullable
    private final LruCache cache;

    @Inject
    CropsClientImpl(Endpoints.API apiEndpoints, Endpoints.Content contentEndpoints, Preferences preferences, Storage storage, AccountsClient accountsClient, NetworkUtility networkUtility) {
        this.apiEndpoints = apiEndpoints;
        this.contentEndpoints = contentEndpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    static String getCategoriesListCacheKey(String language) {
        return String.format(Locale.US, "%s_crop_categories_list", language);
    }

    static String getCropsListCacheKey(String language, long categoryId) {
        return String.format(Locale.US, "%s_crops_list_%d", language, categoryId);
    }

    static String getCropPageCacheKey(String language, long cropId) {
        return String.format(Locale.US, "%s_crop_page_%d", language, cropId);
    }

    @Override
    public Observable<List<CropCategory>> getCategories() {
        if (networkUtility.isInternetConnected()) {
            return getFreshCategories(preferences.getLanguageNormalized());
        } else {
            return getCachedCategories(preferences.getLanguageNormalized())
                    .concatWith(getFreshCategories(preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<List<CropCategory>> getCachedCategories(final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCategoriesListCacheKey(lang);
                    }
                })
                .map(new Func1<String, Responses.CategoriesList>() {
                    @Override
                    public Responses.CategoriesList call(String key) {
                        try {
                            return cache.readJSON(key, CATEGORIES_LIST_READER);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<Responses.CategoriesList, List<CropCategory>>() {
                    @Override
                    public List<CropCategory> call(Responses.CategoriesList response) {
                        return response.result;
                    }
                })
                .onErrorResumeNext(Observable.<List<CropCategory>>empty());
    }

    Observable<List<CropCategory>> getFreshCategories(final String lang) {
        return apiEndpoints
                .getCategories(lang)
                .compose(new TokensRefreshTransformer<Responses.CategoriesList>(accountsClient))
                .doOnNext(new Action1<Responses.CategoriesList>() {
                    @Override
                    public void call(Responses.CategoriesList response) {
                        if (cache == null) return;
                        try {
                            cache.writeJSON(getCategoriesListCacheKey(lang), CATEGORIES_LIST_WRITER, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.CategoriesList, List<CropCategory>>() {
                    @Override
                    public List<CropCategory> call(Responses.CategoriesList response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<List<Crop>> getCrops(long categoryId) {
        if (networkUtility.isInternetConnected()) {
            return getFreshCrops(categoryId, preferences.getLanguageNormalized());
        } else {
            return getCachedCrops(categoryId, preferences.getLanguageNormalized())
                    .concatWith(getFreshCrops(categoryId, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<List<Crop>> getCachedCrops(final long categoryId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCropsListCacheKey(lang, categoryId);
                    }
                })
                .map(new Func1<String, Responses.CropsList>() {
                    @Override
                    public Responses.CropsList call(String key) {
                        try {
                            return cache.readJSON(key, CROPS_LIST_READER);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<Responses.CropsList, List<Crop>>() {
                    @Override
                    public List<Crop> call(Responses.CropsList response) {
                        return response.result;
                    }
                })
                .onErrorResumeNext(Observable.<List<Crop>>empty());
    }

    Observable<List<Crop>> getFreshCrops(final long categoryId, final String lang) {
        return apiEndpoints
                .getCrops(categoryId, lang)
                .compose(new TokensRefreshTransformer<Responses.CropsList>(accountsClient))
                .doOnNext(new Action1<Responses.CropsList>() {
                    @Override
                    public void call(Responses.CropsList response) {
                        if (cache == null) return;

                        try {
                            cache.writeJSON(getCropsListCacheKey(lang, categoryId), CROPS_LIST_WRITER, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.CropsList, List<Crop>>() {
                    @Override
                    public List<Crop> call(Responses.CropsList response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<Document> getCropPage(long cropId) {
        return getCropPage(cropId, preferences.getLanguageNormalized());
    }

    @Override
    public Observable<Document> getCropPage(final long cropId, final String lang) {
        if (networkUtility.isInternetConnected()) {
            return getFreshCropPage(cropId, lang);
        } else {
            return getCachedCropPage(cropId, lang)
                    .concatWith(getFreshCropPage(cropId, lang))
                    .take(1);
        }
    }

    Observable<Document> getCachedCropPage(final long cropId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCropPageCacheKey(lang, cropId);
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String key) {
                        try {
                            return Jsoup.parse(cache.readString(key), "");
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .onErrorResumeNext(Observable.<Document>empty());
    }

    Observable<Document> getFreshCropPage(final long cropId, final String lang) {
        return contentEndpoints
                .getCropPage(lang, cropId)
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        if (throwable instanceof HttpException) {
                        }
                        if (lang.equals(Languages.SD) || lang.equals(Languages.PS)) {
                            return contentEndpoints.getCropPage(Languages.UR, cropId);
                        }
                        return Observable.error(throwable);
                    }
                })
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        try {
                            return responseBody.string();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            responseBody.close();
                        }
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String content) {
                        if (cache == null) return;

                        try {
                            cache.writeString(getCropPageCacheKey(lang, cropId), content);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String content) {
                        return Jsoup.parse(content, "");
                    }
                });
    }

}
