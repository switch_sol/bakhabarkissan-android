package com.switchsolutions.agricultureapplication.api.accounts;

import okhttp3.RequestBody;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

interface Endpoints {

//    @Multipart
//    @POST("v1/accounts/create")
//    Observable<Void> createAccount(
//            @Header("Identifier") String identifier,
//            @Part("name") String name,
//            @Part("number") String number
//    );

    @Multipart
    @POST("api/v1/accounts/create/demo")
    Observable<Responses.Tokens> createAccountDemo(
            @Header("Identifier") String identifier,
            @Part("name") String name,
            @Part("number") String number
    );

    @Multipart
    @POST("v1/accounts/verify")
    Observable<Responses.Tokens> verifyAccount(
            @Part("code") RequestBody code
    );

    @Multipart
    @POST("v1/accounts/update/fcm_token")
    Observable<Void> updateFCMToken(
            @Part("fcm_token") RequestBody token
    );

    @GET("v1/tokens/refresh")
    Observable<Responses.Tokens> refreshToken(
            @Header("Authorization") String refreshToken
    );

}
