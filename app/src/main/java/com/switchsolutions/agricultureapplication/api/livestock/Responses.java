package com.switchsolutions.agricultureapplication.api.livestock;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.Livestock;

import java.util.List;

final class Responses {

    static final class LivestockList {

        @JsonProperty("result")
        public List<Livestock> result;
    }

}
