package com.switchsolutions.agricultureapplication.api.videos;

import com.switchsolutions.agricultureapplication.network.NetworkModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class VideosClientModule {

    @Provides
    @Singleton
    Endpoints providesEndpoints(@Named(NetworkModule.PROVIDER_AGRICULTURE_API) Retrofit retrofit) {
        return retrofit.create(Endpoints.class);
    }

    @Provides
    @Singleton
    public VideosClient providesVideosClient(VideosClientImpl videosClient) {
        return videosClient;
    }

}
