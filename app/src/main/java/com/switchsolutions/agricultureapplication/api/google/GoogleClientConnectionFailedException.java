package com.switchsolutions.agricultureapplication.api.google;

import com.google.android.gms.common.ConnectionResult;

public final class GoogleClientConnectionFailedException extends RuntimeException {

    private final ConnectionResult connectionResult;

    GoogleClientConnectionFailedException(ConnectionResult connectionResult) {
        this.connectionResult = connectionResult;
    }

    public ConnectionResult getConnectionResult() {
        return connectionResult;
    }
}
