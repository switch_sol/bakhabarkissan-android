package com.switchsolutions.agricultureapplication.api.forecast;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.models.Forecast;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

class ForecastClientImpl implements ForecastClient {

    static final Func1<Response, ArrayList<Forecast>> FORECAST_RESPONSE_TO_MODEL = new Func1<Response, ArrayList<Forecast>>() {
        @Override
        public ArrayList<Forecast> call(Response response) {
            ArrayList<Forecast> forecasts = new ArrayList<>(response.forecasts.size());
            for (Response.Forecast forecast : response.forecasts) {
                forecasts.add(new Forecast(
                        new Date(forecast.dt * 1000),
                        forecast.main.temp,
                        forecast.main.humidity,
                        forecast.weather.size() > 0 ? forecast.weather.get(0).id : 800,
                        forecast.clouds.all,
                        forecast.wind.speed,
                        forecast.wind.deg
                ));
            }
            return forecasts;
        }
    };
    static final Func1<ArrayList<Forecast>, ArrayList<Forecast>> FILTER_FUTURE_FORECAST = new Func1<ArrayList<Forecast>, ArrayList<Forecast>>() {
        @Override
        public ArrayList<Forecast> call(ArrayList<Forecast> response) {
            ArrayList<Forecast> filtered = new ArrayList<>();
            for (Forecast forecast : response) {
                long chkHours = TimeUnit.MILLISECONDS.toHours(forecast.getDatetime().getTime());
                long nowHours = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis());
                if (chkHours > nowHours || nowHours - chkHours < 3) {
                    filtered.add(forecast);
                }
            }
            return filtered;
        }
    };
    private static final long CACHE_SIZE = (1024 * 1024) / 2; // 512 KB
    private static final String CACHE_DIRNAME = "cache_forecast";
    private static final String CACHE_FILENAME = "cache_filename";
    private static final String API_KEY = "2e17e512761d2bfb342938dbf5351c4c";
    //    private static final String API_KEY = "2e17e512761d2bfb342938dbf5351c4c";
    private final Endpoints endpoints;
    private final NetworkUtility networkUtility;

    private final ObjectReader forecastListReader;
    private final ObjectWriter forecastListWriter;

    @Nullable
    private final LruCache cache;

    @Inject
    ForecastClientImpl(Endpoints endpoints, Storage storage, NetworkUtility networkUtility) {
        this.endpoints = endpoints;
        this.networkUtility = networkUtility;

        ObjectMapper mapper = new ObjectMapper();
        forecastListReader = mapper.readerFor(Response.class);
        forecastListWriter = mapper.writerFor(Response.class);

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    @Override
    public Observable<ArrayList<Forecast>> getForecast(double lat, double lon) {
        if (cache != null && cache.containsKey(CACHE_FILENAME)) {
            long oldMinutes = TimeUnit.MILLISECONDS.toMinutes(cache.getLastModified(CACHE_FILENAME));
            long newMinutes = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis());
            if (newMinutes - oldMinutes <= 30) {
                return getCachedForecast()
                        .concatWith(getFreshForecast(lat, lon))
                        .take(1);
            }
        }

        if (networkUtility.isInternetConnected()) {
            return getFreshForecast(lat, lon);
        }

        return getCachedForecast()
                .concatWith(getFreshForecast(lat, lon))
                .take(1);
    }

    Observable<ArrayList<Forecast>> getCachedForecast() {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<Response>() {
                    @Override
                    public Response call() throws Exception {
                        try {
                            return cache.readJSON(CACHE_FILENAME, forecastListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(FORECAST_RESPONSE_TO_MODEL)
                .map(FILTER_FUTURE_FORECAST)
                .onErrorResumeNext(Observable.<ArrayList<Forecast>>empty());
    }

    Observable<ArrayList<Forecast>> getFreshForecast(double lat, double lon) {
        try {
            return requestFreshForecast(lat, lon);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    Observable<ArrayList<Forecast>> requestFreshForecast(double lat, double lon) throws Exception {
        return endpoints
                .requestForecast(lat, lon, API_KEY)
                .doOnNext(new Action1<Response>() {
                    @Override
                    public void call(Response response) {
                        if (cache == null) return;
                        try {
                            cache.writeJSON(CACHE_FILENAME, forecastListWriter, response);
                        } catch (IOException e) {
                        } catch (SecurityException e) {
                        }
                    }
                })
                .map(FORECAST_RESPONSE_TO_MODEL)
                .map(FILTER_FUTURE_FORECAST);
    }
}
