package com.switchsolutions.agricultureapplication.api.forum;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.ForumAnswer;
import com.switchsolutions.agricultureapplication.models.ForumCategory;
import com.switchsolutions.agricultureapplication.models.ForumComment;
import com.switchsolutions.agricultureapplication.models.ForumQuestion;
import com.switchsolutions.agricultureapplication.models.ForumVote;

import java.util.List;

final class Responses {

    static final class Categories {

        @JsonProperty("result")
        public List<ForumCategory> result;
    }

    static final class Question {

        @JsonProperty("result")
        public ForumQuestion result;
    }

    static final class Questions {

        @JsonProperty("result")
        public List<ForumQuestion> result;
    }

    static final class Answer {

        @JsonProperty("result")
        public ForumAnswer result;
    }

    static final class Answers {

        @JsonProperty("result")
        public List<ForumAnswer> result;
    }

    static final class Comment {

        @JsonProperty("result")
        public ForumComment result;
    }

    static final class Comments {

        @JsonProperty("result")
        public List<ForumComment> result;
    }

    static final class Vote {

        @JsonProperty("result")
        public ForumVote result;
    }

}
