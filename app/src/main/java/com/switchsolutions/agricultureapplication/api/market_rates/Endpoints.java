package com.switchsolutions.agricultureapplication.api.market_rates;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    @GET("v1/markets/nearby")
    Observable<Responses.MarketsList> getMarketsNearby(
            @Query("lat") double lat,
            @Query("lon") double lon,
            @Query("lang") String lang,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("v1/markets/market/{market-id}/rates")
    Observable<Responses.MarketRatesList> getMarketRates(
            @Path("market-id") long marketId,
            @Query("lang") String lang);
}
