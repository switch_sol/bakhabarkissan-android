package com.switchsolutions.agricultureapplication.api.videos;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

final class VideosClientImpl implements VideosClient {

    private static final long CACHE_SIZE = 1024 * 1024 * 2; // 2MB

    private static final String CACHE_DIRNAME = "cache_videos";

    private final Endpoints endpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    private final ObjectReader categoriesListParentReader;
    private final ObjectWriter categoriesListParentWriter;

    private final ObjectReader categoriesListChildReader;
    private final ObjectWriter categoriesListChildWriter;

    private final ObjectReader videosListReader;
    private final ObjectWriter videosListWriter;

    @Nullable
    private final LruCache cache;

    @Inject
    VideosClientImpl(Endpoints endpoints, Preferences preferences, AccountsClient accountsClient, NetworkUtility networkUtility, Storage storage) {
        this.endpoints = endpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        ObjectMapper objectMapper = new ObjectMapper();

        categoriesListParentReader = objectMapper.readerFor(Responses.CategoriesParentList.class);
        categoriesListParentWriter = objectMapper.writerFor(Responses.CategoriesParentList.class);

        categoriesListChildReader = objectMapper.readerFor(Responses.CategoriesChildList.class);
        categoriesListChildWriter = objectMapper.writerFor(Responses.CategoriesChildList.class);

        videosListReader = objectMapper.readerFor(Responses.VideosList.class);
        videosListWriter = objectMapper.writerFor(Responses.VideosList.class);

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    @Override
    public Observable<ArrayList<VideoCategoryParent>> getCategories(int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getCategoriesFresh(limit, offset, preferences.getLanguageNormalized());
        } else {
            return getCategoriesCached(limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getCategoriesFresh(limit, offset, preferences.getLanguageNormalized()))
                    .first();
        }
    }

    @Override
    public Observable<ArrayList<VideoCategoryParent>> getCategoriesCached(final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<VideoCategoryParent>>() {
                    @Override
                    public ArrayList<VideoCategoryParent> call() throws Exception {
                        String key = getCacheKeyCategoriesParent(lang, limit, offset);

                        Responses.CategoriesParentList response;
                        try {
                            response = cache.readJSON(key, categoriesListParentReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<VideoCategoryParent>>empty());
    }

    @Override
    public Observable<ArrayList<VideoCategoryParent>> getCategoriesFresh(final int limit, final int offset, final String lang) {
        return endpoints
                .getCategories(lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.CategoriesParentList>(accountsClient))
                .doOnNext(new Action1<Responses.CategoriesParentList>() {
                    @Override
                    public void call(Responses.CategoriesParentList response) {
                        if (cache == null) return;

                        String key = getCacheKeyCategoriesParent(lang, limit, offset);
                        try {
                            cache.writeJSON(key, categoriesListParentWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.CategoriesParentList, ArrayList<VideoCategoryParent>>() {
                    @Override
                    public ArrayList<VideoCategoryParent> call(Responses.CategoriesParentList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<VideoCategoryChild>> getCategories(long parentCategoryId, int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getCategoriesFresh(parentCategoryId, limit, offset, preferences.getLanguageNormalized());
        } else {
            return getCategoriesCached(parentCategoryId, limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getCategoriesFresh(parentCategoryId, limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    @Override
    public Observable<ArrayList<VideoCategoryChild>> getCategoriesCached(final long parentCategoryId, final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<VideoCategoryChild>>() {
                    @Override
                    public ArrayList<VideoCategoryChild> call() throws Exception {
                        String key = getCacheKeyCategoriesChild(lang, parentCategoryId, limit, offset);

                        Responses.CategoriesChildList response;
                        try {
                            response = cache.readJSON(key, categoriesListChildReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<VideoCategoryChild>>empty());
    }

    @Override
    public Observable<ArrayList<VideoCategoryChild>> getCategoriesFresh(final long parentCategoryId, final int limit, final int offset, final String lang) {
        return endpoints
                .getCategories(parentCategoryId, lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.CategoriesChildList>(accountsClient))
                .doOnNext(new Action1<Responses.CategoriesChildList>() {
                    @Override
                    public void call(Responses.CategoriesChildList response) {
                        if (cache == null) return;

                        String key = getCacheKeyCategoriesChild(lang, parentCategoryId, limit, offset);
                        try {
                            cache.writeJSON(key, categoriesListChildWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.CategoriesChildList, ArrayList<VideoCategoryChild>>() {
                    @Override
                    public ArrayList<VideoCategoryChild> call(Responses.CategoriesChildList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<Video>> getVideos(long parentCategoryId, long childCategoryId, int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getVideosFresh(parentCategoryId, childCategoryId, limit, offset, preferences.getLanguageNormalized());
        } else {
            return getVideosCached(parentCategoryId, childCategoryId, limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getVideosFresh(parentCategoryId, childCategoryId, limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    @Override
    public Observable<ArrayList<Video>> getVideosCached(final long parentCategoryId, final long childCategoryId, final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<Video>>() {
                    @Override
                    public ArrayList<Video> call() throws Exception {
                        String key = getCacheKeyVideos(lang, parentCategoryId, childCategoryId, limit, offset);

                        Responses.VideosList response;
                        try {
                            response = cache.readJSON(key, videosListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<Video>>empty());
    }

    @Override
    public Observable<ArrayList<Video>> getVideosFresh(final long parentCategoryId, final long childCategoryId, final int limit, final int offset, final String lang) {
        return endpoints
                .getVideos(parentCategoryId, childCategoryId, lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.VideosList>(accountsClient))
                .doOnNext(new Action1<Responses.VideosList>() {
                    @Override
                    public void call(Responses.VideosList response) {
                        if (cache == null) return;

                        String key = getCacheKeyVideos(lang, parentCategoryId, childCategoryId, limit, offset);
                        try {
                            cache.writeJSON(key, videosListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.VideosList, ArrayList<Video>>() {
                    @Override
                    public ArrayList<Video> call(Responses.VideosList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    String getCacheKeyCategoriesParent(String language, int limit, int offset) {
        return String.format(Locale.US, "%s_parent_categories_list_%d_%d", language, limit, offset);
    }

    String getCacheKeyCategoriesChild(String language, long parentCategoryId, int limit, int offset) {
        return String.format(Locale.US, "%s_child_categories_list_%d_%d", language, limit, offset);
    }

    String getCacheKeyVideos(String language, long parentCategoryId, long childCategoryId, int limit, int offset) {
        return String.format(Locale.US, "%s_videos_list_%d_%d_%d_%d", language, parentCategoryId, childCategoryId, limit, offset);
    }
}
