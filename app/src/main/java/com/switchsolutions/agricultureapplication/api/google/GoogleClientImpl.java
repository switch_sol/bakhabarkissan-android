package com.switchsolutions.agricultureapplication.api.google;

import android.content.Context;

import com.switchsolutions.agricultureapplication.utils.Coordinates;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func0;

final class GoogleClientImpl implements GoogleClient {

    private final Context context;

    @Inject
    GoogleClientImpl(Context context) {
        this.context = context;
    }

    @Override
    public Observable<Coordinates> getLocation() {
        try {


            return Observable
                    .defer(new Func0<Observable<Coordinates>>() {
                        @Override
                        public Observable<Coordinates> call() {
                            return Observable
                                    .create(new GetLocationOnSubscribe(context));
                        }
                    });
        } catch (Exception e) {

        }
        return null;
    }
}
