package com.switchsolutions.agricultureapplication.api.accounts;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.JWTTokens;

final class Responses {

    static final class Tokens {

        @JsonProperty("result")
        public JWTTokens result;
    }

}
