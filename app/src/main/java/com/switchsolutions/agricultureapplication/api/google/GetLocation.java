package com.switchsolutions.agricultureapplication.api.google;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.awareness.snapshot.LocationResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.switchsolutions.agricultureapplication.api.location.LocationNotFoundException;
import com.switchsolutions.agricultureapplication.utils.Coordinates;

final class GetLocation extends BaseClient {

    private final Listener listener;

    GetLocation(Context context, Listener listener) {
        super(context);

        this.listener = listener;
    }

    @Override
    void onConnected(GoogleApiClient client) {
        try {
            Awareness.SnapshotApi
                    .getLocation(client)
                    .setResultCallback(new ResultCallback<LocationResult>() {
                        @Override
                        public void onResult(@NonNull LocationResult locationResult) {
                            if (locationResult.getStatus().isSuccess()) {
                                listener.onResult(new Coordinates(locationResult.getLocation()));
                            } else {
                                listener.onError(new LocationNotFoundException());
                            }
                        }
                    });
        } catch (SecurityException e) {
            listener.onError(e);
        }
    }

    @Override
    void onError(Throwable t) {
        listener.onError(t);
    }

    interface Listener {
        void onResult(Coordinates coordinates);

        void onError(Throwable throwable);
    }

}
