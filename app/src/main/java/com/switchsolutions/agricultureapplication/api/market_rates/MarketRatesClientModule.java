package com.switchsolutions.agricultureapplication.api.market_rates;

import com.switchsolutions.agricultureapplication.network.NetworkModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class MarketRatesClientModule {

    @Provides
    @Singleton
    Endpoints providesEndpoints(@Named(NetworkModule.PROVIDER_AGRICULTURE_API) Retrofit retrofit) {
        return retrofit.create(Endpoints.class);
    }

    @Provides
    @Singleton
    public MarketRatesClient providesMarketRatesClient(MarketRatesClientImpl marketRatesClient) {
        return marketRatesClient;
    }
}
