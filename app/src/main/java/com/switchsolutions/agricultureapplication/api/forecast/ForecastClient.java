package com.switchsolutions.agricultureapplication.api.forecast;

import com.switchsolutions.agricultureapplication.models.Forecast;

import java.util.ArrayList;

import rx.Observable;

public interface ForecastClient {

    Observable<ArrayList<Forecast>> getForecast(double lat, double lon);
}
