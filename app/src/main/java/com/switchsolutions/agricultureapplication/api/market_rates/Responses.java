package com.switchsolutions.agricultureapplication.api.market_rates;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.Market;
import com.switchsolutions.agricultureapplication.models.MarketRate;

import java.util.List;

final class Responses {

    static final class MarketsList {

        @JsonProperty("result")
        List<Market> result;
    }

    static final class MarketRatesList {

        @JsonProperty("result")
        List<MarketRate> result;
    }

}
