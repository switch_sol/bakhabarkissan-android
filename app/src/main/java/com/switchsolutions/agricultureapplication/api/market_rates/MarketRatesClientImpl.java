package com.switchsolutions.agricultureapplication.api.market_rates;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.models.Market;
import com.switchsolutions.agricultureapplication.models.MarketRate;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

final class MarketRatesClientImpl implements MarketRatesClient {

    private static final long CACHE_SIZE = 1024 * 1024 * 2; // 2MB

    private static final String CACHE_DIRNAME = "cache_market_rates";

    private final Endpoints endpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    private final ObjectReader marketsListReader;
    private final ObjectWriter marketsListWriter;

    private final ObjectReader marketRatesListReader;
    private final ObjectWriter marketRatesListWriter;

    @Nullable
    private final LruCache cache;

    @Inject
    MarketRatesClientImpl(Endpoints endpoints, Preferences preferences, AccountsClient accountsClient, NetworkUtility networkUtility, Storage storage) {
        this.endpoints = endpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        ObjectMapper objectMapper = new ObjectMapper();

        marketsListReader = objectMapper.readerFor(Responses.MarketsList.class);
        marketsListWriter = objectMapper.writerFor(Responses.MarketsList.class);

        marketRatesListReader = objectMapper.readerFor(Responses.MarketRatesList.class);
        marketRatesListWriter = objectMapper.writerFor(Responses.MarketRatesList.class);

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    @Override
    public Observable<ArrayList<Market>> getMarketsNearby(double lat, double lon, int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getMarketsNearbyFresh(lat, lon, limit, offset, preferences.getLanguageNormalized());
        } else {
            return getMarketsNearbyCached(limit, offset, preferences.getLanguage())
                    .concatWith(getMarketsNearbyFresh(lat, lon, limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    @Override
    public Observable<ArrayList<Market>> getMarketsNearbyCached(final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<Market>>() {
                    @Override
                    public ArrayList<Market> call() throws Exception {
                        String key = getCacheKeyMarketsList(lang, limit, offset);

                        Responses.MarketsList response;
                        try {
                            response = cache.readJSON(key, marketsListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<Market>>empty());
    }

    @Override
    public Observable<ArrayList<Market>> getMarketsNearbyFresh(double lat, double lon, final int limit, final int offset, final String lang) {
        return endpoints
                .getMarketsNearby(lat, lon, lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.MarketsList>(accountsClient))
                .doOnNext(new Action1<Responses.MarketsList>() {
                    @Override
                    public void call(Responses.MarketsList response) {
                        if (cache == null) return;

                        String key = getCacheKeyMarketsList(lang, limit, offset);
                        try {
                            cache.writeJSON(key, marketsListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.MarketsList, ArrayList<Market>>() {
                    @Override
                    public ArrayList<Market> call(Responses.MarketsList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<MarketRate>> getMarketRates(long marketId) {
        if (networkUtility.isInternetConnected()) {
            return getMarketRatesFresh(marketId, preferences.getLanguageNormalized());
        } else {
            return getMarketRatesCached(marketId, preferences.getLanguageNormalized())
                    .concatWith(getMarketRatesFresh(marketId, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    @Override
    public Observable<ArrayList<MarketRate>> getMarketRatesCached(final long marketId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<MarketRate>>() {
                    @Override
                    public ArrayList<MarketRate> call() throws Exception {
                        String key = getCacheKeyMarketRatesList(lang, marketId);

                        Responses.MarketRatesList response;
                        try {
                            response = cache.readJSON(key, marketRatesListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<MarketRate>>empty());
    }

    @Override
    public Observable<ArrayList<MarketRate>> getMarketRatesFresh(final long marketId, final String lang) {
        return endpoints
                .getMarketRates(marketId, lang)
                .compose(new TokensRefreshTransformer<Responses.MarketRatesList>(accountsClient))
                .doOnNext(new Action1<Responses.MarketRatesList>() {
                    @Override
                    public void call(Responses.MarketRatesList response) {
                        if (cache == null) return;

                        String key = getCacheKeyMarketRatesList(lang, marketId);
                        try {
                            cache.writeJSON(key, marketRatesListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.MarketRatesList, ArrayList<MarketRate>>() {
                    @Override
                    public ArrayList<MarketRate> call(Responses.MarketRatesList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    String getCacheKeyMarketsList(String language, int limit, int offset) {
        return String.format(Locale.US, "%s_markets_list_%d_%d", language, limit, offset);
    }

    String getCacheKeyMarketRatesList(String language, long marketId) {
        return String.format(Locale.US, "%s_market_rates_list_%d", language, marketId);
    }
}
