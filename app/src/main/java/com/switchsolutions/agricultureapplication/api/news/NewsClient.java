package com.switchsolutions.agricultureapplication.api.news;

import com.switchsolutions.agricultureapplication.models.NewsArticle;

import org.jsoup.nodes.Document;

import java.util.List;

import rx.Observable;

public interface NewsClient {

    Observable<List<NewsArticle>> getNews(int limit, int offset);

    Observable<Document> getNewsPage(long newsId);
}
