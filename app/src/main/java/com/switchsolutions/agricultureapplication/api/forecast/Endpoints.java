package com.switchsolutions.agricultureapplication.api.forecast;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    @GET("forecast")
    Observable<Response> requestForecast(
            @Query("lat") double lat,
            @Query("lon") double lon,
            @Query("appid") String appId
    );
}
