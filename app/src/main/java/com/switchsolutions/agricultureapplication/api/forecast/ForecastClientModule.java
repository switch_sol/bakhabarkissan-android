package com.switchsolutions.agricultureapplication.api.forecast;

import com.switchsolutions.agricultureapplication.network.NetworkModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ForecastClientModule {

    @Provides
    @Singleton
    Endpoints providesEndpoints(@Named(NetworkModule.PROVIDER_FORECAST) Retrofit retrofit) {
        return retrofit.create(Endpoints.class);
    }

    @Provides
    @Singleton
    public ForecastClient providesForecastClient(ForecastClientImpl forecastClient) {
        return forecastClient;
    }
}
