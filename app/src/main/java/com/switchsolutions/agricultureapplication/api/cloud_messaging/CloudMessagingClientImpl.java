package com.switchsolutions.agricultureapplication.api.cloud_messaging;

import com.google.firebase.messaging.FirebaseMessaging;

import javax.inject.Inject;

final class CloudMessagingClientImpl implements CloudMessagingClient {

    private final FirebaseMessaging firebaseMessaging;

    @Inject
    CloudMessagingClientImpl() {
        this.firebaseMessaging = FirebaseMessaging.getInstance();
    }

    @Override
    public void subscribeToGlobals() {
        subscribeToAppUpdateTopic();
        subscribeToGlobalTopic();
        subscribeToNewsTopic();
    }

    @Override
    public void subscribeToGlobalTopic() {
        this.firebaseMessaging.subscribeToTopic(TOPIC_GLOBAL);
    }

    @Override
    public void subscribeToNewsTopic() {
        this.firebaseMessaging.subscribeToTopic(TOPIC_NEWS);
    }

    @Override
    public void subscribeToAppUpdateTopic() {
        this.firebaseMessaging.subscribeToTopic(TOPIC_APPUPDATE);
    }
}
