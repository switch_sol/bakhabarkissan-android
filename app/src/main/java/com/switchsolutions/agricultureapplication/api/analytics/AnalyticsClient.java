package com.switchsolutions.agricultureapplication.api.analytics;

public interface AnalyticsClient {

    String CATEGORY_MENU = "Menu";
    String CATEGORY_NEWS = "News";
    String CATEGORY_CROPS = "Crops";
    String CATEGORY_LIVESTOCK = "Livestock";
    String CATEGORY_PRODUCTS = "Products";
    String CATEGORY_MARKETS = "Markets";
    String CATEGORY_WEATHER = "Weather";
    String CATEGORY_VIDEOS = "Videos";
    String CATEGORY_PREFERENCES = "Preferences";
    String CATEGORY_ADVERTISEMENTS = "Advertisements";
    String CATEGORY_NOTIFICATION = "Notification";

    String ACTION_SELECTED_NEWS_ARTICLE = "Selected News Article";
    String ACTION_SELECTED_CROP_CATEGORY = "Selected Crop Category";
    String ACTION_SELECTED_CROP = "Selected Crop";
    String ACTION_SELECTED_LIVESTOCK = "Selected Livestock";
    String ACTION_SELECTED_PRODUCT_CATEGORY = "Selected Product Category";
    String ACTION_SELECTED_PRODUCT = "Selected Product";
    String ACTION_SELECTED_PRODUCT_DEALERS_CATEGORY = "Selected Product Dealers Category";
    String ACTION_SELECTED_PRODUCT_DEALER = "Selected Product Dealer";
    String ACTION_SELECTED_MARKET_RATES = "Selected Market Rates";
    String ACTION_SELECTED_VIDEO_CATEGORIES_PARENT = "Selected Video Categories Parent";
    String ACTION_SELECTED_VIDEO_CATEGORIES_CHILD = "Selected Video Categories Child";
    String ACTION_SELECTED_VIDEO = "Selected Video";
    String ACTION_SELECTED_CONTACT_US = "Selected Contact Us";
    String ACTION_SELECTED_ABOUT_US = "Selected About Us";
    String ACTION_SELECTED_LANGUAGE = "Selected Language";

    String ACTION_CALL_PRODUCT_COMPANY = "Call Product Company";
    String ACTION_CALL_PRODUCT_DEALER = "Call Product Dealer";

    String ACTION_VIEWED_BANNER_ADVERTISEMENT = "Viewed Banner Advertisement";
    String ACTION_VIEWED_VIDEO_ADVERTISEMENT = "Viewed Video Advertisement";

    String ACTION_NOTIFICATION_RECEIVED = "Notification Received";
    String ACTION_NOTIFICATION_CLICKED = "Notification Clicked";

    String LABEL_NOTIFICATION_GLOBAL = "Global";
    String LABEL_NOTIFICATION_NEWS = "News";
    String LABEL_NOTIFICATION_PHOTO_ANALYSIS = "Photo Analysis";
    String LABEL_NOTIFICATION_APP_UPDATE = "App Update";

    void sendScreenEvent(String screenName);

    void sendNewsSelectedEvent(long articleId);

    void sendCropCategorySelectedEvent(long cropCategoryId);

    void sendCropSelectedEvent(long cropId);

    void sendLivestockSelectedEvent(long livestockId);

    void sendProductCategorySelectedEvent(long productCategoryId);

    void sendProductSelectedEvent(long productId);

    void sendCallProductCompanyEvent(String number);

    void sendProductDealersCategorySelectedEvent(long productCategoryId);

    void sendProductDealerSelectedEvent(long dealerId);

    void sendCallProductDealerEvent(String number);

    void sendMarketRatesSelectedEvent(long marketId);

    void sendVideosParentCategorySelectedEvent(long parentCategoryId);

    void sendVideosChildCategorySelectedEvent(long childCategoryId);

    void sendVideoSelectedEvent(long videoId);

    void sendContactUsSelectedEvent();

    void sendAboutUsSelectedEvent();

    void sendLanguageSelectedEvent(String language);

    void sendViewedBannerAdvertisementEvent(String image);

    void sendViewedVideoAdvertisementEvent(String video);

    void sendReceivedGlobalNotification();

    void sendClickedGlobalNotification();

    void sendTimeToClickGlobalNotification(long milliseconds);

    void sendReceivedNewsNotification();

    void sendClickedNewsNotification();

    void sendTimeToClickNewsNotification(long milliseconds);

    void sendReceivedPhotoAnalysisNotification();

    void sendClickedPhotoAnalysisNotification();

    void sendTimeToClickPhotoAnalysisNotification(long milliseconds);

    void sendReceivedAppUpdateNotification();

    void sendClickedAppUpdateNotification();

    void sendTimeToClickAppUpdateNotification(long milliseconds);
}
