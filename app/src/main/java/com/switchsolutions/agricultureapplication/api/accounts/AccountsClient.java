package com.switchsolutions.agricultureapplication.api.accounts;

import com.switchsolutions.agricultureapplication.models.JWTTokens;

import rx.Observable;

public interface AccountsClient {

//    Observable<Void> createAccount(String name, String number);
//
    Observable<JWTTokens> createAccountDemo(String name, String number);

    Observable<JWTTokens> verifyAccount(String code);

    Observable<Void> updateFCMToken(String token);

    Observable<JWTTokens> refreshTokens();
}
