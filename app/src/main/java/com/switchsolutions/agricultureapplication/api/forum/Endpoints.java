package com.switchsolutions.agricultureapplication.api.forum;

import okhttp3.RequestBody;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    @GET("v1/forum/categories")
    Observable<Responses.Categories> getCategories(
            @Query("lang") String lang
    );

    @Multipart
    @POST("v1/forum/categories/{category-id}/questions")
    Observable<Responses.Question> createQuestion(
            @Path("category-id") long categoryId,
            @Part("text") RequestBody text
    );

    @Multipart
    @POST("v1/forum/categories/{category-id}/questions/{question-id}/answers")
    Observable<Responses.Answer> createAnswer(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Part("text") RequestBody text
    );

    @Multipart
    @POST("v1/forum/categories/{category-id}/questions/{question-id}/answers/{answer-id}/comments")
    Observable<Responses.Comment> createComment(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Path("answer-id") long answerId,
            @Part("text") RequestBody text
    );

    @GET("v1/forum/categories/{category-id}/questions/new")
    Observable<Responses.Questions> getQuestionsNew(
            @Path("category-id") long categoryId,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("v1/forum/categories/{category-id}/questions/popular")
    Observable<Responses.Questions> getQuestionsPopular(
            @Path("category-id") long categoryId,
            @Query("limit") int limit,
            @Query("offset") int offset
    );

    @GET("v1/forum/categories/{category-id}/questions/{question-id}/answers")
    Observable<Responses.Answers> getAnswers(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId
    );

    @GET("v1/forum/categories/{category-id}/questions/{question-id}/answers/{answer-id}/comments")
    Observable<Responses.Comments> getComments(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Path("answer-id") long answerId
    );

    @POST("v1/forum/categories/{category-id}/questions/{question-id}/vote/increment")
    Observable<Responses.Vote> incrementQuestionVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId
    );

    @POST("v1/forum/categories/{category-id}/questions/{question-id}/vote/decrement")
    Observable<Responses.Vote> decrementQuestionVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId
    );

    @POST("v1/forum/categories/{category-id}/questions/{question-id}/answers/{answer-id}/vote/increment")
    Observable<Responses.Vote> incrementAnswerVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Path("answer-id") long answerId
    );

    @POST("v1/forum/categories/{category-id}/questions/{question-id}/answers/{answer-id}/vote/decrement")
    Observable<Responses.Vote> decrementAnswerVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Path("answer-id") long answerId
    );

    @DELETE("v1/forum/categories/{category-id}/questions/{question-id}/vote")
    Observable<Responses.Vote> deleteQuestionVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId
    );

    @DELETE("v1/forum/categories/{category-id}/questions/{question-id}/answers/{answer-id}/vote")
    Observable<Responses.Vote> deleteAnswerVote(
            @Path("category-id") long categoryId,
            @Path("question-id") long questionId,
            @Path("answer-id") long answerId
    );

}
