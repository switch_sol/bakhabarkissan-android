package com.switchsolutions.agricultureapplication.api.photo_analysis;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

final class Responses {

    static final class PhotoAnalysisList {

        @JsonProperty("result")
        public List<com.switchsolutions.agricultureapplication.models.PhotoAnalysis> result;
    }

    static final class PhotoAnalysis {

        @JsonProperty("result")
        public com.switchsolutions.agricultureapplication.models.PhotoAnalysis result;
    }

}
