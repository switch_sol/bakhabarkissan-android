package com.switchsolutions.agricultureapplication.api.products;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.models.ProductDealer;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

final class ProductsClientImpl implements ProductsClient {

    private static final long CACHE_SIZE = 1024 * 1024 * 10; // 10 MB

    private static final String CACHE_DIRNAME = "cache_products";

    private final Endpoints.API apiEndpoints;
    private final Endpoints.Content contentEndpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    private final ObjectReader categoriesListReader;
    private final ObjectWriter categoriesListWriter;

    private final ObjectReader productsListReader;
    private final ObjectWriter productsListWriter;

    private final ObjectReader productDealersListReader;
    private final ObjectWriter productDealersListWriter;

    @Nullable
    private final LruCache cache;

    @Inject
    ProductsClientImpl(Endpoints.API apiEndpoints, Endpoints.Content contentEndpoints, Preferences preferences, AccountsClient accountsClient, NetworkUtility networkUtility, Storage storage) {
        this.apiEndpoints = apiEndpoints;
        this.contentEndpoints = contentEndpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        ObjectMapper objectMapper = new ObjectMapper();

        categoriesListReader = objectMapper.readerFor(Responses.CategoriesList.class);
        categoriesListWriter = objectMapper.writerFor(Responses.CategoriesList.class);

        productsListReader = objectMapper.readerFor(Responses.ProductsList.class);
        productsListWriter = objectMapper.writerFor(Responses.ProductsList.class);

        productDealersListReader = objectMapper.readerFor(Responses.DealersList.class);
        productDealersListWriter = objectMapper.writerFor(Responses.DealersList.class);

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    static String getCacheKeyCategoriesList(String language) {
        return String.format(Locale.US, "%s_product_categories_list", language);
    }

    static String getCacheKeyProductsList(String language, long categoryId, int limit, int offset) {
        return String.format(Locale.US, "%s_products_list_%d_%d_%d", language, categoryId, limit, offset);
    }

    static String getCacheKeyProductPage(String language, long productId) {
        return String.format(Locale.US, "%s_product_page_%d", language, productId);
    }

    static String getCacheKeyProductDealersNearby(String language, long categoryId, int limit, int offset) {
        return String.format(Locale.US, "%s_product_dealers_nearby_%d_%d_%d", language, categoryId, limit, offset);
    }

    @Override
    public Observable<ArrayList<ProductCategory>> getCategories() {
        if (networkUtility.isInternetConnected()) {
            return getFreshCategories(preferences.getLanguageNormalized());
        } else {
            return getCachedCategories(preferences.getLanguageNormalized())
                    .concatWith(getFreshCategories(preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<ArrayList<ProductCategory>> getCachedCategories(final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCacheKeyCategoriesList(lang);
                    }
                })
                .map(new Func1<String, Responses.CategoriesList>() {
                    @Override
                    public Responses.CategoriesList call(String key) {
                        try {
                            return cache.readJSON(key, categoriesListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<Responses.CategoriesList, ArrayList<ProductCategory>>() {
                    @Override
                    public ArrayList<ProductCategory> call(Responses.CategoriesList response) {
                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<ProductCategory>>empty());
    }

    Observable<ArrayList<ProductCategory>> getFreshCategories(final String lang) {
        return apiEndpoints
                .getCategories(lang)
                .compose(new TokensRefreshTransformer<Responses.CategoriesList>(accountsClient))
                .doOnNext(new Action1<Responses.CategoriesList>() {
                    @Override
                    public void call(Responses.CategoriesList response) {
                        if (cache == null) return;

                        try {
                            cache.writeJSON(getCacheKeyCategoriesList(lang), categoriesListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.CategoriesList, ArrayList<ProductCategory>>() {
                    @Override
                    public ArrayList<ProductCategory> call(Responses.CategoriesList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<Product>> getProducts(long categoryId, int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getFreshProducts(categoryId, limit, offset, preferences.getLanguageNormalized());
        } else {
            return getCachedProducts(categoryId, limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getFreshProducts(categoryId, limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<ArrayList<Product>> getCachedProducts(final long categoryId, final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCacheKeyProductsList(lang, categoryId, limit, offset);
                    }
                })
                .map(new Func1<String, Responses.ProductsList>() {
                    @Override
                    public Responses.ProductsList call(String key) {
                        try {
                            return cache.readJSON(key, productsListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<Responses.ProductsList, ArrayList<Product>>() {
                    @Override
                    public ArrayList<Product> call(Responses.ProductsList response) {
                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<Product>>empty());
    }

    Observable<ArrayList<Product>> getFreshProducts(final long categoryId, final int limit, final int offset, final String lang) {
        return apiEndpoints
                .getProducts(categoryId, lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.ProductsList>(accountsClient))
                .doOnNext(new Action1<Responses.ProductsList>() {
                    @Override
                    public void call(Responses.ProductsList response) {
                        if (cache == null) return;

                        try {
                            cache.writeJSON(getCacheKeyProductsList(lang, categoryId, limit, offset), productsListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.ProductsList, ArrayList<Product>>() {
                    @Override
                    public ArrayList<Product> call(Responses.ProductsList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }

    @Override
    public Observable<ArrayList<HTMLElement>> getProductPage(long productId) {
        if (networkUtility.isInternetConnected()) {
            return getFreshProductPage(productId, preferences.getLanguageNormalized());
        } else {
            return getCachedProductPage(productId, preferences.getLanguageNormalized())
                    .concatWith(getFreshProductPage(productId, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<ArrayList<HTMLElement>> getCachedProductPage(final long productId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getCacheKeyProductPage(lang, productId);
                    }
                })
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String key) {
                        try {
                            return cache.readString(key);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String content) {
                        return Jsoup.parse(content, "");
                    }
                })
                .map(new Func1<Document, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(Document document) {
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                });
    }

    Observable<ArrayList<HTMLElement>> getFreshProductPage(final long productId, final String lang) {
        return contentEndpoints
                .getProductPage(lang, productId)
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        try {
                            return responseBody.string();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            responseBody.close();
                        }
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String content) {
                        if (cache == null) return;

                        try {
                            cache.writeString(getCacheKeyProductPage(lang, productId), content);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String content) {
                        return Jsoup.parse(content, "");
                    }
                })
                .map(new Func1<Document, ArrayList<HTMLElement>>() {
                    @Override
                    public ArrayList<HTMLElement> call(Document document) {
                        ArrayList<HTMLElement> elements = new ArrayList<>();

                        int position = 0;
                        for (Element element : document.body().children()) {
                            HTMLElement htmlElement = HTMLElement.from(element, position);
                            if (htmlElement != null) {
                                elements.add(htmlElement);
                                position++;
                            }
                        }

                        return elements;
                    }
                });
    }

    @Override
    public Observable<ArrayList<ProductDealer>> getProductDealers(long categoryId, double lat, double lon, int limit, int offset) {
        if (networkUtility.isInternetConnected()) {
            return getFreshProductDealers(categoryId, lat, lon, limit, offset, preferences.getLanguageNormalized());
        } else {
            return getCachedProductDealers(categoryId, lat, lon, limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getFreshProductDealers(categoryId, lat, lon, limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<ArrayList<ProductDealer>> getCachedProductDealers(final long categoryId, double lat, double lon, final int limit, final int offset, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<ArrayList<ProductDealer>>() {
                    @Override
                    public ArrayList<ProductDealer> call() throws Exception {
                        String key = getCacheKeyProductDealersNearby(lang, categoryId, limit, offset);

                        Responses.DealersList response;
                        try {
                            response = cache.readJSON(key, productDealersListReader);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }

                        return new ArrayList<>(response.result);
                    }
                })
                .onErrorResumeNext(Observable.<ArrayList<ProductDealer>>empty());
    }

    Observable<ArrayList<ProductDealer>> getFreshProductDealers(final long categoryId, final double lat, final double lon, final int limit, final int offset, final String lang) {
        return apiEndpoints
                .getProductDealers(categoryId, lat, lon, lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.DealersList>(accountsClient))
                .doOnNext(new Action1<Responses.DealersList>() {
                    @Override
                    public void call(Responses.DealersList response) {
                        if (cache == null) return;

                        String key = getCacheKeyProductDealersNearby(lang, categoryId, limit, offset);
                        try {
                            cache.writeJSON(key, productDealersListWriter, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<Responses.DealersList, ArrayList<ProductDealer>>() {
                    @Override
                    public ArrayList<ProductDealer> call(Responses.DealersList response) {
                        return new ArrayList<>(response.result);
                    }
                });
    }
}
