package com.switchsolutions.agricultureapplication.api.news;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.switchsolutions.agricultureapplication.api.accounts.AccountsClient;
import com.switchsolutions.agricultureapplication.api.accounts.TokensRefreshTransformer;
import com.switchsolutions.agricultureapplication.models.NewsArticle;
import com.switchsolutions.agricultureapplication.network.NetworkUtility;
import com.switchsolutions.agricultureapplication.preferences.Preferences;
import com.switchsolutions.agricultureapplication.storage.LruCache;
import com.switchsolutions.agricultureapplication.storage.Storage;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

class NewsClientImpl implements NewsClient {

    private static final long CACHE_SIZE = 1024 * 1024 * 2; // 2MB

    private static final String CACHE_DIRNAME = "cache_news";

    private static final ObjectReader NEWS_LIST_READER;
    private static final ObjectWriter NEWS_LIST_WRITER;

    static {
        ObjectMapper mapper = new ObjectMapper();
        NEWS_LIST_READER = mapper.readerFor(Responses.NewsList.class);
        NEWS_LIST_WRITER = mapper.writerFor(Responses.NewsList.class);
    }

    private final Endpoints.API apiEndpoints;
    private final Endpoints.Content contentEndpoints;
    private final Preferences preferences;
    private final AccountsClient accountsClient;
    private final NetworkUtility networkUtility;

    @Nullable
    private final LruCache cache;

    @Inject
    NewsClientImpl(Endpoints.API apiEndpoints, Endpoints.Content contentEndpoints, Preferences preferences, Storage storage, AccountsClient accountsClient, NetworkUtility networkUtility) {
        this.apiEndpoints = apiEndpoints;
        this.contentEndpoints = contentEndpoints;
        this.preferences = preferences;
        this.accountsClient = accountsClient;
        this.networkUtility = networkUtility;

        LruCache cache;
        try {
            cache = storage.getLruCache(CACHE_DIRNAME, CACHE_SIZE);
        } catch (IOException e) {
            cache = null;
        }
        this.cache = cache;
    }

    static String getNewsListCacheKey(String language) {
        return String.format("news_list_%s", language);
    }

    static String getNewsPageCacheKey(String language, long newsId) {
        return String.format(Locale.US, "article_%s_%d", language, newsId);
    }

    @Override
    public Observable<List<NewsArticle>> getNews(int limit, final int offset) {
        if (networkUtility.isInternetConnected()) {
            return getFreshNews(limit, offset, preferences.getLanguageNormalized());
        } else {
            return getCachedNews(limit, offset, preferences.getLanguageNormalized())
                    .concatWith(getFreshNews(limit, offset, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<List<NewsArticle>> getCachedNews(int limit, final int offset, final String lang) {
        if (cache == null || offset != 0) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getNewsListCacheKey(lang);
                    }
                })
                .map(new Func1<String, Responses.NewsList>() {
                    @Override
                    public Responses.NewsList call(String key) {
                        try {
                            return cache.readJSON(key, NEWS_LIST_READER);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<Responses.NewsList, List<NewsArticle>>() {
                    @Override
                    public List<NewsArticle> call(Responses.NewsList response) {
                        return response.result;
                    }
                })
                .onErrorResumeNext(Observable.<List<NewsArticle>>empty());
    }

    Observable<List<NewsArticle>> getFreshNews(int limit, final int offset, final String lang) {
        return apiEndpoints
                .getNews(lang, limit, offset)
                .compose(new TokensRefreshTransformer<Responses.NewsList>(accountsClient))
                .doOnNext(new Action1<Responses.NewsList>() {
                    @Override
                    public void call(Responses.NewsList response) {
                        if (cache == null || offset != 0) return;

                        try {
                            cache.writeJSON(getNewsListCacheKey(lang), NEWS_LIST_WRITER, response);
                        } catch (IOException e) {

                        }
                    }
                })
                .compose(new TokensRefreshTransformer<Responses.NewsList>(accountsClient))
                .map(new Func1<Responses.NewsList, List<NewsArticle>>() {
                    @Override
                    public List<NewsArticle> call(Responses.NewsList response) {
                        return response.result;
                    }
                });
    }

    @Override
    public Observable<Document> getNewsPage(final long newsId) {
        if (networkUtility.isInternetConnected()) {
            return getFreshNewsPage(newsId, preferences.getLanguageNormalized());
        } else {
            return getCachedNewsPage(newsId, preferences.getLanguageNormalized())
                    .concatWith(getFreshNewsPage(newsId, preferences.getLanguageNormalized()))
                    .take(1);
        }
    }

    Observable<Document> getCachedNewsPage(final long newsId, final String lang) {
        if (cache == null) return Observable.empty();

        return Observable
                .fromCallable(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return getNewsPageCacheKey(lang, newsId);
                    }
                })
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String filename) {
                        try {
                            return cache.readString(filename);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String content) {
                        return Jsoup.parse(content, "");
                    }
                })
                .onErrorResumeNext(Observable.<Document>empty());
    }

    Observable<Document> getFreshNewsPage(final long newsId, final String lang) {
        return contentEndpoints
                .getNewsPage(lang, newsId)
                .map(new Func1<ResponseBody, String>() {
                    @Override
                    public String call(ResponseBody responseBody) {
                        try {
                            return responseBody.string();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        } finally {
                            responseBody.close();
                        }
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String content) {
                        if (cache == null) return;

                        try {
                            cache.writeString(getNewsPageCacheKey(lang, newsId), content);
                        } catch (IOException e) {

                        }
                    }
                })
                .map(new Func1<String, Document>() {
                    @Override
                    public Document call(String s) {
                        return Jsoup.parse(s, "");
                    }
                });
    }
}
