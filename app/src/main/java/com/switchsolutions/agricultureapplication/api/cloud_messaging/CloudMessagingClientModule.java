package com.switchsolutions.agricultureapplication.api.cloud_messaging;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CloudMessagingClientModule {

    @Provides
    @Singleton
    public CloudMessagingClient providesCloudMessagingClient(CloudMessagingClientImpl cloudMessagingClient) {
        return cloudMessagingClient;
    }

}
