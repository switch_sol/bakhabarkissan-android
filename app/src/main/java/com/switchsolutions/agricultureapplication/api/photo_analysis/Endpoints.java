package com.switchsolutions.agricultureapplication.api.photo_analysis;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

public interface Endpoints {

    interface API {

        @POST("v1/photo_analysis")
        Observable<Responses.PhotoAnalysis> createPhotoAnalysisRequest(
                @Body RequestBody body
        );

        @GET("v1/photo_analysis/status/{id}")
        Observable<Responses.PhotoAnalysis> getPhotoAnalysis(
                @Path("id") long id
        );

    }

    interface Content {

        @GET("/pages/photo_analysis/{id}.html")
        Observable<ResponseBody> getPhotoAnalysisResponsePage(
                @Path("id") long id
        );

    }

}
