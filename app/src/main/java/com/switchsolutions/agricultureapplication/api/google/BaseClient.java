package com.switchsolutions.agricultureapplication.api.google;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.awareness.Awareness;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

abstract class BaseClient implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private  GoogleApiClient googleClient = null;

    BaseClient(Context context) {
        try {
            this.googleClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Awareness.API)
                    .build();
        } catch (Exception e) {

        }
    }

    abstract void onConnected(GoogleApiClient client);

    abstract void onError(Throwable t);

    final void connect() {
        googleClient.connect();
    }

    final void disconnect() {
        googleClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        onConnected(googleClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        onError(new GoogleClientConnectionSuspendedException(i));
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        onError(new GoogleClientConnectionFailedException(connectionResult));
    }

}
