package com.switchsolutions.agricultureapplication.api.farming_techniques;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

interface Endpoints {

    interface API {

        @GET("v1/farming_techniques")
        Observable<Responses.FarmingTechniquesList> getTechniques(
                @Query("lang") String lang,
                @Query("limit") int limit,
                @Query("offset") int offset
        );

    }

    interface Content {

        @GET("pages/{lang}/farming_techniques/{farming-technique-id}.html")
        Observable<ResponseBody> getTechniquePage(
                @Path("lang") String lang,
                @Path("farming-technique-id") long farmingTechniqueId
        );

    }

}
