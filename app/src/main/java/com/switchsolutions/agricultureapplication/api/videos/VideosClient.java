package com.switchsolutions.agricultureapplication.api.videos;

import com.switchsolutions.agricultureapplication.models.Video;
import com.switchsolutions.agricultureapplication.models.VideoCategoryChild;
import com.switchsolutions.agricultureapplication.models.VideoCategoryParent;

import java.util.ArrayList;

import rx.Observable;

public interface VideosClient {

    Observable<ArrayList<VideoCategoryParent>> getCategories(int limit, int offset);

    Observable<ArrayList<VideoCategoryParent>> getCategoriesCached(int limit, int offset, String lang);

    Observable<ArrayList<VideoCategoryParent>> getCategoriesFresh(int limit, int offset, String lang);

    Observable<ArrayList<VideoCategoryChild>> getCategories(long parentCategoryId, int limit, int offset);

    Observable<ArrayList<VideoCategoryChild>> getCategoriesCached(long parentCategoryId, int limit, int offset, String lang);

    Observable<ArrayList<VideoCategoryChild>> getCategoriesFresh(long parentCategoryId, int limit, int offset, String lang);

    Observable<ArrayList<Video>> getVideos(long parentCategoryId, long childCategoryId, int limit, int offset);

    Observable<ArrayList<Video>> getVideosCached(long parentCategoryId, long childCategoryId, int limit, int offset, String lang);

    Observable<ArrayList<Video>> getVideosFresh(long parentCategoryId, long childCategoryId, int limit, int offset, String lang);
}
