package com.switchsolutions.agricultureapplication.api.google;

import android.content.Context;

import com.switchsolutions.agricultureapplication.utils.Coordinates;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action0;
import rx.subscriptions.Subscriptions;

final class GetLocationOnSubscribe implements Observable.OnSubscribe<Coordinates> {

    private final Context context;

    GetLocationOnSubscribe(Context context) {
        this.context = context;
    }

    @Override
    public void call(final Subscriber<? super Coordinates> subscriber) {
        final GetLocation operation = new GetLocation(context, new GetLocation.Listener() {
            @Override
            public void onResult(Coordinates coordinates) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onNext(coordinates);
                    subscriber.onCompleted();
                }
            }

            @Override
            public void onError(Throwable throwable) {
                if (!subscriber.isUnsubscribed()) {
                    subscriber.onError(throwable);
                }
            }
        });
        operation.connect();

        subscriber.add(Subscriptions.create(new Action0() {
            @Override
            public void call() {
                operation.disconnect();
            }
        }));
    }
}
