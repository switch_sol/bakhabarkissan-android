package com.switchsolutions.agricultureapplication.api.farming_techniques;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.switchsolutions.agricultureapplication.models.FarmingTechnique;

import java.util.List;

final class Responses {

    @JsonIgnoreProperties(ignoreUnknown = true)
    static final class FarmingTechniquesList {

        @JsonProperty("result")
        public List<FarmingTechnique> result;
    }
}
