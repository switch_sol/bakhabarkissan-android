package com.switchsolutions.agricultureapplication.api.livestock;

import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.Livestock;

import java.util.ArrayList;

import rx.Observable;

public interface LivestockClient {

    Observable<ArrayList<Livestock>> getLivestock(int limit, int offset);

    Observable<ArrayList<Livestock>> getLivestockCached(int limit, int offset, final String lang);

    Observable<ArrayList<Livestock>> getLivestockFresh(int limit, int offset, final String lang);

    Observable<ArrayList<HTMLElement>> getLivestockPage(long livestockId);

    Observable<ArrayList<HTMLElement>> getLivestockPageCached(long livestockId, final String lang);

    Observable<ArrayList<HTMLElement>> getLivestockPageFresh(long livestockId, final String lang);
}
