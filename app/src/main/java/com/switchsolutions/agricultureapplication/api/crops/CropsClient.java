package com.switchsolutions.agricultureapplication.api.crops;

import com.switchsolutions.agricultureapplication.models.Crop;
import com.switchsolutions.agricultureapplication.models.CropCategory;

import org.jsoup.nodes.Document;

import java.util.List;

import rx.Observable;

public interface CropsClient {

    Observable<List<CropCategory>> getCategories();

    Observable<List<Crop>> getCrops(long categoryId);

    Observable<Document> getCropPage(long cropId);

    Observable<Document> getCropPage(long cropId, String lang);
}
