package com.switchsolutions.agricultureapplication.api.photo_analysis;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.PhotoAnalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public interface PhotoAnalysisClient {

    Observable<PhotoAnalysis> upload(@NonNull String text, @Nullable File audio, @Nullable File... images);

    Observable<PhotoAnalysis> update(long id);

    Observable<ArrayList<HTMLElement>> getResponse(long id);

    Observable<List<PhotoAnalysis>> getLocalList();
}
