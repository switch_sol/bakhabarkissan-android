package com.switchsolutions.agricultureapplication.api.products;

import com.switchsolutions.agricultureapplication.html.HTMLElement;
import com.switchsolutions.agricultureapplication.models.Product;
import com.switchsolutions.agricultureapplication.models.ProductCategory;
import com.switchsolutions.agricultureapplication.models.ProductDealer;

import java.util.ArrayList;

import rx.Observable;

public interface ProductsClient {

    Observable<ArrayList<ProductCategory>> getCategories();

    Observable<ArrayList<Product>> getProducts(long categoryId, int limit, int offset);

    Observable<ArrayList<HTMLElement>> getProductPage(long productId);

    Observable<ArrayList<ProductDealer>> getProductDealers(long categoryId, double lat, double lon, int limit, int offset);
}
