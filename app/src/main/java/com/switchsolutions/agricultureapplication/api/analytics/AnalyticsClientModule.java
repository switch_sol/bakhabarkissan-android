package com.switchsolutions.agricultureapplication.api.analytics;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AnalyticsClientModule {

    @Provides
    @Singleton
    public AnalyticsClient providesAnalyticsClient(AnalyticsClientImpl analytics) {
        return analytics;
    }

}
