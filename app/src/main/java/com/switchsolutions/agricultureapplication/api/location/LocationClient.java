package com.switchsolutions.agricultureapplication.api.location;

import com.switchsolutions.agricultureapplication.utils.Coordinates;

import rx.Observable;

public interface LocationClient {

    /*
     * Returns the latest location of the user.
     *
     * @returns an observable that emits the latest location of the user.
     *
     * @see GoogleClientConnectionFailedException
     * @see GoogleClientConnectionSuspendedException
     * @see LocationProviderDisabledException
     * @see LocationNotFoundException
     * @see SecurityException
     */
    Observable<Coordinates> getLocation();

    /*
     * Checks to see whether the Location provider is enabled. This is different from the Location
     * Permission.
     *
     * @returns true if the Location Provider is enabled.
     */
    boolean hasAccess();

    /*
     * Checks to see whether the Location permission is granted.
     *
     * @returns true if user has granted the location permission.
     */
    boolean hasPermission();
}
