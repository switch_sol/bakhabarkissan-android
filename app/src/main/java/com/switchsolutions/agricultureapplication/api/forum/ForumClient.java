package com.switchsolutions.agricultureapplication.api.forum;

import com.switchsolutions.agricultureapplication.models.ForumAnswer;
import com.switchsolutions.agricultureapplication.models.ForumCategory;
import com.switchsolutions.agricultureapplication.models.ForumComment;
import com.switchsolutions.agricultureapplication.models.ForumQuestion;
import com.switchsolutions.agricultureapplication.models.ForumVote;

import java.util.ArrayList;

import rx.Observable;

public interface ForumClient {

    Observable<ArrayList<ForumCategory>> getCategories();

    Observable<ForumQuestion> createQuestion(long categoryId, String text);

    Observable<ForumAnswer> createAnswer(long categoryId, long questionId, String text);

    Observable<ForumComment> createComment(long categoryId, long questionId, long answerId, String text);

    Observable<ArrayList<ForumQuestion>> getQuestionsNew(long categoryId, int limit, int offset);

    Observable<ArrayList<ForumQuestion>> getQuestionsPopular(long categoryId, int limit, int offset);

    Observable<ArrayList<ForumAnswer>> getAnswers(long categoryId, long questionId);

    Observable<ArrayList<ForumComment>> getComments(long categoryId, long questionId, long answerId);

    Observable<ForumVote> incrementQuestionVote(long categoryId, long questionId);

    Observable<ForumVote> decrementQuestionVote(long categoryId, long questionId);

    Observable<ForumVote> deleteQuestionVote(long categoryId, long questionId);

    Observable<ForumVote> incrementAnswerVote(long categoryId, long questionId, long answerId);

    Observable<ForumVote> decrementAnswerVote(long categoryId, long questionId, long answerId);

    Observable<ForumVote> deleteAnswerVote(long categoryId, long questionId, long answerId);
}
