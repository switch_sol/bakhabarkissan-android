package com.switchsolutions.agricultureapplication.eventbus;

import com.switchsolutions.agricultureapplication.BuildConfig;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class EventBusModule {

    @Provides
    @Singleton
    public EventBus providesEventBus() {
        return EventBus
                .builder()
                .eventInheritance(false)
                .logNoSubscriberMessages(BuildConfig.DEBUG)
                .logSubscriberExceptions(BuildConfig.DEBUG)
                .throwSubscriberException(BuildConfig.DEBUG)
                .sendNoSubscriberEvent(BuildConfig.DEBUG)
                .sendSubscriberExceptionEvent(BuildConfig.DEBUG)
                .build();
    }

}
