package com.switchsolutions.agricultureapplication.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;

public class GlobalNotificationClickReceiver extends BroadcastReceiver {

    private static final String KEY_START_TIME = "start_time";

    public static final String ACTION = "com.switchsolutions.agricultureapplication.receivers.GlobalNotificationClickReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        AnalyticsClient analytics = BaseApplication.getComponent(context).analyticsClient();
        analytics.sendClickedGlobalNotification();
        if (intent.getLongExtra(KEY_START_TIME, 0) > 0) {
            analytics.sendTimeToClickGlobalNotification(System.currentTimeMillis() - intent.getLongExtra(KEY_START_TIME, 0));
        }

        Intent mainIntent = new Intent(context, MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(mainIntent);
        stackBuilder.startActivities();
    }

    public static Intent createIntent() {
        return new Intent(ACTION).putExtra(KEY_START_TIME, System.currentTimeMillis());
    }
}
