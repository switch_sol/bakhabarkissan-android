package com.switchsolutions.agricultureapplication.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.BuildConfig;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AppUpdateNotificationClickReceiver extends BroadcastReceiver {

    private static final String KEY_START_TIME = "start_time";

    public static final String ACTION = "com.switchsolutions.agricultureapplication.receivers.AppUpdateNotificationClickReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        AnalyticsClient analytics = BaseApplication.getComponent(context).analyticsClient();
        analytics.sendClickedAppUpdateNotification();
        if (intent.getLongExtra(KEY_START_TIME, 0) > 0) {
            analytics.sendTimeToClickAppUpdateNotification(System.currentTimeMillis() - intent.getLongExtra(KEY_START_TIME, 0));
        }

        final String appPackageName;
        if (BuildConfig.DEBUG) {
            appPackageName = "com.switchsolutions.agricultureapplication.mobilink";
        } else {
            appPackageName = context.getPackageName();
        }

        Intent directIntent;
        try {
            directIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        } catch (android.content.ActivityNotFoundException ignored) {
            directIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }
        directIntent.setFlags(FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(directIntent);
    }

    public static Intent createIntent() {
        return new Intent(ACTION).putExtra(KEY_START_TIME, System.currentTimeMillis());
    }
}
