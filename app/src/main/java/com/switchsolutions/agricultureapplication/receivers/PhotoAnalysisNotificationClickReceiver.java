package com.switchsolutions.agricultureapplication.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.api.analytics.AnalyticsClient;
import com.switchsolutions.agricultureapplication.screens.main.MainScreenActivity;
import com.switchsolutions.agricultureapplication.screens.photo_analyses_list.PhotoAnalysesListActivity;

public class PhotoAnalysisNotificationClickReceiver extends BroadcastReceiver {

    private static final String KEY_START_TIME = "start_time";

    public static final String ACTION = "com.switchsolutions.agricultureapplication.receivers.PhotoAnalysisNotificationClickReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        AnalyticsClient analytics = BaseApplication.getComponent(context).analyticsClient();
        analytics.sendClickedPhotoAnalysisNotification();
        if (intent.getLongExtra(KEY_START_TIME, 0) > 0) {
            analytics.sendTimeToClickPhotoAnalysisNotification(System.currentTimeMillis() - intent.getLongExtra(KEY_START_TIME, 0));
        }

        Intent mainIntent = new Intent(context, MainScreenActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(mainIntent);
        stackBuilder.addNextIntent(PhotoAnalysesListActivity.createIntent(context));
        stackBuilder.startActivities();
    }

    public static Intent createIntent() {
        return new Intent(ACTION).putExtra(KEY_START_TIME, System.currentTimeMillis());
    }
}
