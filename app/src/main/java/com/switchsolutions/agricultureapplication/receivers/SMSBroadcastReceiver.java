package com.switchsolutions.agricultureapplication.receivers;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.switchsolutions.agricultureapplication.BaseApplication;
import com.switchsolutions.agricultureapplication.screens.account_login_verification.AccountVerificationEvents;

import org.greenrobot.eventbus.EventBus;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMSBroadcastReceiver extends BroadcastReceiver {

    private static final Pattern PATTERN = Pattern.compile("\'([a-z0-9]{6})\'");

    final SmsManager smsManager = SmsManager.getDefault();

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            final Object[] pdusObj = (Object[]) bundle.get("pdus");
            if (pdusObj != null) {
                String code = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    code = parseSmsMessagePostKitKat(pdusObj, bundle);
                } else {
                    code = parseSmsMessagePreKitKat(pdusObj, bundle);
                }
                if (code != null) {
                    EventBus eventBus = BaseApplication.getComponent(context).eventBus();
                    eventBus.postSticky(new AccountVerificationEvents.OnVerificationCodeReceived(code));
                }
            }
        }
    }

    private String parseSmsMessagePreKitKat(Object[] pdusObj, Bundle bundle) {
        if (pdusObj != null) {
            for (int i = 0; i < pdusObj.length; i++) {
                String code = parseSmsMessage(SmsMessage.createFromPdu((byte[]) pdusObj[i]));
                if (code != null) {
                    return code;
                }
            }
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private String parseSmsMessagePostKitKat(Object[] pdusObj, Bundle bundle) {
        String format = bundle.getString("format");
        if (pdusObj != null) {
            for (int i = 0; i < pdusObj.length; i++) {
                String code = parseSmsMessage(SmsMessage.createFromPdu((byte[]) pdusObj[i], format));
                if (code != null) {
                    return code;
                }
            }
        }
        return null;
    }

    private String parseSmsMessage(SmsMessage message) {
        String body = message.getMessageBody();
        if (body.contains("BKK")) {
            Matcher matcher = PATTERN.matcher(body);
            if (matcher.find()) {
                String matched = matcher.group(0);
                return matched.substring(1, 7);
            }
        }
        return null;
    }

}
