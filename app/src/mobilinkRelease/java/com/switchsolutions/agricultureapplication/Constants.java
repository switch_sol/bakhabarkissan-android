package com.switchsolutions.agricultureapplication;

public final class Constants {

    public static final String URL_AGRICULTURE_API = "http://115.186.147.181:8181";
    public static final String URL_AGRICULTURE_CONTENT = "http://115.186.147.181:8181";
    public static final String URL_FORECAST = "http://api.openweathermap.org/data/2.5/";
    public static final String URL_SPONSOR = "http://mobilink.com.pk/";

    public static final String SHORTCODE = "03030300000";

    public static final String ANALYTICS_TRACKER = "UA-76561025-1";

    private Constants() {
    }

}